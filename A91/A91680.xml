<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>By vertue of an order of the honourable House of Commons, made on Monday the seventh of this present moneth of February, 1641. We (the committee appointed, to receive the moneys given, by the members of the House of Commons, for the reliefe of the poore distressed people that are come out of Ireland) ...</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A91680</idno>
        <idno type="STC">Wing R1122</idno>
        <idno type="STC">ESTC R232130</idno>
        <idno type="EEBO-CITATION">99897667</idno>
        <idno type="PROQUEST">99897667</idno>
        <idno type="VID">133367</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A91680)</note>
        <note>Transcribed from: (Early English Books Online ; image set 133367)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2517:18)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>By vertue of an order of the honourable House of Commons, made on Monday the seventh of this present moneth of February, 1641. We (the committee appointed, to receive the moneys given, by the members of the House of Commons, for the reliefe of the poore distressed people that are come out of Ireland) ...</title>
            <author>Ayscough, Edward, Sir.</author>
            <author>England and Wales. Parliament. House of Commons.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1642]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from opening lines of text.</note>
            <note>Imprint from Wing CD-ROM, 1996.</note>
            <note>Dated at end: Dated at Westminster, the fifteenth day of February, 1641.</note>
            <note>Signed at end by committee members: Sir Edward Aiscough Knight. Francis Rowse Henry Martin William VVheler esquires.</note>
            <note>Reproduction of original in the Society of Antiquaries, London, England.</note>
            <note>Wing CD-ROM, 1996 paraphrases the title.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Poor laws -- England -- Early works to 1800.</term>
          <term>Ireland -- Economic conditions -- Early works to 1800.</term>
          <term>Broadsides</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-07</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-08</date><label>Pip Willcox</label>
        Sampled and proofread
      </change>
      <change><date>2007-08</date><label>Pip Willcox</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A91680e-10">
    <body xml:id="A91680e-20">
      <div type="order" xml:id="A91680e-30">
        <pb facs="tcp:133367:1" xml:id="A91680-001-a"/>
        <pb facs="tcp:133367:1" rendition="simple:additions" xml:id="A91680-001-b"/>
        <p xml:id="A91680e-40">
          <w lemma="by" ana="#acp-p" reg="By" rend="initialcharacterdecorated" xml:id="A91680-0030" facs="A91680-001-b-0010">BY</w>
          <c> </c>
          <w lemma="virtue" ana="#n1" reg="virtue" xml:id="A91680-0040" facs="A91680-001-b-0020">vertue</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0050" facs="A91680-001-b-0030">of</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="an" xml:id="A91680-0060" facs="A91680-001-b-0040">an</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A91680-0070" facs="A91680-001-b-0050">Order</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0080" facs="A91680-001-b-0060">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0090" facs="A91680-001-b-0070">the</w>
          <c> </c>
          <w lemma="honourable" ana="#j" reg="honourable" xml:id="A91680-0100" facs="A91680-001-b-0080">Honourable</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A91680-0110" facs="A91680-001-b-0090">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0120" facs="A91680-001-b-0100">of</w>
          <c> </c>
          <w lemma="Commons" ana="#n2-nn" reg="Commons" xml:id="A91680-0130" facs="A91680-001-b-0110">Commons</w>
          <pc xml:id="A91680-0140" facs="A91680-001-b-0120">,</pc>
          <c> </c>
          <w lemma="make" ana="#vvn" reg="made" xml:id="A91680-0150" facs="A91680-001-b-0130">made</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A91680-0160" facs="A91680-001-b-0140">on</w>
          <c> </c>
          <w lemma="Monday" ana="#n1-nn" reg="Monday" xml:id="A91680-0170" facs="A91680-001-b-0150">Monday</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0180" facs="A91680-001-b-0160">the</w>
          <c> </c>
          <w lemma="seven" ana="#ord" reg="seventh" xml:id="A91680-0190" facs="A91680-001-b-0170">seventh</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0200" facs="A91680-001-b-0180">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A91680-0210" facs="A91680-001-b-0190">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A91680-0220" facs="A91680-001-b-0200">present</w>
          <c> </c>
          <w lemma="month" ana="#n1" reg="month" xml:id="A91680-0230" facs="A91680-001-b-0210">Moneth</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0240" facs="A91680-001-b-0220">of</w>
          <c> </c>
          <hi xml:id="A91680e-50">
            <w lemma="February" ana="#n1-nn" reg="February" xml:id="A91680-0250" facs="A91680-001-b-0230">February</w>
            <pc xml:id="A91680-0260" facs="A91680-001-b-0240">,</pc>
          </hi>
          <c> </c>
          <w lemma="1641." ana="#crd" reg="1641." xml:id="A91680-0270" facs="A91680-001-b-0250">1641.</w>
          <pc unit="sentence" xml:id="A91680-0270-eos" facs="A91680-001-b-0260"/>
          <c> </c>
          <w lemma="we" ana="#pns" reg="We" xml:id="A91680-0280" facs="A91680-001-b-0270">We</w>
          <c> </c>
          <pc xml:id="A91680-0290" facs="A91680-001-b-0280">(</pc>
          <w lemma="the" ana="#d" reg="The" xml:id="A91680-0300" facs="A91680-001-b-0290">the</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A91680-0310" facs="A91680-001-b-0300">Committee</w>
          <c> </c>
          <w lemma="appoint" ana="#vvn" reg="appointed" xml:id="A91680-0320" facs="A91680-001-b-0310">appointed</w>
          <pc xml:id="A91680-0330" facs="A91680-001-b-0320">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-0340" facs="A91680-001-b-0330">to</w>
          <c> </c>
          <w lemma="receive" ana="#vvi" reg="receive" xml:id="A91680-0350" facs="A91680-001-b-0340">receive</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0360" facs="A91680-001-b-0350">the</w>
          <c> </c>
          <w lemma="money" ana="#n2" reg="monies" xml:id="A91680-0370" facs="A91680-001-b-0360">moneys</w>
          <c> </c>
          <w lemma="give" ana="#vvn" reg="given" xml:id="A91680-0380" facs="A91680-001-b-0370">given</w>
          <pc xml:id="A91680-0390" facs="A91680-001-b-0380">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A91680-0400" facs="A91680-001-b-0390">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0410" facs="A91680-001-b-0400">the</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A91680-0420" facs="A91680-001-b-0410">Members</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0430" facs="A91680-001-b-0420">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0440" facs="A91680-001-b-0430">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A91680-0450" facs="A91680-001-b-0440">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0460" facs="A91680-001-b-0450">of</w>
          <c> </c>
          <w lemma="Commons" ana="#n2-nn" reg="Commons" xml:id="A91680-0470" facs="A91680-001-b-0460">Commons</w>
          <pc xml:id="A91680-0480" facs="A91680-001-b-0470">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A91680-0490" facs="A91680-001-b-0480">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0500" facs="A91680-001-b-0490">the</w>
          <c> </c>
          <w lemma="relief" ana="#n1" reg="relief" xml:id="A91680-0510" facs="A91680-001-b-0500">Reliefe</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0520" facs="A91680-001-b-0510">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0530" facs="A91680-001-b-0520">the</w>
          <c> </c>
          <w lemma="poor" ana="#j" reg="poor" xml:id="A91680-0540" facs="A91680-001-b-0530">poore</w>
          <c> </c>
          <w lemma="distress" ana="#j_vn" reg="distressed" xml:id="A91680-0550" facs="A91680-001-b-0540">distressed</w>
          <c> </c>
          <w lemma="people" ana="#n1" reg="persons" xml:id="A91680-0560" facs="A91680-001-b-0550">people</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A91680-0570" facs="A91680-001-b-0560">that</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A91680-0580" facs="A91680-001-b-0570">are</w>
          <c> </c>
          <w lemma="come" ana="#vvn" reg="come" xml:id="A91680-0590" facs="A91680-001-b-0580">come</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A91680-0600" facs="A91680-001-b-0590">out</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0610" facs="A91680-001-b-0600">of</w>
          <c> </c>
          <hi xml:id="A91680e-60">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A91680-0620" facs="A91680-001-b-0610">Ireland</w>
            <pc xml:id="A91680-0630" facs="A91680-001-b-0620">)</pc>
          </hi>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A91680-0640" facs="A91680-001-b-0630">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-0650" facs="A91680-001-b-0640">to</w>
          <c> </c>
          <w lemma="require" ana="#vvi" reg="require" xml:id="A91680-0660" facs="A91680-001-b-0650">require</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A91680-0670" facs="A91680-001-b-0660">you</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-0680" facs="A91680-001-b-0670">to</w>
          <c> </c>
          <w lemma="send" ana="#vvi" reg="send" xml:id="A91680-0690" facs="A91680-001-b-0680">send</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A91680-0700" facs="A91680-001-b-0690">unto</w>
          <c> </c>
          <w lemma="we" ana="#pno" reg="us" xml:id="A91680-0710" facs="A91680-001-b-0700">us</w>
          <pc xml:id="A91680-0720" facs="A91680-001-b-0710">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A91680-0730" facs="A91680-001-b-0720">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A91680-0740" facs="A91680-001-b-0730">any</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A91680-0750" facs="A91680-001-b-0740">one</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0760" facs="A91680-001-b-0750">of</w>
          <c> </c>
          <w lemma="we" ana="#pno" reg="us" xml:id="A91680-0770" facs="A91680-001-b-0760">us</w>
          <pc xml:id="A91680-0780" facs="A91680-001-b-0770">,</pc>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A91680-0790" facs="A91680-001-b-0780">a</w>
          <c> </c>
          <w lemma="certificate" ana="#n1" reg="certificate" xml:id="A91680-0800" facs="A91680-001-b-0790">Certificate</w>
          <pc xml:id="A91680-0810" facs="A91680-001-b-0800">,</pc>
          <c> </c>
          <w lemma="what" ana="#crq-r" reg="what" xml:id="A91680-0820" facs="A91680-001-b-0810">what</w>
          <c> </c>
          <w lemma="sum" ana="#n2" reg="sums" xml:id="A91680-0830" facs="A91680-001-b-0820">summes</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0840" facs="A91680-001-b-0830">of</w>
          <c> </c>
          <w lemma="money" ana="#n1" reg="money" xml:id="A91680-0850" facs="A91680-001-b-0840">money</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A91680-0860" facs="A91680-001-b-0850">are</w>
          <c> </c>
          <w lemma="collect" ana="#vvn" reg="collected" xml:id="A91680-0870" facs="A91680-001-b-0860">collected</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A91680-0880" facs="A91680-001-b-0870">within</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A91680-0890" facs="A91680-001-b-0880">your</w>
          <c> </c>
          <w lemma="parish" ana="#n1" reg="parish" xml:id="A91680-0900" facs="A91680-001-b-0890">Parish</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A91680-0910" facs="A91680-001-b-0900">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0920" facs="A91680-001-b-0910">the</w>
          <c> </c>
          <w lemma="relief" ana="#n1" reg="relief" xml:id="A91680-0930" facs="A91680-001-b-0920">reliefe</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0940" facs="A91680-001-b-0930">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-0950" facs="A91680-001-b-0940">the</w>
          <c> </c>
          <w lemma="poor" ana="#j" reg="poor" xml:id="A91680-0960" facs="A91680-001-b-0950">poore</w>
          <c> </c>
          <w lemma="distress" ana="#j_vn" reg="distressed" xml:id="A91680-0970" facs="A91680-001-b-0960">distressed</w>
          <c> </c>
          <w lemma="people" ana="#n1" reg="persons" xml:id="A91680-0980" facs="A91680-001-b-0970">people</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-0990" facs="A91680-001-b-0980">of</w>
          <c> </c>
          <hi xml:id="A91680e-70">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A91680-1000" facs="A91680-001-b-0990">Ireland</w>
            <pc xml:id="A91680-1010" facs="A91680-001-b-1000">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A91680-1020" facs="A91680-001-b-1010">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-1030" facs="A91680-001-b-1020">to</w>
          <c> </c>
          <w lemma="require" ana="#vvi" reg="require" xml:id="A91680-1040" facs="A91680-001-b-1030">require</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A91680-1050" facs="A91680-001-b-1040">you</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-1060" facs="A91680-001-b-1050">to</w>
          <c> </c>
          <w lemma="bring" ana="#vvi" reg="bring" xml:id="A91680-1070" facs="A91680-001-b-1060">bring</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A91680-1080" facs="A91680-001-b-1070">in</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A91680-1090" facs="A91680-001-b-1080">such</w>
          <c> </c>
          <w lemma="sum" ana="#n2" reg="sums" xml:id="A91680-1100" facs="A91680-001-b-1090">summes</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-1110" facs="A91680-001-b-1100">of</w>
          <c> </c>
          <w lemma="money" ana="#n1" reg="money" xml:id="A91680-1120" facs="A91680-001-b-1110">money</w>
          <pc xml:id="A91680-1130" facs="A91680-001-b-1120">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A91680-1140" facs="A91680-001-b-1130">as</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A91680-1150" facs="A91680-001-b-1140">you</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A91680-1160" facs="A91680-001-b-1150">have</w>
          <c> </c>
          <w lemma="receive" ana="#vvn" reg="received" xml:id="A91680-1170" facs="A91680-001-b-1160">received</w>
          <pc xml:id="A91680-1180" facs="A91680-001-b-1170">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-1190" facs="A91680-001-b-1180">to</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A91680-1200" facs="A91680-001-b-1190">that</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A91680-1210" facs="A91680-001-b-1200">Committee</w>
          <pc xml:id="A91680-1220" facs="A91680-001-b-1210">,</pc>
          <c> </c>
          <w lemma="who" ana="#crq-r" reg="who" xml:id="A91680-1230" facs="A91680-001-b-1220">who</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A91680-1240" facs="A91680-001-b-1230">are</w>
          <c> </c>
          <w lemma="authorise" ana="#vvn" reg="authorised" xml:id="A91680-1250" facs="A91680-001-b-1240">authorised</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A91680-1260" facs="A91680-001-b-1250">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1270" facs="A91680-001-b-1260">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A91680-1280" facs="A91680-001-b-1270">House</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A91680-1290" facs="A91680-001-b-1280">to</w>
          <c> </c>
          <w lemma="receive" ana="#vvi" reg="receive" xml:id="A91680-1300" facs="A91680-001-b-1290">receive</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1310" facs="A91680-001-b-1300">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A91680-1320" facs="A91680-001-b-1310">same</w>
          <pc unit="sentence" xml:id="A91680-1330" facs="A91680-001-b-1320">.</pc>
        </p>
        <closer xml:id="A91680e-80">
          <dateline xml:id="A91680e-90">
            <w lemma="date" ana="#j_vn" reg="dated" xml:id="A91680-1370" facs="A91680-001-b-1330">Dated</w>
            <c> </c>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A91680-1380" facs="A91680-001-b-1340">at</w>
            <c> </c>
            <hi xml:id="A91680e-100">
              <w lemma="Westminster" ana="#n1-nn" reg="Westminster" xml:id="A91680-1390" facs="A91680-001-b-1350">Westminster</w>
              <pc xml:id="A91680-1400" facs="A91680-001-b-1360">,</pc>
            </hi>
            <date xml:id="A91680e-110">
              <w lemma="the" ana="#d" reg="the" xml:id="A91680-1420" facs="A91680-001-b-1370">the</w>
              <c> </c>
              <w lemma="fifteen" ana="#ord" reg="fifteenth" xml:id="A91680-1430" facs="A91680-001-b-1380">fifteenth</w>
              <c> </c>
              <w lemma="day" ana="#n1" reg="day" xml:id="A91680-1440" facs="A91680-001-b-1390">day</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-1450" facs="A91680-001-b-1400">of</w>
              <c> </c>
              <hi xml:id="A91680e-120">
                <w lemma="February" ana="#n1-nn" reg="February" xml:id="A91680-1460" facs="A91680-001-b-1410">February</w>
                <pc xml:id="A91680-1470" facs="A91680-001-b-1420">,</pc>
              </hi>
              <c> </c>
              <w lemma="1641." ana="#crd" reg="1641." xml:id="A91680-1480" facs="A91680-001-b-1430">1641.</w>
              <pc unit="sentence" xml:id="A91680-1480-eos" facs="A91680-001-b-1440"/>
            </date>
          </dateline>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1500" facs="A91680-001-b-1450">The</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A91680-1510" facs="A91680-001-b-1460">Committee</w>
          <c> </c>
          <w lemma="appoint" ana="#vvn" reg="appointed" xml:id="A91680-1520" facs="A91680-001-b-1470">appointed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A91680-1530" facs="A91680-001-b-1480">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1540" facs="A91680-001-b-1490">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A91680-1550" facs="A91680-001-b-1500">House</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A91680-1560" facs="A91680-001-b-1510">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1570" facs="A91680-001-b-1520">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A91680-1580" facs="A91680-001-b-1530">said</w>
          <c> </c>
          <w lemma="collection" ana="#n1" reg="collection" xml:id="A91680-1590" facs="A91680-001-b-1540">Collection</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A91680-1600" facs="A91680-001-b-1550">are</w>
          <signed xml:id="A91680e-130">
            <list xml:id="A91680e-140">
              <item xml:id="A91680e-150">
                <w lemma="sir" ana="#n1" reg="Sir" xml:id="A91680-1630" facs="A91680-001-b-1560">Sir</w>
                <c> </c>
                <hi xml:id="A91680e-160">
                  <w lemma="Edward" ana="#n1-nn" reg="Edward" xml:id="A91680-1640" facs="A91680-001-b-1570">Edward</w>
                  <c> </c>
                  <w lemma="Aiscough" ana="#n1-nn" reg="Aiscough" xml:id="A91680-1650" facs="A91680-001-b-1580">Aiscough</w>
                </hi>
                <c> </c>
                <w lemma="knight" ana="#n1" reg="knight" xml:id="A91680-1660" facs="A91680-001-b-1590">Knight</w>
                <pc unit="sentence" xml:id="A91680-1670" facs="A91680-001-b-1600">.</pc>
              </item>
              <item xml:id="A91680e-170">
                <list xml:id="A91680e-180">
                  <item xml:id="A91680e-190">
                    <hi xml:id="A91680e-200">
                      <w lemma="Francis" ana="#n1-nn" reg="Francis" xml:id="A91680-1720" facs="A91680-001-b-1610">Francis</w>
                      <c> </c>
                      <w lemma="Rowse" ana="#n1-nn" reg="Rowse" xml:id="A91680-1730" facs="A91680-001-b-1620">Rowse</w>
                    </hi>
                  </item>
                  <item xml:id="A91680e-210">
                    <hi xml:id="A91680e-220">
                      <w lemma="Henry" ana="#n1-nn" reg="Henry" xml:id="A91680-1760" facs="A91680-001-b-1630">Henry</w>
                      <c> </c>
                      <w lemma="Martin" ana="#n1-nn" reg="Martin" xml:id="A91680-1770" facs="A91680-001-b-1640">Martin</w>
                    </hi>
                  </item>
                  <item xml:id="A91680e-230">
                    <hi xml:id="A91680e-240">
                      <w lemma="William" ana="#n1-nn" reg="William" xml:id="A91680-1800" facs="A91680-001-b-1650">William</w>
                      <c> </c>
                      <w lemma="wheler" ana="#n1" reg="wheler" xml:id="A91680-1810" facs="A91680-001-b-1660">VVheler</w>
                    </hi>
                  </item>
                </list>
                <w lemma="esquire" ana="#n2" reg="esquires" xml:id="A91680-1830" facs="A91680-001-b-1670">Esquires</w>
                <pc unit="sentence" xml:id="A91680-1840" facs="A91680-001-b-1680">.</pc>
              </item>
            </list>
          </signed>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A91680-1860" facs="A91680-001-b-1690">To</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1870" facs="A91680-001-b-1700">the</w>
          <c> </c>
          <w lemma="minister" ana="#n1" reg="minister" xml:id="A91680-1880" facs="A91680-001-b-1710">Minister</w>
          <pc xml:id="A91680-1890" facs="A91680-001-b-1720">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A91680-1900" facs="A91680-001-b-1730">and</w>
          <c> </c>
          <w lemma="churchwarden" ana="#n2" reg="churchwardens" xml:id="A91680-1910" facs="A91680-001-b-1740">Churchwardens</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-1920" facs="A91680-001-b-1750">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A91680-1930" facs="A91680-001-b-1760">the</w>
          <c> </c>
          <w lemma="parish" ana="#n1" reg="parish" xml:id="A91680-1940" facs="A91680-001-b-1770">Parish</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A91680-1950" facs="A91680-001-b-1780">of</w>
        </closer>
      </div>
    </body>
    <back xml:id="A91680e-250">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>