<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The mounument of Charles the First, King of England VVho was beheaded before Whit-Hall [sic] January 30th 1648. In the 24th yeare of his reigne.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1649</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A89248</idno>
        <idno type="STC">Wing M2520</idno>
        <idno type="STC">Thomason 669.f.14[36]</idno>
        <idno type="STC">ESTC R211146</idno>
        <idno type="EEBO-CITATION">99869879</idno>
        <idno type="PROQUEST">99869879</idno>
        <idno type="VID">163024</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A89248)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163024)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f14[36])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The mounument of Charles the First, King of England VVho was beheaded before Whit-Hall [sic] January 30th 1648. In the 24th yeare of his reigne.</title>
          </titleStmt>
          <extent>1 sheet ([1] p.) : ill.</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1649]</date>
          </publicationStmt>
          <notesStmt>
            <note>Imprint from Wing.</note>
            <note>Verse - "Here lyes great Charles, first King of that same name,".</note>
            <note>Annotation on Thomason copy: "June 5. 1649".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Charles -- I, -- King of England, 1600-1649 -- Poetry -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-07</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-08</date><label>Jason Colman</label>
        Sampled and proofread
      </change>
      <change><date>2007-08</date><label>Jason Colman</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A89248e-10">
    <body xml:id="A89248e-20">
      <div type="epitaph" xml:id="A89248e-30">
        <pb facs="tcp:163024:1" rendition="simple:additions" xml:id="A89248-001-a"/>
        <head xml:id="A89248e-40">
          <w lemma="the" ana="#d" reg="The" xml:id="A89248-0030" facs="A89248-001-a-0010">THE</w>
          <c> </c>
          <w lemma="monument" ana="#n1" reg="monument" xml:id="A89248-0040" facs="A89248-001-a-0020">MONVMENT</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A89248-0050" facs="A89248-001-a-0030">OF</w>
          <c> </c>
          <w lemma="CHARLES" ana="#n1-nn" reg="Charles" xml:id="A89248-0060" facs="A89248-001-a-0040">CHARLES</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A89248-0070" facs="A89248-001-a-0050">THE</w>
          <c> </c>
          <w lemma="first" ana="#ord" reg="first" xml:id="A89248-0080" facs="A89248-001-a-0060">FIRST</w>
          <pc xml:id="A89248-0090" facs="A89248-001-a-0070">,</pc>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A89248-0100" facs="A89248-001-a-0080">KING</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A89248-0110" facs="A89248-001-a-0090">OF</w>
          <c> </c>
          <w lemma="ENGLAND" ana="#n1-nn" reg="England" xml:id="A89248-0120" facs="A89248-001-a-0100">ENGLAND</w>
          <pc unit="sentence" xml:id="A89248-0130" facs="A89248-001-a-0110">.</pc>
        </head>
        <head type="sub" xml:id="A89248e-50">
          <w lemma="who" ana="#crq-r" reg="Who" xml:id="A89248-0160" facs="A89248-001-a-0120">VVho</w>
          <c> </c>
          <w lemma="be" ana="#vvd" reg="was" xml:id="A89248-0170" facs="A89248-001-a-0130">was</w>
          <c> </c>
          <w lemma="behead" ana="#vvn" reg="beheaded" xml:id="A89248-0180" facs="A89248-001-a-0140">beheaded</w>
          <c> </c>
          <w lemma="before" ana="#acp-p" reg="before" xml:id="A89248-0190" facs="A89248-001-a-0150">before</w>
          <c> </c>
          <hi xml:id="A89248e-60">
            <w lemma="Whitehall" ana="#n1-nn" reg="Whitehall" xml:id="A89248-0200" facs="A89248-001-a-0160">Whit-Hall</w>
          </hi>
          <date xml:id="A89248e-70">
            <hi xml:id="A89248e-80">
              <w lemma="January" ana="#n1-nn" reg="January" xml:id="A89248-0220" facs="A89248-001-a-0170">January</w>
            </hi>
            <c> </c>
            <w lemma="30" part="I" ana="#crd" reg="30" xml:id="A89248-0230.1" facs="A89248-001-a-0180" rendition="hi-mid-3">30th</w>
            <hi rend="sup" xml:id="A89248e-90"/>
            <c> </c>
            <w lemma="1648." ana="#crd" reg="1648." xml:id="A89248-0240" facs="A89248-001-a-0200">1648.</w>
            <pc unit="sentence" xml:id="A89248-0240-eos" facs="A89248-001-a-0210"/>
          </date>
          <lb xml:id="A89248e-100"/>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A89248-0260" facs="A89248-001-a-0220">In</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A89248-0270" facs="A89248-001-a-0230">the</w>
          <c> </c>
          <w lemma="24th" part="I" ana="#ord" reg="24th" xml:id="A89248-0280.1" facs="A89248-001-a-0240" rendition="hi-mid-3">24th</w>
          <hi rend="sup" xml:id="A89248e-110"/>
          <c> </c>
          <w lemma="year" ana="#n1" reg="year" xml:id="A89248-0290" facs="A89248-001-a-0260">yeare</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A89248-0300" facs="A89248-001-a-0270">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A89248-0310" facs="A89248-001-a-0280">his</w>
          <c> </c>
          <w lemma="reign" ana="#n1" reg="reign" xml:id="A89248-0320" facs="A89248-001-a-0290">Reigne</w>
          <pc unit="sentence" xml:id="A89248-0330" facs="A89248-001-a-0300">.</pc>
        </head>
        <q xml:id="A89248e-120">
          <w lemma="mors" ana="#fw-la" reg="mors" xml:id="A89248-0360" facs="A89248-001-a-0310">Mors</w>
          <c> </c>
          <w lemma="mihi" ana="#fw-la" reg="mihi" xml:id="A89248-0370" facs="A89248-001-a-0320">mihi</w>
          <c> </c>
          <w lemma="lucrum" ana="#fw-la" reg="lucrum" xml:id="A89248-0380" facs="A89248-001-a-0330">Lucrum</w>
          <pc unit="sentence" xml:id="A89248-0390" facs="A89248-001-a-0340">.</pc>
        </q>
        <p xml:id="A89248e-130">
          <figure xml:id="A89248e-140">
            <head xml:id="A89248e-150">
              <w lemma="c" ana="#sy" reg="C" xml:id="A89248-0440" facs="A89248-001-a-0350">C</w>
              <c> </c>
              <w lemma="R" ana="#n1-nn" reg="R" xml:id="A89248-0450" facs="A89248-001-a-0360">R</w>
              <c> </c>
              <w lemma="aetatis" ana="#fw-la" reg="aetatis" xml:id="A89248-0460" facs="A89248-001-a-0370">Aetatis</w>
              <c> </c>
              <w lemma="Suae" ana="#fw-la" reg="suae" xml:id="A89248-0470" facs="A89248-001-a-0380">Suae</w>
              <c> </c>
              <w lemma="48" ana="#crd" reg="48" xml:id="A89248-0480" facs="A89248-001-a-0390">48</w>
            </head>
            <figDesc xml:id="A89248e-160">funeral monument of Charles I</figDesc>
          </figure>
        </p>
        <q xml:id="A89248e-170">
          <w lemma="virtus" ana="#fw-la" reg="virtus" xml:id="A89248-0580" facs="A89248-001-a-0400">Virtus</w>
          <c> </c>
          <w lemma="post" ana="#fw-la" reg="post" xml:id="A89248-0590" facs="A89248-001-a-0410">post</w>
          <c> </c>
          <w lemma="funera" ana="#fw-la" reg="funera" xml:id="A89248-0600" facs="A89248-001-a-0420">Funera</w>
          <c> </c>
          <w lemma="vivit" ana="#fw-la" reg="vivit" xml:id="A89248-0610" facs="A89248-001-a-0430">vivit</w>
          <pc unit="sentence" xml:id="A89248-0620" facs="A89248-001-a-0440">.</pc>
        </q>
        <lg xml:id="A89248e-180">
          <l xml:id="A89248e-190">
            <w lemma="here" ana="#av" reg="Here" xml:id="A89248-0650" facs="A89248-001-a-0450">HEre</w>
            <c> </c>
            <w lemma="lie" ana="#vvz" reg="lies" xml:id="A89248-0660" facs="A89248-001-a-0460">lyes</w>
            <c> </c>
            <w lemma="great" ana="#j" reg="great" xml:id="A89248-0670" facs="A89248-001-a-0470">great</w>
            <c> </c>
            <w lemma="CHARLES" ana="#n1-nn" reg="Charles" xml:id="A89248-0680" facs="A89248-001-a-0480">CHARLES</w>
            <pc xml:id="A89248-0690" facs="A89248-001-a-0490">,</pc>
            <c> </c>
            <w lemma="first" ana="#ord" reg="first" xml:id="A89248-0700" facs="A89248-001-a-0500">first</w>
            <c> </c>
            <w lemma="king" ana="#n1" reg="King" xml:id="A89248-0710" facs="A89248-001-a-0510">King</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A89248-0720" facs="A89248-001-a-0520">of</w>
            <c> </c>
            <w lemma="that" ana="#d" reg="that" xml:id="A89248-0730" facs="A89248-001-a-0530">that</w>
            <c> </c>
            <w lemma="same" ana="#d" reg="same" xml:id="A89248-0740" facs="A89248-001-a-0540">same</w>
            <c> </c>
            <w lemma="name" ana="#n1" reg="name" xml:id="A89248-0750" facs="A89248-001-a-0550">name</w>
            <pc xml:id="A89248-0760" facs="A89248-001-a-0560">,</pc>
          </l>
          <l xml:id="A89248e-200">
            <hi xml:id="A89248e-210">
              <w lemma="England" ana="#n1g-nn" reg="England's" xml:id="A89248-0770" facs="A89248-001-a-0570">Englands</w>
            </hi>
            <c> </c>
            <w lemma="great" ana="#j" reg="great" xml:id="A89248-0780" facs="A89248-001-a-0580">great</w>
            <c> </c>
            <w lemma="glory" ana="#n1" reg="glory" xml:id="A89248-0790" facs="A89248-001-a-0590">glory</w>
            <pc xml:id="A89248-0800" facs="A89248-001-a-0600">,</pc>
            <c> </c>
            <hi xml:id="A89248e-220">
              <w lemma="England" ana="#n1g-nn" reg="England's" xml:id="A89248-0810" facs="A89248-001-a-0610">Englands</w>
            </hi>
            <c> </c>
            <w lemma="last" ana="#j_vg" reg="lasting" xml:id="A89248-0820" facs="A89248-001-a-0620">lasting</w>
            <c> </c>
            <w lemma="fame" ana="#n1" reg="fame" xml:id="A89248-0830" facs="A89248-001-a-0630">fame</w>
            <pc xml:id="A89248-0840" facs="A89248-001-a-0640">;</pc>
          </l>
          <l xml:id="A89248e-230">
            <w lemma="a" ana="#d" reg="A" xml:id="A89248-0850" facs="A89248-001-a-0650">A</w>
            <c> </c>
            <w lemma="butcher" ana="#vvn" reg="butchered" xml:id="A89248-0860" facs="A89248-001-a-0660">Butcher'd</w>
            <c> </c>
            <w lemma="martyr" ana="#n1" reg="martyr" xml:id="A89248-0870" facs="A89248-001-a-0670">Martyr</w>
            <c> </c>
            <w lemma="king" ana="#n1" reg="King" xml:id="A89248-0880" facs="A89248-001-a-0680">King</w>
            <pc xml:id="A89248-0890" facs="A89248-001-a-0690">,</pc>
            <c> </c>
            <w lemma="who" ana="#crq-rg" reg="whose" xml:id="A89248-0900" facs="A89248-001-a-0700">whose</w>
            <c> </c>
            <w lemma="patient" ana="#j" reg="patient" xml:id="A89248-0910" facs="A89248-001-a-0710">patient</w>
            <c> </c>
            <w lemma="mildness" ana="#n1" reg="mildness" xml:id="A89248-0920" facs="A89248-001-a-0720">mildnesse</w>
            <pc xml:id="A89248-0930" facs="A89248-001-a-0730">,</pc>
          </l>
          <l xml:id="A89248e-240">
            <w lemma="crown" ana="#vvz" reg="Crowneth" xml:id="A89248-0940" facs="A89248-001-a-0740">Crowneth</w>
            <c> </c>
            <w lemma="himself" ana="#px" reg="himself" xml:id="A89248-0950" facs="A89248-001-a-0750">himselfe</w>
            <c> </c>
            <w lemma="with" ana="#acp-p" reg="with" xml:id="A89248-0960" facs="A89248-001-a-0760">with</w>
            <c> </c>
            <w lemma="fame" ana="#n1" reg="fame" xml:id="A89248-0970" facs="A89248-001-a-0770">Fame</w>
            <pc xml:id="A89248-0980" facs="A89248-001-a-0780">,</pc>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-0990" facs="A89248-001-a-0790">his</w>
            <c> </c>
            <w lemma="foe" ana="#n2" reg="foes" xml:id="A89248-1000" facs="A89248-001-a-0800">foes</w>
            <c> </c>
            <w lemma="with" ana="#acp-p" reg="with" xml:id="A89248-1010" facs="A89248-001-a-0810">with</w>
            <c> </c>
            <w lemma="vildness" ana="#n1" reg="vildness" xml:id="A89248-1020" facs="A89248-001-a-0820">vildnesse</w>
            <pc xml:id="A89248-1030" facs="A89248-001-a-0830">:</pc>
          </l>
          <l xml:id="A89248e-250">
            <w lemma="a" ana="#d" reg="A" xml:id="A89248-1040" facs="A89248-001-a-0840">A</w>
            <c> </c>
            <w lemma="persecute" ana="#j_vn" reg="persecuted" xml:id="A89248-1050" facs="A89248-001-a-0850">persecuted</w>
            <c> </c>
            <w lemma="saint" ana="#n1" reg="saint" xml:id="A89248-1060" facs="A89248-001-a-0860">Saint</w>
            <pc xml:id="A89248-1070" facs="A89248-001-a-0870">,</pc>
            <c> </c>
            <w lemma="for" ana="#acp-p" reg="for" xml:id="A89248-1080" facs="A89248-001-a-0880">for</w>
            <c> </c>
            <w lemma="seven" ana="#crd" reg="seven" xml:id="A89248-1090" facs="A89248-001-a-0890">seven</w>
            <c> </c>
            <w lemma="year" ana="#n2g" reg="years'" xml:id="A89248-1100" facs="A89248-001-a-0900">yeares</w>
            <c> </c>
            <w lemma="space" ana="#n1" reg="space" xml:id="A89248-1110" facs="A89248-001-a-0910">space</w>
          </l>
          <l xml:id="A89248e-260">
            <w lemma="press" ana="#vvn" reg="Pressed" xml:id="A89248-1120" facs="A89248-001-a-0920">Prest</w>
            <c> </c>
            <w lemma="down" ana="#acp-av" reg="down" xml:id="A89248-1130" facs="A89248-001-a-0930">downe</w>
            <c> </c>
            <w lemma="with" ana="#acp-p" reg="with" xml:id="A89248-1140" facs="A89248-001-a-0940">with</w>
            <c> </c>
            <w lemma="sorrow" ana="#n2" reg="sorrows" xml:id="A89248-1150" facs="A89248-001-a-0950">sorrowes</w>
            <pc xml:id="A89248-1160" facs="A89248-001-a-0960">,</pc>
            <c> </c>
            <w lemma="yet" ana="#av" reg="yet" xml:id="A89248-1170" facs="A89248-001-a-0970">yet</w>
            <c> </c>
            <w lemma="hold" ana="#vvd" reg="held" xml:id="A89248-1180" facs="A89248-001-a-0980">held</w>
            <c> </c>
            <w lemma="up" ana="#acp-av" reg="up" xml:id="A89248-1190" facs="A89248-001-a-0990">up</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A89248-1200" facs="A89248-001-a-1000">by</w>
            <c> </c>
            <w lemma="grace" ana="#n1" reg="grace" xml:id="A89248-1210" facs="A89248-001-a-1010">Grace</w>
            <pc xml:id="A89248-1220" facs="A89248-001-a-1020">;</pc>
          </l>
          <l xml:id="A89248e-270">
            <w lemma="in" ana="#acp-p" reg="In" xml:id="A89248-1230" facs="A89248-001-a-1030">In</w>
            <c> </c>
            <w lemma="who" ana="#crq-ro" reg="whom" xml:id="A89248-1240" facs="A89248-001-a-1040">whom</w>
            <c> </c>
            <w lemma="those" ana="#d" reg="those" xml:id="A89248-1250" facs="A89248-001-a-1050">those</w>
            <c> </c>
            <w lemma="virtue" ana="#n2" reg="virtues" xml:id="A89248-1260" facs="A89248-001-a-1060">Vertues</w>
            <c> </c>
            <w lemma="constant" ana="#av_j" reg="constantly" xml:id="A89248-1270" facs="A89248-001-a-1070">constantly</w>
            <c> </c>
            <w lemma="do" ana="#vvd" reg="did" xml:id="A89248-1280" facs="A89248-001-a-1080">did</w>
            <c> </c>
            <w lemma="live" ana="#vvi" reg="live" xml:id="A89248-1290" facs="A89248-001-a-1090">live</w>
            <pc xml:id="A89248-1300" facs="A89248-001-a-1100">,</pc>
          </l>
          <l xml:id="A89248e-280">
            <w lemma="which" ana="#crq-r" reg="Which" xml:id="A89248-1310" facs="A89248-001-a-1110">Which</w>
            <c> </c>
            <w lemma="to" ana="#acp-p" reg="to" xml:id="A89248-1320" facs="A89248-001-a-1120">to</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A89248-1330" facs="A89248-001-a-1130">the</w>
            <c> </c>
            <w lemma="worthy" ana="#n2_j" reg="worthies" xml:id="A89248-1340" facs="A89248-001-a-1140">Worthies</w>
            <c> </c>
            <w lemma="last" ana="#vvg" reg="lasting" xml:id="A89248-1350" facs="A89248-001-a-1150">lasting</w>
            <c> </c>
            <w lemma="fame" ana="#n1" reg="fame" xml:id="A89248-1360" facs="A89248-001-a-1160">fame</w>
            <c> </c>
            <w lemma="do" ana="#vvd" reg="did" xml:id="A89248-1370" facs="A89248-001-a-1170">did</w>
            <c> </c>
            <w lemma="give" ana="#vvi" reg="give" xml:id="A89248-1380" facs="A89248-001-a-1180">give</w>
            <pc xml:id="A89248-1390" facs="A89248-001-a-1190">:</pc>
          </l>
          <l xml:id="A89248e-290">
            <w lemma="patient" ana="#n1" reg="Patient" xml:id="A89248-1400" facs="A89248-001-a-1200">Patient</w>
            <c> </c>
            <w lemma="as" ana="#acp-p" reg="as" xml:id="A89248-1410" facs="A89248-001-a-1210">as</w>
            <c> </c>
            <hi xml:id="A89248e-300">
              <w lemma="Job" ana="#n1-nn" reg="Job" xml:id="A89248-1420" facs="A89248-001-a-1220">Job</w>
              <pc xml:id="A89248-1430" facs="A89248-001-a-1230">,</pc>
            </hi>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A89248-1440" facs="A89248-001-a-1240">and</w>
            <c> </c>
            <w lemma="like" ana="#j" reg="like" xml:id="A89248-1450" facs="A89248-001-a-1250">like</w>
            <c> </c>
            <w lemma="to" ana="#acp-p" reg="to" xml:id="A89248-1460" facs="A89248-001-a-1260">to</w>
            <c> </c>
            <hi xml:id="A89248e-310">
              <w lemma="Moses" ana="#n1-nn" reg="Moses" xml:id="A89248-1470" facs="A89248-001-a-1270">Moses</w>
              <pc xml:id="A89248-1480" facs="A89248-001-a-1280">,</pc>
            </hi>
            <c> </c>
            <w lemma="mild" ana="#j" reg="mild" xml:id="A89248-1490" facs="A89248-001-a-1290">milde</w>
            <pc xml:id="A89248-1500" facs="A89248-001-a-1300">,</pc>
          </l>
          <l xml:id="A89248e-320">
            <hi xml:id="A89248e-330">
              <w lemma="Salomon" ana="#n1g-nn" reg="Salomon's" xml:id="A89248-1510" facs="A89248-001-a-1310">Salomons</w>
            </hi>
            <c> </c>
            <w lemma="wisdom" ana="#n1" reg="wisdom" xml:id="A89248-1520" facs="A89248-001-a-1320">wisedome</w>
            <c> </c>
            <w lemma="make" ana="#vvz" reg="makes" xml:id="A89248-1530" facs="A89248-001-a-1330">makes</w>
            <c> </c>
            <w lemma="he" ana="#pno" reg="him" xml:id="A89248-1540" facs="A89248-001-a-1340">him</w>
            <c> </c>
            <w lemma="wisdom" ana="#n2" reg="wisdoms" xml:id="A89248-1550" facs="A89248-001-a-1350">Wisedomes</w>
            <c> </c>
            <w lemma="child" ana="#n1" reg="child" xml:id="A89248-1560" facs="A89248-001-a-1360">childe</w>
            <pc xml:id="A89248-1570" facs="A89248-001-a-1370">;</pc>
          </l>
          <l xml:id="A89248e-340">
            <w lemma="valiant" ana="#j" reg="Valiant" xml:id="A89248-1580" facs="A89248-001-a-1380">Valiant</w>
            <c> </c>
            <w lemma="as" ana="#acp-p" reg="as" xml:id="A89248-1590" facs="A89248-001-a-1390">as</w>
            <c> </c>
            <hi xml:id="A89248e-350">
              <w lemma="David" ana="#n1-nn" reg="David" xml:id="A89248-1600" facs="A89248-001-a-1400">David</w>
              <pc xml:id="A89248-1610" facs="A89248-001-a-1410">,</pc>
            </hi>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A89248-1620" facs="A89248-001-a-1420">and</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A89248-1630" facs="A89248-001-a-1430">the</w>
            <c> </c>
            <w lemma="chief" ana="#j-s" reg="chiefest" xml:id="A89248-1640" facs="A89248-001-a-1440">chiefest</w>
            <c> </c>
            <w lemma="part" ana="#n1" reg="part" xml:id="A89248-1650" facs="A89248-001-a-1450">part</w>
          </l>
          <l xml:id="A89248e-360">
            <w lemma="like" ana="#j" reg="Like" xml:id="A89248-1660" facs="A89248-001-a-1460">Like</w>
            <c> </c>
            <w lemma="to" ana="#acp-cs" reg="to" xml:id="A89248-1670" facs="A89248-001-a-1470">to</w>
            <c> </c>
            <hi xml:id="A89248e-370">
              <w lemma="Josiah" ana="#n1-nn" reg="Josiah" xml:id="A89248-1680" facs="A89248-001-a-1480">Josiah</w>
              <pc xml:id="A89248-1690" facs="A89248-001-a-1490">,</pc>
            </hi>
            <c> </c>
            <w lemma="zeal" ana="#n1" reg="zeal" xml:id="A89248-1700" facs="A89248-001-a-1500">zeale</w>
            <c> </c>
            <w lemma="possess" ana="#vvn" reg="possessed" xml:id="A89248-1710" facs="A89248-001-a-1510">possest</w>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-1720" facs="A89248-001-a-1520">his</w>
            <c> </c>
            <w lemma="heart" ana="#n1" reg="heart" xml:id="A89248-1730" facs="A89248-001-a-1530">heart</w>
            <pc xml:id="A89248-1740" facs="A89248-001-a-1540">:</pc>
          </l>
          <l xml:id="A89248e-380">
            <w lemma="faithful" ana="#j" reg="Faithful" xml:id="A89248-1750" facs="A89248-001-a-1550">Faithfull</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A89248-1760" facs="A89248-001-a-1560">in</w>
            <c> </c>
            <w lemma="vow" ana="#n2" reg="vows" xml:id="A89248-1770" facs="A89248-001-a-1570">Vowes</w>
            <pc xml:id="A89248-1780" facs="A89248-001-a-1580">,</pc>
            <c> </c>
            <w lemma="yet" ana="#av" reg="yet" xml:id="A89248-1790" facs="A89248-001-a-1590">yet</w>
            <c> </c>
            <w lemma="there" ana="#av" reg="there" xml:id="A89248-1800" facs="A89248-001-a-1600">there</w>
            <c> </c>
            <w lemma="begin" ana="#vvd" reg="began" xml:id="A89248-1810" facs="A89248-001-a-1610">began</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A89248-1820" facs="A89248-001-a-1620">the</w>
            <c> </c>
            <w lemma="strife" ana="#n1" reg="strife" xml:id="A89248-1830" facs="A89248-001-a-1630">strife</w>
            <pc xml:id="A89248-1840" facs="A89248-001-a-1640">,</pc>
          </l>
          <l xml:id="A89248e-390">
            <w lemma="that" ana="#cs" reg="That" xml:id="A89248-1850" facs="A89248-001-a-1650">That</w>
            <c> </c>
            <w lemma="reave" ana="#vvd" reg="reaved" xml:id="A89248-1860" facs="A89248-001-a-1660">reav'd</w>
            <c> </c>
            <w lemma="he" ana="#pno" reg="him" xml:id="A89248-1870" facs="A89248-001-a-1670">him</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A89248-1880" facs="A89248-001-a-1680">of</w>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-1890" facs="A89248-001-a-1690">his</w>
            <c> </c>
            <hi xml:id="A89248e-400">
              <w lemma="queen" ana="#n1" reg="Queen" xml:id="A89248-1900" facs="A89248-001-a-1700">Queene</w>
              <pc xml:id="A89248-1910" facs="A89248-001-a-1710">,</pc>
            </hi>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-1920" facs="A89248-001-a-1720">his</w>
            <c> </c>
            <hi xml:id="A89248e-410">
              <w lemma="crown" ana="#n1" reg="crown" xml:id="A89248-1930" facs="A89248-001-a-1730">Crowne</w>
              <pc xml:id="A89248-1940" facs="A89248-001-a-1740">,</pc>
            </hi>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-1950" facs="A89248-001-a-1750">his</w>
            <c> </c>
            <hi xml:id="A89248e-420">
              <w lemma="life" ana="#n1" reg="life" xml:id="A89248-1960" facs="A89248-001-a-1760">Life</w>
              <pc xml:id="A89248-1970" facs="A89248-001-a-1770">:</pc>
            </hi>
          </l>
          <l xml:id="A89248e-430">
            <w lemma="who" ana="#crq-rg" reg="Whose" xml:id="A89248-1980" facs="A89248-001-a-1780">Whose</w>
            <c> </c>
            <w lemma="true" ana="#j" reg="true" xml:id="A89248-1990" facs="A89248-001-a-1790">true</w>
            <c> </c>
            <w lemma="effigy" ana="#n2" reg="effigies" xml:id="A89248-2000" facs="A89248-001-a-1800">Effigies</w>
            <c> </c>
            <w lemma="draw" ana="#vvn" reg="drawn" xml:id="A89248-2010" facs="A89248-001-a-1810">drawne</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A89248-2020" facs="A89248-001-a-1820">by</w>
            <c> </c>
            <w lemma="just" ana="#j" reg="just" xml:id="A89248-2030" facs="A89248-001-a-1830">just</w>
            <c> </c>
            <w lemma="desert" ana="#n1" reg="desert" xml:id="A89248-2040" facs="A89248-001-a-1840">desert</w>
            <pc xml:id="A89248-2050" facs="A89248-001-a-1850">,</pc>
          </l>
          <l xml:id="A89248e-440">
            <w lemma="cause" ana="#vvz" reg="Causeth" xml:id="A89248-2060" facs="A89248-001-a-1860">Causeth</w>
            <c> </c>
            <w lemma="he" ana="#pno" reg="him" xml:id="A89248-2070" facs="A89248-001-a-1870">him</w>
            <c> </c>
            <w lemma="live" ana="#vvi" reg="live" xml:id="A89248-2080" facs="A89248-001-a-1880">live</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A89248-2090" facs="A89248-001-a-1890">in</w>
            <c> </c>
            <w lemma="each" ana="#d" reg="each" xml:id="A89248-2100" facs="A89248-001-a-1900">each</w>
            <c> </c>
            <w lemma="true" ana="#j" reg="true" xml:id="A89248-2110" facs="A89248-001-a-1910">true</w>
            <c> </c>
            <w lemma="subject" ana="#n2" reg="subjects" xml:id="A89248-2120" facs="A89248-001-a-1920">Subjects</w>
            <c> </c>
            <w lemma="heart" ana="#n1" reg="heart" xml:id="A89248-2130" facs="A89248-001-a-1930">heart</w>
            <pc xml:id="A89248-2140" facs="A89248-001-a-1940">:</pc>
          </l>
          <l xml:id="A89248e-450">
            <w lemma="live" ana="#vvg" reg="Living" xml:id="A89248-2150" facs="A89248-001-a-1950">Living</w>
            <c> </c>
            <w lemma="he" ana="#pns" reg="he" xml:id="A89248-2160" facs="A89248-001-a-1960">he</w>
            <c> </c>
            <w lemma="die" ana="#vvd" reg="died" xml:id="A89248-2170" facs="A89248-001-a-1970">dy'd</w>
            <pc xml:id="A89248-2180" facs="A89248-001-a-1980">,</pc>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A89248-2190" facs="A89248-001-a-1990">and</w>
            <c> </c>
            <w lemma="die" ana="#j_vg" reg="dying" xml:id="A89248-2200" facs="A89248-001-a-2000">dying</w>
            <c> </c>
            <w lemma="life" ana="#n1" reg="life" xml:id="A89248-2210" facs="A89248-001-a-2010">life</w>
            <c> </c>
            <w lemma="he" ana="#pns" reg="he" xml:id="A89248-2220" facs="A89248-001-a-2020">he</w>
            <c> </c>
            <w lemma="gain" ana="#vvd" reg="gained" xml:id="A89248-2230" facs="A89248-001-a-2030">gain'd</w>
            <pc xml:id="A89248-2240" facs="A89248-001-a-2040">,</pc>
          </l>
          <l xml:id="A89248e-460">
            <w lemma="death" ana="#n1" reg="Death" xml:id="A89248-2250" facs="A89248-001-a-2050">Death</w>
            <c> </c>
            <w lemma="conquer" ana="#vvd" reg="conquered" xml:id="A89248-2260" facs="A89248-001-a-2060">conquer'd</w>
            <c> </c>
            <w lemma="he" ana="#pno" reg="him" xml:id="A89248-2270" facs="A89248-001-a-2070">him</w>
            <pc xml:id="A89248-2280" facs="A89248-001-a-2080">,</pc>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A89248-2290" facs="A89248-001-a-2090">his</w>
            <c> </c>
            <w lemma="foe" ana="#n2" reg="foes" xml:id="A89248-2300" facs="A89248-001-a-2100">foes</w>
            <c> </c>
            <w lemma="with" ana="#acp-p" reg="with" xml:id="A89248-2310" facs="A89248-001-a-2110">with</w>
            <c> </c>
            <w lemma="shame" ana="#n1" reg="shame" xml:id="A89248-2320" facs="A89248-001-a-2120">shame</w>
            <c> </c>
            <w lemma="he" ana="#pns" reg="he" xml:id="A89248-2330" facs="A89248-001-a-2130">he</w>
            <c> </c>
            <w lemma="stain" ana="#vvn" reg="stained" xml:id="A89248-2340" facs="A89248-001-a-2140">stain'd</w>
            <pc unit="sentence" xml:id="A89248-2350" facs="A89248-001-a-2150">.</pc>
            <c> </c>
          </l>
        </lg>
      </div>
      <trailer xml:id="A89248e-470">
        <w lemma="finis" ana="#fw-la" reg="Finis" xml:id="A89248-2380" facs="A89248-001-a-2160">FINIS</w>
        <pc unit="sentence" xml:id="A89248-2390" facs="A89248-001-a-2170">.</pc>
      </trailer>
    </body>
    <back xml:id="A89248e-480">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>