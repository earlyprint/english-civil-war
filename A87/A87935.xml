<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A letter from a person of honour, relating the slaughter of a party of 300 horse, by the forces under the command of the Earle of Holland, July 7. 1648.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1648</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A87935</idno>
        <idno type="STC">Wing L1422</idno>
        <idno type="STC">Thomason 669.f.12[72]</idno>
        <idno type="STC">ESTC R210948</idno>
        <idno type="EEBO-CITATION">99869696</idno>
        <idno type="PROQUEST">99869696</idno>
        <idno type="VID">162865</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A87935)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162865)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f12[72])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A letter from a person of honour, relating the slaughter of a party of 300 horse, by the forces under the command of the Earle of Holland, July 7. 1648.</title>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>[s.n.],</publisher>
            <pubPlace>London :</pubPlace>
            <date>printed in the year 1648.</date>
          </publicationStmt>
          <notesStmt>
            <note>Dated at end: July 8. From the Head-quarters at Hounslow-heath.</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Campaigns -- Early works to 1800.</term>
          <term>Great Britain -- History, Military -- 17th century -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A87935e-10">
    <body xml:id="A87935e-20">
      <div type="letter" xml:id="A87935e-30">
        <pb facs="tcp:162865:1" rendition="simple:additions" xml:id="A87935-001-a"/>
        <head xml:id="A87935e-40">
          <w lemma="a" ana="#d" reg="A" xml:id="A87935-0030" facs="A87935-001-a-0010">A</w>
          <c> </c>
          <w lemma="letter" ana="#n1" reg="letter" xml:id="A87935-0040" facs="A87935-001-a-0020">LETTER</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A87935-0050" facs="A87935-001-a-0030">FROM</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87935-0060" facs="A87935-001-a-0040">A</w>
          <c> </c>
          <w lemma="person" ana="#n1" reg="person" xml:id="A87935-0070" facs="A87935-001-a-0050">Person</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0080" facs="A87935-001-a-0060">of</w>
          <c> </c>
          <w lemma="honour" ana="#n1" reg="honour" xml:id="A87935-0090" facs="A87935-001-a-0070">Honour</w>
          <pc xml:id="A87935-0100" facs="A87935-001-a-0080">,</pc>
          <c> </c>
          <w lemma="relate" ana="#vvg" reg="relating" xml:id="A87935-0110" facs="A87935-001-a-0090">relating</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0120" facs="A87935-001-a-0100">the</w>
          <c> </c>
          <w lemma="slaughter" ana="#n1" reg="slaughter" xml:id="A87935-0130" facs="A87935-001-a-0110">slaughter</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0140" facs="A87935-001-a-0120">of</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87935-0150" facs="A87935-001-a-0130">a</w>
          <c> </c>
          <w lemma="party" ana="#n1" reg="party" xml:id="A87935-0160" facs="A87935-001-a-0140">Party</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0170" facs="A87935-001-a-0150">of</w>
          <c> </c>
          <w lemma="300" ana="#crd" reg="300" xml:id="A87935-0180" facs="A87935-001-a-0160">300</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A87935-0190" facs="A87935-001-a-0170">Horse</w>
          <pc xml:id="A87935-0200" facs="A87935-001-a-0180">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A87935-0210" facs="A87935-001-a-0190">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0220" facs="A87935-001-a-0200">the</w>
          <c> </c>
          <w lemma="force" ana="#n2" reg="forces" xml:id="A87935-0230" facs="A87935-001-a-0210">Forces</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A87935-0240" facs="A87935-001-a-0220">under</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0250" facs="A87935-001-a-0230">the</w>
          <c> </c>
          <w lemma="command" ana="#n1" reg="command" xml:id="A87935-0260" facs="A87935-001-a-0240">command</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0270" facs="A87935-001-a-0250">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0280" facs="A87935-001-a-0260">the</w>
          <c> </c>
          <w lemma="earl" ana="#n1" reg="Earl" xml:id="A87935-0290" facs="A87935-001-a-0270">Earle</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0300" facs="A87935-001-a-0280">of</w>
          <c> </c>
          <hi xml:id="A87935e-50">
            <w lemma="Holland" ana="#n1-nn" reg="Holland" xml:id="A87935-0310" facs="A87935-001-a-0290">Holland</w>
            <pc xml:id="A87935-0320" facs="A87935-001-a-0300">,</pc>
          </hi>
          <date xml:id="A87935e-60">
            <hi xml:id="A87935e-70">
              <w lemma="July" ana="#n1-nn" reg="July" xml:id="A87935-0340" facs="A87935-001-a-0310">July</w>
              <c> </c>
              <w lemma="7." ana="#crd" reg="7." xml:id="A87935-0350" facs="A87935-001-a-0320">7.</w>
              <c> </c>
              <w lemma="1648." ana="#crd" reg="1648." xml:id="A87935-0360" facs="A87935-001-a-0330">1648.</w>
              <pc unit="sentence" xml:id="A87935-0360-eos" facs="A87935-001-a-0340"/>
              <c> </c>
            </hi>
          </date>
        </head>
        <opener xml:id="A87935e-80">
          <salute xml:id="A87935e-90">
            <w lemma="sir" ana="#n1" reg="Sir" xml:id="A87935-0400" facs="A87935-001-a-0350">SIR</w>
            <pc xml:id="A87935-0410" facs="A87935-001-a-0360">,</pc>
          </salute>
        </opener>
        <p xml:id="A87935e-100">
          <w lemma="to" ana="#acp-cs" reg="To" rend="initialcharacterdecorated" xml:id="A87935-0440" facs="A87935-001-a-0370">TO</w>
          <c> </c>
          <w lemma="prevent" ana="#vvi" reg="prevent" xml:id="A87935-0450" facs="A87935-001-a-0380">prevent</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0460" facs="A87935-001-a-0390">the</w>
          <c> </c>
          <w lemma="usual" ana="#j" reg="usual" xml:id="A87935-0470" facs="A87935-001-a-0400">usuall</w>
          <c> </c>
          <w lemma="practice" ana="#n1" reg="practice" xml:id="A87935-0480" facs="A87935-001-a-0410">practice</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0490" facs="A87935-001-a-0420">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0500" facs="A87935-001-a-0430">the</w>
          <c> </c>
          <w lemma="adverse" ana="#j" reg="adverse" xml:id="A87935-0510" facs="A87935-001-a-0440">adverse</w>
          <c> </c>
          <w lemma="party" ana="#n1" reg="party" xml:id="A87935-0520" facs="A87935-001-a-0450">Party</w>
          <pc xml:id="A87935-0530" facs="A87935-001-a-0460">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87935-0540" facs="A87935-001-a-0470">to</w>
          <c> </c>
          <w lemma="belie" ana="#vvi" reg="belie" xml:id="A87935-0550" facs="A87935-001-a-0480">belie</w>
          <c> </c>
          <w lemma="we" ana="#pno" reg="us" xml:id="A87935-0560" facs="A87935-001-a-0490">us</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A87935-0570" facs="A87935-001-a-0500">out</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0580" facs="A87935-001-a-0510">of</w>
          <c> </c>
          <w lemma="victory" ana="#n2" reg="victories" xml:id="A87935-0590" facs="A87935-001-a-0520">Victories</w>
          <pc xml:id="A87935-0600" facs="A87935-001-a-0530">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87935-0610" facs="A87935-001-a-0540">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87935-0620" facs="A87935-001-a-0550">to</w>
          <c> </c>
          <w lemma="encourage" ana="#vvi" reg="encourage" xml:id="A87935-0630" facs="A87935-001-a-0560">encourage</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-0640" facs="A87935-001-a-0570">their</w>
          <c> </c>
          <w lemma="welaffected-one" ana="#n2" reg="welaffected-ones" xml:id="A87935-0650" facs="A87935-001-a-0580">welaffected-ones</w>
          <pc xml:id="A87935-0660" facs="A87935-001-a-0590">,</pc>
          <c> </c>
          <w lemma="who" ana="#crq-r" reg="who" xml:id="A87935-0670" facs="A87935-001-a-0600">who</w>
          <c> </c>
          <w lemma="follow" ana="#vvb" reg="follow" xml:id="A87935-0680" facs="A87935-001-a-0610">follow</w>
          <c> </c>
          <w lemma="only" ana="#j" reg="only" xml:id="A87935-0690" facs="A87935-001-a-0620">onely</w>
          <c> </c>
          <w lemma="power" ana="#n1" reg="power" xml:id="A87935-0700" facs="A87935-001-a-0630">Power</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87935-0710" facs="A87935-001-a-0640">and</w>
          <c> </c>
          <w lemma="success" ana="#n1" reg="success" xml:id="A87935-0720" facs="A87935-001-a-0650">Successe</w>
          <pc xml:id="A87935-0730" facs="A87935-001-a-0660">,</pc>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A87935-0740" facs="A87935-001-a-0670">not</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0750" facs="A87935-001-a-0680">the</w>
          <c> </c>
          <w lemma="justice" ana="#n1" reg="justice" xml:id="A87935-0760" facs="A87935-001-a-0690">Justice</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-0770" facs="A87935-001-a-0700">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-0780" facs="A87935-001-a-0710">the</w>
          <c> </c>
          <w lemma="cause" ana="#n1" reg="cause" xml:id="A87935-0790" facs="A87935-001-a-0720">Cause</w>
          <pc xml:id="A87935-0800" facs="A87935-001-a-0730">;</pc>
          <c> </c>
          <pc xml:id="A87935-0810" facs="A87935-001-a-0740">(</pc>
          <w lemma="for" ana="#acp-cs" reg="For" xml:id="A87935-0820" facs="A87935-001-a-0750">for</w>
          <c> </c>
          <w lemma="i" ana="#pns" reg="i" xml:id="A87935-0830" facs="A87935-001-a-0760">I</w>
          <c> </c>
          <w lemma="can" ana="#vmb-x" reg="cannot" xml:id="A87935-0840" facs="A87935-001-a-0770">cannot</w>
          <c> </c>
          <w lemma="believe" ana="#vvi" reg="believe" xml:id="A87935-0850" facs="A87935-001-a-0780">believe</w>
          <pc xml:id="A87935-0860" facs="A87935-001-a-0790">,</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A87935-0870" facs="A87935-001-a-0800">they</w>
          <c> </c>
          <w lemma="can" ana="#vmb" reg="can" xml:id="A87935-0880" facs="A87935-001-a-0810">can</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A87935-0890" facs="A87935-001-a-0820">by</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A87935-0900" facs="A87935-001-a-0830">these</w>
          <c> </c>
          <w lemma="art" ana="#n2" reg="arts" xml:id="A87935-0910" facs="A87935-001-a-0840">Arts</w>
          <pc xml:id="A87935-0920" facs="A87935-001-a-0850">,</pc>
          <c> </c>
          <w lemma="discourage" ana="#vvi" reg="discourage" xml:id="A87935-0930" facs="A87935-001-a-0860">discourage</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A87935-0940" facs="A87935-001-a-0870">those</w>
          <c> </c>
          <w lemma="who" ana="#crq-r" reg="who" xml:id="A87935-0950" facs="A87935-001-a-0880">who</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A87935-0960" facs="A87935-001-a-0890">are</w>
          <c> </c>
          <w lemma="resolve" ana="#vvn" reg="resolved" xml:id="A87935-0970" facs="A87935-001-a-0900">resolved</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87935-0980" facs="A87935-001-a-0910">to</w>
          <c> </c>
          <w lemma="join" ana="#vvi" reg="join" xml:id="A87935-0990" facs="A87935-001-a-0920">joyne</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A87935-1000" facs="A87935-001-a-0930">in</w>
          <c> </c>
          <w lemma="vindicate" ana="#vvg" reg="vindicating" xml:id="A87935-1010" facs="A87935-001-a-0940">vindicating</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-1020" facs="A87935-001-a-0950">their</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A87935-1030" facs="A87935-001-a-0960">King</w>
          <pc xml:id="A87935-1040" facs="A87935-001-a-0970">,</pc>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-1050" facs="A87935-001-a-0980">their</w>
          <c> </c>
          <w lemma="religion" ana="#n1" reg="religion" xml:id="A87935-1060" facs="A87935-001-a-0990">Religion</w>
          <pc xml:id="A87935-1070" facs="A87935-001-a-1000">,</pc>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-1080" facs="A87935-001-a-1010">their</w>
          <c> </c>
          <w lemma="law" ana="#n2" reg="laws" xml:id="A87935-1090" facs="A87935-001-a-1020">Laws</w>
          <c> </c>
          <pc xml:id="A87935-1100" facs="A87935-001-a-1030">&amp;</pc>
          <c> </c>
          <w lemma="liberty" ana="#n2" reg="liberties" xml:id="A87935-1110" facs="A87935-001-a-1040">Liberties</w>
          <pc xml:id="A87935-1120" facs="A87935-001-a-1050">)</pc>
          <c> </c>
          <w lemma="i" ana="#pns" reg="i" xml:id="A87935-1130" facs="A87935-001-a-1060">I</w>
          <c> </c>
          <w lemma="send" ana="#vvb" reg="send" xml:id="A87935-1140" facs="A87935-001-a-1070">send</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87935-1150" facs="A87935-001-a-1080">you</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A87935-1160" facs="A87935-001-a-1090">this</w>
          <c> </c>
          <w lemma="brief" ana="#j" reg="brief" xml:id="A87935-1170" facs="A87935-001-a-1100">brief</w>
          <c> </c>
          <w lemma="information" ana="#n1" reg="information" xml:id="A87935-1180" facs="A87935-001-a-1110">information</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-1190" facs="A87935-001-a-1120">of</w>
          <c> </c>
          <w lemma="what" ana="#crq-r" reg="what" xml:id="A87935-1200" facs="A87935-001-a-1130">what</w>
          <c> </c>
          <w lemma="happen" ana="#vvd" reg="happened" xml:id="A87935-1210" facs="A87935-001-a-1140">hapned</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-1220" facs="A87935-001-a-1150">the</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A87935-1230" facs="A87935-001-a-1160">last</w>
          <c> </c>
          <w lemma="night" ana="#n1" reg="night" xml:id="A87935-1240" facs="A87935-001-a-1170">night</w>
          <pc xml:id="A87935-1250" facs="A87935-001-a-1180">:</pc>
          <c> </c>
          <w lemma="after" ana="#acp-p" reg="after" xml:id="A87935-1260" facs="A87935-001-a-1190">After</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87935-1270" facs="A87935-001-a-1200">a</w>
          <c> </c>
          <w lemma="short" ana="#j" reg="short" xml:id="A87935-1280" facs="A87935-001-a-1210">short</w>
          <c> </c>
          <w lemma="skirmish" ana="#n1" reg="skirmish" xml:id="A87935-1290" facs="A87935-001-a-1220">Skirmish</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-1300" facs="A87935-001-a-1230">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A87935-1310" facs="A87935-001-a-1240">our</w>
          <c> </c>
          <w lemma="scout" ana="#n2" reg="scouts" xml:id="A87935-1320" facs="A87935-001-a-1250">Scouts</w>
          <pc xml:id="A87935-1330" facs="A87935-001-a-1260">,</pc>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A87935-1340" facs="A87935-001-a-1270">we</w>
          <c> </c>
          <w lemma="understand" ana="#vvd" reg="understood" xml:id="A87935-1350" facs="A87935-001-a-1280">understood</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A87935-1360" facs="A87935-001-a-1290">by</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A87935-1370" facs="A87935-001-a-1300">them</w>
          <pc xml:id="A87935-1380" facs="A87935-001-a-1310">,</pc>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87935-1390" facs="A87935-001-a-1320">a</w>
          <c> </c>
          <w lemma="party" ana="#n1" reg="party" xml:id="A87935-1400" facs="A87935-001-a-1330">Party</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-1410" facs="A87935-001-a-1340">of</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A87935-1420" facs="A87935-001-a-1350">Horse</w>
          <c> </c>
          <w lemma="be" ana="#vvd" reg="were" xml:id="A87935-1430" facs="A87935-001-a-1360">were</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A87935-1440" facs="A87935-001-a-1370">not</w>
          <c> </c>
          <w lemma="far" ana="#av_j" reg="far" xml:id="A87935-1450" facs="A87935-001-a-1380">far</w>
          <c> </c>
          <w lemma="off" ana="#acp-av" reg="off" xml:id="A87935-1460" facs="A87935-001-a-1390">off</w>
          <pc xml:id="A87935-1470" facs="A87935-001-a-1400">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87935-1480" facs="A87935-001-a-1410">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-1490" facs="A87935-001-a-1420">the</w>
          <c> </c>
          <w lemma="better" ana="#j-c" reg="better" xml:id="A87935-1500" facs="A87935-001-a-1430">better</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87935-1510" facs="A87935-001-a-1440">to</w>
          <c> </c>
          <w lemma="entertain" ana="#vvi" reg="entertain" xml:id="A87935-1520" facs="A87935-001-a-1450">entertain</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A87935-1530" facs="A87935-001-a-1460">them</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A87935-1540" facs="A87935-001-a-1470">we</w>
          <c> </c>
          <w lemma="line" ana="#vvn" reg="lined" xml:id="A87935-1550" facs="A87935-001-a-1480">lined</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-1560" facs="A87935-001-a-1490">the</w>
          <c> </c>
          <w lemma="hedge" ana="#n2" reg="hedges" xml:id="A87935-1570" facs="A87935-001-a-1500">Hedges</w>
          <c> </c>
          <pc xml:id="A87935-1580" facs="A87935-001-a-1510">(</pc>
          <w lemma="my" ana="#po" reg="My" xml:id="A87935-1590" facs="A87935-001-a-1520">my</w>
          <c> </c>
          <w lemma="haste" ana="#n1" reg="haste" xml:id="A87935-1600" facs="A87935-001-a-1530">haste</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A87935-1610" facs="A87935-001-a-1540">will</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A87935-1620" facs="A87935-001-a-1550">not</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A87935-1630" facs="A87935-001-a-1560">give</w>
          <c> </c>
          <w lemma="i" ana="#pno" reg="me" xml:id="A87935-1640" facs="A87935-001-a-1570">me</w>
          <c> </c>
          <w lemma="leave" ana="#vvi" reg="leave" xml:id="A87935-1650" facs="A87935-001-a-1580">leave</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87935-1660" facs="A87935-001-a-1590">to</w>
          <c> </c>
          <w lemma="write" ana="#vvi" reg="write" xml:id="A87935-1670" facs="A87935-001-a-1600">write</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87935-1680" facs="A87935-001-a-1610">you</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87935-1690" facs="A87935-001-a-1620">a</w>
          <c> </c>
          <w lemma="relation" ana="#n1" reg="relation" xml:id="A87935-1700" facs="A87935-001-a-1630">Relation</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A87935-1710" facs="A87935-001-a-1640">at</w>
          <c> </c>
          <w lemma="large" ana="#j" reg="large" xml:id="A87935-1720" facs="A87935-001-a-1650">large</w>
          <pc xml:id="A87935-1730" facs="A87935-001-a-1660">)</pc>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A87935-1740" facs="A87935-001-a-1670">we</w>
          <c> </c>
          <w lemma="lose" ana="#vvd" reg="lost" xml:id="A87935-1750" facs="A87935-001-a-1680">lost</w>
          <c> </c>
          <w lemma="about" ana="#acp-av" reg="about" xml:id="A87935-1760" facs="A87935-001-a-1690">about</w>
          <c> </c>
          <w lemma="10" ana="#crd" reg="10" xml:id="A87935-1770" facs="A87935-001-a-1700">10</w>
          <c> </c>
          <w lemma="man" ana="#n2" reg="men" xml:id="A87935-1780" facs="A87935-001-a-1710">men</w>
          <pc xml:id="A87935-1790" facs="A87935-001-a-1720">,</pc>
          <c> </c>
          <w lemma="but" ana="#acp-cc" reg="but" xml:id="A87935-1800" facs="A87935-001-a-1730">but</w>
          <c> </c>
          <w lemma="ample" ana="#av_j" reg="amply" xml:id="A87935-1810" facs="A87935-001-a-1740">amply</w>
          <c> </c>
          <w lemma="revenge" ana="#vvn" reg="revenged" xml:id="A87935-1820" facs="A87935-001-a-1750">revenged</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-1830" facs="A87935-001-a-1760">their</w>
          <c> </c>
          <w lemma="death" ana="#n2" reg="deaths" xml:id="A87935-1840" facs="A87935-001-a-1770">deaths</w>
          <pc xml:id="A87935-1850" facs="A87935-001-a-1780">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A87935-1860" facs="A87935-001-a-1790">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-1870" facs="A87935-001-a-1800">the</w>
          <c> </c>
          <w lemma="slaughter" ana="#n1" reg="slaughter" xml:id="A87935-1880" facs="A87935-001-a-1810">slaughter</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-1890" facs="A87935-001-a-1820">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-1900" facs="A87935-001-a-1830">the</w>
          <c> </c>
          <w lemma="great" ana="#j-s" reg="greatest" xml:id="A87935-1910" facs="A87935-001-a-1840">greatest</w>
          <c> </c>
          <w lemma="part" ana="#n1" reg="part" xml:id="A87935-1920" facs="A87935-001-a-1850">part</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-1930" facs="A87935-001-a-1860">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A87935-1940" facs="A87935-001-a-1870">their</w>
          <c> </c>
          <w lemma="300" ana="#crd" reg="300" xml:id="A87935-1950" facs="A87935-001-a-1880">300</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A87935-1960" facs="A87935-001-a-1890">Horse</w>
          <pc unit="sentence" xml:id="A87935-1970" facs="A87935-001-a-1900">.</pc>
          <c> </c>
          <w lemma="my" ana="#po" reg="My" xml:id="A87935-1980" facs="A87935-001-a-1910">My</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A87935-1990" facs="A87935-001-a-1920">Lord</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A87935-2000" facs="A87935-001-a-1930">now</w>
          <c> </c>
          <w lemma="march" ana="#vvz" reg="Marcheth" xml:id="A87935-2010" facs="A87935-001-a-1940">marcheth</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A87935-2020" facs="A87935-001-a-1950">with</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A87935-2030" facs="A87935-001-a-1960">his</w>
          <c> </c>
          <w lemma="full" ana="#j" reg="full" xml:id="A87935-2040" facs="A87935-001-a-1970">full</w>
          <c> </c>
          <w lemma="body" ana="#n1" reg="body" xml:id="A87935-2050" facs="A87935-001-a-1980">body</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87935-2060" facs="A87935-001-a-1990">of</w>
          <c> </c>
          <w lemma="man" ana="#n2" reg="men" xml:id="A87935-2070" facs="A87935-001-a-2000">Men</w>
          <pc xml:id="A87935-2080" facs="A87935-001-a-2010">:</pc>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A87935-2090" facs="A87935-001-a-2020">this</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87935-2100" facs="A87935-001-a-2030">you</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A87935-2110" facs="A87935-001-a-2040">may</w>
          <c> </c>
          <w lemma="affirm" ana="#vvi" reg="affirm" xml:id="A87935-2120" facs="A87935-001-a-2050">affirm</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A87935-2130" facs="A87935-001-a-2060">to</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A87935-2140" facs="A87935-001-a-2070">your</w>
          <c> </c>
          <w lemma="friend" ana="#n2" reg="friends" xml:id="A87935-2150" facs="A87935-001-a-2080">friends</w>
          <pc xml:id="A87935-2160" facs="A87935-001-a-2090">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A87935-2170" facs="A87935-001-a-2100">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A87935-2180" facs="A87935-001-a-2110">any</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A87935-2190" facs="A87935-001-a-2120">one</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87935-2200" facs="A87935-001-a-2130">you</w>
          <c> </c>
          <w lemma="meet" ana="#vvb" reg="meet" xml:id="A87935-2210" facs="A87935-001-a-2140">meet</w>
          <c> </c>
          <w lemma="withal" ana="#av" reg="withal" xml:id="A87935-2220" facs="A87935-001-a-2150">withall</w>
          <pc xml:id="A87935-2230" facs="A87935-001-a-2160">,</pc>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A87935-2240" facs="A87935-001-a-2170">from</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A87935-2250" facs="A87935-001-a-2180">your</w>
          <c> </c>
          <pc xml:id="A87935-2260" facs="A87935-001-a-2190">—</pc>
        </p>
        <closer xml:id="A87935e-110">
          <dateline xml:id="A87935e-120">
            <date xml:id="A87935e-130">
              <w lemma="July" ana="#n1-nn" reg="July" xml:id="A87935-2310" facs="A87935-001-a-2200">JULY</w>
              <c> </c>
              <w lemma="8." ana="#crd" reg="8." xml:id="A87935-2320" facs="A87935-001-a-2210">8.</w>
              <pc unit="sentence" xml:id="A87935-2320-eos" facs="A87935-001-a-2220"/>
            </date>
            <w lemma="from" ana="#acp-p" reg="from" xml:id="A87935-2340" facs="A87935-001-a-2230">From</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A87935-2350" facs="A87935-001-a-2240">the</w>
            <c> </c>
            <w lemma="headquarters" ana="#n2" reg="headquarters" xml:id="A87935-2360" facs="A87935-001-a-2250">Head-quarters</w>
            <c> </c>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A87935-2370" facs="A87935-001-a-2260">at</w>
            <c> </c>
            <w lemma="hounslow-heath" ana="#n1" reg="hounslow-heath" xml:id="A87935-2380" facs="A87935-001-a-2270">Hounslow-heath</w>
            <pc unit="sentence" xml:id="A87935-2390" facs="A87935-001-a-2280">.</pc>
          </dateline>
        </closer>
      </div>
    </body>
    <back xml:id="A87935e-140">
      <div type="colophon" xml:id="A87935e-150">
        <p xml:id="A87935e-160">
          <w lemma="LONDON" ana="#n1-nn" reg="London" xml:id="A87935-2440" facs="A87935-001-a-2290">LONDON</w>
          <pc xml:id="A87935-2450" facs="A87935-001-a-2300">,</pc>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A87935-2460" facs="A87935-001-a-2310">Printed</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A87935-2470" facs="A87935-001-a-2320">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87935-2480" facs="A87935-001-a-2330">the</w>
          <c> </c>
          <w lemma="year" ana="#n1" reg="year" xml:id="A87935-2490" facs="A87935-001-a-2340">Year</w>
          <c> </c>
          <w lemma="1648." ana="#crd" reg="1648." xml:id="A87935-2500" facs="A87935-001-a-2350">1648.</w>
          <pc unit="sentence" xml:id="A87935-2500-eos" facs="A87935-001-a-2360"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>