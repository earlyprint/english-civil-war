<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>John Lilburne. Anagram. O! J burn in hell.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1653</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A87588</idno>
        <idno type="STC">Wing J763</idno>
        <idno type="STC">Thomason E703_21</idno>
        <idno type="STC">ESTC R207209</idno>
        <idno type="EEBO-CITATION">99866278</idno>
        <idno type="PROQUEST">99866278</idno>
        <idno type="VID">118545</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A87588)</note>
        <note>Transcribed from: (Early English Books Online ; image set 118545)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 108:E703[21])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>John Lilburne. Anagram. O! J burn in hell.</title>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1653]</date>
          </publicationStmt>
          <notesStmt>
            <note>An acrostic on John Lilburne's name.</note>
            <note>Imprint from Wing.</note>
            <note>Annotation on Thomason copy: "July. 1658".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Lilburne, John, 1614?-1657 -- Poetry -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A87588e-10">
    <body xml:id="A87588e-20">
      <div type="acrostic_poem" xml:id="A87588e-30">
        <pb facs="tcp:118545:1" rendition="simple:additions" xml:id="A87588-001-a"/>
        <head xml:id="A87588e-40">
          <w lemma="JOHN" ana="#n1-nn" reg="John" xml:id="A87588-0030" facs="A87588-001-a-0010">IOHN</w>
          <c> </c>
          <w lemma="Lilburne" ana="#n1-nn" reg="Lilburne" xml:id="A87588-0040" facs="A87588-001-a-0020">LILBVRNE</w>
          <pc unit="sentence" xml:id="A87588-0050" facs="A87588-001-a-0030">.</pc>
        </head>
        <head type="sub" xml:id="A87588e-50">
          <w lemma="anagram" ana="#n1" reg="Anagram" xml:id="A87588-0080" facs="A87588-001-a-0040">Anagram</w>
          <pc unit="sentence" xml:id="A87588-0090" facs="A87588-001-a-0050">.</pc>
          <c> </c>
          <lb xml:id="A87588e-60"/>
          <hi xml:id="A87588e-70">
            <w lemma="o" ana="#uh" reg="o!" xml:id="A87588-0100" facs="A87588-001-a-0060">O!</w>
            <c> </c>
            <w lemma="i" ana="#pns" reg="i" xml:id="A87588-0110" facs="A87588-001-a-0070">I</w>
            <c> </c>
            <w lemma="burn" ana="#vvb" reg="burn" xml:id="A87588-0120" facs="A87588-001-a-0080">burn</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A87588-0130" facs="A87588-001-a-0090">in</w>
            <c> </c>
            <w lemma="helL." ana="#n-ab" reg="hell." xml:id="A87588-0140" facs="A87588-001-a-0100">helL.</w>
            <pc unit="sentence" xml:id="A87588-0140-eos" facs="A87588-001-a-0110"/>
            <c> </c>
          </hi>
        </head>
        <lg xml:id="A87588e-80">
          <l xml:id="A87588e-90">
            <hi xml:id="A87588e-100">
              <w lemma="if" part="I" ana="#cs" reg="if" xml:id="A87588-0170.1" facs="A87588-001-a-0120" rendition="hi-1">IF</w>
            </hi>
            <c> </c>
            <w lemma="a" ana="#d" reg="a" xml:id="A87588-0180" facs="A87588-001-a-0140">a</w>
            <c> </c>
            <w lemma="bold" ana="#j" reg="bold" xml:id="A87588-0190" facs="A87588-001-a-0150">bold</w>
            <c> </c>
            <w lemma="traitor" ana="#n1" reg="traitor" xml:id="A87588-0200" facs="A87588-001-a-0160">Traytor</w>
            <c> </c>
            <pc xml:id="A87588-0210" facs="A87588-001-a-0170">(</pc>
            <c> </c>
            <w lemma="against" ana="#acp-p" reg="'gainst" xml:id="A87588-0230" facs="A87588-001-a-0190">'gainst</w>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A87588-0240" facs="A87588-001-a-0200">his</w>
            <c> </c>
            <w lemma="god" ana="#n1" reg="god" xml:id="A87588-0250" facs="A87588-001-a-0210">God</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A87588-0260" facs="A87588-001-a-0220">and</w>
            <c> </c>
            <w lemma="king" ana="#n1" reg="King" xml:id="A87588-0270" facs="A87588-001-a-0230">King</w>
            <pc xml:id="A87588-0280" facs="A87588-001-a-0240">)</pc>
          </l>
          <l xml:id="A87588e-110">
            <hi xml:id="A87588e-120">
              <w lemma="of" part="I" ana="#acp-p" reg="of" xml:id="A87588-0290.1" facs="A87588-001-a-0250" rendition="hi-1">OF</w>
            </hi>
            <c> </c>
            <w lemma="mercy" ana="#n1" reg="mercy" xml:id="A87588-0300" facs="A87588-001-a-0270">Mercy</w>
            <c> </c>
            <w lemma="may" ana="#vmb" reg="May" xml:id="A87588-0310" facs="A87588-001-a-0280">may</w>
            <c> </c>
            <w lemma="have" ana="#vvi" reg="have" xml:id="A87588-0320" facs="A87588-001-a-0290">have</w>
            <c> </c>
            <w lemma="share" ana="#n1" reg="share" xml:id="A87588-0330" facs="A87588-001-a-0300">share</w>
            <pc xml:id="A87588-0340" facs="A87588-001-a-0310">,</pc>
            <c> </c>
            <hi xml:id="A87588e-130">
              <w lemma="John" ana="#n1-nn" reg="John" xml:id="A87588-0350" facs="A87588-001-a-0320">John</w>
              <c> </c>
              <w lemma="lilburn" ana="#n1-nn" reg="Lilburn" xml:id="A87588-0360" facs="A87588-001-a-0330">Lilburn</w>
            </hi>
            <c> </c>
            <w lemma="bring" ana="#vvi" reg="bring" xml:id="A87588-0370" facs="A87588-001-a-0340">bring</w>
            <pc xml:id="A87588-0380" facs="A87588-001-a-0350">:</pc>
          </l>
          <l xml:id="A87588e-140">
            <hi xml:id="A87588e-150">
              <w lemma="he" part="I" ana="#pns" reg="he" xml:id="A87588-0390.1" facs="A87588-001-a-0360" rendition="hi-1">HE</w>
            </hi>
            <c> </c>
            <w lemma="kick" ana="#vvz" reg="kicks" xml:id="A87588-0400" facs="A87588-001-a-0380">kicks</w>
            <c> </c>
            <w lemma="against" ana="#acp-p" reg="'gainst" xml:id="A87588-0420" facs="A87588-001-a-0400">'gainst</w>
            <c> </c>
            <w lemma="king" ana="#n1" reg="King" xml:id="A87588-0430" facs="A87588-001-a-0410">King</w>
            <pc xml:id="A87588-0440" facs="A87588-001-a-0420">,</pc>
            <c> </c>
            <w lemma="priest" ana="#n1" reg="priest" xml:id="A87588-0450" facs="A87588-001-a-0430">Priest</w>
            <pc xml:id="A87588-0460" facs="A87588-001-a-0440">,</pc>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A87588-0470" facs="A87588-001-a-0450">and</w>
            <c> </c>
            <w lemma="prophet" ana="#n1" reg="prophet" xml:id="A87588-0480" facs="A87588-001-a-0460">Prophet</w>
            <c> </c>
            <w lemma="too" ana="#av" reg="too" xml:id="A87588-0490" facs="A87588-001-a-0470">too</w>
            <pc xml:id="A87588-0500" facs="A87588-001-a-0480">,</pc>
          </l>
          <l xml:id="A87588e-160">
            <hi xml:id="A87588e-170">
              <w lemma="no" part="I" ana="#d-x" reg="no" xml:id="A87588-0510.1" facs="A87588-001-a-0490" rendition="hi-1">NO</w>
            </hi>
            <c> </c>
            <w lemma="mischief" ana="#n1" reg="mischief" xml:id="A87588-0520" facs="A87588-001-a-0510">mischief</w>
            <c> </c>
            <w lemma="under" ana="#acp-p" reg="under" xml:id="A87588-0530" facs="A87588-001-a-0520">under</w>
            <c> </c>
            <w lemma="heaven" ana="#n1" reg="heaven" xml:id="A87588-0540" facs="A87588-001-a-0530">Heaven</w>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A87588-0550" facs="A87588-001-a-0540">that</w>
            <c> </c>
            <w lemma="he|will" ana="#pns|vmb" reg="he'll" xml:id="A87588-0560" facs="A87588-001-a-0550">he'll</w>
            <c> </c>
            <w lemma="not" ana="#xx" reg="not" xml:id="A87588-0570" facs="A87588-001-a-0560">not</w>
            <c> </c>
            <w lemma="do" ana="#vvi" reg="do" xml:id="A87588-0580" facs="A87588-001-a-0570">do</w>
            <pc unit="sentence" xml:id="A87588-0590" facs="A87588-001-a-0580">.</pc>
            <c> </c>
          </l>
        </lg>
        <lg xml:id="A87588e-180">
          <l xml:id="A87588e-190">
            <hi xml:id="A87588e-200">
              <w lemma="law|have" part="I" ana="#n1|vvz" reg="law's" xml:id="A87588-0620.1" facs="A87588-001-a-0590" rendition="hi-1">LAws</w>
            </hi>
            <c> </c>
            <w lemma="sacred" ana="#j" reg="sacred" xml:id="A87588-0630" facs="A87588-001-a-0610">Sacred</w>
            <pc xml:id="A87588-0640" facs="A87588-001-a-0620">,</pc>
            <c> </c>
            <w lemma="national" ana="#j" reg="national" xml:id="A87588-0650" facs="A87588-001-a-0630">National</w>
            <pc xml:id="A87588-0660" facs="A87588-001-a-0640">,</pc>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A87588-0670" facs="A87588-001-a-0650">and</w>
            <c> </c>
            <w lemma="most" ana="#av-s_d" reg="most" xml:id="A87588-0680" facs="A87588-001-a-0660">most</w>
            <c> </c>
            <w lemma="human" ana="#j" reg="human" xml:id="A87588-0690" facs="A87588-001-a-0670">Humane</w>
            <pc xml:id="A87588-0700" facs="A87588-001-a-0680">,</pc>
          </l>
          <l xml:id="A87588e-210">
            <hi xml:id="A87588e-220">
              <w lemma="illegal" part="I" ana="#j" reg="illegal" xml:id="A87588-0710.1" facs="A87588-001-a-0690" rendition="hi-1">ILlegal</w>
            </hi>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="are" xml:id="A87588-0720" facs="A87588-001-a-0710">are</w>
            <pc xml:id="A87588-0730" facs="A87588-001-a-0720">,</pc>
            <c> </c>
            <w lemma="if" ana="#cs" reg="if" xml:id="A87588-0740" facs="A87588-001-a-0730">if</w>
            <c> </c>
            <w lemma="Jack" ana="#n1-nn" reg="Jack" xml:id="A87588-0750" facs="A87588-001-a-0740">Jack</w>
            <c> </c>
            <pc xml:id="A87588-0760" facs="A87588-001-a-0750">(</pc>
            <w lemma="the" ana="#d" reg="The" xml:id="A87588-0770" facs="A87588-001-a-0760">the</w>
            <c> </c>
            <hi xml:id="A87588e-230">
              <w lemma="Jew" ana="#n1-nn" reg="Jew" xml:id="A87588-0780" facs="A87588-001-a-0770">Jew</w>
            </hi>
            <pc xml:id="A87588-0790" facs="A87588-001-a-0780">)</pc>
            <c> </c>
            <w lemma="complain" ana="#vvb" reg="complain" xml:id="A87588-0800" facs="A87588-001-a-0790">complain</w>
            <pc unit="sentence" xml:id="A87588-0810" facs="A87588-001-a-0800">.</pc>
            <c> </c>
          </l>
          <l xml:id="A87588e-240">
            <hi xml:id="A87588e-250">
              <w lemma="lend" part="I" ana="#vvb" reg="lend" xml:id="A87588-0820.1" facs="A87588-001-a-0810" rendition="hi-1">LEnd</w>
            </hi>
            <c> </c>
            <w lemma="i" ana="#pno" reg="me" xml:id="A87588-0830" facs="A87588-001-a-0830">me</w>
            <c> </c>
            <w lemma="your" ana="#po" reg="your" xml:id="A87588-0840" facs="A87588-001-a-0840">your</w>
            <c> </c>
            <w lemma="aid" ana="#n1" reg="aid" xml:id="A87588-0850" facs="A87588-001-a-0850">aid</w>
            <pc xml:id="A87588-0860" facs="A87588-001-a-0860">,</pc>
            <c> </c>
            <w lemma="you" ana="#pn" reg="you" xml:id="A87588-0870" facs="A87588-001-a-0870">you</w>
            <c> </c>
            <w lemma="limbners" ana="#n1g-nn" reg="Limbners'" xml:id="A87588-0880" facs="A87588-001-a-0880">Limbners</w>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A87588-0890" facs="A87588-001-a-0890">that</w>
            <c> </c>
            <w lemma="can" ana="#vmb" reg="can" xml:id="A87588-0900" facs="A87588-001-a-0900">can</w>
            <c> </c>
            <w lemma="paint" ana="#vvi" reg="paint" xml:id="A87588-0910" facs="A87588-001-a-0910">paint</w>
          </l>
          <l xml:id="A87588e-260">
            <hi xml:id="A87588e-270">
              <w lemma="britain" part="I" ana="#n1g-nn" reg="Britain's" xml:id="A87588-0920.1" facs="A87588-001-a-0920" rendition="hi-1">BRitain's</w>
            </hi>
            <c> </c>
            <w lemma="white" ana="#j" reg="white" xml:id="A87588-0930" facs="A87588-001-a-0940">white</w>
            <c> </c>
            <w lemma="devil" ana="#n2" reg="devils" xml:id="A87588-0940" facs="A87588-001-a-0950">Devils</w>
            <pc xml:id="A87588-0950" facs="A87588-001-a-0960">,</pc>
            <c> </c>
            <w lemma="or" ana="#cc" reg="or" xml:id="A87588-0960" facs="A87588-001-a-0970">or</w>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A87588-0970" facs="A87588-001-a-0980">his</w>
            <c> </c>
            <w lemma="black" ana="#j" reg="black" xml:id="A87588-0980" facs="A87588-001-a-0990">black</w>
            <c> </c>
            <w lemma="grim" ana="#j" reg="grim" xml:id="A87588-0990" facs="A87588-001-a-1000">grim</w>
            <c> </c>
            <w lemma="saint" ana="#n1" reg="saint" xml:id="A87588-1000" facs="A87588-001-a-1010">Saint</w>
            <pc unit="sentence" xml:id="A87588-1010" facs="A87588-001-a-1020">.</pc>
            <c> </c>
          </l>
          <l xml:id="A87588e-280">
            <hi xml:id="A87588e-290">
              <w lemma="VAne" part="I" ana="#n1-nn" reg="Vane" xml:id="A87588-1020.1" facs="A87588-001-a-1030" rendition="hi-1">VAne</w>
            </hi>
            <pc xml:id="A87588-1030" facs="A87588-001-a-1050">,</pc>
            <c> </c>
            <w lemma="Mildmay" ana="#n1-nn" reg="Mildmay" xml:id="A87588-1040" facs="A87588-001-a-1060">Mildmay</w>
            <pc xml:id="A87588-1050" facs="A87588-001-a-1070">,</pc>
            <c> </c>
            <w lemma="Bradshaw" ana="#n1-nn" reg="Bradshaw" xml:id="A87588-1060" facs="A87588-001-a-1080">Bradshaw</w>
            <pc xml:id="A87588-1070" facs="A87588-001-a-1090">,</pc>
            <c> </c>
            <w lemma="Haslerig" ana="#n1-nn" reg="Haslerig" xml:id="A87588-1080" facs="A87588-001-a-1100">Haslerig</w>
            <pc xml:id="A87588-1090" facs="A87588-001-a-1110">,</pc>
            <c> </c>
            <hi xml:id="A87588e-300">
              <w lemma="and" ana="#cc" reg="and" xml:id="A87588-1100" facs="A87588-001-a-1120">and</w>
            </hi>
            <c> </c>
            <w lemma="Pim" ana="#n1-nn" reg="Pim" xml:id="A87588-1110" facs="A87588-001-a-1130">Pim</w>
            <pc xml:id="A87588-1120" facs="A87588-001-a-1140">,</pc>
          </l>
          <l xml:id="A87588e-310">
            <hi xml:id="A87588e-320">
              <w lemma="rogue" part="I" ana="#n1g" reg="rogue's" xml:id="A87588-1130.1" facs="A87588-001-a-1150" rendition="hi-1">ROgues</w>
            </hi>
            <c> </c>
            <w lemma="most" ana="#av-s_d" reg="most" xml:id="A87588-1140" facs="A87588-001-a-1170">most</w>
            <c> </c>
            <w lemma="complea•" ana="#fw-la" reg="complea•" xml:id="A87588-1150" facs="A87588-001-a-1180">complea•</w>
            <pc xml:id="A87588-1160" facs="A87588-001-a-1190">,</pc>
            <c> </c>
            <w lemma="but" ana="#acp-cc" reg="but" xml:id="A87588-1170" facs="A87588-001-a-1200">but</w>
            <c> </c>
            <w lemma="puney" ana="#n2" reg="puneys" xml:id="A87588-1180" facs="A87588-001-a-1210">Puneys</w>
            <c> </c>
            <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A87588-1190" facs="A87588-001-a-1220">unto</w>
            <c> </c>
            <w lemma="he" ana="#pno" reg="him" xml:id="A87588-1200" facs="A87588-001-a-1230">him</w>
            <pc xml:id="A87588-1210" facs="A87588-001-a-1240">:</pc>
          </l>
          <l xml:id="A87588e-330">
            <hi xml:id="A87588e-340">
              <w lemma="none" part="I" ana="#pi-x" reg="none" xml:id="A87588-1220.1" facs="A87588-001-a-1250" rendition="hi-1">NOne</w>
            </hi>
            <c> </c>
            <w lemma="but" ana="#acp-cc" reg="but" xml:id="A87588-1230" facs="A87588-001-a-1270">but</w>
            <c> </c>
            <w lemma="himself" ana="#px" reg="himself" xml:id="A87588-1240" facs="A87588-001-a-1280">himself</w>
            <pc xml:id="A87588-1250" facs="A87588-001-a-1290">,</pc>
            <c> </c>
            <w lemma="himself" ana="#px" reg="himself" xml:id="A87588-1260" facs="A87588-001-a-1300">himself</w>
            <c> </c>
            <w lemma="can" ana="#vmb" reg="can" xml:id="A87588-1270" facs="A87588-001-a-1310">can</w>
            <c> </c>
            <w lemma="parallel" ana="#vvi" reg="parallel" xml:id="A87588-1280" facs="A87588-001-a-1320">parallel</w>
            <pc unit="sentence" xml:id="A87588-1290" facs="A87588-001-a-1330">.</pc>
            <c> </c>
          </l>
          <l xml:id="A87588e-350">
            <hi xml:id="A87588e-360">
              <w lemma="expect" part="I" ana="#vvg" reg="expecting" xml:id="A87588-1300.1" facs="A87588-001-a-1340" rendition="hi-1">EXpecting</w>
            </hi>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A87588-1310" facs="A87588-001-a-1360">this</w>
            <c> </c>
            <pc xml:id="A87588-1320" facs="A87588-001-a-1370">(</pc>
            <w lemma="then" ana="#av" reg="Then" xml:id="A87588-1330" facs="A87588-001-a-1380">then</w>
            <pc xml:id="A87588-1340" facs="A87588-001-a-1390">)</pc>
            <c> </c>
            <hi xml:id="A87588e-370">
              <w lemma="oh" ana="#uh" reg="oh" xml:id="A87588-1350" facs="A87588-001-a-1400">o</w>
              <pc unit="sentence" xml:id="A87588-1360" facs="A87588-001-a-1410">!</pc>
              <c> </c>
              <w lemma="i" ana="#pns" reg="I" xml:id="A87588-1370" facs="A87588-001-a-1420">I</w>
              <c> </c>
              <w lemma="burn" ana="#vvb" reg="burn" xml:id="A87588-1380" facs="A87588-001-a-1430">burn</w>
              <c> </c>
              <w lemma="in" ana="#acp-p" reg="in" xml:id="A87588-1390" facs="A87588-001-a-1440">in</w>
              <c> </c>
              <w lemma="helL." ana="#n-ab" reg="hell." xml:id="A87588-1400" facs="A87588-001-a-1450">helL.</w>
              <pc unit="sentence" xml:id="A87588-1400-eos" facs="A87588-001-a-1460"/>
              <c> </c>
            </hi>
          </l>
        </lg>
        <closer xml:id="A87588e-380">
          <dateline xml:id="A87588e-390">
            <date xml:id="A87588e-400">
              <add xml:id="A87588e-410">
                <w lemma="July" ana="#n1-nn" reg="July" xml:id="A87588-1450" facs="A87588-001-a-1470">July</w>
                <pc xml:id="A87588-1460" facs="A87588-001-a-1480">.</pc>
                <c> </c>
                <w lemma="1••8" ana="#n1" reg="1••8" xml:id="A87588-1470" facs="A87588-001-a-1490">1••8</w>
              </add>
            </date>
          </dateline>
        </closer>
      </div>
    </body>
    <back xml:id="A87588e-420">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>