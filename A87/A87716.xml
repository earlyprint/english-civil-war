<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>To Mr. Thomas Edwards. Sir you stand as one professing your selfe to be instructed by Christ with abilities from God, to throw downe errour, and therefore to that end doe preach every third day, may it therefore please you and those that imploy you in that worke, to give those leave whom you so brand, as bubliquely to object against what you say, when your sermon is ended, as you declare your selfe: and vve hope it will be an encrease of further light to all that feare God and put a large advantage into your hands if you have the trueth on your side, to cause it to shine with more clearnesse and I hope we shall doe it with moderation as becometh Christians. Yours, William Kiffin.</title>
        <author>Kiffin, William, 1616-1701.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1644</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A87716</idno>
        <idno type="STC">Wing K426</idno>
        <idno type="STC">Thomason E17_6</idno>
        <idno type="STC">ESTC R12008</idno>
        <idno type="EEBO-CITATION">99859187</idno>
        <idno type="PROQUEST">99859187</idno>
        <idno type="VID">111254</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A87716)</note>
        <note>Transcribed from: (Early English Books Online ; image set 111254)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 3:E17[6])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>To Mr. Thomas Edwards. Sir you stand as one professing your selfe to be instructed by Christ with abilities from God, to throw downe errour, and therefore to that end doe preach every third day, may it therefore please you and those that imploy you in that worke, to give those leave whom you so brand, as bubliquely to object against what you say, when your sermon is ended, as you declare your selfe: and vve hope it will be an encrease of further light to all that feare God and put a large advantage into your hands if you have the trueth on your side, to cause it to shine with more clearnesse and I hope we shall doe it with moderation as becometh Christians. Yours, William Kiffin.</title>
            <author>Kiffin, William, 1616-1701.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.],</publisher>
            <pubPlace>[London :</pubPlace>
            <date>Novemb. 15. 1644.</date>
          </publicationStmt>
          <notesStmt>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Edwards, Thomas, 1599-1647.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A87716e-10">
    <body xml:id="A87716e-20">
      <div type="text" xml:id="A87716e-30">
        <pb facs="tcp:111254:1" rendition="simple:additions" xml:id="A87716-001-a"/>
        <head xml:id="A87716e-40">
          <w lemma="to" ana="#acp-cs" reg="To" xml:id="A87716-0030" facs="A87716-001-a-0010">To</w>
          <c> </c>
          <w lemma="mr." ana="#n-ab" reg="Mr." xml:id="A87716-0040" facs="A87716-001-a-0020">Mr.</w>
          <c> </c>
          <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A87716-0050" facs="A87716-001-a-0030">Thomas</w>
          <c> </c>
          <w lemma="Edwards" ana="#n1-nn" reg="Edwards" xml:id="A87716-0060" facs="A87716-001-a-0040">Edwards</w>
          <pc unit="sentence" xml:id="A87716-0070" facs="A87716-001-a-0050">.</pc>
        </head>
        <opener xml:id="A87716e-50">
          <salute xml:id="A87716e-60">
            <w lemma="sir" ana="#n1" reg="Sir" xml:id="A87716-0110" facs="A87716-001-a-0060">Sir</w>
          </salute>
        </opener>
        <p xml:id="A87716e-70">
          <w lemma="you" ana="#pn" reg="You" xml:id="A87716-0140" facs="A87716-001-a-0070">You</w>
          <c> </c>
          <w lemma="stand" ana="#vvb" reg="stand" xml:id="A87716-0150" facs="A87716-001-a-0080">stand</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A87716-0160" facs="A87716-001-a-0090">as</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A87716-0170" facs="A87716-001-a-0100">one</w>
          <c> </c>
          <w lemma="profess" ana="#vvg" reg="professing" xml:id="A87716-0180" facs="A87716-001-a-0110">professing</w>
          <c> </c>
          <w lemma="yourself" part="I" ana="#px" reg="yourself" xml:id="A87716-0190.1" facs="A87716-001-a-0120">your selfe</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-0200" facs="A87716-001-a-0140">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A87716-0210" facs="A87716-001-a-0150">be</w>
          <c> </c>
          <w lemma="instruct" ana="#vvn" reg="instructed" xml:id="A87716-0220" facs="A87716-001-a-0160">instructed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A87716-0230" facs="A87716-001-a-0170">by</w>
          <c> </c>
          <w lemma="Christ" ana="#n1-nn" reg="Christ" xml:id="A87716-0240" facs="A87716-001-a-0180">Christ</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A87716-0250" facs="A87716-001-a-0190">with</w>
          <c> </c>
          <w lemma="ability" ana="#n2" reg="abilities" xml:id="A87716-0260" facs="A87716-001-a-0200">Abilities</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A87716-0270" facs="A87716-001-a-0210">from</w>
          <c> </c>
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A87716-0280" facs="A87716-001-a-0220">God</w>
          <pc xml:id="A87716-0290" facs="A87716-001-a-0230">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-0300" facs="A87716-001-a-0240">to</w>
          <c> </c>
          <w lemma="throw" ana="#vvi" reg="throw" xml:id="A87716-0310" facs="A87716-001-a-0250">throw</w>
          <c> </c>
          <w lemma="dowue" ana="#j" reg="dowue" xml:id="A87716-0320" facs="A87716-001-a-0260">dowue</w>
          <c> </c>
          <w lemma="error" ana="#n1" reg="error" xml:id="A87716-0330" facs="A87716-001-a-0270">Errour</w>
          <pc xml:id="A87716-0340" facs="A87716-001-a-0280">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87716-0350" facs="A87716-001-a-0290">and</w>
          <c> </c>
          <w lemma="therefore" ana="#av" reg="therefore" xml:id="A87716-0360" facs="A87716-001-a-0300">therefore</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A87716-0370" facs="A87716-001-a-0310">to</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A87716-0380" facs="A87716-001-a-0320">that</w>
          <c> </c>
          <w lemma="end" ana="#n1" reg="end" xml:id="A87716-0390" facs="A87716-001-a-0330">end</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A87716-0400" facs="A87716-001-a-0340">doe</w>
          <c> </c>
          <w lemma="preach" ana="#vvi" reg="preach" xml:id="A87716-0410" facs="A87716-001-a-0350">preach</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A87716-0420" facs="A87716-001-a-0360">every</w>
          <c> </c>
          <w lemma="three" ana="#ord" reg="third" xml:id="A87716-0430" facs="A87716-001-a-0370">third</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A87716-0440" facs="A87716-001-a-0380">Day</w>
          <pc xml:id="A87716-0450" facs="A87716-001-a-0390">,</pc>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A87716-0460" facs="A87716-001-a-0400">may</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A87716-0470" facs="A87716-001-a-0410">it</w>
          <c> </c>
          <w lemma="therefore" ana="#av" reg="therefore" xml:id="A87716-0480" facs="A87716-001-a-0420">therefore</w>
          <c> </c>
          <w lemma="please" ana="#vvb" reg="please" xml:id="A87716-0490" facs="A87716-001-a-0430">please</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-0500" facs="A87716-001-a-0440">you</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87716-0510" facs="A87716-001-a-0450">and</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A87716-0520" facs="A87716-001-a-0460">those</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A87716-0530" facs="A87716-001-a-0470">that</w>
          <c> </c>
          <w lemma="employ" ana="#vvb" reg="employ" xml:id="A87716-0540" facs="A87716-001-a-0480">imploy</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-0550" facs="A87716-001-a-0490">you</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A87716-0560" facs="A87716-001-a-0500">in</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A87716-0570" facs="A87716-001-a-0510">that</w>
          <c> </c>
          <w lemma="work" ana="#n1" reg="work" xml:id="A87716-0580" facs="A87716-001-a-0520">worke</w>
          <pc xml:id="A87716-0590" facs="A87716-001-a-0530">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-0600" facs="A87716-001-a-0540">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A87716-0610" facs="A87716-001-a-0550">give</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A87716-0620" facs="A87716-001-a-0560">these</w>
          <c> </c>
          <w lemma="leave" ana="#n1" reg="leave" xml:id="A87716-0630" facs="A87716-001-a-0570">leave</w>
          <c> </c>
          <w lemma="who" ana="#crq-ro" reg="whom" xml:id="A87716-0640" facs="A87716-001-a-0580">whom</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-0650" facs="A87716-001-a-0590">you</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A87716-0660" facs="A87716-001-a-0600">so</w>
          <c> </c>
          <w lemma="brand" ana="#n1" reg="brand" xml:id="A87716-0670" facs="A87716-001-a-0610">brand</w>
          <pc xml:id="A87716-0680" facs="A87716-001-a-0620">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A87716-0690" facs="A87716-001-a-0630">as</w>
          <c> </c>
          <w lemma="bublique" ana="#av_j" reg="bubliquely" xml:id="A87716-0700" facs="A87716-001-a-0640">bubliquely</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-0710" facs="A87716-001-a-0650">to</w>
          <c> </c>
          <w lemma="object" ana="#vvi" reg="object" xml:id="A87716-0720" facs="A87716-001-a-0660">object</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A87716-0730" facs="A87716-001-a-0670">against</w>
          <c> </c>
          <w lemma="what" ana="#crq-r" reg="what" xml:id="A87716-0740" facs="A87716-001-a-0680">what</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-0750" facs="A87716-001-a-0690">you</w>
          <c> </c>
          <w lemma="say" ana="#vvb" reg="say" xml:id="A87716-0760" facs="A87716-001-a-0700">say</w>
          <pc xml:id="A87716-0770" facs="A87716-001-a-0710">,</pc>
          <c> </c>
          <w lemma="when" ana="#crq-cs" reg="when" xml:id="A87716-0780" facs="A87716-001-a-0720">when</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A87716-0790" facs="A87716-001-a-0730">your</w>
          <c> </c>
          <w lemma="sermon" ana="#n1" reg="sermon" xml:id="A87716-0800" facs="A87716-001-a-0740">Sermon</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A87716-0810" facs="A87716-001-a-0750">is</w>
          <c> </c>
          <w lemma="end" ana="#vvn" reg="ended" xml:id="A87716-0820" facs="A87716-001-a-0760">ended</w>
          <pc xml:id="A87716-0830" facs="A87716-001-a-0770">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A87716-0840" facs="A87716-001-a-0780">as</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-0850" facs="A87716-001-a-0790">you</w>
          <c> </c>
          <w lemma="declare" ana="#vvb" reg="declare" xml:id="A87716-0860" facs="A87716-001-a-0800">declare</w>
          <c> </c>
          <w lemma="yourself" part="I" ana="#px" reg="yourself" xml:id="A87716-0870.1" facs="A87716-001-a-0810">your selfe</w>
          <pc xml:id="A87716-0880" facs="A87716-001-a-0830">:</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87716-0890" facs="A87716-001-a-0840">and</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A87716-0900" facs="A87716-001-a-0850">we</w>
          <c> </c>
          <w lemma="hope" ana="#vvb" reg="hope" xml:id="A87716-0910" facs="A87716-001-a-0860">hope</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A87716-0920" facs="A87716-001-a-0870">it</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A87716-0930" facs="A87716-001-a-0880">will</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A87716-0940" facs="A87716-001-a-0890">be</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="an" xml:id="A87716-0950" facs="A87716-001-a-0900">an</w>
          <c> </c>
          <w lemma="increase" ana="#n1" reg="increase" xml:id="A87716-0960" facs="A87716-001-a-0910">encrease</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A87716-0970" facs="A87716-001-a-0920">of</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A87716-0980" facs="A87716-001-a-0930">further</w>
          <c> </c>
          <w lemma="light" ana="#n1" reg="light" xml:id="A87716-0990" facs="A87716-001-a-0940">Light</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A87716-1000" facs="A87716-001-a-0950">to</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A87716-1010" facs="A87716-001-a-0960">all</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A87716-1020" facs="A87716-001-a-0970">that</w>
          <c> </c>
          <w lemma="fear" ana="#vvb" reg="fear" xml:id="A87716-1030" facs="A87716-001-a-0980">feare</w>
          <c> </c>
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A87716-1040" facs="A87716-001-a-0990">God</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87716-1050" facs="A87716-001-a-1000">and</w>
          <c> </c>
          <w lemma="put" ana="#vvi" reg="put" xml:id="A87716-1060" facs="A87716-001-a-1010">put</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A87716-1070" facs="A87716-001-a-1020">a</w>
          <c> </c>
          <w lemma="large" ana="#j" reg="large" xml:id="A87716-1080" facs="A87716-001-a-1030">large</w>
          <c> </c>
          <w lemma="advantage" ana="#n1" reg="advantage" xml:id="A87716-1090" facs="A87716-001-a-1040">Advantage</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A87716-1100" facs="A87716-001-a-1050">into</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A87716-1110" facs="A87716-001-a-1060">your</w>
          <c> </c>
          <w lemma="hanq" ana="#n2" reg="hanqs" xml:id="A87716-1120" facs="A87716-001-a-1070">hanqs</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A87716-1130" facs="A87716-001-a-1080">if</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A87716-1140" facs="A87716-001-a-1090">you</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A87716-1150" facs="A87716-001-a-1100">have</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A87716-1160" facs="A87716-001-a-1110">the</w>
          <c> </c>
          <w lemma="truth" ana="#n1" reg="truth" xml:id="A87716-1170" facs="A87716-001-a-1120">Trueth</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A87716-1180" facs="A87716-001-a-1130">on</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A87716-1190" facs="A87716-001-a-1140">your</w>
          <c> </c>
          <w lemma="side" ana="#n1" reg="side" xml:id="A87716-1200" facs="A87716-001-a-1150">side</w>
          <pc xml:id="A87716-1210" facs="A87716-001-a-1160">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-1220" facs="A87716-001-a-1170">to</w>
          <c> </c>
          <w lemma="cause" ana="#vvi" reg="cause" xml:id="A87716-1230" facs="A87716-001-a-1180">cause</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A87716-1240" facs="A87716-001-a-1190">it</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A87716-1250" facs="A87716-001-a-1200">to</w>
          <c> </c>
          <w lemma="shine" ana="#vvi" reg="shine" xml:id="A87716-1260" facs="A87716-001-a-1210">shine</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A87716-1270" facs="A87716-001-a-1220">with</w>
          <c> </c>
          <w lemma="more" ana="#d-c" reg="more" xml:id="A87716-1280" facs="A87716-001-a-1230">more</w>
          <c> </c>
          <w lemma="clearness" ana="#n1" reg="clearness" xml:id="A87716-1290" facs="A87716-001-a-1240">clearnesse</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A87716-1300" facs="A87716-001-a-1250">and</w>
          <c> </c>
          <w lemma="i" ana="#pns" reg="i" xml:id="A87716-1310" facs="A87716-001-a-1260">I</w>
          <c> </c>
          <w lemma="hope" ana="#vvb" reg="hope" xml:id="A87716-1320" facs="A87716-001-a-1270">hope</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A87716-1330" facs="A87716-001-a-1280">we</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A87716-1340" facs="A87716-001-a-1290">shall</w>
          <c> </c>
          <w lemma="do" ana="#vvi" reg="do" xml:id="A87716-1350" facs="A87716-001-a-1300">doe</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A87716-1360" facs="A87716-001-a-1310">it</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A87716-1370" facs="A87716-001-a-1320">with</w>
          <c> </c>
          <w lemma="moderation" ana="#n1" reg="moderation" xml:id="A87716-1380" facs="A87716-001-a-1330">Moderation</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A87716-1390" facs="A87716-001-a-1340">as</w>
          <c> </c>
          <w lemma="become" ana="#vvz" reg="becometh" xml:id="A87716-1400" facs="A87716-001-a-1350">becommeth</w>
          <c> </c>
          <w lemma="christian" ana="#n2-nn_j" reg="Christians" xml:id="A87716-1410" facs="A87716-001-a-1360">Christians</w>
          <pc unit="sentence" xml:id="A87716-1420" facs="A87716-001-a-1370">.</pc>
        </p>
        <closer xml:id="A87716e-80">
          <dateline xml:id="A87716e-90">
            <date xml:id="A87716e-100">
              <w lemma="Novemb." ana="#n-ab" reg="novemb." xml:id="A87716-1470" facs="A87716-001-a-1380">Novemb.</w>
              <c> </c>
              <w lemma="15." ana="#crd" reg="15." xml:id="A87716-1480" facs="A87716-001-a-1390">15.</w>
              <c> </c>
              <w lemma="1644." ana="#crd" reg="1644." xml:id="A87716-1490" facs="A87716-001-a-1400">1644.</w>
              <pc unit="sentence" xml:id="A87716-1490-eos" facs="A87716-001-a-1410"/>
            </date>
          </dateline>
          <signed xml:id="A87716e-110">
            <w lemma="you" ana="#png" reg="Yours" xml:id="A87716-1520" facs="A87716-001-a-1420">Yours</w>
            <pc xml:id="A87716-1530" facs="A87716-001-a-1430">,</pc>
            <c> </c>
            <w lemma="William" ana="#n1-nn" reg="William" xml:id="A87716-1540" facs="A87716-001-a-1440">William</w>
            <c> </c>
            <w lemma="kiff••" ana="#fw-la" reg="kiff••" xml:id="A87716-1550" facs="A87716-001-a-1450">Kiff••</w>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A87716e-120">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>