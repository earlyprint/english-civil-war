<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>An act for continuance of the imposition upon coals, towards the building and maintaining ships for garding the seas</title>
        <author>England and Wales.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1652</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A37555</idno>
        <idno type="STC">Wing E1012</idno>
        <idno type="STC">ESTC R40491</idno>
        <idno type="EEBO-CITATION">19340522</idno>
        <idno type="OCLC">ocm 19340522</idno>
        <idno type="VID">108713</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A37555)</note>
        <note>Transcribed from: (Early English Books Online ; image set 108713)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1668:19)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>An act for continuance of the imposition upon coals, towards the building and maintaining ships for garding the seas</title>
            <author>England and Wales.</author>
          </titleStmt>
          <extent>1 broadside.</extent>
          <publicationStmt>
            <publisher>Printed by John Field ...,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1652.</date>
          </publicationStmt>
          <notesStmt>
            <note>Reproduction of original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>England and Wales. -- Parliament. -- Act for laying an imposition upon coals towards the building and maintaining ships for garding the seas.</term>
          <term>Coal -- Taxation -- Great Britain.</term>
          <term>Taxation -- Law and legislation -- Great Britain.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-05</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-06</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-07</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2008-07</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A37555e-10">
    <body xml:id="A37555e-20">
      <div type="Act_of_Parliament" xml:id="A37555e-30">
        <pb facs="tcp:108713:1" rendition="simple:additions" xml:id="A37555-001-a"/>
        <head xml:id="A37555e-40">
          <figure xml:id="A37555e-50">
            <figDesc xml:id="A37555e-60">seal or coat of arms of the Commonwealth</figDesc>
          </figure>
        </head>
        <opener xml:id="A37555e-70">
          <dateline xml:id="A37555e-80">
            <date xml:id="A37555e-90">
              <add xml:id="A37555e-100">
                <w lemma="22." ana="#crd" reg="22." xml:id="A37555-0170" facs="A37555-001-a-0010">22.</w>
                <pc unit="sentence" xml:id="A37555-0170-eos" facs="A37555-001-a-0020"/>
                <c> </c>
                <w lemma="march" ana="#n1" reg="March" xml:id="A37555-0180" facs="A37555-001-a-0030">March</w>
                <c> </c>
                <w lemma="1653" ana="#crd" reg="1653" xml:id="A37555-0190" facs="A37555-001-a-0040">1653</w>
              </add>
            </date>
          </dateline>
        </opener>
        <head xml:id="A37555e-110">
          <w lemma="a" ana="#d" reg="An" xml:id="A37555-0220" facs="A37555-001-a-0050">AN</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A37555-0230" facs="A37555-001-a-0060">ACT</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A37555-0240" facs="A37555-001-a-0070">For</w>
          <c> </c>
          <w lemma="continuance" ana="#n1" reg="continuance" xml:id="A37555-0250" facs="A37555-001-a-0080">continuance</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A37555-0260" facs="A37555-001-a-0090">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-0270" facs="A37555-001-a-0100">the</w>
          <c> </c>
          <w lemma="imposition" ana="#n1" reg="imposition" xml:id="A37555-0280" facs="A37555-001-a-0110">Imposition</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A37555-0290" facs="A37555-001-a-0120">upon</w>
          <c> </c>
          <w lemma="coal" ana="#n2" reg="coals" xml:id="A37555-0300" facs="A37555-001-a-0130">Coals</w>
          <pc xml:id="A37555-0310" facs="A37555-001-a-0140">,</pc>
          <c> </c>
          <w lemma="towards" ana="#acp-p" reg="towards" xml:id="A37555-0320" facs="A37555-001-a-0150">Towards</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-0330" facs="A37555-001-a-0160">the</w>
          <c> </c>
          <w lemma="building" ana="#n1" reg="building" xml:id="A37555-0340" facs="A37555-001-a-0170">Building</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0350" facs="A37555-001-a-0180">and</w>
          <c> </c>
          <w lemma="maintain" ana="#vvg" reg="maintaining" xml:id="A37555-0360" facs="A37555-001-a-0190">Maintaining</w>
          <c> </c>
          <w lemma="ship" ana="#n2" reg="ships" xml:id="A37555-0370" facs="A37555-001-a-0200">Ships</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A37555-0380" facs="A37555-001-a-0210">for</w>
          <c> </c>
          <w lemma="guard" ana="#vvg" reg="guarding" xml:id="A37555-0390" facs="A37555-001-a-0220">garding</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-0400" facs="A37555-001-a-0230">the</w>
          <c> </c>
          <w lemma="sea" ana="#n2" reg="seas" xml:id="A37555-0410" facs="A37555-001-a-0240">Seas</w>
          <pc unit="sentence" xml:id="A37555-0420" facs="A37555-001-a-0250">.</pc>
        </head>
        <p xml:id="A37555e-120">
          <w lemma="be" ana="#vvb" reg="Be" rend="initialcharacterdecorated" xml:id="A37555-0450" facs="A37555-001-a-0260">BE</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A37555-0460" facs="A37555-001-a-0270">it</w>
          <c> </c>
          <w lemma="enact" ana="#vvn" reg="enacted" xml:id="A37555-0470" facs="A37555-001-a-0280">Enacted</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A37555-0480" facs="A37555-001-a-0290">by</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A37555-0490" facs="A37555-001-a-0300">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A37555-0500" facs="A37555-001-a-0310">present</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A37555-0510" facs="A37555-001-a-0320">Parliament</w>
          <pc xml:id="A37555-0520" facs="A37555-001-a-0330">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0530" facs="A37555-001-a-0340">and</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A37555-0540" facs="A37555-001-a-0350">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-0550" facs="A37555-001-a-0360">the</w>
          <c> </c>
          <w lemma="authority" ana="#n1" reg="authority" xml:id="A37555-0560" facs="A37555-001-a-0370">Authority</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A37555-0570" facs="A37555-001-a-0380">thereof</w>
          <pc xml:id="A37555-0580" facs="A37555-001-a-0390">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A37555-0590" facs="A37555-001-a-0400">That</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A37555-0600" facs="A37555-001-a-0410">one</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A37555-0610" facs="A37555-001-a-0420">Act</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A37555-0620" facs="A37555-001-a-0430">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A37555-0630" facs="A37555-001-a-0440">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A37555-0640" facs="A37555-001-a-0450">present</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A37555-0650" facs="A37555-001-a-0460">Parliament</w>
          <pc xml:id="A37555-0660" facs="A37555-001-a-0470">,</pc>
          <c> </c>
          <w lemma="entitle" ana="#vvn" reg="entitled" xml:id="A37555-0670" facs="A37555-001-a-0480">Entituled</w>
          <pc xml:id="A37555-0680" facs="A37555-001-a-0490">,</pc>
          <c> </c>
          <hi xml:id="A37555e-130">
            <w lemma="a" ana="#d" reg="an" xml:id="A37555-0690" facs="A37555-001-a-0500">An</w>
            <c> </c>
            <w lemma="act" ana="#n1" reg="act" xml:id="A37555-0700" facs="A37555-001-a-0510">Act</w>
            <c> </c>
            <w lemma="for" ana="#acp-p" reg="for" xml:id="A37555-0710" facs="A37555-001-a-0520">for</w>
            <c> </c>
            <w lemma="lay" ana="#vvg" reg="laying" xml:id="A37555-0720" facs="A37555-001-a-0530">laying</w>
            <c> </c>
            <w lemma="a" ana="#d" reg="an" xml:id="A37555-0730" facs="A37555-001-a-0540">an</w>
            <c> </c>
            <w lemma="imposition" ana="#n1" reg="imposition" xml:id="A37555-0740" facs="A37555-001-a-0550">Imposition</w>
            <c> </c>
            <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A37555-0750" facs="A37555-001-a-0560">upon</w>
            <c> </c>
            <w lemma="coal" ana="#n2" reg="coals" xml:id="A37555-0760" facs="A37555-001-a-0570">Coals</w>
            <pc xml:id="A37555-0770" facs="A37555-001-a-0580">,</pc>
            <c> </c>
            <w lemma="towards" ana="#acp-p" reg="towards" xml:id="A37555-0780" facs="A37555-001-a-0590">towards</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A37555-0790" facs="A37555-001-a-0600">the</w>
            <c> </c>
            <w lemma="building" ana="#n1" reg="building" xml:id="A37555-0800" facs="A37555-001-a-0610">Building</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0810" facs="A37555-001-a-0620">and</w>
            <c> </c>
            <w lemma="maintain" ana="#vvg" reg="maintaining" xml:id="A37555-0820" facs="A37555-001-a-0630">Maintaining</w>
            <c> </c>
            <w lemma="ship" ana="#n2" reg="ships" xml:id="A37555-0830" facs="A37555-001-a-0640">Ships</w>
            <c> </c>
            <w lemma="for" ana="#acp-p" reg="for" xml:id="A37555-0840" facs="A37555-001-a-0650">for</w>
            <c> </c>
            <w lemma="guard" ana="#vvg" reg="guarding" xml:id="A37555-0850" facs="A37555-001-a-0660">garding</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A37555-0860" facs="A37555-001-a-0670">the</w>
            <c> </c>
            <w lemma="sea" ana="#n2" reg="seas" xml:id="A37555-0870" facs="A37555-001-a-0680">Seas</w>
            <pc xml:id="A37555-0880" facs="A37555-001-a-0690">:</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0890" facs="A37555-001-a-0700">And</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A37555-0900" facs="A37555-001-a-0710">all</w>
          <c> </c>
          <w lemma="power" ana="#n2" reg="powers" xml:id="A37555-0910" facs="A37555-001-a-0720">Powers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0920" facs="A37555-001-a-0730">and</w>
          <c> </c>
          <w lemma="clause" ana="#n2" reg="clauses" xml:id="A37555-0930" facs="A37555-001-a-0740">Clauses</w>
          <c> </c>
          <w lemma="therein" ana="#av" reg="therein" xml:id="A37555-0940" facs="A37555-001-a-0750">therein</w>
          <c> </c>
          <w lemma="contain" ana="#vvn" reg="contained" xml:id="A37555-0950" facs="A37555-001-a-0760">contained</w>
          <pc xml:id="A37555-0960" facs="A37555-001-a-0770">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A37555-0970" facs="A37555-001-a-0780">be</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-0980" facs="A37555-001-a-0790">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A37555-0990" facs="A37555-001-a-0800">are</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A37555-1000" facs="A37555-001-a-0810">hereby</w>
          <c> </c>
          <w lemma="continue" ana="#vvn" reg="continue" xml:id="A37555-1010" facs="A37555-001-a-0820">continued</w>
          <pc xml:id="A37555-1020" facs="A37555-001-a-0830">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1030" facs="A37555-001-a-0840">and</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A37555-1040" facs="A37555-001-a-0850">shall</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1050" facs="A37555-001-a-0860">and</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A37555-1060" facs="A37555-001-a-0870">do</w>
          <c> </c>
          <w lemma="stand" ana="#vvi" reg="stand" xml:id="A37555-1070" facs="A37555-001-a-0880">stand</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A37555-1080" facs="A37555-001-a-0890">in</w>
          <c> </c>
          <w lemma="full" ana="#j" reg="full" xml:id="A37555-1090" facs="A37555-001-a-0900">full</w>
          <c> </c>
          <w lemma="force" ana="#n1" reg="force" xml:id="A37555-1100" facs="A37555-001-a-0910">force</w>
          <c> </c>
          <w lemma="until" ana="#acp-p" reg="until" xml:id="A37555-1110" facs="A37555-001-a-0920">until</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-1120" facs="A37555-001-a-0930">the</w>
          <c> </c>
          <w lemma="six" ana="#crd" reg="six" xml:id="A37555-1130" facs="A37555-001-a-0940">Six</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1140" facs="A37555-001-a-0950">and</w>
          <c> </c>
          <w lemma="twenty" ana="#ord" reg="twentieth" xml:id="A37555-1150" facs="A37555-001-a-0960">twentieth</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A37555-1160" facs="A37555-001-a-0970">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A37555-1170" facs="A37555-001-a-0980">of</w>
          <c> </c>
          <hi xml:id="A37555e-140">
            <w lemma="march" ana="#n1" reg="March" xml:id="A37555-1180" facs="A37555-001-a-0990">March</w>
            <pc xml:id="A37555-1190" facs="A37555-001-a-1000">,</pc>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A37555-1200" facs="A37555-001-a-1010">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-1210" facs="A37555-001-a-1020">the</w>
          <c> </c>
          <w lemma="year" ana="#n1" reg="year" xml:id="A37555-1220" facs="A37555-001-a-1030">year</w>
          <c> </c>
          <w lemma="one" ana="#crd" reg="one" xml:id="A37555-1230" facs="A37555-001-a-1040">One</w>
          <c> </c>
          <w lemma="thousand" ana="#crd" reg="thousand" xml:id="A37555-1240" facs="A37555-001-a-1050">thousand</w>
          <c> </c>
          <w lemma="six" ana="#crd" reg="six" xml:id="A37555-1250" facs="A37555-001-a-1060">six</w>
          <c> </c>
          <w lemma="hundred" ana="#crd" reg="hundred" xml:id="A37555-1260" facs="A37555-001-a-1070">hundred</w>
          <c> </c>
          <w lemma="fifty" ana="#crd" reg="fifty" xml:id="A37555-1270" facs="A37555-001-a-1080">fifty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1280" facs="A37555-001-a-1090">and</w>
          <c> </c>
          <w lemma="four" ana="#crd" reg="four" xml:id="A37555-1290" facs="A37555-001-a-1100">four</w>
          <pc unit="sentence" xml:id="A37555-1300" facs="A37555-001-a-1110">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A37555e-150">
      <div type="license" xml:id="A37555e-160">
        <opener xml:id="A37555e-170">
          <dateline xml:id="A37555e-180">
            <date xml:id="A37555e-190">
              <hi xml:id="A37555e-200">
                <w lemma="Tuesday" ana="#n1-nn" reg="Tuesday" xml:id="A37555-1370" facs="A37555-001-a-1120">Tuesday</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A37555-1380" facs="A37555-001-a-1130">the</w>
                <c> </c>
                <w lemma="two" ana="#crd" reg="two" xml:id="A37555-1390" facs="A37555-001-a-1140">Two</w>
                <c> </c>
                <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1400" facs="A37555-001-a-1150">and</w>
                <c> </c>
                <w lemma="twenty" ana="#ord" reg="twentieth" xml:id="A37555-1410" facs="A37555-001-a-1160">twentieth</w>
                <c> </c>
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A37555-1420" facs="A37555-001-a-1170">of</w>
              </hi>
              <c> </c>
              <w lemma="march" ana="#n1" reg="March" xml:id="A37555-1430" facs="A37555-001-a-1180">March</w>
              <pc xml:id="A37555-1440" facs="A37555-001-a-1190">,</pc>
              <c> </c>
              <w lemma="1652." ana="#crd" reg="1652." xml:id="A37555-1450" facs="A37555-001-a-1200">1652.</w>
              <pc unit="sentence" xml:id="A37555-1450-eos" facs="A37555-001-a-1210"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A37555e-210">
          <hi xml:id="A37555e-220">
            <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A37555-1480" facs="A37555-001-a-1220">ORdered</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A37555-1490" facs="A37555-001-a-1230">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A37555-1500" facs="A37555-001-a-1240">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A37555-1510" facs="A37555-001-a-1250">Parliament</w>
            <pc xml:id="A37555-1520" facs="A37555-001-a-1260">,</pc>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A37555-1530" facs="A37555-001-a-1270">That</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A37555-1540" facs="A37555-001-a-1280">this</w>
            <c> </c>
            <w lemma="act" ana="#n1" reg="act" xml:id="A37555-1550" facs="A37555-001-a-1290">Act</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="be" xml:id="A37555-1560" facs="A37555-001-a-1300">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A37555-1570" facs="A37555-001-a-1310">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A37555-1580" facs="A37555-001-a-1320">printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A37555-1590" facs="A37555-001-a-1330">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A37555-1600" facs="A37555-001-a-1340">published</w>
            <pc unit="sentence" xml:id="A37555-1610" facs="A37555-001-a-1350">.</pc>
            <c> </c>
          </hi>
        </p>
        <closer xml:id="A37555e-230">
          <signed xml:id="A37555e-240">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A37555-1650" facs="A37555-001-a-1360">Hen</w>
            <pc xml:id="A37555-1660" facs="A37555-001-a-1370">:</pc>
            <c> </c>
            <w lemma="Scobell" ana="#n1-nn" reg="Scobell" xml:id="A37555-1670" facs="A37555-001-a-1380">Scobell</w>
            <pc xml:id="A37555-1680" facs="A37555-001-a-1390">,</pc>
            <c> </c>
            <w lemma="cleric" ana="#j" reg="cleric" xml:id="A37555-1690" facs="A37555-001-a-1400">Cleric</w>
            <pc unit="sentence" xml:id="A37555-1700" facs="A37555-001-a-1410">.</pc>
            <c> </c>
            <w lemma="parliamenti" ana="#fw-la" reg="Parliamenti" xml:id="A37555-1710" facs="A37555-001-a-1420">Parliamenti</w>
            <pc unit="sentence" xml:id="A37555-1720" facs="A37555-001-a-1430">.</pc>
          </signed>
        </closer>
      </div>
      <div type="colophon" xml:id="A37555e-250">
        <p xml:id="A37555e-260">
          <hi xml:id="A37555e-270">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A37555-1760" facs="A37555-001-a-1440">London</w>
            <pc xml:id="A37555-1770" facs="A37555-001-a-1450">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A37555-1780" facs="A37555-001-a-1460">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A37555-1790" facs="A37555-001-a-1470">by</w>
          <c> </c>
          <hi xml:id="A37555e-280">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A37555-1800" facs="A37555-001-a-1480">John</w>
            <c> </c>
            <w lemma="field" ana="#n1" reg="field" xml:id="A37555-1810" facs="A37555-001-a-1490">Field</w>
            <pc xml:id="A37555-1820" facs="A37555-001-a-1500">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A37555-1830" facs="A37555-001-a-1510">Printed</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A37555-1840" facs="A37555-001-a-1520">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A37555-1850" facs="A37555-001-a-1530">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A37555-1860" facs="A37555-001-a-1540">Parliament</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A37555-1870" facs="A37555-001-a-1550">of</w>
          <c> </c>
          <hi xml:id="A37555e-290">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A37555-1880" facs="A37555-001-a-1560">England</w>
            <pc unit="sentence" xml:id="A37555-1890" facs="A37555-001-a-1570">.</pc>
            <c> </c>
          </hi>
          <w lemma="1652." ana="#crd" reg="1652." xml:id="A37555-1900" facs="A37555-001-a-1580">1652.</w>
          <pc unit="sentence" xml:id="A37555-1900-eos" facs="A37555-001-a-1590"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>