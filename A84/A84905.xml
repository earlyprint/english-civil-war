<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A proclamation by his Excellency the Lord Generall, for the regulating of souldiers in their march to Ireland.</title>
        <author>Fairfax, Thomas Fairfax, Baron, 1612-1671.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1649</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A84905</idno>
        <idno type="STC">Wing F218</idno>
        <idno type="STC">Thomason 669.f.14[4]</idno>
        <idno type="STC">ESTC R211053</idno>
        <idno type="EEBO-CITATION">99869789</idno>
        <idno type="PROQUEST">99869789</idno>
        <idno type="VID">162992</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A84905)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162992)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f14[4])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A proclamation by his Excellency the Lord Generall, for the regulating of souldiers in their march to Ireland.</title>
            <author>Fairfax, Thomas Fairfax, Baron, 1612-1671.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed for John Playford, and are to be sold at his shop in the Inner Temple,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1649.</date>
          </publicationStmt>
          <notesStmt>
            <note>Signed and dated at end: T. Fairfax. 17 March 1648.</note>
            <note>Annotation on Thomason copy: "March 28 1648".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Soldiers -- Great Britain -- Conduct of life -- Early works to 1800.</term>
          <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A84905e-10">
    <body xml:id="A84905e-20">
      <div type="proclamation" xml:id="A84905e-30">
        <pb facs="tcp:162992:1" rendition="simple:additions" xml:id="A84905-001-a"/>
        <head xml:id="A84905e-40">
          <w lemma="a" ana="#d" reg="A" xml:id="A84905-0030" facs="A84905-001-a-0010">A</w>
          <c> </c>
          <w lemma="proclamation" ana="#n1" reg="proclamation" xml:id="A84905-0040" facs="A84905-001-a-0020">PROCLAMATION</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84905-0050" facs="A84905-001-a-0030">By</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A84905-0060" facs="A84905-001-a-0040">his</w>
          <c> </c>
          <w lemma="excellency" ana="#n1" reg="excellency" xml:id="A84905-0070" facs="A84905-001-a-0050">Excellency</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0080" facs="A84905-001-a-0060">the</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A84905-0090" facs="A84905-001-a-0070">Lord</w>
          <c> </c>
          <w lemma="general" ana="#n1" reg="general" xml:id="A84905-0100" facs="A84905-001-a-0080">Generall</w>
          <pc xml:id="A84905-0110" facs="A84905-001-a-0090">,</pc>
        </head>
        <head type="sub" xml:id="A84905e-50">
          <hi xml:id="A84905e-60">
            <w lemma="for" ana="#acp-p" reg="for" xml:id="A84905-0140" facs="A84905-001-a-0100">For</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A84905-0150" facs="A84905-001-a-0110">the</w>
            <c> </c>
            <w lemma="regulate" ana="#n1_vg" reg="regulating" xml:id="A84905-0160" facs="A84905-001-a-0120">regulating</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-0170" facs="A84905-001-a-0130">of</w>
            <c> </c>
            <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A84905-0180" facs="A84905-001-a-0140">Souldiers</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-0190" facs="A84905-001-a-0150">in</w>
            <c> </c>
            <w lemma="their" ana="#po" reg="their" xml:id="A84905-0200" facs="A84905-001-a-0160">their</w>
            <c> </c>
            <w lemma="march" ana="#n1" reg="March" xml:id="A84905-0210" facs="A84905-001-a-0170">march</w>
            <c> </c>
            <w lemma="to" ana="#acp-p" reg="to" xml:id="A84905-0220" facs="A84905-001-a-0180">to</w>
            <c> </c>
            <hi xml:id="A84905e-70">
              <w lemma="IRELAND" ana="#n1-nn" reg="Ireland" xml:id="A84905-0230" facs="A84905-001-a-0190">IRELAND</w>
              <pc unit="sentence" xml:id="A84905-0240" facs="A84905-001-a-0200">.</pc>
              <c> </c>
            </hi>
          </hi>
        </head>
        <p xml:id="A84905e-80">
          <w lemma="whereas" ana="#cs" reg="Whereas" rend="initialcharacterdecorated" xml:id="A84905-0270" facs="A84905-001-a-0210">WHereas</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A84905-0280" facs="A84905-001-a-0220">it</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A84905-0290" facs="A84905-001-a-0230">is</w>
          <c> </c>
          <w lemma="credible" ana="#av_j" reg="credibly" xml:id="A84905-0300" facs="A84905-001-a-0240">credibly</w>
          <c> </c>
          <w lemma="report" ana="#vvn" reg="reported" xml:id="A84905-0310" facs="A84905-001-a-0250">reported</w>
          <pc xml:id="A84905-0320" facs="A84905-001-a-0260">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84905-0330" facs="A84905-001-a-0270">that</w>
          <c> </c>
          <w lemma="divers" ana="#j" reg="divers" xml:id="A84905-0340" facs="A84905-001-a-0280">divers</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A84905-0350" facs="A84905-001-a-0290">Souldiers</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-0360" facs="A84905-001-a-0300">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A84905-0370" facs="A84905-001-a-0310">their</w>
          <c> </c>
          <w lemma="march" ana="#n1" reg="March" xml:id="A84905-0380" facs="A84905-001-a-0320">march</w>
          <c> </c>
          <w lemma="through" ana="#acp-p" reg="through" xml:id="A84905-0390" facs="A84905-001-a-0330">through</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A84905-0400" facs="A84905-001-a-0340">severall</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A84905-0410" facs="A84905-001-a-0350">Counties</w>
          <c> </c>
          <w lemma="towards" ana="#acp-p" reg="towards" xml:id="A84905-0420" facs="A84905-001-a-0360">towards</w>
          <c> </c>
          <hi xml:id="A84905e-90">
            <w lemma="Jreland" ana="#n1-nn" reg="Jreland" xml:id="A84905-0430" facs="A84905-001-a-0370">Jreland</w>
            <pc xml:id="A84905-0440" facs="A84905-001-a-0380">,</pc>
          </hi>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A84905-0450" facs="A84905-001-a-0390">have</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-0460" facs="A84905-001-a-0400">and</w>
          <c> </c>
          <w lemma="still" ana="#av" reg="still" xml:id="A84905-0470" facs="A84905-001-a-0410">still</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A84905-0480" facs="A84905-001-a-0420">do</w>
          <c> </c>
          <w lemma="harrass" ana="#vvi" reg="harrass" xml:id="A84905-0490" facs="A84905-001-a-0430">harrasse</w>
          <pc xml:id="A84905-0500" facs="A84905-001-a-0440">,</pc>
          <c> </c>
          <w lemma="plunder" ana="#vvb" reg="plunder" xml:id="A84905-0510" facs="A84905-001-a-0450">plunder</w>
          <pc xml:id="A84905-0520" facs="A84905-001-a-0460">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-0530" facs="A84905-001-a-0470">and</w>
          <c> </c>
          <w lemma="act" ana="#vvi" reg="act" xml:id="A84905-0540" facs="A84905-001-a-0480">act</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A84905-0550" facs="A84905-001-a-0490">great</w>
          <c> </c>
          <w lemma="violence" ana="#n2" reg="violences" xml:id="A84905-0560" facs="A84905-001-a-0500">violencies</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-0570" facs="A84905-001-a-0510">and</w>
          <c> </c>
          <w lemma="insolence" ana="#n2" reg="insolences" xml:id="A84905-0580" facs="A84905-001-a-0520">insolences</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-0590" facs="A84905-001-a-0530">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0600" facs="A84905-001-a-0540">the</w>
          <c> </c>
          <w lemma="country" ana="#n1" reg="country" xml:id="A84905-0610" facs="A84905-001-a-0550">Countrey</w>
          <pc xml:id="A84905-0620" facs="A84905-001-a-0560">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84905-0630" facs="A84905-001-a-0570">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0640" facs="A84905-001-a-0580">the</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A84905-0650" facs="A84905-001-a-0590">great</w>
          <c> </c>
          <w lemma="injury" ana="#n1" reg="injury" xml:id="A84905-0660" facs="A84905-001-a-0600">injurie</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-0670" facs="A84905-001-a-0610">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0680" facs="A84905-001-a-0620">the</w>
          <c> </c>
          <w lemma="people" ana="#n1" reg="persons" xml:id="A84905-0690" facs="A84905-001-a-0630">people</w>
          <pc xml:id="A84905-0700" facs="A84905-001-a-0640">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-0710" facs="A84905-001-a-0650">and</w>
          <c> </c>
          <w lemma="dishonour" ana="#n1" reg="dishonour" xml:id="A84905-0720" facs="A84905-001-a-0660">dishonor</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A84905-0730" facs="A84905-001-a-0670">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0740" facs="A84905-001-a-0680">the</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A84905-0750" facs="A84905-001-a-0690">Army</w>
          <pc xml:id="A84905-0760" facs="A84905-001-a-0700">,</pc>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-cs" reg="notwithstanding" xml:id="A84905-0770" facs="A84905-001-a-0710">notwithstanding</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0780" facs="A84905-001-a-0720">the</w>
          <c> </c>
          <w lemma="power" ana="#n1" reg="power" xml:id="A84905-0790" facs="A84905-001-a-0730">power</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84905-0800" facs="A84905-001-a-0740">by</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A84905-0810" facs="A84905-001-a-0750">a</w>
          <c> </c>
          <w lemma="former" ana="#j" reg="former" xml:id="A84905-0820" facs="A84905-001-a-0760">former</w>
          <c> </c>
          <w lemma="proclamation" ana="#n1" reg="proclamation" xml:id="A84905-0830" facs="A84905-001-a-0770">Proclamation</w>
          <c> </c>
          <w lemma="give" ana="#vvn" reg="given" xml:id="A84905-0840" facs="A84905-001-a-0780">given</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A84905-0850" facs="A84905-001-a-0790">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-0860" facs="A84905-001-a-0800">the</w>
          <c> </c>
          <w lemma="country" ana="#n1" reg="country" xml:id="A84905-0870" facs="A84905-001-a-0810">Countrey</w>
          <pc xml:id="A84905-0880" facs="A84905-001-a-0820">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84905-0890" facs="A84905-001-a-0830">to</w>
          <c> </c>
          <w lemma="suppress" ana="#vvi" reg="suppress" xml:id="A84905-0900" facs="A84905-001-a-0840">suppresse</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-0910" facs="A84905-001-a-0850">and</w>
          <c> </c>
          <w lemma="secure" ana="#vvi" reg="secure" xml:id="A84905-0920" facs="A84905-001-a-0860">secure</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A84905-0930" facs="A84905-001-a-0870">them</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A84905-0940" facs="A84905-001-a-0880">so</w>
          <c> </c>
          <w lemma="do" ana="#vvg" reg="doing" xml:id="A84905-0950" facs="A84905-001-a-0890">doing</w>
          <pc xml:id="A84905-0960" facs="A84905-001-a-0900">;</pc>
          <c> </c>
          <w lemma="i" ana="#pns" reg="i" xml:id="A84905-0970" facs="A84905-001-a-0910">I</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A84905-0980" facs="A84905-001-a-0920">do</w>
          <c> </c>
          <w lemma="therefore" ana="#av" reg="therefore" xml:id="A84905-0990" facs="A84905-001-a-0930">therefore</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A84905-1000" facs="A84905-001-a-0940">hereby</w>
          <c> </c>
          <w lemma="require" ana="#vvi" reg="require" xml:id="A84905-1010" facs="A84905-001-a-0950">require</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A84905-1020" facs="A84905-001-a-0960">all</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A84905-1030" facs="A84905-001-a-0970">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-1040" facs="A84905-001-a-0980">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A84905-1050" facs="A84905-001-a-0990">Souldiers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-1060" facs="A84905-001-a-1000">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-1070" facs="A84905-001-a-1010">the</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A84905-1080" facs="A84905-001-a-1020">Army</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A84905-1090" facs="A84905-001-a-1030">under</w>
          <c> </c>
          <w lemma="my" ana="#po" reg="my" xml:id="A84905-1100" facs="A84905-001-a-1040">my</w>
          <c> </c>
          <w lemma="command" ana="#n1" reg="command" xml:id="A84905-1110" facs="A84905-001-a-1050">Command</w>
          <pc xml:id="A84905-1120" facs="A84905-001-a-1060">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84905-1130" facs="A84905-001-a-1070">that</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A84905-1140" facs="A84905-001-a-1080">do</w>
          <c> </c>
          <w lemma="quarter" ana="#n1" reg="quarter" xml:id="A84905-1150" facs="A84905-001-a-1090">quarter</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-1160" facs="A84905-001-a-1100">in</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84905-1170" facs="A84905-001-a-1110">or</w>
          <c> </c>
          <w lemma="near" ana="#acp-p" reg="near" xml:id="A84905-1180" facs="A84905-001-a-1120">neere</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A84905-1190" facs="A84905-001-a-1130">such</w>
          <c> </c>
          <w lemma="place" ana="#n2" reg="places" xml:id="A84905-1200" facs="A84905-001-a-1140">places</w>
          <pc xml:id="A84905-1210" facs="A84905-001-a-1150">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84905-1220" facs="A84905-001-a-1160">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A84905-1230" facs="A84905-001-a-1170">be</w>
          <c> </c>
          <w lemma="aid" ana="#vvg" reg="aiding" xml:id="A84905-1240" facs="A84905-001-a-1180">aiding</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-1250" facs="A84905-001-a-1190">and</w>
          <c> </c>
          <w lemma="assist" ana="#vvg" reg="assisting" xml:id="A84905-1260" facs="A84905-001-a-1200">assisting</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A84905-1270" facs="A84905-001-a-1210">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-1280" facs="A84905-001-a-1220">the</w>
          <c> </c>
          <w lemma="people" ana="#n1" reg="persons" xml:id="A84905-1290" facs="A84905-001-a-1230">people</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-1300" facs="A84905-001-a-1240">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-1310" facs="A84905-001-a-1250">the</w>
          <c> </c>
          <w lemma="country" ana="#n1" reg="country" xml:id="A84905-1320" facs="A84905-001-a-1260">Countrey</w>
          <pc xml:id="A84905-1330" facs="A84905-001-a-1270">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A84905-1340" facs="A84905-001-a-1280">for</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A84905-1350" facs="A84905-001-a-1290">their</w>
          <c> </c>
          <w lemma="relief" ana="#n1" reg="relief" xml:id="A84905-1360" facs="A84905-001-a-1300">relief</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A84905-1370" facs="A84905-001-a-1310">against</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-1380" facs="A84905-001-a-1320">the</w>
          <c> </c>
          <w lemma="outrage" ana="#n2" reg="outrages" xml:id="A84905-1390" facs="A84905-001-a-1330">outrages</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-1400" facs="A84905-001-a-1340">and</w>
          <c> </c>
          <w lemma="violence" ana="#n2" reg="violences" xml:id="A84905-1410" facs="A84905-001-a-1350">violences</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-1420" facs="A84905-001-a-1360">of</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A84905-1430" facs="A84905-001-a-1370">any</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A84905-1440" facs="A84905-001-a-1380">such</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A84905-1450" facs="A84905-001-a-1390">Souldiers</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-1460" facs="A84905-001-a-1400">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A84905-1470" facs="A84905-001-a-1410">their</w>
          <c> </c>
          <w lemma="march" ana="#n1" reg="March" xml:id="A84905-1480" facs="A84905-001-a-1420">march</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84905-1490" facs="A84905-001-a-1430">or</w>
          <c> </c>
          <w lemma="otherwise" ana="#av" reg="otherwise" xml:id="A84905-1500" facs="A84905-001-a-1440">otherwise</w>
          <pc xml:id="A84905-1510" facs="A84905-001-a-1450">:</pc>
          <c> </c>
          <w lemma="provide" ana="#vvn" reg="provided" xml:id="A84905-1520" facs="A84905-001-a-1460">Provided</w>
          <pc xml:id="A84905-1530" facs="A84905-001-a-1470">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84905-1540" facs="A84905-001-a-1480">that</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A84905-1550" facs="A84905-001-a-1490">it</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A84905-1560" facs="A84905-001-a-1500">is</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A84905-1570" facs="A84905-001-a-1510">not</w>
          <c> </c>
          <w lemma="intend" ana="#vvn" reg="intended" xml:id="A84905-1580" facs="A84905-001-a-1520">intended</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A84905-1590" facs="A84905-001-a-1530">hereby</w>
          <pc xml:id="A84905-1600" facs="A84905-001-a-1540">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84905-1610" facs="A84905-001-a-1550">that</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A84905-1620" facs="A84905-001-a-1560">those</w>
          <c> </c>
          <w lemma="engage" ana="#vvn" reg="engaged" xml:id="A84905-1630" facs="A84905-001-a-1570">ingaged</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A84905-1640" facs="A84905-001-a-1580">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-1650" facs="A84905-001-a-1590">the</w>
          <c> </c>
          <w lemma="irish" ana="#j-nn" reg="Irish" xml:id="A84905-1660" facs="A84905-001-a-1600">Irish</w>
          <c> </c>
          <w lemma="service" ana="#n1" reg="service" xml:id="A84905-1670" facs="A84905-001-a-1610">Service</w>
          <pc xml:id="A84905-1680" facs="A84905-001-a-1620">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A84905-1690" facs="A84905-001-a-1630">be</w>
          <c> </c>
          <w lemma="discourage" ana="#vvn" reg="discouraged" xml:id="A84905-1700" facs="A84905-001-a-1640">discouraged</w>
          <pc xml:id="A84905-1710" facs="A84905-001-a-1650">,</pc>
          <c> </c>
          <w lemma="disturb" ana="#vvn" reg="disturbed" xml:id="A84905-1720" facs="A84905-001-a-1660">disturbed</w>
          <pc xml:id="A84905-1730" facs="A84905-001-a-1670">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84905-1740" facs="A84905-001-a-1680">or</w>
          <c> </c>
          <w lemma="interrupt" ana="#vvn" reg="interrupted" xml:id="A84905-1750" facs="A84905-001-a-1690">interrupted</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-1760" facs="A84905-001-a-1700">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A84905-1770" facs="A84905-001-a-1710">their</w>
          <c> </c>
          <w lemma="march" ana="#n1" reg="March" xml:id="A84905-1780" facs="A84905-001-a-1720">march</w>
          <pc xml:id="A84905-1790" facs="A84905-001-a-1730">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-1800" facs="A84905-001-a-1740">and</w>
          <c> </c>
          <w lemma="orderly" ana="#av_j" reg="orderly" xml:id="A84905-1810" facs="A84905-001-a-1750">orderly</w>
          <c> </c>
          <w lemma="quarter" ana="#j_vg" reg="quartering" xml:id="A84905-1820" facs="A84905-001-a-1760">quartering</w>
          <pc unit="sentence" xml:id="A84905-1830" facs="A84905-001-a-1770">.</pc>
        </p>
        <closer xml:id="A84905e-100">
          <dateline xml:id="A84905e-110">
            <w lemma="give" ana="#vvn" reg="given" xml:id="A84905-1870" facs="A84905-001-a-1780">Given</w>
            <c> </c>
            <w lemma="under" ana="#acp-p" reg="under" xml:id="A84905-1880" facs="A84905-001-a-1790">under</w>
            <c> </c>
            <w lemma="my" ana="#po" reg="my" xml:id="A84905-1890" facs="A84905-001-a-1800">my</w>
            <c> </c>
            <w lemma="hand" ana="#n1" reg="hand" xml:id="A84905-1900" facs="A84905-001-a-1810">Hand</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A84905-1910" facs="A84905-001-a-1820">and</w>
            <c> </c>
            <w lemma="seal" ana="#n1" reg="seal" xml:id="A84905-1920" facs="A84905-001-a-1830">Seal</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-1930" facs="A84905-001-a-1840">in</w>
            <c> </c>
            <w lemma="Queen-street" ana="#n1-nn" reg="Queen-street" xml:id="A84905-1940" facs="A84905-001-a-1850">Queen-street</w>
            <pc xml:id="A84905-1950" facs="A84905-001-a-1860">,</pc>
            <date xml:id="A84905e-120">
              <w lemma="this" ana="#d" reg="this" xml:id="A84905-1960" facs="A84905-001-a-1870">this</w>
              <c> </c>
              <w lemma="17." ana="#crd" reg="17." xml:id="A84905-1970" facs="A84905-001-a-1880">17.</w>
              <c> </c>
              <w lemma="day" ana="#n1" reg="day" xml:id="A84905-1980" facs="A84905-001-a-1890">day</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A84905-1990" facs="A84905-001-a-1900">of</w>
              <c> </c>
              <w lemma="march" ana="#n1" reg="March" xml:id="A84905-2000" facs="A84905-001-a-1910">March</w>
              <pc xml:id="A84905-2010" facs="A84905-001-a-1920">,</pc>
              <c> </c>
              <w lemma="1648." ana="#crd" reg="1648." xml:id="A84905-2020" facs="A84905-001-a-1930">1648.</w>
              <pc unit="sentence" xml:id="A84905-2020-eos" facs="A84905-001-a-1940"/>
            </date>
          </dateline>
          <signed xml:id="A84905e-130">
            <w lemma="t." ana="#n-ab" reg="T." xml:id="A84905-2050" facs="A84905-001-a-1950">T.</w>
            <c> </c>
            <w lemma="FAIFAX" ana="#n1-nn" reg="Faifax" xml:id="A84905-2060" facs="A84905-001-a-1960">FAIFAX</w>
            <pc unit="sentence" xml:id="A84905-2070" facs="A84905-001-a-1970">.</pc>
          </signed>
        </closer>
      </div>
      <div type="license" xml:id="A84905e-140">
        <p xml:id="A84905e-150">
          <w lemma="imprimatur" ana="#fw-la" reg="Imprimatur" xml:id="A84905-2110" facs="A84905-001-a-1980">Imprimatur</w>
          <pc unit="sentence" xml:id="A84905-2120" facs="A84905-001-a-1990">.</pc>
        </p>
        <closer xml:id="A84905e-160">
          <signed xml:id="A84905e-170">
            <hi xml:id="A84905e-180">
              <w lemma="Hen." ana="#n-ab" reg="hen." xml:id="A84905-2160" facs="A84905-001-a-2000">Hen.</w>
              <c> </c>
              <w lemma="Whalley" ana="#n1-nn" reg="Whalley" xml:id="A84905-2180" facs="A84905-001-a-2020">Whalley</w>
              <pc xml:id="A84905-2190" facs="A84905-001-a-2030">,</pc>
            </hi>
            <c> </c>
            <w lemma="advocate" ana="#n1" reg="advocate" xml:id="A84905-2200" facs="A84905-001-a-2040">Advocate</w>
            <pc unit="sentence" xml:id="A84905-2210" facs="A84905-001-a-2050">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A84905e-190">
      <div type="colophon" xml:id="A84905e-200">
        <p xml:id="A84905e-210">
          <hi xml:id="A84905e-220">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A84905-2260" facs="A84905-001-a-2060">London</w>
            <pc xml:id="A84905-2270" facs="A84905-001-a-2070">:</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A84905-2280" facs="A84905-001-a-2080">Printed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A84905-2290" facs="A84905-001-a-2090">for</w>
          <c> </c>
          <hi xml:id="A84905e-230">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A84905-2300" facs="A84905-001-a-2100">John</w>
            <c> </c>
            <w lemma="Playford" ana="#n1-nn" reg="Playford" xml:id="A84905-2310" facs="A84905-001-a-2110">Playford</w>
            <pc xml:id="A84905-2320" facs="A84905-001-a-2120">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84905-2330" facs="A84905-001-a-2130">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A84905-2340" facs="A84905-001-a-2140">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84905-2350" facs="A84905-001-a-2150">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A84905-2360" facs="A84905-001-a-2160">be</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A84905-2370" facs="A84905-001-a-2170">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84905-2380" facs="A84905-001-a-2180">at</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A84905-2390" facs="A84905-001-a-2190">his</w>
          <c> </c>
          <w lemma="shop" ana="#n1" reg="shop" xml:id="A84905-2400" facs="A84905-001-a-2200">shop</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84905-2410" facs="A84905-001-a-2210">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84905-2420" facs="A84905-001-a-2220">the</w>
          <c> </c>
          <w lemma="inner" ana="#j" reg="inner" xml:id="A84905-2430" facs="A84905-001-a-2230">Inner</w>
          <c> </c>
          <w lemma="temple" ana="#n1" reg="temple" xml:id="A84905-2440" facs="A84905-001-a-2240">Temple</w>
          <pc unit="sentence" xml:id="A84905-2450" facs="A84905-001-a-2250">.</pc>
          <c> </c>
          <w lemma="1649." ana="#crd" reg="1649." xml:id="A84905-2460" facs="A84905-001-a-2260">1649.</w>
          <pc unit="sentence" xml:id="A84905-2460-eos" facs="A84905-001-a-2270"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>