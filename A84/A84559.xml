<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>An act appointing judges for the admiralty.</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1659</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A84559</idno>
        <idno type="STC">Wing E979</idno>
        <idno type="STC">Thomason 669.f.21[37]</idno>
        <idno type="STC">ESTC R211187</idno>
        <idno type="EEBO-CITATION">99869918</idno>
        <idno type="PROQUEST">99869918</idno>
        <idno type="VID">163533</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A84559)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163533)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f21[37])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>An act appointing judges for the admiralty.</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by John Field, Printer to the Parliament. And are to be sold at the seven Stars in Fleetstreet, over against Dunstans Church,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1659.</date>
          </publicationStmt>
          <notesStmt>
            <note>Order to print dated: 9th of May 1659. Signed: Tho. St Nicholas Clerk of the Parliament.</note>
            <note>Annotation on Thomason copy: "May 20".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Admiralty -- England -- Early works to 1800.</term>
          <term>Judges -- England -- Selection and appointment -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A84559e-10">
    <body xml:id="A84559e-20">
      <div type="Act_of_Parliament" xml:id="A84559e-30">
        <pb facs="tcp:163533:1" rendition="simple:additions" xml:id="A84559-001-a"/>
        <head xml:id="A84559e-40">
          <figure xml:id="A84559e-50"/>
          <lb xml:id="A84559e-60"/>
          <w lemma="a" ana="#d" reg="an" xml:id="A84559-0030" facs="A84559-001-a-0010">AN</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A84559-0040" facs="A84559-001-a-0020">ACT</w>
          <c> </c>
          <w lemma="appoint" ana="#vvg" reg="appointing" xml:id="A84559-0050" facs="A84559-001-a-0030">APPOINTING</w>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A84559-0060" facs="A84559-001-a-0040">JUDGES</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A84559-0070" facs="A84559-001-a-0050">FOR</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0080" facs="A84559-001-a-0060">THE</w>
          <c> </c>
          <w lemma="admiralty" ana="#n1" reg="admiralty" xml:id="A84559-0090" facs="A84559-001-a-0070">ADMIRALTY</w>
          <pc unit="sentence" xml:id="A84559-0100" facs="A84559-001-a-0080">.</pc>
        </head>
        <p xml:id="A84559e-70">
          <w lemma="be" ana="#vvb" reg="Be" rend="initialcharacterdecorated" xml:id="A84559-0140" facs="A84559-001-a-0090">BE</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A84559-0150" facs="A84559-001-a-0100">it</w>
          <c> </c>
          <w lemma="enact" ana="#vvn" reg="enacted" xml:id="A84559-0160" facs="A84559-001-a-0110">Enacted</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84559-0170" facs="A84559-001-a-0120">by</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A84559-0180" facs="A84559-001-a-0130">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A84559-0190" facs="A84559-001-a-0140">present</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A84559-0200" facs="A84559-001-a-0150">Parliament</w>
          <pc xml:id="A84559-0210" facs="A84559-001-a-0160">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0220" facs="A84559-001-a-0170">and</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84559-0230" facs="A84559-001-a-0180">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0240" facs="A84559-001-a-0190">the</w>
          <c> </c>
          <w lemma="authority" ana="#n1" reg="authority" xml:id="A84559-0250" facs="A84559-001-a-0200">Authority</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A84559-0260" facs="A84559-001-a-0210">thereof</w>
          <pc xml:id="A84559-0270" facs="A84559-001-a-0220">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84559-0280" facs="A84559-001-a-0230">That</w>
          <c> </c>
          <hi xml:id="A84559e-80">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A84559-0290" facs="A84559-001-a-0240">John</w>
            <c> </c>
            <w lemma="Godolphin" ana="#n1-nn" reg="Godolphin" xml:id="A84559-0300" facs="A84559-001-a-0250">Godolphin</w>
            <pc xml:id="A84559-0310" facs="A84559-001-a-0260">,</pc>
          </hi>
          <c> </c>
          <w lemma="doctor" ana="#n1" reg="doctor" xml:id="A84559-0320" facs="A84559-001-a-0270">Doctor</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-0330" facs="A84559-001-a-0280">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0340" facs="A84559-001-a-0290">the</w>
          <c> </c>
          <w lemma="law" ana="#n2" reg="laws" xml:id="A84559-0350" facs="A84559-001-a-0300">Laws</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0360" facs="A84559-001-a-0310">and</w>
          <c> </c>
          <w lemma="charter" ana="#n2" reg="charters" xml:id="A84559-0370" facs="A84559-001-a-0320">Charters</w>
          <pc xml:id="A84559-0380" facs="A84559-001-a-0330">,</pc>
          <c> </c>
          <hi xml:id="A84559e-90">
            <w lemma="George" ana="#n1-nn" reg="George" xml:id="A84559-0390" facs="A84559-001-a-0340">George</w>
            <c> </c>
            <w lemma="Cocke" ana="#n1-nn" reg="Cocke" xml:id="A84559-0400" facs="A84559-001-a-0350">Cocke</w>
          </hi>
          <abbr xml:id="A84559e-100">
            <c> </c>
            <w lemma="esq" ana="#n-ab" reg="esq" xml:id="A84559-0410" facs="A84559-001-a-0360">Esq</w>
          </abbr>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A84559-0420" facs="A84559-001-a-0370">be</w>
          <pc xml:id="A84559-0430" facs="A84559-001-a-0380">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0440" facs="A84559-001-a-0390">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A84559-0450" facs="A84559-001-a-0400">are</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A84559-0460" facs="A84559-001-a-0410">hereby</w>
          <c> </c>
          <w lemma="nominate" ana="#vvn" reg="nominated" xml:id="A84559-0470" facs="A84559-001-a-0420">Nominated</w>
          <pc xml:id="A84559-0480" facs="A84559-001-a-0430">,</pc>
          <c> </c>
          <w lemma="constitute" ana="#vvn" reg="constituted" xml:id="A84559-0490" facs="A84559-001-a-0440">Constituted</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0500" facs="A84559-001-a-0450">and</w>
          <c> </c>
          <w lemma="appoint" ana="#vvn" reg="appointed" xml:id="A84559-0510" facs="A84559-001-a-0460">Appointed</w>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A84559-0520" facs="A84559-001-a-0470">Judges</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-0530" facs="A84559-001-a-0480">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0540" facs="A84559-001-a-0490">the</w>
          <c> </c>
          <w lemma="admiralty" ana="#n1" reg="admiralty" xml:id="A84559-0550" facs="A84559-001-a-0500">Admiralty</w>
          <pc unit="sentence" xml:id="A84559-0560" facs="A84559-001-a-0510">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A84559-0570" facs="A84559-001-a-0520">And</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0580" facs="A84559-001-a-0530">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A84559-0590" facs="A84559-001-a-0540">said</w>
          <c> </c>
          <hi xml:id="A84559e-110">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A84559-0600" facs="A84559-001-a-0550">John</w>
            <c> </c>
            <w lemma="Godolphin" ana="#n1-nn" reg="Godolphin" xml:id="A84559-0610" facs="A84559-001-a-0560">Godolphin</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0620" facs="A84559-001-a-0570">and</w>
          <c> </c>
          <hi xml:id="A84559e-120">
            <w lemma="Charles" ana="#n1-nn" reg="Charles" xml:id="A84559-0630" facs="A84559-001-a-0580">Charls</w>
            <c> </c>
            <w lemma="George" ana="#n1-nn" reg="George" xml:id="A84559-0640" facs="A84559-001-a-0590">George</w>
            <c> </c>
            <w lemma="cock" ana="#n1" reg="cock" xml:id="A84559-0650" facs="A84559-001-a-0600">Cocke</w>
            <pc xml:id="A84559-0660" facs="A84559-001-a-0610">,</pc>
          </hi>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A84559-0670" facs="A84559-001-a-0620">are</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A84559-0680" facs="A84559-001-a-0630">hereby</w>
          <c> </c>
          <w lemma="authorize" ana="#vvn" reg="authorised" xml:id="A84559-0690" facs="A84559-001-a-0640">Authorized</w>
          <pc xml:id="A84559-0700" facs="A84559-001-a-0650">,</pc>
          <c> </c>
          <w lemma="impower" ana="#vvn" reg="impowered" xml:id="A84559-0710" facs="A84559-001-a-0660">Impowered</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0720" facs="A84559-001-a-0670">and</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A84559-0730" facs="A84559-001-a-0680">Required</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84559-0740" facs="A84559-001-a-0690">to</w>
          <c> </c>
          <w lemma="hear" ana="#vvi" reg="hear" xml:id="A84559-0750" facs="A84559-001-a-0700">hear</w>
          <pc xml:id="A84559-0760" facs="A84559-001-a-0710">,</pc>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A84559-0770" facs="A84559-001-a-0720">Order</w>
          <pc xml:id="A84559-0780" facs="A84559-001-a-0730">,</pc>
          <c> </c>
          <w lemma="determine" ana="#vvb" reg="determine" xml:id="A84559-0790" facs="A84559-001-a-0740">Determine</w>
          <pc xml:id="A84559-0800" facs="A84559-001-a-0750">,</pc>
          <c> </c>
          <w lemma="adjudge" ana="#vvb" reg="adjudge" xml:id="A84559-0810" facs="A84559-001-a-0760">Adjudge</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0820" facs="A84559-001-a-0770">and</w>
          <c> </c>
          <w lemma="decree" ana="#n1" reg="decree" xml:id="A84559-0830" facs="A84559-001-a-0780">Decree</w>
          <pc xml:id="A84559-0840" facs="A84559-001-a-0790">,</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84559-0850" facs="A84559-001-a-0800">in</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A84559-0860" facs="A84559-001-a-0810">all</w>
          <c> </c>
          <w lemma="matter" ana="#n2" reg="matters" xml:id="A84559-0870" facs="A84559-001-a-0820">matters</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-0880" facs="A84559-001-a-0830">and</w>
          <c> </c>
          <w lemma="thing" ana="#n2" reg="things" xml:id="A84559-0890" facs="A84559-001-a-0840">things</w>
          <c> </c>
          <w lemma="as" ana="#acp-p" reg="as" xml:id="A84559-0900" facs="A84559-001-a-0850">as</w>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A84559-0910" facs="A84559-001-a-0860">Judges</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-0920" facs="A84559-001-a-0870">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-0930" facs="A84559-001-a-0880">the</w>
          <c> </c>
          <w lemma="admiralty" ana="#n1" reg="admiralty" xml:id="A84559-0940" facs="A84559-001-a-0890">Admiralty</w>
          <pc xml:id="A84559-0950" facs="A84559-001-a-0900">,</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84559-0960" facs="A84559-001-a-0910">in</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A84559-0970" facs="A84559-001-a-0920">as</w>
          <c> </c>
          <w lemma="full" ana="#j" reg="full" xml:id="A84559-0980" facs="A84559-001-a-0930">full</w>
          <c> </c>
          <w lemma="large" ana="#j" reg="large" xml:id="A84559-0990" facs="A84559-001-a-0940">large</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-1000" facs="A84559-001-a-0950">and</w>
          <c> </c>
          <w lemma="ample" ana="#j" reg="ample" xml:id="A84559-1010" facs="A84559-001-a-0960">ample</w>
          <c> </c>
          <w lemma="manner" ana="#n1" reg="manner" xml:id="A84559-1020" facs="A84559-001-a-0970">maner</w>
          <c> </c>
          <w lemma="as" ana="#acp-p" reg="as" xml:id="A84559-1030" facs="A84559-001-a-0980">as</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A84559-1040" facs="A84559-001-a-0990">any</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A84559-1050" facs="A84559-001-a-1000">other</w>
          <c> </c>
          <w lemma="judge" ana="#n1" reg="judge" xml:id="A84559-1060" facs="A84559-001-a-1010">Judge</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84559-1070" facs="A84559-001-a-1020">or</w>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A84559-1080" facs="A84559-001-a-1030">Judges</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1090" facs="A84559-001-a-1040">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-1100" facs="A84559-001-a-1050">the</w>
          <c> </c>
          <w lemma="admiralty" ana="#n1" reg="admiralty" xml:id="A84559-1110" facs="A84559-001-a-1060">Admiralty</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84559-1120" facs="A84559-001-a-1070">at</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A84559-1130" facs="A84559-001-a-1080">any</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A84559-1140" facs="A84559-001-a-1090">time</w>
          <c> </c>
          <w lemma="heretofore" ana="#av" reg="heretofore" xml:id="A84559-1150" facs="A84559-001-a-1100">heretofore</w>
          <c> </c>
          <w lemma="might" ana="#n1" reg="might" xml:id="A84559-1160" facs="A84559-001-a-1110">might</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84559-1170" facs="A84559-001-a-1120">or</w>
          <c> </c>
          <w lemma="ought" ana="#vmd" reg="ought" xml:id="A84559-1180" facs="A84559-001-a-1130">ought</w>
          <c> </c>
          <w lemma="lawful" ana="#av_j" reg="lawfully" xml:id="A84559-1190" facs="A84559-001-a-1140">lawfully</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84559-1200" facs="A84559-001-a-1150">to</w>
          <c> </c>
          <w lemma="do" ana="#vvi" reg="do" xml:id="A84559-1210" facs="A84559-001-a-1160">do</w>
          <pc xml:id="A84559-1220" facs="A84559-001-a-1170">:</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84559-1230" facs="A84559-001-a-1180">To</w>
          <c> </c>
          <w lemma="have" ana="#vvi" reg="have" xml:id="A84559-1240" facs="A84559-001-a-1190">have</w>
          <pc xml:id="A84559-1250" facs="A84559-001-a-1200">,</pc>
          <c> </c>
          <w lemma="hold" ana="#vvb" reg="hold" xml:id="A84559-1260" facs="A84559-001-a-1210">hold</w>
          <pc xml:id="A84559-1270" facs="A84559-001-a-1220">,</pc>
          <c> </c>
          <w lemma="exercise" ana="#n1" reg="exercise" xml:id="A84559-1280" facs="A84559-001-a-1230">exercise</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-1290" facs="A84559-001-a-1240">and</w>
          <c> </c>
          <w lemma="enjoy" ana="#vvi" reg="enjoy" xml:id="A84559-1300" facs="A84559-001-a-1250">enjoy</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-1310" facs="A84559-001-a-1260">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A84559-1320" facs="A84559-001-a-1270">said</w>
          <c> </c>
          <w lemma="office" ana="#n1" reg="office" xml:id="A84559-1330" facs="A84559-001-a-1280">Office</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84559-1340" facs="A84559-001-a-1290">or</w>
          <c> </c>
          <w lemma="place" ana="#n1" reg="place" xml:id="A84559-1350" facs="A84559-001-a-1300">Place</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1360" facs="A84559-001-a-1310">of</w>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A84559-1370" facs="A84559-001-a-1320">Judges</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1380" facs="A84559-001-a-1330">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-1390" facs="A84559-001-a-1340">the</w>
          <c> </c>
          <w lemma="admiralty" ana="#n1" reg="admiralty" xml:id="A84559-1400" facs="A84559-001-a-1350">Admiralty</w>
          <pc xml:id="A84559-1410" facs="A84559-001-a-1360">,</pc>
          <c> </c>
          <w lemma="until" ana="#acp-cs" reg="until" xml:id="A84559-1420" facs="A84559-001-a-1370">until</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-1430" facs="A84559-001-a-1380">the</w>
          <c> </c>
          <w lemma="thirty" ana="#ord" reg="thirtieth" xml:id="A84559-1440" facs="A84559-001-a-1390">Thirtieth</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A84559-1450" facs="A84559-001-a-1400">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1460" facs="A84559-001-a-1410">of</w>
          <c> </c>
          <hi xml:id="A84559e-130">
            <w lemma="June" ana="#n1-nn" reg="June" xml:id="A84559-1470" facs="A84559-001-a-1420">June</w>
          </hi>
          <c> </c>
          <w lemma="1659." ana="#crd" reg="1659." xml:id="A84559-1480" facs="A84559-001-a-1430">1659.</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-1490" facs="A84559-001-a-1440">and</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A84559-1500" facs="A84559-001-a-1450">no</w>
          <c> </c>
          <w lemma="long" ana="#av-c_j" reg="longer" xml:id="A84559-1510" facs="A84559-001-a-1460">longer</w>
          <pc unit="sentence" xml:id="A84559-1520" facs="A84559-001-a-1470">.</pc>
        </p>
      </div>
      <div type="license" xml:id="A84559e-140">
        <p xml:id="A84559e-150">
          <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A84559-1560" facs="A84559-001-a-1480">ORdered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84559-1570" facs="A84559-001-a-1490">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-1580" facs="A84559-001-a-1500">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A84559-1590" facs="A84559-001-a-1510">Parliament</w>
          <pc xml:id="A84559-1600" facs="A84559-001-a-1520">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84559-1610" facs="A84559-001-a-1530">That</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A84559-1620" facs="A84559-001-a-1540">this</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A84559-1630" facs="A84559-001-a-1550">Act</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A84559-1640" facs="A84559-001-a-1560">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A84559-1650" facs="A84559-001-a-1570">forthwith</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A84559-1660" facs="A84559-001-a-1580">Printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-1670" facs="A84559-001-a-1590">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A84559-1680" facs="A84559-001-a-1600">Published</w>
          <pc unit="sentence" xml:id="A84559-1690" facs="A84559-001-a-1610">.</pc>
        </p>
        <closer xml:id="A84559e-160">
          <dateline xml:id="A84559e-170">
            <w lemma="pass" ana="#vvn" reg="passed" xml:id="A84559-1730" facs="A84559-001-a-1620">Passed</w>
            <date xml:id="A84559e-180">
              <w lemma="the" ana="#d" reg="the" xml:id="A84559-1740" facs="A84559-001-a-1630">the</w>
              <c> </c>
              <w lemma="19th" part="I" ana="#ord" reg="19th" xml:id="A84559-1750.1" facs="A84559-001-a-1640" rendition="hi-mid-3">19th</w>
              <hi rend="sup" xml:id="A84559e-190"/>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1760" facs="A84559-001-a-1660">of</w>
              <c> </c>
              <hi xml:id="A84559e-200">
                <w lemma="May" ana="#n1-nn" reg="May" xml:id="A84559-1770" facs="A84559-001-a-1670">May</w>
              </hi>
              <c> </c>
              <w lemma="1659." ana="#crd" reg="1659." xml:id="A84559-1780" facs="A84559-001-a-1680">1659.</w>
              <pc unit="sentence" xml:id="A84559-1780-eos" facs="A84559-001-a-1690"/>
            </date>
          </dateline>
          <signed xml:id="A84559e-210">
            <w lemma="tho" ana="#av" reg="Tho" xml:id="A84559-1810" facs="A84559-001-a-1700">THO</w>
            <pc unit="sentence" xml:id="A84559-1820" facs="A84559-001-a-1710">.</pc>
            <c> </c>
            <w lemma="st" part="I" ana="#n-ab" reg="Saint" xml:id="A84559-1830.1" facs="A84559-001-a-1720" rendition="hi-mid-2">St</w>
            <hi rend="sup" xml:id="A84559e-220"/>
            <c> </c>
            <w lemma="NICHOLAS" ana="#n1-nn" reg="Nicholas" xml:id="A84559-1840" facs="A84559-001-a-1740">NICHOLAS</w>
            <c> </c>
            <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A84559-1850" facs="A84559-001-a-1750">Clerk</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A84559-1860" facs="A84559-001-a-1760">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A84559-1870" facs="A84559-001-a-1770">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A84559-1880" facs="A84559-001-a-1780">Parliament</w>
            <pc unit="sentence" xml:id="A84559-1890" facs="A84559-001-a-1790">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A84559e-230">
      <div type="colophon" xml:id="A84559e-240">
        <p xml:id="A84559e-250">
          <hi xml:id="A84559e-260">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A84559-1940" facs="A84559-001-a-1800">London</w>
            <pc xml:id="A84559-1950" facs="A84559-001-a-1810">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A84559-1960" facs="A84559-001-a-1820">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84559-1970" facs="A84559-001-a-1830">by</w>
          <c> </c>
          <w lemma="JOHN" ana="#n1-nn" reg="John" xml:id="A84559-1980" facs="A84559-001-a-1840">JOHN</w>
          <c> </c>
          <w lemma="field" ana="#n1" reg="field" xml:id="A84559-1990" facs="A84559-001-a-1850">FIELD</w>
          <pc xml:id="A84559-2000" facs="A84559-001-a-1860">,</pc>
          <c> </c>
          <w lemma="printer" ana="#n1" reg="printer" xml:id="A84559-2010" facs="A84559-001-a-1870">Printer</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A84559-2020" facs="A84559-001-a-1880">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-2030" facs="A84559-001-a-1890">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A84559-2040" facs="A84559-001-a-1900">Parliament</w>
          <pc xml:id="A84559-2050" facs="A84559-001-a-1910">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84559-2060" facs="A84559-001-a-1920">And</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A84559-2070" facs="A84559-001-a-1930">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84559-2080" facs="A84559-001-a-1940">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A84559-2090" facs="A84559-001-a-1950">be</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A84559-2100" facs="A84559-001-a-1960">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84559-2110" facs="A84559-001-a-1970">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84559-2120" facs="A84559-001-a-1980">the</w>
          <c> </c>
          <w lemma="seven" ana="#crd" reg="seven" xml:id="A84559-2130" facs="A84559-001-a-1990">seven</w>
          <c> </c>
          <w lemma="star" ana="#n2" reg="stars" xml:id="A84559-2140" facs="A84559-001-a-2000">Stars</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84559-2150" facs="A84559-001-a-2010">in</w>
          <c> </c>
          <hi xml:id="A84559e-270">
            <w lemma="Fleetstreet" ana="#n1-nn" reg="Fleetstreet" xml:id="A84559-2160" facs="A84559-001-a-2020">Fleetstreet</w>
            <pc xml:id="A84559-2170" facs="A84559-001-a-2030">,</pc>
          </hi>
          <c> </c>
          <w lemma="over" ana="#acp-av" reg="over" xml:id="A84559-2180" facs="A84559-001-a-2040">over</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A84559-2190" facs="A84559-001-a-2050">against</w>
          <c> </c>
          <hi xml:id="A84559e-280">
            <w lemma="Dunstan" ana="#n1g-nn" reg="Dunstan's" xml:id="A84559-2200" facs="A84559-001-a-2060">Dunstans</w>
          </hi>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A84559-2210" facs="A84559-001-a-2070">Church</w>
          <pc xml:id="A84559-2220" facs="A84559-001-a-2080">,</pc>
          <c> </c>
          <w lemma="1659." ana="#crd" reg="1659." xml:id="A84559-2230" facs="A84559-001-a-2090">1659.</w>
          <pc unit="sentence" xml:id="A84559-2230-eos" facs="A84559-001-a-2100"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>