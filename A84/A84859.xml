<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>All gentlemen and others, may be pleased to take notice, that there is a stranger come into these parts, whose name is Peter Francesse that hath brought with him out of the kingdome of Persia, perfect remedy for the gout, the sciatica, the running gout, and all aches in the limbs, ...</title>
        <author>Francesse, Peter.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1656</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A84859</idno>
        <idno type="STC">Wing F2055</idno>
        <idno type="STC">Thomason 669.f.20[41]</idno>
        <idno type="STC">ESTC R211874</idno>
        <idno type="EEBO-CITATION">99870551</idno>
        <idno type="PROQUEST">99870551</idno>
        <idno type="VID">163458</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A84859)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163458)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f20[41])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>All gentlemen and others, may be pleased to take notice, that there is a stranger come into these parts, whose name is Peter Francesse that hath brought with him out of the kingdome of Persia, perfect remedy for the gout, the sciatica, the running gout, and all aches in the limbs, ...</title>
            <author>Francesse, Peter.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1656]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from first lines of text.</note>
            <note>Imprint from Wing.</note>
            <note>An advertisement of a cure for gout and sciatica offered by Peter Francesse.--Thomason catalogue.</note>
            <note>Annotation on Thomason copy: "London Decemb. 1656".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Medicine, Popular -- England -- Early works to 1800.</term>
          <term>Patent medicines -- England -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-09</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-10</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-10</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A84859e-10">
    <body xml:id="A84859e-20">
      <div type="handbill" xml:id="A84859e-30">
        <pb facs="tcp:163458:1" rendition="simple:additions" xml:id="A84859-001-a"/>
        <p xml:id="A84859e-40">
          <w lemma="all" ana="#av_d" reg="All" xml:id="A84859-0030" facs="A84859-001-a-0010">ALl</w>
          <c> </c>
          <w lemma="gentleman" ana="#n2" reg="gentlemen" xml:id="A84859-0040" facs="A84859-001-a-0020">Gentlemen</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84859-0050" facs="A84859-001-a-0030">and</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A84859-0060" facs="A84859-001-a-0040">others</w>
          <pc xml:id="A84859-0070" facs="A84859-001-a-0050">,</pc>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A84859-0080" facs="A84859-001-a-0060">may</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A84859-0090" facs="A84859-001-a-0070">be</w>
          <c> </c>
          <w lemma="please" ana="#vvn" reg="pleased" xml:id="A84859-0100" facs="A84859-001-a-0080">pleased</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A84859-0110" facs="A84859-001-a-0090">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A84859-0120" facs="A84859-001-a-0100">take</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A84859-0130" facs="A84859-001-a-0110">notice</w>
          <pc xml:id="A84859-0140" facs="A84859-001-a-0120">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84859-0150" facs="A84859-001-a-0130">that</w>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A84859-0160" facs="A84859-001-a-0140">there</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A84859-0170" facs="A84859-001-a-0150">is</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A84859-0180" facs="A84859-001-a-0160">a</w>
          <c> </c>
          <w lemma="stranger" ana="#n1" reg="stranger" xml:id="A84859-0190" facs="A84859-001-a-0170">Stranger</w>
          <c> </c>
          <w lemma="come" ana="#vvn" reg="come" xml:id="A84859-0200" facs="A84859-001-a-0180">come</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A84859-0210" facs="A84859-001-a-0190">into</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A84859-0220" facs="A84859-001-a-0200">these</w>
          <c> </c>
          <w lemma="part" ana="#n2" reg="parts" xml:id="A84859-0230" facs="A84859-001-a-0210">parts</w>
          <pc xml:id="A84859-0240" facs="A84859-001-a-0220">,</pc>
          <c> </c>
          <w lemma="who" ana="#crq-rg" reg="whose" xml:id="A84859-0250" facs="A84859-001-a-0230">whose</w>
          <c> </c>
          <w lemma="name" ana="#n1" reg="name" xml:id="A84859-0260" facs="A84859-001-a-0240">name</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A84859-0270" facs="A84859-001-a-0250">is</w>
          <c> </c>
          <hi xml:id="A84859e-50">
            <w lemma="Peter" ana="#n1-nn" reg="Peter" xml:id="A84859-0280" facs="A84859-001-a-0260">Peter</w>
            <c> </c>
            <w lemma="francess" ana="#n1-nn" reg="Francesse" xml:id="A84859-0290" facs="A84859-001-a-0270">Francesse</w>
          </hi>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84859-0300" facs="A84859-001-a-0280">that</w>
          <c> </c>
          <w lemma="have" ana="#vvz" reg="hath" xml:id="A84859-0310" facs="A84859-001-a-0290">hath</w>
          <c> </c>
          <w lemma="bring" ana="#vvn" reg="brought" xml:id="A84859-0320" facs="A84859-001-a-0300">brought</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A84859-0330" facs="A84859-001-a-0310">with</w>
          <c> </c>
          <w lemma="he" ana="#pno" reg="him" xml:id="A84859-0340" facs="A84859-001-a-0320">him</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A84859-0350" facs="A84859-001-a-0330">out</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-0360" facs="A84859-001-a-0340">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0370" facs="A84859-001-a-0350">the</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A84859-0380" facs="A84859-001-a-0360">Kingdome</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-0390" facs="A84859-001-a-0370">of</w>
          <c> </c>
          <hi xml:id="A84859e-60">
            <w lemma="Persia" ana="#n1-nn" reg="Persia" xml:id="A84859-0400" facs="A84859-001-a-0380">Persia</w>
            <pc xml:id="A84859-0410" facs="A84859-001-a-0390">,</pc>
          </hi>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A84859-0420" facs="A84859-001-a-0400">a</w>
          <c> </c>
          <w lemma="perfect" ana="#j" reg="perfect" xml:id="A84859-0430" facs="A84859-001-a-0410">perfect</w>
          <c> </c>
          <w lemma="remedy" ana="#n1" reg="remedy" xml:id="A84859-0440" facs="A84859-001-a-0420">Remedy</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A84859-0450" facs="A84859-001-a-0430">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0460" facs="A84859-001-a-0440">the</w>
          <c> </c>
          <w lemma="gout" ana="#n1" reg="gout" xml:id="A84859-0470" facs="A84859-001-a-0450">Gout</w>
          <pc xml:id="A84859-0480" facs="A84859-001-a-0460">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0490" facs="A84859-001-a-0470">the</w>
          <c> </c>
          <w lemma="sciatica" ana="#n1" reg="sciatica" xml:id="A84859-0500" facs="A84859-001-a-0480">Sciatica</w>
          <pc xml:id="A84859-0510" facs="A84859-001-a-0490">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0520" facs="A84859-001-a-0500">the</w>
          <c> </c>
          <w lemma="run" ana="#j_vg" reg="running" xml:id="A84859-0530" facs="A84859-001-a-0510">running</w>
          <c> </c>
          <w lemma="gout" ana="#n1" reg="gout" xml:id="A84859-0540" facs="A84859-001-a-0520">Gout</w>
          <pc xml:id="A84859-0550" facs="A84859-001-a-0530">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84859-0560" facs="A84859-001-a-0540">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A84859-0570" facs="A84859-001-a-0550">all</w>
          <c> </c>
          <w lemma="ache" ana="#n2" reg="aches" xml:id="A84859-0580" facs="A84859-001-a-0560">Aches</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-0590" facs="A84859-001-a-0570">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0600" facs="A84859-001-a-0580">the</w>
          <c> </c>
          <w lemma="limb" ana="#n2" reg="limbs" xml:id="A84859-0610" facs="A84859-001-a-0590">Limbs</w>
          <pc xml:id="A84859-0620" facs="A84859-001-a-0600">,</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-0630" facs="A84859-001-a-0610">in</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A84859-0640" facs="A84859-001-a-0620">all</w>
          <c> </c>
          <w lemma="part" ana="#n2" reg="parts" xml:id="A84859-0650" facs="A84859-001-a-0630">parts</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-0660" facs="A84859-001-a-0640">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0670" facs="A84859-001-a-0650">the</w>
          <c> </c>
          <w lemma="body" ana="#n1" reg="body" xml:id="A84859-0680" facs="A84859-001-a-0660">body</w>
          <pc unit="sentence" xml:id="A84859-0690" facs="A84859-001-a-0670">.</pc>
          <c> </c>
          <w lemma="also" ana="#av" reg="Also" xml:id="A84859-0700" facs="A84859-001-a-0680">Also</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0710" facs="A84859-001-a-0690">the</w>
          <c> </c>
          <w lemma="disease" ana="#n1" reg="disease" xml:id="A84859-0720" facs="A84859-001-a-0700">Disease</w>
          <c> </c>
          <w lemma="common" ana="#av_j" reg="commonly" xml:id="A84859-0730" facs="A84859-001-a-0710">commonly</w>
          <c> </c>
          <w lemma="call" ana="#vvn" reg="called" xml:id="A84859-0740" facs="A84859-001-a-0720">called</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0750" facs="A84859-001-a-0730">the</w>
          <c> </c>
          <w lemma="king" ana="#n1g" reg="King's" xml:id="A84859-0760" facs="A84859-001-a-0740">Kings</w>
          <c> </c>
          <w lemma="evil" ana="#j" reg="evil" xml:id="A84859-0770" facs="A84859-001-a-0750">Evill</w>
          <pc xml:id="A84859-0780" facs="A84859-001-a-0760">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0790" facs="A84859-001-a-0770">the</w>
          <c> </c>
          <w lemma="palsy" ana="#n1" reg="palsy" xml:id="A84859-0800" facs="A84859-001-a-0780">Palsie</w>
          <pc xml:id="A84859-0810" facs="A84859-001-a-0790">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84859-0820" facs="A84859-001-a-0800">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A84859-0830" facs="A84859-001-a-0810">any</w>
          <c> </c>
          <w lemma="benumbedness" ana="#n1" reg="benumbedness" xml:id="A84859-0840" facs="A84859-001-a-0820">Benumbednesse</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-0850" facs="A84859-001-a-0830">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-0860" facs="A84859-001-a-0840">the</w>
          <c> </c>
          <w lemma="limb" ana="#n2" reg="limbs" xml:id="A84859-0870" facs="A84859-001-a-0850">Limbs</w>
          <pc unit="sentence" xml:id="A84859-0880" facs="A84859-001-a-0860">.</pc>
        </p>
        <p xml:id="A84859e-70">
          <hi xml:id="A84859e-80">
            <w lemma="the" ana="#d" reg="the" xml:id="A84859-0910" facs="A84859-001-a-0870">The</w>
            <c> </c>
            <w lemma="professor" ana="#n1" reg="professor" xml:id="A84859-0920" facs="A84859-001-a-0880">Professor</w>
            <c> </c>
            <w lemma="hereof" ana="#av" reg="hereof" xml:id="A84859-0930" facs="A84859-001-a-0890">hereof</w>
            <c> </c>
            <w lemma="do" ana="#vvz" reg="doth" xml:id="A84859-0940" facs="A84859-001-a-0900">doth</w>
            <c> </c>
            <w lemma="cure" ana="#n1" reg="cure" xml:id="A84859-0950" facs="A84859-001-a-0910">Cure</w>
            <c> </c>
            <w lemma="all" ana="#d" reg="all" xml:id="A84859-0960" facs="A84859-001-a-0920">all</w>
            <c> </c>
            <w lemma="these" ana="#d" reg="these" xml:id="A84859-0970" facs="A84859-001-a-0930">these</w>
            <c> </c>
            <w lemma="disease" ana="#n2" reg="diseases" xml:id="A84859-0980" facs="A84859-001-a-0940">Diseases</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A84859-0990" facs="A84859-001-a-0950">by</w>
            <c> </c>
            <w lemma="plaster" ana="#n2" reg="plasters" xml:id="A84859-1000" facs="A84859-001-a-0960">Plaisters</w>
            <pc xml:id="A84859-1010" facs="A84859-001-a-0970">,</pc>
            <c> </c>
            <w lemma="without" ana="#acp-p" reg="without" xml:id="A84859-1020" facs="A84859-001-a-0980">without</w>
            <c> </c>
            <w lemma="apply" ana="#vvg" reg="applying" xml:id="A84859-1030" facs="A84859-001-a-0990">applying</w>
            <c> </c>
            <w lemma="any" ana="#d" reg="any" xml:id="A84859-1040" facs="A84859-001-a-1000">any</w>
            <c> </c>
            <w lemma="thing" ana="#n1" reg="thing" xml:id="A84859-1050" facs="A84859-001-a-1010">thing</w>
            <c> </c>
            <w lemma="inward" ana="#av_j" reg="inwardly" xml:id="A84859-1060" facs="A84859-001-a-1020">inwardly</w>
            <pc unit="sentence" xml:id="A84859-1070" facs="A84859-001-a-1030">.</pc>
            <c> </c>
          </hi>
        </p>
        <p xml:id="A84859e-90">
          <w lemma="he" ana="#pns" reg="He" xml:id="A84859-1100" facs="A84859-001-a-1040">He</w>
          <c> </c>
          <w lemma="give" ana="#vvz" reg="giveth" xml:id="A84859-1110" facs="A84859-001-a-1050">giveth</w>
          <c> </c>
          <w lemma="ease" ana="#n1" reg="ease" xml:id="A84859-1120" facs="A84859-001-a-1060">ease</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-1130" facs="A84859-001-a-1070">in</w>
          <c> </c>
          <w lemma="one" ana="#crd" reg="one" xml:id="A84859-1140" facs="A84859-001-a-1080">one</w>
          <c> </c>
          <w lemma="hour" ana="#n2g" reg="hour's" xml:id="A84859-1150" facs="A84859-001-a-1090">houres</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A84859-1160" facs="A84859-001-a-1100">time</w>
          <pc xml:id="A84859-1170" facs="A84859-001-a-1110">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84859-1180" facs="A84859-001-a-1120">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-1190" facs="A84859-001-a-1130">the</w>
          <c> </c>
          <w lemma="party" ana="#n1" reg="party" xml:id="A84859-1200" facs="A84859-001-a-1140">Party</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A84859-1210" facs="A84859-001-a-1150">shall</w>
          <c> </c>
          <w lemma="never" ana="#av-x" reg="never" xml:id="A84859-1220" facs="A84859-001-a-1160">never</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A84859-1230" facs="A84859-001-a-1170">bee</w>
          <c> </c>
          <w lemma="trouble" ana="#vvn" reg="troubled" xml:id="A84859-1240" facs="A84859-001-a-1180">troubled</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A84859-1250" facs="A84859-001-a-1190">with</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A84859-1260" facs="A84859-001-a-1200">these</w>
          <c> </c>
          <w lemma="disease" ana="#n2" reg="diseases" xml:id="A84859-1270" facs="A84859-001-a-1210">Diseases</w>
          <c> </c>
          <w lemma="again" ana="#av" reg="again" xml:id="A84859-1280" facs="A84859-001-a-1220">again</w>
          <pc xml:id="A84859-1290" facs="A84859-001-a-1230">,</pc>
          <c> </c>
          <w lemma="after" ana="#acp-cs" reg="after" xml:id="A84859-1300" facs="A84859-001-a-1240">after</w>
          <c> </c>
          <w lemma="he" ana="#pns" reg="he" xml:id="A84859-1310" facs="A84859-001-a-1250">he</w>
          <c> </c>
          <w lemma="have" ana="#vvz" reg="hath" xml:id="A84859-1320" facs="A84859-001-a-1260">hath</w>
          <c> </c>
          <w lemma="cure" ana="#vvn" reg="cured" xml:id="A84859-1330" facs="A84859-001-a-1270">cured</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A84859-1340" facs="A84859-001-a-1280">them</w>
          <pc unit="sentence" xml:id="A84859-1350" facs="A84859-001-a-1290">.</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="The" xml:id="A84859-1360" facs="A84859-001-a-1300">The</w>
          <c> </c>
          <w lemma="truth" ana="#n1" reg="truth" xml:id="A84859-1370" facs="A84859-001-a-1310">truth</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-1380" facs="A84859-001-a-1320">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A84859-1390" facs="A84859-001-a-1330">this</w>
          <c> </c>
          <w lemma="many" ana="#d" reg="many" xml:id="A84859-1400" facs="A84859-001-a-1340">many</w>
          <c> </c>
          <w lemma="can" ana="#vmb" reg="can" xml:id="A84859-1410" facs="A84859-001-a-1350">can</w>
          <c> </c>
          <w lemma="testify" ana="#vvi" reg="testify" xml:id="A84859-1420" facs="A84859-001-a-1360">testify</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A84859-1430" facs="A84859-001-a-1370">that</w>
          <c> </c>
          <w lemma="be" ana="#vvd" reg="was" xml:id="A84859-1440" facs="A84859-001-a-1380">was</w>
          <c> </c>
          <w lemma="cure" ana="#j_vn" reg="cured" xml:id="A84859-1450" facs="A84859-001-a-1390">Cured</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A84859-1460" facs="A84859-001-a-1400">by</w>
          <c> </c>
          <w lemma="he" ana="#pno" reg="him" xml:id="A84859-1470" facs="A84859-001-a-1410">him</w>
          <c> </c>
          <w lemma="twenty" ana="#crd" reg="twenty" xml:id="A84859-1480" facs="A84859-001-a-1420">twenty</w>
          <c> </c>
          <w lemma="year" ana="#n2" reg="years" xml:id="A84859-1490" facs="A84859-001-a-1430">years</w>
          <c> </c>
          <w lemma="ago" ana="#av" reg="ago" xml:id="A84859-1500" facs="A84859-001-a-1440">ago</w>
          <pc unit="sentence" xml:id="A84859-1510" facs="A84859-001-a-1450">.</pc>
        </p>
        <p xml:id="A84859e-100">
          <w lemma="you" ana="#pn" reg="You" xml:id="A84859-1540" facs="A84859-001-a-1460">You</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A84859-1550" facs="A84859-001-a-1470">may</w>
          <c> </c>
          <w lemma="hear" ana="#vvi" reg="hear" xml:id="A84859-1560" facs="A84859-001-a-1480">hear</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-1570" facs="A84859-001-a-1490">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A84859-1580" facs="A84859-001-a-1500">this</w>
          <c> </c>
          <w lemma="gentleman" ana="#n1" reg="gentleman" xml:id="A84859-1590" facs="A84859-001-a-1510">Gentleman</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84859-1600" facs="A84859-001-a-1520">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-1610" facs="A84859-001-a-1530">the</w>
          <c> </c>
          <hi xml:id="A84859e-110">
            <w lemma="bosom" ana="#n1g" reg="bosom's" xml:id="A84859-1620" facs="A84859-001-a-1540">Bosomes</w>
            <c> </c>
            <w lemma="inn" ana="#n1" reg="inn" xml:id="A84859-1630" facs="A84859-001-a-1550">Inne</w>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-1640" facs="A84859-001-a-1560">in</w>
          <c> </c>
          <hi xml:id="A84859e-120">
            <w lemma="Laurence" ana="#n1-nn" reg="Lawrence" xml:id="A84859-1650" facs="A84859-001-a-1570">Lawrence</w>
            <c> </c>
            <w lemma="lane" ana="#n1" reg="lane" xml:id="A84859-1660" facs="A84859-001-a-1580">Lane</w>
            <pc xml:id="A84859-1670" facs="A84859-001-a-1590">,</pc>
          </hi>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84859-1680" facs="A84859-001-a-1600">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-1690" facs="A84859-001-a-1610">the</w>
          <c> </c>
          <w lemma="low" ana="#j-c" reg="lower" xml:id="A84859-1700" facs="A84859-001-a-1620">lower</w>
          <c> </c>
          <w lemma="end" ana="#n1" reg="end" xml:id="A84859-1710" facs="A84859-001-a-1630">end</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-1720" facs="A84859-001-a-1640">of</w>
          <c> </c>
          <hi xml:id="A84859e-130">
            <w lemma="cheap" ana="#j" reg="cheap" xml:id="A84859-1730" facs="A84859-001-a-1650">Cheape</w>
            <c> </c>
            <w lemma="side" ana="#n1" reg="side" xml:id="A84859-1740" facs="A84859-001-a-1660">side</w>
            <pc xml:id="A84859-1750" facs="A84859-001-a-1670">,</pc>
          </hi>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A84859-1760" facs="A84859-001-a-1680">or</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84859-1770" facs="A84859-001-a-1690">at</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A84859-1780" facs="A84859-001-a-1700">his</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A84859-1790" facs="A84859-001-a-1710">house</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-1800" facs="A84859-001-a-1720">in</w>
          <c> </c>
          <hi xml:id="A84859e-140">
            <w lemma="little" ana="#j" reg="little" xml:id="A84859-1810" facs="A84859-001-a-1730">Little</w>
            <c> </c>
            <w lemma="Moorefields" ana="#n1-nn" reg="Moorefields" xml:id="A84859-1820" facs="A84859-001-a-1740">Moorefields</w>
            <pc xml:id="A84859-1830" facs="A84859-001-a-1750">,</pc>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A84859-1840" facs="A84859-001-a-1760">in</w>
          <c> </c>
          <hi xml:id="A84859e-150">
            <w lemma="White" ana="#n1g-nn" reg="White's" xml:id="A84859-1850" facs="A84859-001-a-1770">Whites</w>
            <c> </c>
            <w lemma="alley" ana="#n1" reg="alley" xml:id="A84859-1860" facs="A84859-001-a-1780">Alley</w>
            <pc xml:id="A84859-1870" facs="A84859-001-a-1790">,</pc>
          </hi>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A84859-1880" facs="A84859-001-a-1800">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-1890" facs="A84859-001-a-1810">the</w>
          <c> </c>
          <w lemma="corner" ana="#n1" reg="corner" xml:id="A84859-1900" facs="A84859-001-a-1820">corner</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-1910" facs="A84859-001-a-1830">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A84859-1920" facs="A84859-001-a-1840">the</w>
          <c> </c>
          <hi xml:id="A84859e-160">
            <w lemma="black" ana="#j" reg="black" xml:id="A84859-1930" facs="A84859-001-a-1850">Black</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A84859-1940" facs="A84859-001-a-1860">and</w>
          <c> </c>
          <hi xml:id="A84859e-170">
            <w lemma="white" ana="#j" reg="white" xml:id="A84859-1950" facs="A84859-001-a-1870">White</w>
            <c> </c>
            <w lemma="house" ana="#n1" reg="house" xml:id="A84859-1960" facs="A84859-001-a-1880">House</w>
            <pc xml:id="A84859-1970" facs="A84859-001-a-1890">,</pc>
          </hi>
          <c> </c>
          <w lemma="where" ana="#crq-cs" reg="where" xml:id="A84859-1980" facs="A84859-001-a-1900">where</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A84859-1990" facs="A84859-001-a-1910">one</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A84859-2000" facs="A84859-001-a-1920">of</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A84859-2010" facs="A84859-001-a-1930">these</w>
          <c> </c>
          <w lemma="bill" ana="#n2" reg="bills" xml:id="A84859-2020" facs="A84859-001-a-1940">Bills</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A84859-2030" facs="A84859-001-a-1950">shall</w>
          <c> </c>
          <w lemma="stick" ana="#vvi" reg="stick" xml:id="A84859-2040" facs="A84859-001-a-1960">stick</w>
          <pc unit="sentence" xml:id="A84859-2050" facs="A84859-001-a-1970">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A84859e-180">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>