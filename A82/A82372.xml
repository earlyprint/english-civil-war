<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>An act for continuing the High Court of Justice.</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1651</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A82372</idno>
        <idno type="STC">Wing E1023</idno>
        <idno type="STC">Thomason 669.f.16[26]</idno>
        <idno type="STC">ESTC R211379</idno>
        <idno type="EEBO-CITATION">99870107</idno>
        <idno type="PROQUEST">99870107</idno>
        <idno type="VID">163183</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A82372)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163183)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[26])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>An act for continuing the High Court of Justice.</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by John Field, Printer to the Parliament of England,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1651.</date>
          </publicationStmt>
          <notesStmt>
            <note>Order to print dated: Thursday the Five and twentieth of September, 1651. Signed: Hen: Scobell, Cleric. Parliamenti.</note>
            <note>With engraving of Parliamentary seal at head of text.</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Courts -- England -- Early works to 1800.</term>
          <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A82372e-10">
    <body xml:id="A82372e-20">
      <div type="text" xml:id="A82372e-30">
        <pb facs="tcp:163183:1" rendition="simple:additions" xml:id="A82372-001-a"/>
        <head xml:id="A82372e-40">
          <figure xml:id="A82372e-50">
            <figDesc xml:id="A82372e-60">blazon or coat of arms incorporating the Commonwealth Flag (1649-1651)</figDesc>
          </figure>
          <w lemma="a" ana="#d" reg="an" xml:id="A82372-0030" facs="A82372-001-a-0010">AN</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A82372-0040" facs="A82372-001-a-0020">ACT</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A82372-0050" facs="A82372-001-a-0030">For</w>
          <c> </c>
          <w lemma="continue" ana="#vvg" reg="continuing" xml:id="A82372-0060" facs="A82372-001-a-0040">Continuing</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0070" facs="A82372-001-a-0050">the</w>
          <c> </c>
          <w lemma="high" ana="#j" reg="high" xml:id="A82372-0080" facs="A82372-001-a-0060">High</w>
          <c> </c>
          <w lemma="court" ana="#n1" reg="court" xml:id="A82372-0090" facs="A82372-001-a-0070">Court</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0100" facs="A82372-001-a-0080">of</w>
          <c> </c>
          <w lemma="justice" ana="#n1" reg="justice" xml:id="A82372-0110" facs="A82372-001-a-0090">Justice</w>
          <pc unit="sentence" xml:id="A82372-0120" facs="A82372-001-a-0100">.</pc>
        </head>
        <p xml:id="A82372e-70">
          <w lemma="be" ana="#vvb" reg="Be" rend="initialcharacterdecorated" xml:id="A82372-0290" facs="A82372-001-a-0110">BE</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A82372-0300" facs="A82372-001-a-0120">it</w>
          <c> </c>
          <w lemma="enact" ana="#vvn" reg="enacted" xml:id="A82372-0310" facs="A82372-001-a-0130">Enacted</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82372-0320" facs="A82372-001-a-0140">by</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A82372-0330" facs="A82372-001-a-0150">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A82372-0340" facs="A82372-001-a-0160">present</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82372-0350" facs="A82372-001-a-0170">Parliament</w>
          <pc xml:id="A82372-0360" facs="A82372-001-a-0180">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0370" facs="A82372-001-a-0190">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0380" facs="A82372-001-a-0200">the</w>
          <c> </c>
          <w lemma="authority" ana="#n1" reg="authority" xml:id="A82372-0390" facs="A82372-001-a-0210">Authority</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A82372-0400" facs="A82372-001-a-0220">thereof</w>
          <pc xml:id="A82372-0410" facs="A82372-001-a-0230">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A82372-0420" facs="A82372-001-a-0240">That</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82372-0430" facs="A82372-001-a-0250">all</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0440" facs="A82372-001-a-0260">and</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A82372-0450" facs="A82372-001-a-0270">every</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0460" facs="A82372-001-a-0280">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A82372-0470" facs="A82372-001-a-0290">respective</w>
          <c> </c>
          <w lemma="commissioner" ana="#n2" reg="commissioners" xml:id="A82372-0480" facs="A82372-001-a-0300">Commissioners</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0490" facs="A82372-001-a-0310">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0500" facs="A82372-001-a-0320">the</w>
          <c> </c>
          <w lemma="high" ana="#j" reg="high" xml:id="A82372-0510" facs="A82372-001-a-0330">High</w>
          <c> </c>
          <w lemma="court" ana="#n1" reg="court" xml:id="A82372-0520" facs="A82372-001-a-0340">Court</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0530" facs="A82372-001-a-0350">of</w>
          <c> </c>
          <w lemma="justice" ana="#n1" reg="justice" xml:id="A82372-0540" facs="A82372-001-a-0360">Iustice</w>
          <pc xml:id="A82372-0550" facs="A82372-001-a-0370">,</pc>
          <c> </c>
          <w lemma="constitute" ana="#vvn" reg="constituted" xml:id="A82372-0560" facs="A82372-001-a-0380">Constituted</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0570" facs="A82372-001-a-0390">and</w>
          <c> </c>
          <w lemma="appoint" ana="#vvn" reg="appointed" xml:id="A82372-0580" facs="A82372-001-a-0400">Appointed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82372-0590" facs="A82372-001-a-0410">by</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A82372-0600" facs="A82372-001-a-0420">several</w>
          <c> </c>
          <w lemma="act" ana="#n2" reg="acts" xml:id="A82372-0610" facs="A82372-001-a-0430">Acts</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0620" facs="A82372-001-a-0440">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A82372-0630" facs="A82372-001-a-0450">this</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A82372-0640" facs="A82372-001-a-0460">present</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82372-0650" facs="A82372-001-a-0470">Parliament</w>
          <pc xml:id="A82372-0660" facs="A82372-001-a-0480">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0670" facs="A82372-001-a-0490">and</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A82372-0680" facs="A82372-001-a-0500">now</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A82372-0690" facs="A82372-001-a-0510">Members</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0700" facs="A82372-001-a-0520">of</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A82372-0710" facs="A82372-001-a-0530">that</w>
          <c> </c>
          <w lemma="court" ana="#n1" reg="court" xml:id="A82372-0720" facs="A82372-001-a-0540">Court</w>
          <pc xml:id="A82372-0730" facs="A82372-001-a-0550">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0740" facs="A82372-001-a-0560">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82372-0750" facs="A82372-001-a-0570">all</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0760" facs="A82372-001-a-0580">and</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A82372-0770" facs="A82372-001-a-0590">every</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0780" facs="A82372-001-a-0600">the</w>
          <c> </c>
          <w lemma="power" ana="#n2" reg="powers" xml:id="A82372-0790" facs="A82372-001-a-0610">Powers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0800" facs="A82372-001-a-0620">and</w>
          <c> </c>
          <w lemma="authority" ana="#n2" reg="authorities" xml:id="A82372-0810" facs="A82372-001-a-0630">Authorities</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-0820" facs="A82372-001-a-0640">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-0830" facs="A82372-001-a-0650">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82372-0840" facs="A82372-001-a-0660">said</w>
          <c> </c>
          <w lemma="high" ana="#j" reg="high" xml:id="A82372-0850" facs="A82372-001-a-0670">High</w>
          <c> </c>
          <w lemma="court" ana="#n1" reg="court" xml:id="A82372-0860" facs="A82372-001-a-0680">Court</w>
          <pc xml:id="A82372-0870" facs="A82372-001-a-0690">,</pc>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A82372-0880" facs="A82372-001-a-0700">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82372-0890" facs="A82372-001-a-0710">their</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A82372-0900" facs="A82372-001-a-0720">several</w>
          <c> </c>
          <w lemma="limitation" ana="#n2" reg="limitations" xml:id="A82372-0910" facs="A82372-001-a-0730">Limitations</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-0920" facs="A82372-001-a-0740">and</w>
          <c> </c>
          <w lemma="restriction" ana="#n2" reg="restrictions" xml:id="A82372-0930" facs="A82372-001-a-0750">Restrictions</w>
          <pc xml:id="A82372-0940" facs="A82372-001-a-0760">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A82372-0950" facs="A82372-001-a-0770">shall</w>
          <c> </c>
          <w lemma="continue" ana="#vvi" reg="continue" xml:id="A82372-0960" facs="A82372-001-a-0780">continue</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A82372-0970" facs="A82372-001-a-0790">in</w>
          <c> </c>
          <w lemma="force" ana="#n1" reg="force" xml:id="A82372-0980" facs="A82372-001-a-0800">force</w>
          <c> </c>
          <w lemma="until" ana="#acp-p" reg="until" xml:id="A82372-0990" facs="A82372-001-a-0810">until</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-1000" facs="A82372-001-a-0820">the</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A82372-1010" facs="A82372-001-a-0830">last</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A82372-1020" facs="A82372-001-a-0840">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-1030" facs="A82372-001-a-0850">of</w>
          <c> </c>
          <hi xml:id="A82372e-80">
            <w lemma="December" ana="#n1-nn" reg="December" xml:id="A82372-1040" facs="A82372-001-a-0860">December</w>
          </hi>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A82372-1050" facs="A82372-001-a-0870">next</w>
          <c> </c>
          <w lemma="ensue" ana="#vvg" reg="ensuing" xml:id="A82372-1060" facs="A82372-001-a-0880">ensuing</w>
          <pc unit="sentence" xml:id="A82372-1070" facs="A82372-001-a-0890">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A82372e-90">
      <div type="license" xml:id="A82372e-100">
        <opener xml:id="A82372e-110">
          <dateline xml:id="A82372e-120">
            <date xml:id="A82372e-130">
              <w lemma="thursday" ana="#n1-nn" reg="Thursday" xml:id="A82372-1140" facs="A82372-001-a-0900">Thursday</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A82372-1150" facs="A82372-001-a-0910">the</w>
              <c> </c>
              <w lemma="five" ana="#crd" reg="five" xml:id="A82372-1160" facs="A82372-001-a-0920">Five</w>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A82372-1170" facs="A82372-001-a-0930">and</w>
              <c> </c>
              <w lemma="twenty" ana="#ord" reg="twentieth" xml:id="A82372-1180" facs="A82372-001-a-0940">twentieth</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-1190" facs="A82372-001-a-0950">of</w>
              <c> </c>
              <hi xml:id="A82372e-140">
                <w lemma="September" ana="#n1-nn" reg="September" xml:id="A82372-1200" facs="A82372-001-a-0960">September</w>
                <pc xml:id="A82372-1210" facs="A82372-001-a-0970">,</pc>
                <c> </c>
                <w lemma="1651." ana="#crd" reg="1651." xml:id="A82372-1220" facs="A82372-001-a-0980">1651.</w>
                <pc unit="sentence" xml:id="A82372-1220-eos" facs="A82372-001-a-0990"/>
                <c> </c>
              </hi>
            </date>
          </dateline>
        </opener>
        <p xml:id="A82372e-150">
          <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A82372-1250" facs="A82372-001-a-1000">ORdered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82372-1260" facs="A82372-001-a-1010">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-1270" facs="A82372-001-a-1020">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82372-1280" facs="A82372-001-a-1030">Parliament</w>
          <pc xml:id="A82372-1290" facs="A82372-001-a-1040">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A82372-1300" facs="A82372-001-a-1050">That</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A82372-1310" facs="A82372-001-a-1060">this</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A82372-1320" facs="A82372-001-a-1070">Act</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A82372-1330" facs="A82372-001-a-1080">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A82372-1340" facs="A82372-001-a-1090">forthwith</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A82372-1350" facs="A82372-001-a-1100">Printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82372-1360" facs="A82372-001-a-1110">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A82372-1370" facs="A82372-001-a-1120">Published</w>
          <pc unit="sentence" xml:id="A82372-1380" facs="A82372-001-a-1130">.</pc>
        </p>
        <closer xml:id="A82372e-160">
          <signed xml:id="A82372e-170">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A82372-1420" facs="A82372-001-a-1140">Hen</w>
            <pc xml:id="A82372-1430" facs="A82372-001-a-1150">:</pc>
            <c> </c>
            <w lemma="Scobell" ana="#n1-nn" reg="Scobell" xml:id="A82372-1440" facs="A82372-001-a-1160">Scobell</w>
            <pc xml:id="A82372-1450" facs="A82372-001-a-1170">,</pc>
            <c> </c>
            <w lemma="cleric" ana="#j" reg="cleric" xml:id="A82372-1460" facs="A82372-001-a-1180">Cleric</w>
            <pc unit="sentence" xml:id="A82372-1470" facs="A82372-001-a-1190">.</pc>
            <c> </c>
            <w lemma="parliamenti" ana="#fw-la" reg="Parliamenti" xml:id="A82372-1480" facs="A82372-001-a-1200">Parliamenti</w>
            <pc unit="sentence" xml:id="A82372-1490" facs="A82372-001-a-1210">.</pc>
          </signed>
        </closer>
      </div>
      <div type="colophon" xml:id="A82372e-180">
        <p xml:id="A82372e-190">
          <hi xml:id="A82372e-200">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A82372-1530" facs="A82372-001-a-1220">London</w>
            <pc xml:id="A82372-1540" facs="A82372-001-a-1230">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A82372-1550" facs="A82372-001-a-1240">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82372-1560" facs="A82372-001-a-1250">by</w>
          <c> </c>
          <hi xml:id="A82372e-210">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A82372-1570" facs="A82372-001-a-1260">John</w>
            <c> </c>
            <w lemma="field" ana="#n1" reg="field" xml:id="A82372-1580" facs="A82372-001-a-1270">Field</w>
            <pc xml:id="A82372-1590" facs="A82372-001-a-1280">,</pc>
          </hi>
          <c> </c>
          <w lemma="printer" ana="#n1" reg="printer" xml:id="A82372-1600" facs="A82372-001-a-1290">Printer</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82372-1610" facs="A82372-001-a-1300">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82372-1620" facs="A82372-001-a-1310">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82372-1630" facs="A82372-001-a-1320">Parliament</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82372-1640" facs="A82372-001-a-1330">of</w>
          <c> </c>
          <hi xml:id="A82372e-220">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A82372-1650" facs="A82372-001-a-1340">England</w>
            <pc unit="sentence" xml:id="A82372-1660" facs="A82372-001-a-1350">.</pc>
            <c> </c>
          </hi>
          <w lemma="1651." ana="#crd" reg="1651." xml:id="A82372-1670" facs="A82372-001-a-1360">1651.</w>
          <pc unit="sentence" xml:id="A82372-1670-eos" facs="A82372-001-a-1370"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>