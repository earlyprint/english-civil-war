<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Sabbathi 19. Junii 1647. The Lords and Commons in Parliament having well accepted the obedience and readinesse of those officers and souldiers who have compiled with their orders, have ordered and ordained, and by authority aforesaid doe order and ordaine, that all and every the said officers and souldiers aforesaid, shall be freed, ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1647</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A82879</idno>
        <idno type="STC">Wing E1637</idno>
        <idno type="STC">Thomason 669.f.11[27]</idno>
        <idno type="STC">ESTC R210491</idno>
        <idno type="EEBO-CITATION">99869286</idno>
        <idno type="PROQUEST">99869286</idno>
        <idno type="VID">162677</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A82879)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162677)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f11[27])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Sabbathi 19. Junii 1647. The Lords and Commons in Parliament having well accepted the obedience and readinesse of those officers and souldiers who have compiled with their orders, have ordered and ordained, and by authority aforesaid doe order and ordaine, that all and every the said officers and souldiers aforesaid, shall be freed, ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>printed for John Wright at the Kings Head in the Old Bayley,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1647.</date>
          </publicationStmt>
          <notesStmt>
            <note>Any officer or soldier who has left the colours of Sir Thomas Fairfax or any other officer, in obedience to the orders of Parliament shall be freed from any penalty incurred or inflicted for so doing -- Cf. Steele.</note>
            <note>Ordered to be printed and published 19 June 1647. Signed: Joh. Brown Cler. Parliamentorum.</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- Militia -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-11</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-11</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A82879e-10">
    <body xml:id="A82879e-20">
      <div type="text" xml:id="A82879e-30">
        <pb facs="tcp:162677:1" rendition="simple:additions" xml:id="A82879-001-a"/>
        <opener xml:id="A82879e-40">
          <dateline xml:id="A82879e-50">
            <date xml:id="A82879e-60">
              <hi xml:id="A82879e-70">
                <w lemma="die" ana="#fw-la" reg="die" xml:id="A82879-0050" facs="A82879-001-a-0010">Die</w>
                <c> </c>
                <w lemma="sabhathi" ana="#uh" reg="sabhathi" xml:id="A82879-0060" facs="A82879-001-a-0020">Sabhathi</w>
              </hi>
              <c> </c>
              <w lemma="19" ana="#crd" reg="19" xml:id="A82879-0070" facs="A82879-001-a-0030">19.</w>
              <pc unit="sentence" xml:id="A82879-0070-eos" facs="A82879-001-a-0040"/>
              <c> </c>
              <hi xml:id="A82879e-80">
                <w lemma="Junii" ana="#fw-la" reg="junii" xml:id="A82879-0080" facs="A82879-001-a-0050">Junii</w>
              </hi>
              <c> </c>
              <w lemma="1647." ana="#crd" reg="1647." xml:id="A82879-0090" facs="A82879-001-a-0060">1647.</w>
              <pc unit="sentence" xml:id="A82879-0090-eos" facs="A82879-001-a-0070"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A82879e-90">
          <w lemma="the" ana="#d" reg="The" xml:id="A82879-0120" facs="A82879-001-a-0080">THe</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A82879-0130" facs="A82879-001-a-0090">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0140" facs="A82879-001-a-0100">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A82879-0150" facs="A82879-001-a-0110">Commons</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A82879-0160" facs="A82879-001-a-0120">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82879-0170" facs="A82879-001-a-0130">Parliament</w>
          <c> </c>
          <w lemma="have" ana="#vvg" reg="having" xml:id="A82879-0180" facs="A82879-001-a-0140">having</w>
          <c> </c>
          <w lemma="well" ana="#av" reg="well" xml:id="A82879-0190" facs="A82879-001-a-0150">well</w>
          <c> </c>
          <w lemma="accept" ana="#vvn" reg="accepted" xml:id="A82879-0200" facs="A82879-001-a-0160">accepted</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-0210" facs="A82879-001-a-0170">the</w>
          <c> </c>
          <w lemma="obedience" ana="#n1" reg="obedience" xml:id="A82879-0220" facs="A82879-001-a-0180">obedience</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0230" facs="A82879-001-a-0190">and</w>
          <c> </c>
          <w lemma="readiness" ana="#n1" reg="readiness" xml:id="A82879-0240" facs="A82879-001-a-0200">readinesse</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-0250" facs="A82879-001-a-0210">of</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A82879-0260" facs="A82879-001-a-0220">those</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A82879-0270" facs="A82879-001-a-0230">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0280" facs="A82879-001-a-0240">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A82879-0290" facs="A82879-001-a-0250">Souldiers</w>
          <c> </c>
          <w lemma="who" ana="#crq-r" reg="who" xml:id="A82879-0300" facs="A82879-001-a-0260">who</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A82879-0310" facs="A82879-001-a-0270">have</w>
          <c> </c>
          <w lemma="comply" ana="#vvn" reg="complied" xml:id="A82879-0320" facs="A82879-001-a-0280">complied</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A82879-0330" facs="A82879-001-a-0290">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82879-0340" facs="A82879-001-a-0300">their</w>
          <c> </c>
          <w lemma="order" ana="#n2" reg="orders" xml:id="A82879-0350" facs="A82879-001-a-0310">Orders</w>
          <pc xml:id="A82879-0360" facs="A82879-001-a-0320">,</pc>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A82879-0370" facs="A82879-001-a-0330">have</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A82879-0380" facs="A82879-001-a-0340">Ordered</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0390" facs="A82879-001-a-0350">and</w>
          <c> </c>
          <w lemma="ordain" ana="#vvn" reg="ordained" xml:id="A82879-0400" facs="A82879-001-a-0360">Ordained</w>
          <pc xml:id="A82879-0410" facs="A82879-001-a-0370">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0420" facs="A82879-001-a-0380">and</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82879-0430" facs="A82879-001-a-0390">by</w>
          <c> </c>
          <w lemma="authority" ana="#n1" reg="authority" xml:id="A82879-0440" facs="A82879-001-a-0400">authority</w>
          <c> </c>
          <w lemma="aforesaid" ana="#j" reg="aforesaid" xml:id="A82879-0450" facs="A82879-001-a-0410">aforesaid</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A82879-0460" facs="A82879-001-a-0420">doe</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A82879-0470" facs="A82879-001-a-0430">Order</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0480" facs="A82879-001-a-0440">and</w>
          <c> </c>
          <w lemma="ordain" ana="#vvi" reg="ordain" xml:id="A82879-0490" facs="A82879-001-a-0450">Ordaine</w>
          <pc xml:id="A82879-0500" facs="A82879-001-a-0460">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A82879-0510" facs="A82879-001-a-0470">That</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82879-0520" facs="A82879-001-a-0480">all</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0530" facs="A82879-001-a-0490">and</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A82879-0540" facs="A82879-001-a-0500">every</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-0550" facs="A82879-001-a-0510">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82879-0560" facs="A82879-001-a-0520">said</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A82879-0570" facs="A82879-001-a-0530">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0580" facs="A82879-001-a-0540">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A82879-0590" facs="A82879-001-a-0550">Souldiers</w>
          <c> </c>
          <w lemma="aforesaid" ana="#j" reg="aforesaid" xml:id="A82879-0600" facs="A82879-001-a-0560">aforesaid</w>
          <pc xml:id="A82879-0610" facs="A82879-001-a-0570">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A82879-0620" facs="A82879-001-a-0580">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A82879-0630" facs="A82879-001-a-0590">be</w>
          <c> </c>
          <w lemma="free" ana="#vvn" reg="freed" xml:id="A82879-0640" facs="A82879-001-a-0600">freed</w>
          <pc xml:id="A82879-0650" facs="A82879-001-a-0610">,</pc>
          <c> </c>
          <w lemma="exonerate" ana="#vvn" reg="exonerated" xml:id="A82879-0660" facs="A82879-001-a-0620">exonerated</w>
          <pc xml:id="A82879-0670" facs="A82879-001-a-0630">,</pc>
          <c> </c>
          <w lemma="acquit" ana="#vvn" reg="acquitted" xml:id="A82879-0680" facs="A82879-001-a-0640">acquitted</w>
          <pc xml:id="A82879-0690" facs="A82879-001-a-0650">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0700" facs="A82879-001-a-0660">and</w>
          <c> </c>
          <w lemma="discharge" ana="#vvn" reg="discharged" xml:id="A82879-0710" facs="A82879-001-a-0670">discharged</w>
          <pc xml:id="A82879-0720" facs="A82879-001-a-0680">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0730" facs="A82879-001-a-0690">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A82879-0740" facs="A82879-001-a-0700">are</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A82879-0750" facs="A82879-001-a-0710">in</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0760" facs="A82879-001-a-0720">and</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A82879-0770" facs="A82879-001-a-0730">hereby</w>
          <c> </c>
          <w lemma="declare" ana="#vvn" reg="declared" xml:id="A82879-0780" facs="A82879-001-a-0740">declared</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0790" facs="A82879-001-a-0750">and</w>
          <c> </c>
          <w lemma="adjudge" ana="#vvn" reg="adjudged" xml:id="A82879-0800" facs="A82879-001-a-0760">adjudged</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A82879-0810" facs="A82879-001-a-0770">from</w>
          <c> </c>
          <w lemma="henceforth" ana="#av" reg="henceforth" xml:id="A82879-0820" facs="A82879-001-a-0780">henceforth</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A82879-0830" facs="A82879-001-a-0790">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A82879-0840" facs="A82879-001-a-0800">be</w>
          <c> </c>
          <w lemma="free" ana="#j" reg="free" xml:id="A82879-0850" facs="A82879-001-a-0810">free</w>
          <pc xml:id="A82879-0860" facs="A82879-001-a-0820">,</pc>
          <c> </c>
          <w lemma="exonerate" ana="#vvi" reg="exonerate" xml:id="A82879-0870" facs="A82879-001-a-0830">exonerate</w>
          <pc xml:id="A82879-0880" facs="A82879-001-a-0840">,</pc>
          <c> </c>
          <w lemma="acquit" ana="#vvb" reg="acquit" xml:id="A82879-0890" facs="A82879-001-a-0850">acquit</w>
          <pc xml:id="A82879-0900" facs="A82879-001-a-0860">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0910" facs="A82879-001-a-0870">and</w>
          <c> </c>
          <w lemma="discharge" ana="#vvn" reg="discharged" xml:id="A82879-0920" facs="A82879-001-a-0880">discharged</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-0930" facs="A82879-001-a-0890">of</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0940" facs="A82879-001-a-0900">and</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A82879-0950" facs="A82879-001-a-0910">from</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82879-0960" facs="A82879-001-a-0920">all</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-0970" facs="A82879-001-a-0930">and</w>
          <c> </c>
          <w lemma="whatsoever" ana="#crq-r" reg="whatsoever" xml:id="A82879-0980" facs="A82879-001-a-0940">whatsoever</w>
          <c> </c>
          <w lemma="prosecution" ana="#n1" reg="prosecution" xml:id="A82879-0990" facs="A82879-001-a-0950">prosecution</w>
          <pc xml:id="A82879-1000" facs="A82879-001-a-0960">,</pc>
          <c> </c>
          <w lemma="judgement" ana="#n1" reg="judgement" xml:id="A82879-1010" facs="A82879-001-a-0970">judgement</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1020" facs="A82879-001-a-0980">of</w>
          <c> </c>
          <w lemma="life" ana="#n1" reg="life" xml:id="A82879-1030" facs="A82879-001-a-0990">life</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1040" facs="A82879-001-a-1000">or</w>
          <c> </c>
          <w lemma="member" ana="#n1" reg="member" xml:id="A82879-1050" facs="A82879-001-a-1010">member</w>
          <pc xml:id="A82879-1060" facs="A82879-001-a-1020">,</pc>
          <c> </c>
          <w lemma="pain" ana="#n2" reg="pains" xml:id="A82879-1070" facs="A82879-001-a-1030">paines</w>
          <pc xml:id="A82879-1080" facs="A82879-001-a-1040">,</pc>
          <c> </c>
          <w lemma="penalty" ana="#n2" reg="penalties" xml:id="A82879-1090" facs="A82879-001-a-1050">penalties</w>
          <pc xml:id="A82879-1100" facs="A82879-001-a-1060">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82879-1110" facs="A82879-001-a-1070">and</w>
          <c> </c>
          <w lemma="punishment" ana="#n2" reg="punishments" xml:id="A82879-1120" facs="A82879-001-a-1080">punishments</w>
          <c> </c>
          <w lemma="whatsoever" ana="#crq-r" reg="whatsoever" xml:id="A82879-1130" facs="A82879-001-a-1090">whatsoever</w>
          <c> </c>
          <w lemma="incur" ana="#vvn" reg="incurred" xml:id="A82879-1140" facs="A82879-001-a-1100">incurred</w>
          <pc xml:id="A82879-1150" facs="A82879-001-a-1110">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1160" facs="A82879-001-a-1120">or</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A82879-1170" facs="A82879-001-a-1130">that</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A82879-1180" facs="A82879-001-a-1140">shall</w>
          <pc xml:id="A82879-1190" facs="A82879-001-a-1150">,</pc>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A82879-1200" facs="A82879-001-a-1160">may</w>
          <pc xml:id="A82879-1210" facs="A82879-001-a-1170">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1220" facs="A82879-001-a-1180">or</w>
          <c> </c>
          <w lemma="can" ana="#vmb" reg="can" xml:id="A82879-1230" facs="A82879-001-a-1190">can</w>
          <pc xml:id="A82879-1240" facs="A82879-001-a-1200">,</pc>
          <c> </c>
          <w lemma="might" ana="#n1" reg="might" xml:id="A82879-1250" facs="A82879-001-a-1210">might</w>
          <pc xml:id="A82879-1260" facs="A82879-001-a-1220">,</pc>
          <c> </c>
          <w lemma="can" ana="#vmd" reg="could" xml:id="A82879-1270" facs="A82879-001-a-1230">could</w>
          <pc xml:id="A82879-1280" facs="A82879-001-a-1240">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmd" reg="should" xml:id="A82879-1290" facs="A82879-001-a-1250">should</w>
          <pc xml:id="A82879-1300" facs="A82879-001-a-1260">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1310" facs="A82879-001-a-1270">or</w>
          <c> </c>
          <w lemma="aught" ana="#pi" reg="aught" xml:id="A82879-1320" facs="A82879-001-a-1280">ought</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A82879-1330" facs="A82879-001-a-1290">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A82879-1340" facs="A82879-001-a-1300">be</w>
          <c> </c>
          <w lemma="inflict" ana="#vvn" reg="inflicted" xml:id="A82879-1350" facs="A82879-001-a-1310">inflicted</w>
          <pc xml:id="A82879-1360" facs="A82879-001-a-1320">,</pc>
          <c> </c>
          <w lemma="prosecute" ana="#vvn" reg="prosecuted" xml:id="A82879-1370" facs="A82879-001-a-1330">prosecuted</w>
          <pc xml:id="A82879-1380" facs="A82879-001-a-1340">,</pc>
          <c> </c>
          <w lemma="have" ana="#vvd" reg="had" xml:id="A82879-1390" facs="A82879-001-a-1350">had</w>
          <pc xml:id="A82879-1400" facs="A82879-001-a-1360">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1410" facs="A82879-001-a-1370">or</w>
          <c> </c>
          <w lemma="pursue" ana="#vvn" reg="pursued" xml:id="A82879-1420" facs="A82879-001-a-1380">pursued</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A82879-1430" facs="A82879-001-a-1390">against</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A82879-1440" facs="A82879-001-a-1400">them</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1450" facs="A82879-001-a-1410">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A82879-1460" facs="A82879-001-a-1420">any</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1470" facs="A82879-001-a-1430">of</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A82879-1480" facs="A82879-001-a-1440">them</w>
          <pc xml:id="A82879-1490" facs="A82879-001-a-1450">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A82879-1500" facs="A82879-001-a-1460">for</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1510" facs="A82879-001-a-1470">or</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82879-1520" facs="A82879-001-a-1480">by</w>
          <c> </c>
          <w lemma="reason" ana="#n1" reg="reason" xml:id="A82879-1530" facs="A82879-001-a-1490">reason</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1540" facs="A82879-001-a-1500">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-1550" facs="A82879-001-a-1510">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82879-1560" facs="A82879-001-a-1520">said</w>
          <c> </c>
          <w lemma="pretend" ana="#j_vn" reg="pretended" xml:id="A82879-1570" facs="A82879-001-a-1530">pretended</w>
          <c> </c>
          <w lemma="offence" ana="#n1" reg="offence" xml:id="A82879-1580" facs="A82879-001-a-1540">offence</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1590" facs="A82879-001-a-1550">or</w>
          <c> </c>
          <w lemma="offence" ana="#n2" reg="offences" xml:id="A82879-1600" facs="A82879-001-a-1560">offences</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1610" facs="A82879-001-a-1570">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82879-1620" facs="A82879-001-a-1580">their</w>
          <c> </c>
          <w lemma="departure" ana="#n1" reg="departure" xml:id="A82879-1630" facs="A82879-001-a-1590">departure</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A82879-1640" facs="A82879-001-a-1600">from</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82879-1650" facs="A82879-001-a-1610">their</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82879-1660" facs="A82879-001-a-1620">said</w>
          <c> </c>
          <w lemma="colour" ana="#n2" reg="colours" xml:id="A82879-1670" facs="A82879-001-a-1630">Colours</w>
          <pc xml:id="A82879-1680" facs="A82879-001-a-1640">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1690" facs="A82879-001-a-1650">or</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A82879-1700" facs="A82879-001-a-1660">from</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A82879-1710" facs="A82879-001-a-1670">under</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-1720" facs="A82879-001-a-1680">the</w>
          <c> </c>
          <w lemma="command" ana="#n1" reg="command" xml:id="A82879-1730" facs="A82879-001-a-1690">command</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1740" facs="A82879-001-a-1700">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-1750" facs="A82879-001-a-1710">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82879-1760" facs="A82879-001-a-1720">said</w>
          <c> </c>
          <w lemma="sir" ana="#n1" reg="Sir" xml:id="A82879-1770" facs="A82879-001-a-1730">Sir</w>
          <c> </c>
          <hi xml:id="A82879e-100">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A82879-1780" facs="A82879-001-a-1740">Thomas</w>
            <c> </c>
            <w lemma="Fairfax" ana="#n1-nn" reg="Fairfax" xml:id="A82879-1790" facs="A82879-001-a-1750">Fairfax</w>
            <pc xml:id="A82879-1800" facs="A82879-001-a-1760">,</pc>
          </hi>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1810" facs="A82879-001-a-1770">or</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A82879-1820" facs="A82879-001-a-1780">from</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1830" facs="A82879-001-a-1790">or</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A82879-1840" facs="A82879-001-a-1800">under</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-1850" facs="A82879-001-a-1810">the</w>
          <c> </c>
          <w lemma="command" ana="#n1" reg="command" xml:id="A82879-1860" facs="A82879-001-a-1820">command</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82879-1870" facs="A82879-001-a-1830">of</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A82879-1880" facs="A82879-001-a-1840">any</w>
          <c> </c>
          <w lemma="military" ana="#j" reg="military" xml:id="A82879-1890" facs="A82879-001-a-1850">Military</w>
          <c> </c>
          <w lemma="officer" ana="#n1" reg="officer" xml:id="A82879-1900" facs="A82879-001-a-1860">Officer</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82879-1910" facs="A82879-001-a-1870">or</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A82879-1920" facs="A82879-001-a-1880">Officers</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A82879-1930" facs="A82879-001-a-1890">under</w>
          <c> </c>
          <w lemma="he" ana="#pno" reg="him" xml:id="A82879-1940" facs="A82879-001-a-1900">him</w>
          <c> </c>
          <w lemma="whatsoever" ana="#crq-r" reg="whatsoever" xml:id="A82879-1950" facs="A82879-001-a-1910">whatsoever</w>
          <pc unit="sentence" xml:id="A82879-1960" facs="A82879-001-a-1920">.</pc>
        </p>
        <div type="license" xml:id="A82879e-110">
          <opener xml:id="A82879e-120">
            <dateline xml:id="A82879e-130">
              <date xml:id="A82879e-140">
                <w lemma="die" ana="#fw-la" reg="die" xml:id="A82879-2020" facs="A82879-001-a-1930">Die</w>
                <c> </c>
                <w lemma="sabbathi" ana="#fw-la" reg="sabbathi" xml:id="A82879-2030" facs="A82879-001-a-1940">Sabbathi</w>
                <c> </c>
                <w lemma="19" ana="#crd" reg="19" xml:id="A82879-2040" facs="A82879-001-a-1950">19.</w>
                <pc unit="sentence" xml:id="A82879-2040-eos" facs="A82879-001-a-1960"/>
                <c> </c>
                <w lemma="Junii" ana="#fw-la" reg="Junii" xml:id="A82879-2050" facs="A82879-001-a-1970">Junii</w>
                <c> </c>
                <w lemma="1647." ana="#crd" reg="1647." xml:id="A82879-2060" facs="A82879-001-a-1980">1647.</w>
                <pc unit="sentence" xml:id="A82879-2060-eos" facs="A82879-001-a-1990"/>
              </date>
            </dateline>
          </opener>
          <p xml:id="A82879e-150">
            <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A82879-2090" facs="A82879-001-a-2000">ORdered</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A82879-2100" facs="A82879-001-a-2010">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A82879-2110" facs="A82879-001-a-2020">the</w>
            <c> </c>
            <w lemma="lord" ana="#n2" reg="Lords" xml:id="A82879-2120" facs="A82879-001-a-2030">Lords</w>
            <c> </c>
            <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A82879-2130" facs="A82879-001-a-2040">assembled</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A82879-2140" facs="A82879-001-a-2050">in</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82879-2150" facs="A82879-001-a-2060">Parliament</w>
            <pc xml:id="A82879-2160" facs="A82879-001-a-2070">,</pc>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A82879-2170" facs="A82879-001-a-2080">That</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A82879-2180" facs="A82879-001-a-2090">this</w>
            <c> </c>
            <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A82879-2190" facs="A82879-001-a-2100">Ordinance</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="be" xml:id="A82879-2200" facs="A82879-001-a-2110">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A82879-2210" facs="A82879-001-a-2120">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A82879-2220" facs="A82879-001-a-2130">printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A82879-2230" facs="A82879-001-a-2140">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A82879-2240" facs="A82879-001-a-2150">published</w>
            <pc unit="sentence" xml:id="A82879-2250" facs="A82879-001-a-2160">.</pc>
          </p>
          <closer xml:id="A82879e-160">
            <signed xml:id="A82879e-170">
              <w lemma="joh." ana="#n-ab" reg="Joh." xml:id="A82879-2290" facs="A82879-001-a-2170">Joh.</w>
              <c> </c>
              <w lemma="Brown" ana="#n1-nn" reg="Brown" xml:id="A82879-2300" facs="A82879-001-a-2180">Brown</w>
              <c> </c>
              <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A82879-2310" facs="A82879-001-a-2190">Cler.</w>
              <c> </c>
              <w lemma="parliamentorum" ana="#fw-la" reg="parliamentorum" xml:id="A82879-2320" facs="A82879-001-a-2200">Parliamentorum</w>
              <pc unit="sentence" xml:id="A82879-2330" facs="A82879-001-a-2210">.</pc>
            </signed>
          </closer>
        </div>
      </div>
    </body>
    <back xml:id="A82879e-180">
      <div type="colophon" xml:id="A82879e-190">
        <p xml:id="A82879e-200">
          <w lemma="London" ana="#n1-nn" reg="London" xml:id="A82879-2380" facs="A82879-001-a-2220">London</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A82879-2390" facs="A82879-001-a-2230">printed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A82879-2400" facs="A82879-001-a-2240">for</w>
          <c> </c>
          <hi xml:id="A82879e-210">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A82879-2410" facs="A82879-001-a-2250">John</w>
            <c> </c>
            <w lemma="Wright" ana="#n1-nn" reg="Wright" xml:id="A82879-2420" facs="A82879-001-a-2260">Wright</w>
          </hi>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A82879-2430" facs="A82879-001-a-2270">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-2440" facs="A82879-001-a-2280">the</w>
          <c> </c>
          <w lemma="king" ana="#n1g" reg="King's" xml:id="A82879-2450" facs="A82879-001-a-2290">Kings</w>
          <c> </c>
          <w lemma="head" ana="#n1" reg="head" xml:id="A82879-2460" facs="A82879-001-a-2300">Head</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A82879-2470" facs="A82879-001-a-2310">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82879-2480" facs="A82879-001-a-2320">the</w>
          <c> </c>
          <w lemma="old" ana="#j" reg="old" xml:id="A82879-2490" facs="A82879-001-a-2330">Old</w>
          <c> </c>
          <w lemma="Bayley" ana="#n1-nn" reg="Bayley" xml:id="A82879-2500" facs="A82879-001-a-2340">Bayley</w>
          <pc unit="sentence" xml:id="A82879-2510" facs="A82879-001-a-2350">.</pc>
          <c> </c>
          <w lemma="1647." ana="#crd" reg="1647." xml:id="A82879-2520" facs="A82879-001-a-2360">1647.</w>
          <pc unit="sentence" xml:id="A82879-2520-eos" facs="A82879-001-a-2370"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>