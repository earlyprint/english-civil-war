<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Thursday the thirteenth of October, 1653. An act for confirmation of the sale of the lands and estate of Sir Iohn Stowel knight of the Bath.</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1653</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A82352</idno>
        <idno type="STC">Wing E1006</idno>
        <idno type="STC">Thomason 669.f.17[62]</idno>
        <idno type="STC">ESTC R211725</idno>
        <idno type="EEBO-CITATION">99870431</idno>
        <idno type="PROQUEST">99870431</idno>
        <idno type="VID">163310</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A82352)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163310)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f17[62])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Thursday the thirteenth of October, 1653. An act for confirmation of the sale of the lands and estate of Sir Iohn Stowel knight of the Bath.</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by John Field, Printer to the Parliament of England,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1653.</date>
          </publicationStmt>
          <notesStmt>
            <note>Signed: Hen: Scobell, Clerk of the Parliament.</note>
            <note>Annotation on Thomason copy: "Nouemb: ye 8th".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Stawell, John, -- Sir, 1599-1662 -- Estate -- Early works to 1800.</term>
          <term>Real property -- England -- Early works to 1800.</term>
          <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A82352e-10">
    <body xml:id="A82352e-20">
      <div type="text" xml:id="A82352e-30">
        <pb facs="tcp:163310:1" rendition="simple:additions" xml:id="A82352-001-a"/>
        <head xml:id="A82352e-40">
          <figure xml:id="A82352e-50">
            <figDesc xml:id="A82352e-60">blazon or coat of arms incorporating the Commonwealth Flag (1649-1651)</figDesc>
          </figure>
          <hi xml:id="A82352e-70">
            <w lemma="thursday" ana="#n1-nn" reg="Thursday" xml:id="A82352-0030" facs="A82352-001-a-0010">Thursday</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A82352-0040" facs="A82352-001-a-0020">the</w>
            <c> </c>
            <w lemma="thirteen" ana="#ord" reg="thirteenth" xml:id="A82352-0050" facs="A82352-001-a-0030">Thirteenth</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0060" facs="A82352-001-a-0040">of</w>
          </hi>
          <c> </c>
          <w lemma="October" ana="#n1-nn" reg="October" xml:id="A82352-0070" facs="A82352-001-a-0050">October</w>
          <pc xml:id="A82352-0080" facs="A82352-001-a-0060">,</pc>
          <c> </c>
          <w lemma="1653." ana="#crd" reg="1653." xml:id="A82352-0090" facs="A82352-001-a-0070">1653.</w>
          <pc unit="sentence" xml:id="A82352-0090-eos" facs="A82352-001-a-0080"/>
          <c> </c>
          <w lemma="a" ana="#d" reg="An" xml:id="A82352-0100" facs="A82352-001-a-0090">AN</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A82352-0110" facs="A82352-001-a-0100">ACT</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A82352-0120" facs="A82352-001-a-0110">For</w>
          <c> </c>
          <w lemma="confirmation" ana="#n1" reg="confirmation" xml:id="A82352-0130" facs="A82352-001-a-0120">confirmation</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0140" facs="A82352-001-a-0130">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-0150" facs="A82352-001-a-0140">the</w>
          <c> </c>
          <w lemma="sale" ana="#n1" reg="sale" xml:id="A82352-0160" facs="A82352-001-a-0150">Sale</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0170" facs="A82352-001-a-0160">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-0180" facs="A82352-001-a-0170">the</w>
          <c> </c>
          <w lemma="land" ana="#n2" reg="lands" xml:id="A82352-0190" facs="A82352-001-a-0180">LANDS</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-0200" facs="A82352-001-a-0190">and</w>
          <c> </c>
          <w lemma="estate" ana="#n1" reg="estate" xml:id="A82352-0210" facs="A82352-001-a-0200">ESTATE</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0220" facs="A82352-001-a-0210">Of</w>
          <c> </c>
          <w lemma="sir" ana="#n1" reg="Sir" xml:id="A82352-0230" facs="A82352-001-a-0220">Sir</w>
          <c> </c>
          <hi xml:id="A82352e-80">
            <w lemma="JOHN" ana="#n1-nn" reg="John" xml:id="A82352-0240" facs="A82352-001-a-0230">IOHN</w>
            <c> </c>
            <w lemma="Stowel" ana="#n1-nn" reg="Stowel" xml:id="A82352-0250" facs="A82352-001-a-0240">STOWEL</w>
          </hi>
          <c> </c>
          <w lemma="knight" ana="#n1" reg="knight" xml:id="A82352-0260" facs="A82352-001-a-0250">Knight</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0270" facs="A82352-001-a-0260">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-0280" facs="A82352-001-a-0270">the</w>
          <c> </c>
          <w lemma="bath" ana="#n1" reg="bath" xml:id="A82352-0290" facs="A82352-001-a-0280">Bath</w>
          <pc unit="sentence" xml:id="A82352-0300" facs="A82352-001-a-0290">.</pc>
        </head>
        <p xml:id="A82352e-90">
          <w lemma="be" ana="#vvb" reg="Be" rend="initialcharacterdecorated" xml:id="A82352-0470" facs="A82352-001-a-0300">BE</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A82352-0480" facs="A82352-001-a-0310">it</w>
          <c> </c>
          <w lemma="enact" ana="#vvn" reg="enacted" xml:id="A82352-0490" facs="A82352-001-a-0320">Enacted</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-0500" facs="A82352-001-a-0330">and</w>
          <c> </c>
          <w lemma="declare" ana="#vvn" reg="declared" xml:id="A82352-0510" facs="A82352-001-a-0340">Declared</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82352-0520" facs="A82352-001-a-0350">by</w>
          <c> </c>
          <w lemma="authority" ana="#n1" reg="authority" xml:id="A82352-0530" facs="A82352-001-a-0360">Authority</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0540" facs="A82352-001-a-0370">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82352-0550" facs="A82352-001-a-0380">Parliament</w>
          <pc xml:id="A82352-0560" facs="A82352-001-a-0390">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A82352-0570" facs="A82352-001-a-0400">That</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82352-0580" facs="A82352-001-a-0410">all</w>
          <c> </c>
          <w lemma="sale" ana="#n2" reg="t'sales" xml:id="A82352-0590" facs="A82352-001-a-0420">Sales</w>
          <c> </c>
          <w lemma="make" ana="#vvd" reg="made" xml:id="A82352-0600" facs="A82352-001-a-0430">made</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0610" facs="A82352-001-a-0440">of</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A82352-0620" facs="A82352-001-a-0450">any</w>
          <c> </c>
          <w lemma="estate" ana="#n1" reg="estate" xml:id="A82352-0630" facs="A82352-001-a-0460">Estate</w>
          <pc xml:id="A82352-0640" facs="A82352-001-a-0470">,</pc>
          <c> </c>
          <w lemma="land" ana="#n2" reg="lands" xml:id="A82352-0650" facs="A82352-001-a-0480">Lands</w>
          <pc xml:id="A82352-0660" facs="A82352-001-a-0490">,</pc>
          <c> </c>
          <w lemma="tenement" ana="#n2" reg="tenements" xml:id="A82352-0670" facs="A82352-001-a-0500">Tenements</w>
          <pc xml:id="A82352-0680" facs="A82352-001-a-0510">,</pc>
          <c> </c>
          <w lemma="hereditament" ana="#n2" reg="hereditaments" xml:id="A82352-0690" facs="A82352-001-a-0520">Hereditaments</w>
          <pc xml:id="A82352-0700" facs="A82352-001-a-0530">,</pc>
          <c> </c>
          <w lemma="good" ana="#n2_j" reg="goods" xml:id="A82352-0710" facs="A82352-001-a-0540">Goods</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82352-0720" facs="A82352-001-a-0550">or</w>
          <c> </c>
          <w lemma="chattel" ana="#n2" reg="chattels" xml:id="A82352-0730" facs="A82352-001-a-0560">Chattels</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0740" facs="A82352-001-a-0570">of</w>
          <c> </c>
          <w lemma="sir" ana="#n1" reg="Sir" xml:id="A82352-0750" facs="A82352-001-a-0580">Sir</w>
          <c> </c>
          <hi xml:id="A82352e-100">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A82352-0760" facs="A82352-001-a-0590">John</w>
            <c> </c>
            <w lemma="Stowel" ana="#n1-nn" reg="Stowel" xml:id="A82352-0770" facs="A82352-001-a-0600">Stowel</w>
          </hi>
          <c> </c>
          <w lemma="knight" ana="#n1" reg="knight" xml:id="A82352-0780" facs="A82352-001-a-0610">Knight</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0790" facs="A82352-001-a-0620">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-0800" facs="A82352-001-a-0630">the</w>
          <c> </c>
          <w lemma="bath" ana="#n1" reg="bath" xml:id="A82352-0810" facs="A82352-001-a-0640">Bath</w>
          <pc xml:id="A82352-0820" facs="A82352-001-a-0650">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82352-0830" facs="A82352-001-a-0660">by</w>
          <c> </c>
          <w lemma="virtue" ana="#n1" reg="virtue" xml:id="A82352-0840" facs="A82352-001-a-0670">vertue</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82352-0850" facs="A82352-001-a-0680">or</w>
          <c> </c>
          <w lemma="appointment" ana="#n1" reg="appointment" xml:id="A82352-0860" facs="A82352-001-a-0690">appointment</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0870" facs="A82352-001-a-0700">of</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A82352-0880" facs="A82352-001-a-0710">any</w>
          <c> </c>
          <w lemma="act" ana="#n1" reg="act" xml:id="A82352-0890" facs="A82352-001-a-0720">Act</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82352-0900" facs="A82352-001-a-0730">or</w>
          <c> </c>
          <w lemma="act" ana="#n2" reg="acts" xml:id="A82352-0910" facs="A82352-001-a-0740">Acts</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-0920" facs="A82352-001-a-0750">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82352-0930" facs="A82352-001-a-0760">Parliament</w>
          <pc xml:id="A82352-0940" facs="A82352-001-a-0770">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A82352-0950" facs="A82352-001-a-0780">are</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A82352-0960" facs="A82352-001-a-0790">hereby</w>
          <c> </c>
          <w lemma="confirm" ana="#vvn" reg="confirmed" xml:id="A82352-0970" facs="A82352-001-a-0800">confirmed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-0980" facs="A82352-001-a-0810">and</w>
          <c> </c>
          <w lemma="establish" ana="#vvn" reg="established" xml:id="A82352-0990" facs="A82352-001-a-0820">established</w>
          <pc xml:id="A82352-1000" facs="A82352-001-a-0830">:</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1010" facs="A82352-001-a-0840">And</w>
          <c> </c>
          <w lemma="accord" ana="#av_vg" reg="accordingly" xml:id="A82352-1020" facs="A82352-001-a-0850">accordingly</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A82352-1030" facs="A82352-001-a-0860">all</w>
          <c> </c>
          <w lemma="purchaser" ana="#n2" reg="purchasers" xml:id="A82352-1040" facs="A82352-001-a-0870">Purchasers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1050" facs="A82352-001-a-0880">and</w>
          <c> </c>
          <w lemma="buyer" ana="#n2" reg="buyers" xml:id="A82352-1060" facs="A82352-001-a-0890">Buyers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-1070" facs="A82352-001-a-0900">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1080" facs="A82352-001-a-0910">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A82352-1090" facs="A82352-001-a-0920">same</w>
          <pc xml:id="A82352-1100" facs="A82352-001-a-0930">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A82352-1110" facs="A82352-001-a-0940">shall</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1120" facs="A82352-001-a-0950">and</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A82352-1130" facs="A82352-001-a-0960">may</w>
          <c> </c>
          <w lemma="have" ana="#vvi" reg="have" xml:id="A82352-1140" facs="A82352-001-a-0970">have</w>
          <pc xml:id="A82352-1150" facs="A82352-001-a-0980">,</pc>
          <c> </c>
          <w lemma="hold" ana="#vvb" reg="hold" xml:id="A82352-1160" facs="A82352-001-a-0990">hold</w>
          <pc xml:id="A82352-1170" facs="A82352-001-a-1000">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1180" facs="A82352-001-a-1010">and</w>
          <c> </c>
          <w lemma="quiet" ana="#av_j" reg="quietly" xml:id="A82352-1190" facs="A82352-001-a-1020">quietly</w>
          <c> </c>
          <w lemma="enjoy" ana="#vvi" reg="enjoy" xml:id="A82352-1200" facs="A82352-001-a-1030">enjoy</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1210" facs="A82352-001-a-1040">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A82352-1220" facs="A82352-001-a-1050">same</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82352-1230" facs="A82352-001-a-1060">to</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A82352-1240" facs="A82352-001-a-1070">them</w>
          <pc xml:id="A82352-1250" facs="A82352-001-a-1080">,</pc>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82352-1260" facs="A82352-001-a-1090">their</w>
          <c> </c>
          <w lemma="heir" ana="#n2" reg="heirs" xml:id="A82352-1270" facs="A82352-001-a-1100">Heirs</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1280" facs="A82352-001-a-1110">and</w>
          <c> </c>
          <w lemma="assign" ana="#vvz" reg="assigns" xml:id="A82352-1290" facs="A82352-001-a-1120">Assigns</w>
          <pc xml:id="A82352-1300" facs="A82352-001-a-1130">,</pc>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A82352-1310" facs="A82352-001-a-1140">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82352-1320" facs="A82352-001-a-1150">to</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A82352-1330" facs="A82352-001-a-1160">their</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A82352-1340" facs="A82352-001-a-1170">respective</w>
          <c> </c>
          <w lemma="interest" ana="#n2" reg="interests" xml:id="A82352-1350" facs="A82352-001-a-1180">Interests</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1360" facs="A82352-001-a-1190">and</w>
          <c> </c>
          <w lemma="estate" ana="#n2" reg="estates" xml:id="A82352-1370" facs="A82352-001-a-1200">Estates</w>
          <c> </c>
          <w lemma="purchase" ana="#vvn" reg="purchased" xml:id="A82352-1380" facs="A82352-001-a-1210">purchased</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82352-1390" facs="A82352-001-a-1220">or</w>
          <c> </c>
          <w lemma="buy" ana="#vvn" reg="bought" xml:id="A82352-1400" facs="A82352-001-a-1230">bought</w>
          <pc xml:id="A82352-1410" facs="A82352-001-a-1240">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1420" facs="A82352-001-a-1250">and</w>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A82352-1430" facs="A82352-001-a-1260">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82352-1440" facs="A82352-001-a-1270">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1450" facs="A82352-001-a-1280">the</w>
          <c> </c>
          <w lemma="rule" ana="#n2" reg="rules" xml:id="A82352-1460" facs="A82352-001-a-1290">Rules</w>
          <pc xml:id="A82352-1470" facs="A82352-001-a-1300">,</pc>
          <c> </c>
          <w lemma="condition" ana="#n2" reg="conditions" xml:id="A82352-1480" facs="A82352-001-a-1310">Conditions</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A82352-1490" facs="A82352-001-a-1320">and</w>
          <c> </c>
          <w lemma="limitation" ana="#n2" reg="limitations" xml:id="A82352-1500" facs="A82352-001-a-1330">Limitations</w>
          <c> </c>
          <w lemma="prescribe" ana="#vvn" reg="prescribed" xml:id="A82352-1510" facs="A82352-001-a-1340">prescribed</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A82352-1520" facs="A82352-001-a-1350">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1530" facs="A82352-001-a-1360">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A82352-1540" facs="A82352-001-a-1370">said</w>
          <c> </c>
          <w lemma="act" ana="#n2" reg="acts" xml:id="A82352-1550" facs="A82352-001-a-1380">Acts</w>
          <pc xml:id="A82352-1560" facs="A82352-001-a-1390">,</pc>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A82352-1570" facs="A82352-001-a-1400">Any</w>
          <c> </c>
          <w lemma="law" ana="#n1" reg="law" xml:id="A82352-1580" facs="A82352-001-a-1410">Law</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A82352-1590" facs="A82352-001-a-1420">or</w>
          <c> </c>
          <w lemma="judgement" ana="#n1" reg="judgement" xml:id="A82352-1600" facs="A82352-001-a-1430">Iudgement</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82352-1610" facs="A82352-001-a-1440">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1620" facs="A82352-001-a-1450">the</w>
          <c> </c>
          <w lemma="contrary" ana="#j" reg="contrary" xml:id="A82352-1630" facs="A82352-001-a-1460">contrary</w>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-av" reg="notwithstanding" xml:id="A82352-1640" facs="A82352-001-a-1470">notwithstanding</w>
          <pc unit="sentence" xml:id="A82352-1650" facs="A82352-001-a-1480">.</pc>
        </p>
        <closer xml:id="A82352e-110">
          <signed xml:id="A82352e-120">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A82352-1690" facs="A82352-001-a-1490">Hen</w>
            <pc xml:id="A82352-1700" facs="A82352-001-a-1500">:</pc>
            <c> </c>
            <w lemma="Scobell" ana="#n1-nn" reg="Scobell" xml:id="A82352-1710" facs="A82352-001-a-1510">Scobell</w>
            <pc xml:id="A82352-1720" facs="A82352-001-a-1520">,</pc>
            <c> </c>
            <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A82352-1730" facs="A82352-001-a-1530">Clerk</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-1740" facs="A82352-001-a-1540">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A82352-1750" facs="A82352-001-a-1550">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82352-1760" facs="A82352-001-a-1560">Parliament</w>
            <pc unit="sentence" xml:id="A82352-1770" facs="A82352-001-a-1570">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A82352e-130">
      <div type="colophon" xml:id="A82352e-140">
        <p xml:id="A82352e-150">
          <hi xml:id="A82352e-160">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A82352-1820" facs="A82352-001-a-1580">London</w>
            <pc xml:id="A82352-1830" facs="A82352-001-a-1590">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A82352-1840" facs="A82352-001-a-1600">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A82352-1850" facs="A82352-001-a-1610">by</w>
          <c> </c>
          <hi xml:id="A82352e-170">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A82352-1860" facs="A82352-001-a-1620">John</w>
            <c> </c>
            <w lemma="field" ana="#n1" reg="field" xml:id="A82352-1870" facs="A82352-001-a-1630">Field</w>
            <pc xml:id="A82352-1880" facs="A82352-001-a-1640">,</pc>
          </hi>
          <c> </c>
          <w lemma="printer" ana="#n1" reg="printer" xml:id="A82352-1890" facs="A82352-001-a-1650">Printer</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A82352-1900" facs="A82352-001-a-1660">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A82352-1910" facs="A82352-001-a-1670">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A82352-1920" facs="A82352-001-a-1680">Parliament</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A82352-1930" facs="A82352-001-a-1690">of</w>
          <c> </c>
          <hi xml:id="A82352e-180">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A82352-1940" facs="A82352-001-a-1700">England</w>
            <pc unit="sentence" xml:id="A82352-1950" facs="A82352-001-a-1710">.</pc>
            <c> </c>
          </hi>
          <w lemma="1653." ana="#crd" reg="1653." xml:id="A82352-1960" facs="A82352-001-a-1720">1653.</w>
          <pc unit="sentence" xml:id="A82352-1960-eos" facs="A82352-001-a-1730"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>