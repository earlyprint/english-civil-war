<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The Committee of the Militia London, and the liberties thereof, earnestly desire you to enquire what armes are in your ward, ...</title>
        <author>City of London (England). Committee for the Militia.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1648</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A80247</idno>
        <idno type="STC">Wing C5566</idno>
        <idno type="STC">Thomason 669.f.12[29]</idno>
        <idno type="STC">ESTC R210784</idno>
        <idno type="EEBO-CITATION">99869541</idno>
        <idno type="PROQUEST">99869541</idno>
        <idno type="VID">162822</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A80247)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162822)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f12[29])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The Committee of the Militia London, and the liberties thereof, earnestly desire you to enquire what armes are in your ward, ...</title>
            <author>City of London (England). Committee for the Militia.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1648]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from opening lines of text.</note>
            <note>Imprint from Wing.</note>
            <note>"Dated at Guild-Hall the Twentyeth of May, 1648. Signed in the name, and by the Warrant of the Committee of the Militia London, [blank], Clerk to the said committee."</note>
            <note>Annotations on Thomason copy: "By Adam Bankes"; "farmingdon within"; [on verso, most likely not by Thomason] "To mr. George Thomason comon Counsell-man".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-11</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-11</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A80247e-10">
    <body xml:id="A80247e-20">
      <div type="text" xml:id="A80247e-30">
        <pb facs="tcp:162822:1" rendition="simple:additions" xml:id="A80247-001-a"/>
        <p xml:id="A80247e-40">
          <w lemma="the" ana="#d" reg="The" rend="initialcharacterdecorated" xml:id="A80247-0030" facs="A80247-001-a-0010">THe</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A80247-0040" facs="A80247-001-a-0020">Committee</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-0050" facs="A80247-001-a-0030">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-0060" facs="A80247-001-a-0040">the</w>
          <c> </c>
          <w lemma="militia" ana="#n1" reg="militia" xml:id="A80247-0070" facs="A80247-001-a-0050">Militia</w>
          <c> </c>
          <hi xml:id="A80247e-50">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A80247-0080" facs="A80247-001-a-0060">London</w>
            <pc xml:id="A80247-0090" facs="A80247-001-a-0070">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0100" facs="A80247-001-a-0080">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-0110" facs="A80247-001-a-0090">the</w>
          <c> </c>
          <w lemma="liberty" ana="#n2" reg="liberties" xml:id="A80247-0120" facs="A80247-001-a-0100">Liberties</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A80247-0130" facs="A80247-001-a-0110">thereof</w>
          <pc xml:id="A80247-0140" facs="A80247-001-a-0120">,</pc>
          <c> </c>
          <w lemma="carnest" ana="#av_j" reg="carnestly" xml:id="A80247-0150" facs="A80247-001-a-0130">carnestly</w>
          <c> </c>
          <w lemma="desire" ana="#vvb" reg="desire" xml:id="A80247-0160" facs="A80247-001-a-0140">desire</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A80247-0170" facs="A80247-001-a-0150">you</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A80247-0180" facs="A80247-001-a-0160">to</w>
          <c> </c>
          <w lemma="inquire" ana="#vvi" reg="inquire" xml:id="A80247-0190" facs="A80247-001-a-0170">enquire</w>
          <c> </c>
          <w lemma="what" ana="#crq-r" reg="what" xml:id="A80247-0200" facs="A80247-001-a-0180">what</w>
          <c> </c>
          <w lemma="arm" ana="#n2" reg="arms" xml:id="A80247-0210" facs="A80247-001-a-0190">Armes</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A80247-0220" facs="A80247-001-a-0200">are</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A80247-0230" facs="A80247-001-a-0210">in</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A80247-0240" facs="A80247-001-a-0220">your</w>
          <c> </c>
          <w lemma="ward" ana="#n1" reg="ward" xml:id="A80247-0250" facs="A80247-001-a-0230">Ward</w>
          <pc xml:id="A80247-0260" facs="A80247-001-a-0240">,</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A80247-0270" facs="A80247-001-a-0250">which</w>
          <c> </c>
          <w lemma="belong" ana="#vvb" reg="belong" xml:id="A80247-0280" facs="A80247-001-a-0260">belong</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A80247-0290" facs="A80247-001-a-0270">to</w>
          <c> </c>
          <w lemma="auxiliary" ana="#n2_j" reg="auxiliaries" xml:id="A80247-0300" facs="A80247-001-a-0280">Auxiliaries</w>
          <pc xml:id="A80247-0310" facs="A80247-001-a-0290">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0320" facs="A80247-001-a-0300">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A80247-0330" facs="A80247-001-a-0310">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A80247-0340" facs="A80247-001-a-0320">take</w>
          <c> </c>
          <w lemma="care" ana="#n1" reg="care" xml:id="A80247-0350" facs="A80247-001-a-0330">care</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A80247-0360" facs="A80247-001-a-0340">that</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A80247-0370" facs="A80247-001-a-0350">good</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0380" facs="A80247-001-a-0360">and</w>
          <c> </c>
          <w lemma="sufficient" ana="#j" reg="sufficient" xml:id="A80247-0390" facs="A80247-001-a-0370">sufficient</w>
          <c> </c>
          <w lemma="guard" ana="#n2" reg="guards" xml:id="A80247-0400" facs="A80247-001-a-0380">Guards</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A80247-0410" facs="A80247-001-a-0390">be</w>
          <c> </c>
          <w lemma="set" ana="#vvn" reg="set" xml:id="A80247-0420" facs="A80247-001-a-0400">set</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A80247-0430" facs="A80247-001-a-0410">upon</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A80247-0440" facs="A80247-001-a-0420">them</w>
          <pc xml:id="A80247-0450" facs="A80247-001-a-0430">,</pc>
          <c> </c>
          <w lemma="because" ana="#acp-cs" reg="because" xml:id="A80247-0460" facs="A80247-001-a-0440">because</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-0470" facs="A80247-001-a-0450">the</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A80247-0480" facs="A80247-001-a-0460">Committee</w>
          <c> </c>
          <w lemma="have" ana="#vvz" reg="hath" xml:id="A80247-0490" facs="A80247-001-a-0470">hath</w>
          <c> </c>
          <w lemma="information" ana="#n1" reg="information" xml:id="A80247-0500" facs="A80247-001-a-0480">information</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A80247-0510" facs="A80247-001-a-0490">that</w>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A80247-0520" facs="A80247-001-a-0500">there</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A80247-0530" facs="A80247-001-a-0510">is</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A80247-0540" facs="A80247-001-a-0520">a</w>
          <c> </c>
          <w lemma="purpose" ana="#n1" reg="purpose" xml:id="A80247-0550" facs="A80247-001-a-0530">purpose</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-0560" facs="A80247-001-a-0540">of</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A80247-0570" facs="A80247-001-a-0550">some</w>
          <c> </c>
          <w lemma="evil" ana="#n1_j" reg="evil" xml:id="A80247-0580" facs="A80247-001-a-0560">evill</w>
          <c> </c>
          <w lemma="dispose" ana="#vvn" reg="disposed" xml:id="A80247-0590" facs="A80247-001-a-0570">disposed</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A80247-0600" facs="A80247-001-a-0580">Persons</w>
          <c> </c>
          <w lemma="speedy" ana="#av_j" reg="speedily" xml:id="A80247-0610" facs="A80247-001-a-0590">speedily</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A80247-0620" facs="A80247-001-a-0600">to</w>
          <c> </c>
          <w lemma="seize" ana="#vvi" reg="seize" xml:id="A80247-0630" facs="A80247-001-a-0610">seize</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-0640" facs="A80247-001-a-0620">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A80247-0650" facs="A80247-001-a-0630">said</w>
          <c> </c>
          <w lemma="arm" ana="#n2" reg="arms" xml:id="A80247-0660" facs="A80247-001-a-0640">Armes</w>
          <pc xml:id="A80247-0670" facs="A80247-001-a-0650">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0680" facs="A80247-001-a-0660">and</w>
          <c> </c>
          <w lemma="use" ana="#vvi" reg="use" xml:id="A80247-0690" facs="A80247-001-a-0670">use</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A80247-0700" facs="A80247-001-a-0680">them</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A80247-0710" facs="A80247-001-a-0690">upon</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A80247-0720" facs="A80247-001-a-0700">a</w>
          <c> </c>
          <w lemma="mischievous" ana="#j" reg="mischievous" xml:id="A80247-0730" facs="A80247-001-a-0710">mischievous</w>
          <c> </c>
          <w lemma="design" ana="#n1" reg="design" xml:id="A80247-0740" facs="A80247-001-a-0720">design</w>
          <pc xml:id="A80247-0750" facs="A80247-001-a-0730">:</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0760" facs="A80247-001-a-0740">And</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A80247-0770" facs="A80247-001-a-0750">within</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A80247-0780" facs="A80247-001-a-0760">a</w>
          <c> </c>
          <w lemma="very" ana="#j" reg="very" xml:id="A80247-0790" facs="A80247-001-a-0770">very</w>
          <c> </c>
          <w lemma="few" ana="#d" reg="few" xml:id="A80247-0800" facs="A80247-001-a-0780">few</w>
          <c> </c>
          <w lemma="day" ana="#n2" reg="days" xml:id="A80247-0810" facs="A80247-001-a-0790">days</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-0820" facs="A80247-001-a-0800">the</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A80247-0830" facs="A80247-001-a-0810">Committee</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A80247-0840" facs="A80247-001-a-0820">will</w>
          <c> </c>
          <w lemma="ease" ana="#vvi" reg="ease" xml:id="A80247-0850" facs="A80247-001-a-0830">ease</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-0860" facs="A80247-001-a-0840">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A80247-0870" facs="A80247-001-a-0850">this</w>
          <c> </c>
          <w lemma="trouble" ana="#n1" reg="trouble" xml:id="A80247-0880" facs="A80247-001-a-0860">trouble</w>
          <pc xml:id="A80247-0890" facs="A80247-001-a-0870">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A80247-0900" facs="A80247-001-a-0880">and</w>
          <c> </c>
          <w lemma="provide" ana="#vvi" reg="provide" xml:id="A80247-0910" facs="A80247-001-a-0890">provide</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A80247-0920" facs="A80247-001-a-0900">a</w>
          <c> </c>
          <w lemma="convenient" ana="#j" reg="convenient" xml:id="A80247-0930" facs="A80247-001-a-0910">convenient</w>
          <c> </c>
          <w lemma="place" ana="#n1" reg="place" xml:id="A80247-0940" facs="A80247-001-a-0920">place</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A80247-0950" facs="A80247-001-a-0930">for</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A80247-0960" facs="A80247-001-a-0940">them</w>
          <pc xml:id="A80247-0970" facs="A80247-001-a-0950">,</pc>
          <c> </c>
          <w lemma="where" ana="#crq-cs" reg="where" xml:id="A80247-0980" facs="A80247-001-a-0960">where</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A80247-0990" facs="A80247-001-a-0970">they</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A80247-1000" facs="A80247-001-a-0980">may</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A80247-1010" facs="A80247-001-a-0990">be</w>
          <c> </c>
          <w lemma="safe" ana="#av_j" reg="safely" xml:id="A80247-1020" facs="A80247-001-a-1000">safely</w>
          <c> </c>
          <w lemma="keep" ana="#vvn" reg="kept" xml:id="A80247-1030" facs="A80247-001-a-1010">kept</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A80247-1040" facs="A80247-001-a-1020">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-1050" facs="A80247-001-a-1030">the</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A80247-1060" facs="A80247-001-a-1040">publike</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A80247-1070" facs="A80247-001-a-1050">good</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-1080" facs="A80247-001-a-1060">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A80247-1090" facs="A80247-001-a-1070">the</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A80247-1100" facs="A80247-001-a-1080">City</w>
          <pc xml:id="A80247-1110" facs="A80247-001-a-1090">,</pc>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A80247-1120" facs="A80247-001-a-1100">if</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A80247-1130" facs="A80247-001-a-1110">you</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A80247-1140" facs="A80247-001-a-1120">shall</w>
          <c> </c>
          <w lemma="think" ana="#vvi" reg="think" xml:id="A80247-1150" facs="A80247-001-a-1130">think</w>
          <c> </c>
          <w lemma="fit" ana="#j" reg="fit" xml:id="A80247-1160" facs="A80247-001-a-1140">fit</w>
          <pc unit="sentence" xml:id="A80247-1170" facs="A80247-001-a-1150">.</pc>
        </p>
        <closer xml:id="A80247e-60">
          <dateline xml:id="A80247e-70">
            <w lemma="date" ana="#j_vn" reg="dated" xml:id="A80247-1210" facs="A80247-001-a-1160">Dated</w>
            <c> </c>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A80247-1220" facs="A80247-001-a-1170">at</w>
            <c> </c>
            <hi xml:id="A80247e-80">
              <w lemma="Guild-hall" ana="#n1-nn" reg="Guild-hall" xml:id="A80247-1230" facs="A80247-001-a-1180">Guild-Hall</w>
            </hi>
            <date xml:id="A80247e-90">
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1250" facs="A80247-001-a-1190">the</w>
              <c> </c>
              <w lemma="twenty" ana="#ord" reg="twentyeth" xml:id="A80247-1260" facs="A80247-001-a-1200">Twentyeth</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-1270" facs="A80247-001-a-1210">of</w>
              <c> </c>
              <w lemma="May" ana="#n1-nn" reg="May" xml:id="A80247-1280" facs="A80247-001-a-1220">May</w>
              <pc xml:id="A80247-1290" facs="A80247-001-a-1230">,</pc>
              <c> </c>
              <w lemma="1648." ana="#crd" reg="1648." xml:id="A80247-1300" facs="A80247-001-a-1240">1648.</w>
              <pc unit="sentence" xml:id="A80247-1300-eos" facs="A80247-001-a-1250"/>
            </date>
          </dateline>
          <signed xml:id="A80247e-100">
            <hi xml:id="A80247e-110">
              <w lemma="sign" ana="#j_vn" reg="signed" xml:id="A80247-1330" facs="A80247-001-a-1260">Signed</w>
              <c> </c>
              <w lemma="in" ana="#acp-p" reg="in" xml:id="A80247-1340" facs="A80247-001-a-1270">in</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1350" facs="A80247-001-a-1280">the</w>
              <c> </c>
              <w lemma="name" ana="#n1" reg="name" xml:id="A80247-1360" facs="A80247-001-a-1290">Name</w>
              <pc xml:id="A80247-1370" facs="A80247-001-a-1300">,</pc>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A80247-1380" facs="A80247-001-a-1310">and</w>
              <c> </c>
              <w lemma="by" ana="#acp-p" reg="by" xml:id="A80247-1390" facs="A80247-001-a-1320">by</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1400" facs="A80247-001-a-1330">the</w>
              <c> </c>
              <w lemma="warrant" ana="#n1" reg="warrant" xml:id="A80247-1410" facs="A80247-001-a-1340">Warrant</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-1420" facs="A80247-001-a-1350">of</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1430" facs="A80247-001-a-1360">the</w>
              <c> </c>
              <w lemma="committee" ana="#n1" reg="committee" xml:id="A80247-1440" facs="A80247-001-a-1370">Committee</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-1450" facs="A80247-001-a-1380">of</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1460" facs="A80247-001-a-1390">the</w>
            </hi>
            <c> </c>
            <w lemma="militia" ana="#n1" reg="militia" xml:id="A80247-1470" facs="A80247-001-a-1400">Militia</w>
            <c> </c>
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A80247-1480" facs="A80247-001-a-1410">London</w>
            <pc xml:id="A80247-1490" facs="A80247-001-a-1420">,</pc>
            <add xml:id="A80247e-120">
              <c> </c>
              <w lemma="by" ana="#acp-p" reg="by" xml:id="A80247-1500" facs="A80247-001-a-1430">BY</w>
              <c> </c>
              <w lemma="Adam" ana="#n1-nn" reg="Adam" xml:id="A80247-1510" facs="A80247-001-a-1440">Adam</w>
              <c> </c>
              <w lemma="Banckes" ana="#n1-nn" reg="Banckes" xml:id="A80247-1520" facs="A80247-001-a-1450">Banckes</w>
            </add>
            <c> </c>
            <hi xml:id="A80247e-130">
              <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A80247-1530" facs="A80247-001-a-1460">Clerk</w>
              <c> </c>
              <w lemma="to" ana="#acp-p" reg="to" xml:id="A80247-1540" facs="A80247-001-a-1470">to</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1550" facs="A80247-001-a-1480">the</w>
              <c> </c>
              <w lemma="say" ana="#j_vn" reg="said" xml:id="A80247-1560" facs="A80247-001-a-1490">said</w>
              <c> </c>
              <w lemma="committee" ana="#n1" reg="committee" xml:id="A80247-1570" facs="A80247-001-a-1500">Committee</w>
              <pc unit="sentence" xml:id="A80247-1580" facs="A80247-001-a-1510">.</pc>
              <c> </c>
            </hi>
          </signed>
        </closer>
        <postscript xml:id="A80247e-140">
          <p xml:id="A80247e-150">
            <hi xml:id="A80247e-160">
              <w lemma="to" ana="#acp-p" reg="to" xml:id="A80247-1620" facs="A80247-001-a-1520">To</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1630" facs="A80247-001-a-1530">the</w>
              <c> </c>
              <w lemma="deputy" ana="#n1" reg="deputy" xml:id="A80247-1640" facs="A80247-001-a-1540">Deputy</w>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A80247-1650" facs="A80247-001-a-1550">and</w>
              <c> </c>
              <w lemma="common-council-man" ana="#n2" reg="common-council-men" xml:id="A80247-1660" facs="A80247-001-a-1560">Common-councell-men</w>
              <c> </c>
              <w lemma="in" ana="#acp-p" reg="in" xml:id="A80247-1670" facs="A80247-001-a-1570">in</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A80247-1680" facs="A80247-001-a-1580">the</w>
              <c> </c>
              <w lemma="ward" ana="#n1" reg="ward" xml:id="A80247-1690" facs="A80247-001-a-1590">Ward</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A80247-1700" facs="A80247-001-a-1600">of</w>
            </hi>
            <add xml:id="A80247e-170">
              <c> </c>
              <w lemma="farington" ana="#n1-nn" reg="Farington" xml:id="A80247-1710" facs="A80247-001-a-1610">farington</w>
              <c> </c>
              <w lemma="wthin" part="I" ana="#n1" reg="wthin" xml:id="A80247-1720.1" facs="A80247-001-a-1620" rendition="hi-mid-2">wthin</w>
              <hi rend="sup" xml:id="A80247e-180"/>
            </add>
          </p>
        </postscript>
      </div>
    </body>
    <back xml:id="A80247e-190">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>