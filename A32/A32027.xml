<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>[A proclamation commanding the muster master generall ...]</title>
        <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1643</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A32027</idno>
        <idno type="STC">Wing C2556</idno>
        <idno type="STC">ESTC R39029</idno>
        <idno type="EEBO-CITATION">18206659</idno>
        <idno type="OCLC">ocm 18206659</idno>
        <idno type="VID">107096</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A32027)</note>
        <note>Transcribed from: (Early English Books Online ; image set 107096)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1629:46)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>[A proclamation commanding the muster master generall ...]</title>
            <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
            <author>Charles I, King of England, 1600-1649.</author>
          </titleStmt>
          <extent>1 broadside.</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[Oxford :</pubPlace>
            <date>1643]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title and imprint suggested by Wing.</note>
            <note>At head of fragment: ... punished with death. No muster-master shall receive or ...</note>
            <note>In ms. at foot of fragment: Part of a broadside issued at Oxford by Ch. I.</note>
            <note>Imperfect: fragment. Best copy available for photographing.</note>
            <note>"Given at our court at Oxford, this eleaventh day of Novemb ..."</note>
            <note>Reproduction of original in the Bodleian Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Civil War, 1642-1649.</term>
          <term>Great Britain -- Politics and government -- 1642-1649.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-05</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-06</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-06</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A32027e-10">
    <body xml:id="A32027e-20">
      <div type="text" xml:id="A32027e-30">
        <pb facs="tcp:107096:1" rendition="simple:additions" xml:id="A32027-001-a"/>
        <p xml:id="A32027e-40">
          <w xml:id="A32027-0030" lemma="〈…〉" ana="#zz" facs="A32027-001-a-0010">〈…〉</w>
          <c> </c>
          <w lemma="punish" ana="#vvn" reg="punished" xml:id="A32027-0040" facs="A32027-001-a-0020">punished</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A32027-0050" facs="A32027-001-a-0030">with</w>
          <c> </c>
          <w lemma="death" ana="#n1" reg="death" xml:id="A32027-0060" facs="A32027-001-a-0040">death</w>
          <pc unit="sentence" xml:id="A32027-0070" facs="A32027-001-a-0050">.</pc>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="No" xml:id="A32027-0080" facs="A32027-001-a-0060">No</w>
          <c> </c>
          <w lemma="muster-master" ana="#n1" reg="muster-master" xml:id="A32027-0090" facs="A32027-001-a-0070">Muster-master</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A32027-0100" facs="A32027-001-a-0080">shall</w>
          <c> </c>
          <w lemma="receive" ana="#vvi" reg="receive" xml:id="A32027-0110" facs="A32027-001-a-0090">receive</w>
          <c> </c>
          <w lemma="of•" ana="#fw-la" reg="of•" xml:id="A32027-0120" facs="A32027-001-a-0100">of•</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A32027-0130" facs="A32027-001-a-0110">so</w>
          <c> </c>
          <w lemma="testify" ana="#vvn" reg="testified" xml:id="A32027-0140" facs="A32027-001-a-0120">testified</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A32027-0150" facs="A32027-001-a-0130">upon</w>
          <c> </c>
          <w lemma="pain" ana="#n1" reg="pain" xml:id="A32027-0160" facs="A32027-001-a-0140">paine</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-0170" facs="A32027-001-a-0150">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-0180" facs="A32027-001-a-0160">the</w>
          <c> </c>
          <w lemma="loss" ana="#n1" reg="loss" xml:id="A32027-0190" facs="A32027-001-a-0170">losse</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-0200" facs="A32027-001-a-0180">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A32027-0210" facs="A32027-001-a-0190">his</w>
          <c> </c>
          <w lemma="place" ana="#n1" reg="place" xml:id="A32027-0220" facs="A32027-001-a-0200">place</w>
          <pc xml:id="A32027-0230" facs="A32027-001-a-0210">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-0240" facs="A32027-001-a-0220">and</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A32027-0250" facs="A32027-001-a-0230">further</w>
          <c> </c>
          <w lemma="himself" ana="#px" reg="himself" xml:id="A32027-0260" facs="A32027-001-a-0240">himselfe</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A32027-0270" facs="A32027-001-a-0250">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-0280" facs="A32027-001-a-0260">the</w>
          <c> </c>
          <w lemma="muster" ana="#n1" reg="muster" xml:id="A32027-0290" facs="A32027-001-a-0270">Muster</w>
          <pc xml:id="A32027-0300" facs="A32027-001-a-0280">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A32027-0310" facs="A32027-001-a-0290">or</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A32027-0320" facs="A32027-001-a-0300">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A32027-0330" facs="A32027-001-a-0310">be</w>
          <c> </c>
          <w lemma="enroll" ana="#vvn" reg="enrolled" xml:id="A32027-0340" facs="A32027-001-a-0320">enrolled</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A32027-0350" facs="A32027-001-a-0330">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-0360" facs="A32027-001-a-0340">the</w>
          <c> </c>
          <w lemma="muster" ana="#n1" reg="muster" xml:id="A32027-0370" facs="A32027-001-a-0350">Muster</w>
          <c> </c>
          <w lemma="ro•" ana="#fw-la" reg="ro•" xml:id="A32027-0380" facs="A32027-001-a-0360">Ro•</w>
          <c> </c>
          <w lemma="birth" ana="#n1" reg="birth" xml:id="A32027-0390" facs="A32027-001-a-0370">birth</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A32027-0400" facs="A32027-001-a-0380">or</w>
          <c> </c>
          <w lemma="habitation" ana="#n1" reg="habitation" xml:id="A32027-0410" facs="A32027-001-a-0390">habitation</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A32027-0420" facs="A32027-001-a-0400">upon</w>
          <c> </c>
          <w lemma="pain" ana="#n1" reg="pain" xml:id="A32027-0430" facs="A32027-001-a-0410">paine</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-0440" facs="A32027-001-a-0420">of</w>
          <c> </c>
          <w lemma="death" ana="#n1" reg="death" xml:id="A32027-0450" facs="A32027-001-a-0430">death</w>
          <pc unit="sentence" xml:id="A32027-0460" facs="A32027-001-a-0440">.</pc>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="To" xml:id="A32027-0470" facs="A32027-001-a-0450">To</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A32027-0480" facs="A32027-001-a-0460">which</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A32027-0490" facs="A32027-001-a-0470">We</w>
          <c> </c>
          <w lemma="h•" ana="#fw-la" reg="h•" xml:id="A32027-0500" facs="A32027-001-a-0480">h•</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A32027-0510" facs="A32027-001-a-0490">shall</w>
          <c> </c>
          <w lemma="after" ana="#acp-p" reg="after" xml:id="A32027-0520" facs="A32027-001-a-0500">after</w>
          <c> </c>
          <w lemma="twenty" ana="#crd" reg="twenty" xml:id="A32027-0530" facs="A32027-001-a-0510">twenty</w>
          <c> </c>
          <w lemma="day" ana="#n2" reg="days" xml:id="A32027-0540" facs="A32027-001-a-0520">dayes</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A32027-0550" facs="A32027-001-a-0530">from</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-0560" facs="A32027-001-a-0540">the</w>
          <c> </c>
          <w lemma="publication" ana="#n1" reg="publication" xml:id="A32027-0570" facs="A32027-001-a-0550">publication</w>
          <c> </c>
          <w lemma="hereof" ana="#av" reg="hereof" xml:id="A32027-0580" facs="A32027-001-a-0560">hereof</w>
          <pc xml:id="A32027-0590" facs="A32027-001-a-0570">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A32027-0600" facs="A32027-001-a-0580">be</w>
          <c> </c>
          <w lemma="〈◊〉" ana="#zz" reg="〈◊〉" xml:id="A32027-0610" facs="A32027-001-a-0590">〈◊〉</w>
          <c> </c>
          <w lemma="horseman" ana="#n1" reg="horseman" xml:id="A32027-0620" facs="A32027-001-a-0600">Horseman</w>
          <c> </c>
          <w lemma="without" ana="#acp-p" reg="without" xml:id="A32027-0630" facs="A32027-001-a-0610">without</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A32027-0640" facs="A32027-001-a-0620">a</w>
          <c> </c>
          <w lemma="sword" ana="#n1" reg="sword" xml:id="A32027-0650" facs="A32027-001-a-0630">Sword</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A32027-0660" facs="A32027-001-a-0640">or</w>
          <c> </c>
          <w lemma="pistol" ana="#n1" reg="pistol" xml:id="A32027-0670" facs="A32027-001-a-0650">Pistoll</w>
          <pc unit="sentence" xml:id="A32027-0680" facs="A32027-001-a-0660">.</pc>
          <c> </c>
          <w lemma="nor" ana="#cc-x" reg="Nor" xml:id="A32027-0690" facs="A32027-001-a-0670">Nor</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A32027-0700" facs="A32027-001-a-0680">that</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A32027-0710" facs="A32027-001-a-0690">any</w>
          <c> </c>
          <w lemma="n•" ana="#n1" reg="n•" xml:id="A32027-0720" facs="A32027-001-a-0700">N•</w>
          <c> </c>
          <w lemma="muster-master" ana="#n1" reg="muster-master" xml:id="A32027-0730" facs="A32027-001-a-0710">Muster-master</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A32027-0740" facs="A32027-001-a-0720">or</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A32027-0750" facs="A32027-001-a-0730">his</w>
          <c> </c>
          <w lemma="deputy" ana="#n2" reg="deputies" xml:id="A32027-0760" facs="A32027-001-a-0740">Deputies</w>
          <pc xml:id="A32027-0770" facs="A32027-001-a-0750">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-0780" facs="A32027-001-a-0760">and</w>
          <c> </c>
          <w lemma="attest" ana="#vvn" reg="attested" xml:id="A32027-0790" facs="A32027-001-a-0770">attested</w>
          <c> </c>
          <w lemma="under" ana="#acp-p" reg="under" xml:id="A32027-0800" facs="A32027-001-a-0780">under</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A32027-0810" facs="A32027-001-a-0790">their</w>
          <c> </c>
          <w lemma="ha••spective" ana="#j" reg="ha••spective" xml:id="A32027-0820" facs="A32027-001-a-0800">ha••spective</w>
          <c> </c>
          <w lemma="garrison" ana="#n2" reg="garrisons" xml:id="A32027-0830" facs="A32027-001-a-0810">Garrisons</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-0840" facs="A32027-001-a-0820">and</w>
          <c> </c>
          <w lemma="command" ana="#n2" reg="commands" xml:id="A32027-0850" facs="A32027-001-a-0830">Commands</w>
          <c> </c>
          <w lemma="without" ana="#acp-p" reg="without" xml:id="A32027-0860" facs="A32027-001-a-0840">without</w>
          <c> </c>
          <w lemma="licence" ana="#n1" reg="licence" xml:id="A32027-0870" facs="A32027-001-a-0850">Licence</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-0880" facs="A32027-001-a-0860">of</w>
          <c> </c>
          <w lemma="th••ther" ana="#n1" reg="th••ther" xml:id="A32027-0890" facs="A32027-001-a-0870">th••ther</w>
          <c> </c>
          <w lemma="punishment" ana="#n1" reg="punishment" xml:id="A32027-0900" facs="A32027-001-a-0880">punishment</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A32027-0910" facs="A32027-001-a-0890">as</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A32027-0920" facs="A32027-001-a-0900">is</w>
          <c> </c>
          <w lemma="mention" ana="#vvn" reg="mentioned" xml:id="A32027-0930" facs="A32027-001-a-0910">mentioned</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A32027-0940" facs="A32027-001-a-0920">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-0950" facs="A32027-001-a-0930">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A32027-0960" facs="A32027-001-a-0940">said</w>
          <c> </c>
          <w lemma="article" ana="#n2" reg="articles" xml:id="A32027-0970" facs="A32027-001-a-0950">Articles</w>
          <pc unit="sentence" xml:id="A32027-0980" facs="A32027-001-a-0960">.</pc>
          <c> </c>
          <w lemma="all" ana="#av_d" reg="All" xml:id="A32027-0990" facs="A32027-001-a-0970">All</w>
          <c> </c>
          <w lemma="〈◊〉" ana="#zz" reg="〈◊〉" xml:id="A32027-1000" facs="A32027-001-a-0980">〈◊〉</w>
          <c> </c>
          <w lemma="general" ana="#n1" reg="general" xml:id="A32027-1010" facs="A32027-001-a-0990">Generall</w>
          <pc xml:id="A32027-1020" facs="A32027-001-a-1000">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1030" facs="A32027-001-a-1010">and</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A32027-1040" facs="A32027-001-a-1020">his</w>
          <c> </c>
          <w lemma="deputy" ana="#n2" reg="deputies" xml:id="A32027-1050" facs="A32027-001-a-1030">Deputies</w>
          <pc xml:id="A32027-1060" facs="A32027-001-a-1040">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1070" facs="A32027-001-a-1050">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A32027-1080" facs="A32027-001-a-1060">all</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A32027-1090" facs="A32027-001-a-1070">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1100" facs="A32027-001-a-1080">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A32027-1110" facs="A32027-001-a-1090">Souldiers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1120" facs="A32027-001-a-1100">of</w>
          <c> </c>
          <w lemma="〈◊〉" ana="#zz" reg="〈◊〉" xml:id="A32027-1130" facs="A32027-001-a-1110">〈◊〉</w>
          <c> </c>
          <w lemma="inflict" ana="#vvi" reg="inflict" xml:id="A32027-1140" facs="A32027-001-a-1120">inflict</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1150" facs="A32027-001-a-1130">the</w>
          <c> </c>
          <w lemma="penalty" ana="#n2" reg="penalties" xml:id="A32027-1160" facs="A32027-001-a-1140">penalties</w>
          <c> </c>
          <w lemma="therein" ana="#av" reg="therein" xml:id="A32027-1170" facs="A32027-001-a-1150">therein</w>
          <c> </c>
          <w lemma="mention" ana="#vvn" reg="mentioned" xml:id="A32027-1180" facs="A32027-001-a-1160">mentioned</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A32027-1190" facs="A32027-001-a-1170">with</w>
          <c> </c>
          <w lemma="severity" ana="#n1" reg="severity" xml:id="A32027-1200" facs="A32027-001-a-1180">severity</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A32027-1210" facs="A32027-001-a-1190">upon</w>
          <c> </c>
          <w lemma="〈◊〉" ana="#zz" reg="〈◊〉" xml:id="A32027-1220" facs="A32027-001-a-1200">〈◊〉</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1230" facs="A32027-001-a-1210">And</w>
          <c> </c>
          <w lemma="because" ana="#acp-cs" reg="because" xml:id="A32027-1240" facs="A32027-001-a-1220">because</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1250" facs="A32027-001-a-1230">the</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A32027-1260" facs="A32027-001-a-1240">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1270" facs="A32027-001-a-1250">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A32027-1280" facs="A32027-001-a-1260">Souldiers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1290" facs="A32027-001-a-1270">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1300" facs="A32027-001-a-1280">the</w>
          <c> </c>
          <w lemma="foot" ana="#n1" reg="foot" xml:id="A32027-1310" facs="A32027-001-a-1290">Foot</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1320" facs="A32027-001-a-1300">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A32027-1330" facs="A32027-001-a-1310">Our</w>
          <c> </c>
          <w lemma="•bled" ana="#j_vn" reg="•bled" xml:id="A32027-1340" facs="A32027-001-a-1320">•bled</w>
          <c> </c>
          <w lemma="cheerful" ana="#av_j" reg="cheerfully" xml:id="A32027-1350" facs="A32027-001-a-1330">chearefully</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A32027-1360" facs="A32027-001-a-1340">to</w>
          <c> </c>
          <w lemma="perform" ana="#vvi" reg="perform" xml:id="A32027-1370" facs="A32027-001-a-1350">performe</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1380" facs="A32027-001-a-1360">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A32027-1390" facs="A32027-001-a-1370">same</w>
          <pc unit="sentence" xml:id="A32027-1400" facs="A32027-001-a-1380">.</pc>
          <c> </c>
          <w lemma="we" ana="#pns" reg="We" xml:id="A32027-1410" facs="A32027-001-a-1390">We</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A32027-1420" facs="A32027-001-a-1400">are</w>
          <c> </c>
          <w lemma="gracious" ana="#av_j" reg="graciously" xml:id="A32027-1430" facs="A32027-001-a-1410">gratiously</w>
          <c> </c>
          <w lemma="•taines" ana="#vvz" reg="•taines" xml:id="A32027-1440" facs="A32027-001-a-1420">•taines</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1450" facs="A32027-001-a-1430">of</w>
          <c> </c>
          <w lemma="foot" ana="#n1" reg="foot" xml:id="A32027-1460" facs="A32027-001-a-1440">Foot</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1470" facs="A32027-001-a-1450">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A32027-1480" facs="A32027-001-a-1460">Our</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A32027-1490" facs="A32027-001-a-1470">Army</w>
          <pc xml:id="A32027-1500" facs="A32027-001-a-1480">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A32027-1510" facs="A32027-001-a-1490">shall</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A32027-1520" facs="A32027-001-a-1500">every</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1530" facs="A32027-001-a-1510">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A32027-1540" facs="A32027-001-a-1520">their</w>
          <c> </c>
          <w lemma="receive" ana="#n1" reg="receive" xml:id="A32027-1550" facs="A32027-001-a-1530">receive</w>
          <c> </c>
          <w lemma="t•" ana="#zz" reg="t•" xml:id="A32027-1560" facs="A32027-001-a-1540">t•</w>
          <c> </c>
          <w lemma="common" ana="#j" reg="common" xml:id="A32027-1570" facs="A32027-001-a-1550">common</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A32027-1580" facs="A32027-001-a-1560">Souldiers</w>
          <c> </c>
          <w lemma="each" ana="#d" reg="each" xml:id="A32027-1590" facs="A32027-001-a-1570">each</w>
          <c> </c>
          <w lemma="four" ana="#crd" reg="four" xml:id="A32027-1600" facs="A32027-001-a-1580">foure</w>
          <c> </c>
          <w lemma="shilling" ana="#n2" reg="shillings" xml:id="A32027-1610" facs="A32027-001-a-1590">shillings</w>
          <c> </c>
          <w lemma="weekly" ana="#j" reg="weekly" xml:id="A32027-1620" facs="A32027-001-a-1600">weekly</w>
          <pc xml:id="A32027-1630" facs="A32027-001-a-1610">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1640" facs="A32027-001-a-1620">the</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A32027-1650" facs="A32027-001-a-1630">other</w>
          <c> </c>
          <w lemma="t•" ana="#zz" reg="t•" xml:id="A32027-1660" facs="A32027-001-a-1640">t•</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A32027-1670" facs="A32027-001-a-1650">upon</w>
          <c> </c>
          <w lemma="account" ana="#n1" reg="account" xml:id="A32027-1680" facs="A32027-001-a-1660">accompt</w>
          <pc unit="sentence" xml:id="A32027-1690" facs="A32027-001-a-1670">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A32027-1700" facs="A32027-001-a-1680">And</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1710" facs="A32027-001-a-1690">the</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A32027-1720" facs="A32027-001-a-1700">Officers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1730" facs="A32027-001-a-1710">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A32027-1740" facs="A32027-001-a-1720">Souldiers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1750" facs="A32027-001-a-1730">of</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A32027-1760" facs="A32027-001-a-1740">Horse</w>
          <pc xml:id="A32027-1770" facs="A32027-001-a-1750">,</pc>
          <c> </c>
          <w lemma="〈◊〉" ana="#n1" reg="〈◊〉" xml:id="A32027-1780" facs="A32027-001-a-1760">〈◊〉</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1790" facs="A32027-001-a-1770">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A32027-1800" facs="A32027-001-a-1780">the</w>
          <c> </c>
          <w lemma="contribution" ana="#n2" reg="contributions" xml:id="A32027-1810" facs="A32027-001-a-1790">Contributions</w>
          <pc unit="sentence" xml:id="A32027-1820" facs="A32027-001-a-1800">.</pc>
          <c> </c>
          <w lemma="this" ana="#d" reg="This" xml:id="A32027-1830" facs="A32027-001-a-1810">This</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A32027-1840" facs="A32027-001-a-1820">Our</w>
          <c> </c>
          <w lemma="pleasure" ana="#n1" reg="pleasure" xml:id="A32027-1850" facs="A32027-001-a-1830">Pleasure</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A32027-1860" facs="A32027-001-a-1840">We</w>
          <c> </c>
          <w lemma="command" ana="#vvb" reg="command" xml:id="A32027-1870" facs="A32027-001-a-1850">command</w>
          <c> </c>
          <w lemma="〈◊〉" ana="#zz" reg="〈◊〉" xml:id="A32027-1880" facs="A32027-001-a-1860">〈◊〉</w>
          <c> </c>
          <w lemma="troop" ana="#n1" reg="troop" xml:id="A32027-1890" facs="A32027-001-a-1870">Troop</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1900" facs="A32027-001-a-1880">and</w>
          <c> </c>
          <w lemma="company" ana="#n1" reg="company" xml:id="A32027-1910" facs="A32027-001-a-1890">Company</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-1920" facs="A32027-001-a-1900">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A32027-1930" facs="A32027-001-a-1910">Our</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A32027-1940" facs="A32027-001-a-1920">Army</w>
          <pc xml:id="A32027-1950" facs="A32027-001-a-1930">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A32027-1960" facs="A32027-001-a-1940">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A32027-1970" facs="A32027-001-a-1950">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A32027-1980" facs="A32027-001-a-1960">be</w>
          <c> </c>
          <w lemma="full" ana="#av_j" reg="fully" xml:id="A32027-1990" facs="A32027-001-a-1970">fully</w>
          <c> </c>
          <w lemma="obey" ana="#vvi" reg="obey" xml:id="A32027-2000" facs="A32027-001-a-1980">obeye</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A32027-2010" facs="A32027-001-a-1990">from</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A32027-2020" facs="A32027-001-a-2000">time</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A32027-2030" facs="A32027-001-a-2010">to</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A32027-2040" facs="A32027-001-a-2020">time</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A32027-2050" facs="A32027-001-a-2030">to</w>
          <c> </c>
          <w lemma="receive" ana="#vvi" reg="receive" xml:id="A32027-2060" facs="A32027-001-a-2040">receive</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A32027-2070" facs="A32027-001-a-2050">a</w>
          <c> </c>
          <w lemma="strict" ana="#j" reg="strict" xml:id="A32027-2080" facs="A32027-001-a-2060">strict</w>
          <c> </c>
          <w lemma="account" ana="#n1" reg="account" xml:id="A32027-2090" facs="A32027-001-a-2070">accompt</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-2100" facs="A32027-001-a-2080">of</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A32027-2110" facs="A32027-001-a-2090">every</w>
          <c> </c>
          <w lemma="person•" ana="#n1" reg="person•" xml:id="A32027-2120" facs="A32027-001-a-2100">Person•</w>
        </p>
        <closer xml:id="A32027e-50">
          <dateline xml:id="A32027e-60">
            <hi xml:id="A32027e-70">
              <w lemma="give" ana="#vvn" reg="given" xml:id="A32027-2160" facs="A32027-001-a-2110">Given</w>
              <c> </c>
              <w lemma="at" ana="#acp-p" reg="at" xml:id="A32027-2170" facs="A32027-001-a-2120">at</w>
              <c> </c>
              <w lemma="our" ana="#po" reg="our" xml:id="A32027-2180" facs="A32027-001-a-2130">Our</w>
              <c> </c>
              <w lemma="court" ana="#n1" reg="court" xml:id="A32027-2190" facs="A32027-001-a-2140">Court</w>
              <c> </c>
              <w lemma="at" ana="#acp-p" reg="at" xml:id="A32027-2200" facs="A32027-001-a-2150">at</w>
            </hi>
            <c> </c>
            <w lemma="Oxford" ana="#n1-nn" reg="Oxford" xml:id="A32027-2210" facs="A32027-001-a-2160">Oxford</w>
            <pc xml:id="A32027-2220" facs="A32027-001-a-2170">,</pc>
            <date xml:id="A32027e-80">
              <hi xml:id="A32027e-90">
                <w lemma="this" ana="#d" reg="this" xml:id="A32027-2240" facs="A32027-001-a-2180">this</w>
                <c> </c>
                <w lemma="eleven" ana="#ord" reg="eleventh" xml:id="A32027-2250" facs="A32027-001-a-2190">Eleaventh</w>
                <c> </c>
                <w lemma="day" ana="#n1" reg="day" xml:id="A32027-2260" facs="A32027-001-a-2200">day</w>
                <c> </c>
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A32027-2270" facs="A32027-001-a-2210">of</w>
              </hi>
              <c> </c>
              <w lemma="November" ana="#n1-nn" reg="November" xml:id="A32027-2280" facs="A32027-001-a-2220">November</w>
            </date>
          </dateline>
        </closer>
        <closer xml:id="A32027e-100">
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A32027-2310" facs="A32027-001-a-2230">God</w>
          <c> </c>
          <w lemma="save" ana="#vvi" reg="save" xml:id="A32027-2320" facs="A32027-001-a-2240">save</w>
          <c> </c>
          <w xml:id="A32027-2330" lemma="〈…〉" ana="#zz" facs="A32027-001-a-2250">〈…〉</w>
        </closer>
      </div>
    </body>
    <back xml:id="A32027e-110">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>