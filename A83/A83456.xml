<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Monday June 27th 1659. Resolved, that this Parliament doth declare, that, for the encouragement of a Godly, preaching, learned ministry throughout the nation, the payment of tithes shall continue as now they are ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1659</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83456</idno>
        <idno type="STC">Wing E2261</idno>
        <idno type="STC">ESTC R211221</idno>
        <idno type="EEBO-CITATION">45097733</idno>
        <idno type="OCLC">ocm 45097733</idno>
        <idno type="VID">171327</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83456)</note>
        <note>Transcribed from: (Early English Books Online ; image set 171327)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2571:41)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Monday June 27th 1659. Resolved, that this Parliament doth declare, that, for the encouragement of a Godly, preaching, learned ministry throughout the nation, the payment of tithes shall continue as now they are ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet (1 p.).</extent>
          <publicationStmt>
            <publisher>Printed by John Field and Henry Hills, printers to the Parliament. And are to be sold at the Sev[en] Stars in Fleetstreet, over against Dunstans Church,</publisher>
            <pubPlace>London: :</pubPlace>
            <date>1659.</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from caption and first words of text.</note>
            <note>Reproduction of original in the Henry E. Huntington Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Tithes -- Great Britain -- Early works to 1800.</term>
          <term>Great Britain -- Politics and government -- 1649-1660.</term>
          <term>Broadsides -- England -- 17th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83456e-10">
    <body xml:id="A83456e-20">
      <div type="Parliamentary_resolution" xml:id="A83456e-30">
        <pb facs="tcp:171327:1" n="1" xml:id="A83456-001-a"/>
        <head xml:id="A83456e-40">
          <figure xml:id="A83456e-50">
            <figDesc xml:id="A83456e-60">blazon or coat of arms</figDesc>
          </figure>
        </head>
        <opener xml:id="A83456e-70">
          <dateline xml:id="A83456e-80">
            <date xml:id="A83456e-90">
              <w lemma="Monday" ana="#n1-nn" reg="Monday" xml:id="A83456-0140" facs="A83456-001-a-0010">Monday</w>
              <c> </c>
              <hi xml:id="A83456e-100">
                <w lemma="June" ana="#n1-nn" reg="June" xml:id="A83456-0150" facs="A83456-001-a-0020">June</w>
              </hi>
              <c> </c>
              <w lemma="27" part="I" ana="#ord" reg="27th" xml:id="A83456-0160.1" facs="A83456-001-a-0030" rendition="hi-mid-3">27th</w>
              <hi rend="sup" xml:id="A83456e-110"/>
              <c> </c>
              <w lemma="1659." ana="#crd" reg="1659." xml:id="A83456-0170" facs="A83456-001-a-0050">1659.</w>
              <pc unit="sentence" xml:id="A83456-0170-eos" facs="A83456-001-a-0060"/>
            </date>
          </dateline>
          <lb xml:id="A83456e-120"/>
          <hi xml:id="A83456e-130">
            <w lemma="resolve" ana="#vvn" reg="resolved" xml:id="A83456-0190" facs="A83456-001-a-0070">Resolved</w>
            <pc xml:id="A83456-0200" facs="A83456-001-a-0080">,</pc>
          </hi>
        </opener>
        <p xml:id="A83456e-140">
          <w lemma="that" ana="#cs" reg="That" rend="initialcharacterdecorated" xml:id="A83456-0230" facs="A83456-001-a-0090">THat</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83456-0240" facs="A83456-001-a-0100">this</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83456-0250" facs="A83456-001-a-0110">Parliament</w>
          <c> </c>
          <w lemma="do" ana="#vvz" reg="doth" xml:id="A83456-0260" facs="A83456-001-a-0120">doth</w>
          <c> </c>
          <w lemma="declare" ana="#vvi" reg="declare" xml:id="A83456-0270" facs="A83456-001-a-0130">Declare</w>
          <pc xml:id="A83456-0280" facs="A83456-001-a-0140">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83456-0290" facs="A83456-001-a-0150">that</w>
          <pc xml:id="A83456-0300" facs="A83456-001-a-0160">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83456-0310" facs="A83456-001-a-0170">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-0320" facs="A83456-001-a-0180">the</w>
          <c> </c>
          <w lemma="encouragement" ana="#n1" reg="encouragement" xml:id="A83456-0330" facs="A83456-001-a-0190">Encouragement</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83456-0340" facs="A83456-001-a-0200">of</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83456-0350" facs="A83456-001-a-0210">a</w>
          <c> </c>
          <w lemma="godly" ana="#j" reg="godly" xml:id="A83456-0360" facs="A83456-001-a-0220">Godly</w>
          <pc xml:id="A83456-0370" facs="A83456-001-a-0230">,</pc>
          <c> </c>
          <w lemma="preach" ana="#vvg" reg="preaching" xml:id="A83456-0380" facs="A83456-001-a-0240">Preaching</w>
          <pc xml:id="A83456-0390" facs="A83456-001-a-0250">,</pc>
          <c> </c>
          <w lemma="learned" ana="#j" reg="learned" xml:id="A83456-0400" facs="A83456-001-a-0260">Learned</w>
          <c> </c>
          <w lemma="ministry" ana="#n1" reg="ministry" xml:id="A83456-0410" facs="A83456-001-a-0270">Ministry</w>
          <c> </c>
          <w lemma="throughout" ana="#acp-p" reg="throughout" xml:id="A83456-0420" facs="A83456-001-a-0280">throughout</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-0430" facs="A83456-001-a-0290">the</w>
          <c> </c>
          <w lemma="nation" ana="#n1" reg="nation" xml:id="A83456-0440" facs="A83456-001-a-0300">Nation</w>
          <pc xml:id="A83456-0450" facs="A83456-001-a-0310">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-0460" facs="A83456-001-a-0320">the</w>
          <c> </c>
          <w lemma="payment" ana="#n1" reg="payment" xml:id="A83456-0470" facs="A83456-001-a-0330">Payment</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83456-0480" facs="A83456-001-a-0340">of</w>
          <c> </c>
          <w lemma="tithe" ana="#n2" reg="tithes" xml:id="A83456-0490" facs="A83456-001-a-0350">Tithes</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83456-0500" facs="A83456-001-a-0360">shall</w>
          <c> </c>
          <w lemma="continue" ana="#vvi" reg="continue" xml:id="A83456-0510" facs="A83456-001-a-0370">continue</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83456-0520" facs="A83456-001-a-0380">as</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A83456-0530" facs="A83456-001-a-0390">now</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83456-0540" facs="A83456-001-a-0400">they</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83456-0550" facs="A83456-001-a-0410">are</w>
          <pc xml:id="A83456-0560" facs="A83456-001-a-0420">,</pc>
          <c> </c>
          <w lemma="unless" ana="#cs" reg="unless" xml:id="A83456-0570" facs="A83456-001-a-0430">unless</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83456-0580" facs="A83456-001-a-0440">this</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83456-0590" facs="A83456-001-a-0450">Parliament</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83456-0600" facs="A83456-001-a-0460">shall</w>
          <c> </c>
          <w lemma="find" ana="#vvi" reg="find" xml:id="A83456-0610" facs="A83456-001-a-0470">finde</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A83456-0620" facs="A83456-001-a-0480">out</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A83456-0630" facs="A83456-001-a-0490">some</w>
          <c> </c>
          <w lemma="other" ana="#pi_d" reg="other" xml:id="A83456-0640" facs="A83456-001-a-0500">other</w>
          <c> </c>
          <w lemma="more" ana="#av-c_d" reg="more" xml:id="A83456-0650" facs="A83456-001-a-0510">more</w>
          <c> </c>
          <w lemma="equal" ana="#j" reg="equal" xml:id="A83456-0660" facs="A83456-001-a-0520">equal</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83456-0670" facs="A83456-001-a-0530">and</w>
          <c> </c>
          <w lemma="comfortable" ana="#j" reg="comfortable" xml:id="A83456-0680" facs="A83456-001-a-0540">Comfortable</w>
          <c> </c>
          <w lemma="maintenance" ana="#n1" reg="maintenance" xml:id="A83456-0690" facs="A83456-001-a-0550">Maintenance</w>
          <pc xml:id="A83456-0700" facs="A83456-001-a-0560">,</pc>
          <c> </c>
          <w lemma="both" ana="#av_d" reg="both" xml:id="A83456-0710" facs="A83456-001-a-0570">both</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83456-0720" facs="A83456-001-a-0580">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-0730" facs="A83456-001-a-0590">the</w>
          <c> </c>
          <w lemma="ministry" ana="#n1" reg="ministry" xml:id="A83456-0740" facs="A83456-001-a-0600">Ministry</w>
          <pc xml:id="A83456-0750" facs="A83456-001-a-0610">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83456-0760" facs="A83456-001-a-0620">and</w>
          <c> </c>
          <w lemma="satisfaction" ana="#n1" reg="satisfaction" xml:id="A83456-0770" facs="A83456-001-a-0630">satisfaction</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83456-0780" facs="A83456-001-a-0640">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-0790" facs="A83456-001-a-0650">the</w>
          <c> </c>
          <w lemma="people" ana="#n1" reg="persons" xml:id="A83456-0800" facs="A83456-001-a-0660">People</w>
          <pc unit="sentence" xml:id="A83456-0810" facs="A83456-001-a-0670">.</pc>
        </p>
        <div type="license" xml:id="A83456e-150">
          <opener xml:id="A83456e-160">
            <w lemma="resolve" ana="#vvn" reg="Resolved" xml:id="A83456-0850" facs="A83456-001-a-0680">Resolved</w>
            <pc xml:id="A83456-0860" facs="A83456-001-a-0690">,</pc>
          </opener>
          <p xml:id="A83456e-170">
            <w lemma="that" ana="#cs" reg="That" xml:id="A83456-0890" facs="A83456-001-a-0700">THat</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A83456-0900" facs="A83456-001-a-0710">this</w>
            <c> </c>
            <w lemma="vote" ana="#n1" reg="vote" xml:id="A83456-0910" facs="A83456-001-a-0720">Vote</w>
            <c> </c>
            <w lemma="be" ana="#vvi" reg="be" xml:id="A83456-0920" facs="A83456-001-a-0730">be</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A83456-0930" facs="A83456-001-a-0740">Printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A83456-0940" facs="A83456-001-a-0750">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A83456-0950" facs="A83456-001-a-0760">published</w>
            <pc unit="sentence" xml:id="A83456-0960" facs="A83456-001-a-0770">.</pc>
          </p>
          <closer xml:id="A83456e-180">
            <signed xml:id="A83456e-190">
              <w lemma="tho" ana="#av" reg="Tho" xml:id="A83456-1000" facs="A83456-001-a-0780">THO</w>
              <pc unit="sentence" xml:id="A83456-1010" facs="A83456-001-a-0790">.</pc>
              <c> </c>
              <w lemma="st" part="I" ana="#n-ab" reg="Saint" xml:id="A83456-1020.1" facs="A83456-001-a-0800" rendition="hi-mid-2">St</w>
              <hi rend="sup" xml:id="A83456e-200"/>
              <c> </c>
              <w lemma="NICHOLAS" ana="#n1-nn" reg="Nicholas" xml:id="A83456-1030" facs="A83456-001-a-0820">NICHOLAS</w>
              <c> </c>
              <hi xml:id="A83456e-210">
                <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A83456-1040" facs="A83456-001-a-0830">Clerk</w>
                <c> </c>
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A83456-1050" facs="A83456-001-a-0840">of</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A83456-1060" facs="A83456-001-a-0850">the</w>
                <c> </c>
                <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83456-1070" facs="A83456-001-a-0860">Parliament</w>
                <pc unit="sentence" xml:id="A83456-1080" facs="A83456-001-a-0870">.</pc>
                <c> </c>
              </hi>
            </signed>
          </closer>
        </div>
      </div>
    </body>
    <back xml:id="A83456e-220">
      <div type="colophon" xml:id="A83456e-230">
        <p xml:id="A83456e-240">
          <hi xml:id="A83456e-250">
            <w lemma="LONDON" ana="#n1-nn" reg="London" xml:id="A83456-1130" facs="A83456-001-a-0880">LONDON</w>
            <pc xml:id="A83456-1140" facs="A83456-001-a-0890">:</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83456-1150" facs="A83456-001-a-0900">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83456-1160" facs="A83456-001-a-0910">by</w>
          <c> </c>
          <hi xml:id="A83456e-260">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A83456-1170" facs="A83456-001-a-0920">John</w>
            <c> </c>
            <w lemma="field" ana="#n1" reg="field" xml:id="A83456-1180" facs="A83456-001-a-0930">Field</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83456-1190" facs="A83456-001-a-0940">and</w>
          <c> </c>
          <hi xml:id="A83456e-270">
            <w lemma="Henry" ana="#n1-nn" reg="Henry" xml:id="A83456-1200" facs="A83456-001-a-0950">Henry</w>
            <c> </c>
            <w lemma="hill" ana="#n2" reg="hills" xml:id="A83456-1210" facs="A83456-001-a-0960">Hills</w>
            <pc xml:id="A83456-1220" facs="A83456-001-a-0970">,</pc>
          </hi>
          <c> </c>
          <w lemma="printer" ana="#n2" reg="printers" xml:id="A83456-1230" facs="A83456-001-a-0980">Printers</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83456-1240" facs="A83456-001-a-0990">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-1250" facs="A83456-001-a-1000">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83456-1260" facs="A83456-001-a-1010">PARLIAMENT</w>
          <pc unit="sentence" xml:id="A83456-1270" facs="A83456-001-a-1020">.</pc>
        </p>
        <p xml:id="A83456e-280">
          <w lemma="and" ana="#cc" reg="And" xml:id="A83456-1300" facs="A83456-001-a-1030">And</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83456-1310" facs="A83456-001-a-1040">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83456-1320" facs="A83456-001-a-1050">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83456-1330" facs="A83456-001-a-1060">be</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A83456-1340" facs="A83456-001-a-1070">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83456-1350" facs="A83456-001-a-1080">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83456-1360" facs="A83456-001-a-1090">the</w>
          <c> </c>
          <w lemma="seu••" ana="#fw-la" reg="seu••" xml:id="A83456-1370" facs="A83456-001-a-1100">sev••</w>
          <c> </c>
          <w lemma="star" ana="#n2" reg="stars" xml:id="A83456-1380" facs="A83456-001-a-1110">Stars</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83456-1390" facs="A83456-001-a-1120">in</w>
          <c> </c>
          <hi xml:id="A83456e-290">
            <w lemma="Fleetstreet" ana="#n1-nn" reg="Fleetstreet" xml:id="A83456-1400" facs="A83456-001-a-1130">Fleetstreet</w>
            <pc xml:id="A83456-1410" facs="A83456-001-a-1140">,</pc>
          </hi>
          <c> </c>
          <w lemma="over" ana="#acp-av" reg="over" xml:id="A83456-1420" facs="A83456-001-a-1150">over</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A83456-1430" facs="A83456-001-a-1160">against</w>
          <c> </c>
          <hi xml:id="A83456e-300">
            <w lemma="Dunstan" ana="#n1g-nn" reg="Dunstan's" xml:id="A83456-1440" facs="A83456-001-a-1170">Dunstans</w>
          </hi>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A83456-1450" facs="A83456-001-a-1180">Church</w>
          <pc xml:id="A83456-1460" facs="A83456-001-a-1190">,</pc>
          <c> </c>
          <w lemma="1659." ana="#crd" reg="1659." xml:id="A83456-1470" facs="A83456-001-a-1200">1659.</w>
          <pc unit="sentence" xml:id="A83456-1470-eos" facs="A83456-001-a-1210"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>