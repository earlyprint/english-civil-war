<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The House of Commons, upon late information received from their armies in Ireland, have tenderly considered the great extremities they are in ...</title>
        <author>England and Wales. Parliament. House of Commons.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1644</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83706</idno>
        <idno type="STC">Wing E2580</idno>
        <idno type="STC">ESTC R212191</idno>
        <idno type="EEBO-CITATION">45097750</idno>
        <idno type="OCLC">ocm 45097750</idno>
        <idno type="VID">171343</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83706)</note>
        <note>Transcribed from: (Early English Books Online ; image set 171343)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2571:57)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The House of Commons, upon late information received from their armies in Ireland, have tenderly considered the great extremities they are in ...</title>
            <author>England and Wales. Parliament. House of Commons.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>Printed for Edward Husbands.,</publisher>
            <pubPlace>London, :</pubPlace>
            <date>[1644]</date>
          </publicationStmt>
          <notesStmt>
            <note>"27. August. 1644. Ordered that this order be forthwith printed and published, and carefully dispersed: Hen. Elsynge, cler. Parl. D. Com."</note>
            <note>Reproduction of original in the Henry E. Huntington Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Civil War, 1642-1649.</term>
          <term>Great Britain -- Politics and government -- 1642-1649.</term>
          <term>Broadsides -- England -- 17th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83706e-10">
    <body xml:id="A83706e-20">
      <div type="order" xml:id="A83706e-30">
        <pb facs="tcp:171343:1" xml:id="A83706-001-a"/>
        <p xml:id="A83706e-40">
          <w lemma="the" ana="#d" reg="The" xml:id="A83706-0030" facs="A83706-001-a-0010">THe</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83706-0040" facs="A83706-001-a-0020">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-0050" facs="A83706-001-a-0030">of</w>
          <c> </c>
          <w lemma="Commons" ana="#n2-nn" reg="Commons" xml:id="A83706-0060" facs="A83706-001-a-0040">Commons</w>
          <pc xml:id="A83706-0070" facs="A83706-001-a-0050">,</pc>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A83706-0080" facs="A83706-001-a-0060">upon</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A83706-0090" facs="A83706-001-a-0070">late</w>
          <c> </c>
          <w lemma="information" ana="#n1" reg="information" xml:id="A83706-0100" facs="A83706-001-a-0080">Information</w>
          <c> </c>
          <w lemma="receive" ana="#vvn" reg="received" xml:id="A83706-0110" facs="A83706-001-a-0090">received</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A83706-0120" facs="A83706-001-a-0100">from</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83706-0130" facs="A83706-001-a-0110">their</w>
          <c> </c>
          <w lemma="army" ana="#n2" reg="armies" xml:id="A83706-0140" facs="A83706-001-a-0120">Armies</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0150" facs="A83706-001-a-0130">in</w>
          <c> </c>
          <hi xml:id="A83706e-50">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A83706-0160" facs="A83706-001-a-0140">Ireland</w>
            <pc xml:id="A83706-0170" facs="A83706-001-a-0150">,</pc>
          </hi>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A83706-0180" facs="A83706-001-a-0160">have</w>
          <c> </c>
          <w lemma="tender" ana="#av_j" reg="tenderly" xml:id="A83706-0190" facs="A83706-001-a-0170">tenderly</w>
          <c> </c>
          <w lemma="consider" ana="#vvn" reg="considered" xml:id="A83706-0200" facs="A83706-001-a-0180">considered</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0210" facs="A83706-001-a-0190">the</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A83706-0220" facs="A83706-001-a-0200">great</w>
          <c> </c>
          <w lemma="extremity" ana="#n2" reg="extremities" xml:id="A83706-0230" facs="A83706-001-a-0210">extremities</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83706-0240" facs="A83706-001-a-0220">they</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83706-0250" facs="A83706-001-a-0230">are</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0260" facs="A83706-001-a-0240">in</w>
          <pc xml:id="A83706-0270" facs="A83706-001-a-0250">,</pc>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A83706-0280" facs="A83706-001-a-0260">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83706-0290" facs="A83706-001-a-0270">their</w>
          <c> </c>
          <w lemma="constant" ana="#j" reg="constant" xml:id="A83706-0300" facs="A83706-001-a-0280">constant</w>
          <c> </c>
          <w lemma="resolution" ana="#n2" reg="resolutions" xml:id="A83706-0310" facs="A83706-001-a-0290">resolutions</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83706-0320" facs="A83706-001-a-0300">to</w>
          <c> </c>
          <w lemma="proceed" ana="#vvi" reg="proceed" xml:id="A83706-0330" facs="A83706-001-a-0310">proceed</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0340" facs="A83706-001-a-0320">in</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A83706-0350" facs="A83706-001-a-0330">that</w>
          <c> </c>
          <w lemma="work" ana="#n1" reg="work" xml:id="A83706-0360" facs="A83706-001-a-0340">work</w>
          <pc xml:id="A83706-0370" facs="A83706-001-a-0350">,</pc>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-cs" reg="notwithstanding" xml:id="A83706-0380" facs="A83706-001-a-0360">notwithstanding</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83706-0390" facs="A83706-001-a-0370">all</w>
          <c> </c>
          <w lemma="difficulty" ana="#n2" reg="difficulties" xml:id="A83706-0400" facs="A83706-001-a-0380">difficulties</w>
          <pc xml:id="A83706-0410" facs="A83706-001-a-0390">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83706-0420" facs="A83706-001-a-0400">and</w>
          <c> </c>
          <w lemma="thereupon" ana="#av" reg="thereupon" xml:id="A83706-0430" facs="A83706-001-a-0410">thereupon</w>
          <pc xml:id="A83706-0440" facs="A83706-001-a-0420">,</pc>
          <c> </c>
          <w lemma="have" ana="#vvg" reg="having" xml:id="A83706-0450" facs="A83706-001-a-0430">having</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A83706-0460" facs="A83706-001-a-0440">now</w>
          <c> </c>
          <w lemma="lay" ana="#vvn" reg="laid" xml:id="A83706-0470" facs="A83706-001-a-0450">laid</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83706-0480" facs="A83706-001-a-0460">a</w>
          <c> </c>
          <w lemma="certain" ana="#j" reg="certain" xml:id="A83706-0490" facs="A83706-001-a-0470">certain</w>
          <c> </c>
          <w lemma="foundation" ana="#n1" reg="foundation" xml:id="A83706-0500" facs="A83706-001-a-0480">foundation</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-0510" facs="A83706-001-a-0490">of</w>
          <c> </c>
          <w lemma="credit" ana="#n1" reg="credit" xml:id="A83706-0520" facs="A83706-001-a-0500">credit</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-0530" facs="A83706-001-a-0510">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0540" facs="A83706-001-a-0520">the</w>
          <c> </c>
          <w lemma="encouragement" ana="#n1" reg="encouragement" xml:id="A83706-0550" facs="A83706-001-a-0530">incouragement</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-0560" facs="A83706-001-a-0540">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83706-0570" facs="A83706-001-a-0550">their</w>
          <c> </c>
          <w lemma="force" ana="#n2" reg="forces" xml:id="A83706-0580" facs="A83706-001-a-0560">Forces</w>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A83706-0590" facs="A83706-001-a-0570">there</w>
          <pc xml:id="A83706-0600" facs="A83706-001-a-0580">,</pc>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A83706-0610" facs="A83706-001-a-0590">do</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A83706-0620" facs="A83706-001-a-0600">Order</w>
          <pc xml:id="A83706-0630" facs="A83706-001-a-0610">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83706-0640" facs="A83706-001-a-0620">That</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83706-0650" facs="A83706-001-a-0630">it</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83706-0660" facs="A83706-001-a-0640">be</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A83706-0670" facs="A83706-001-a-0650">published</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83706-0680" facs="A83706-001-a-0660">to</w>
          <c> </c>
          <w lemma="morrow" ana="#n1" reg="morrow" xml:id="A83706-0690" facs="A83706-001-a-0670">morrow</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0700" facs="A83706-001-a-0680">the</w>
          <c> </c>
          <w lemma="28." ana="#crd" reg="28." xml:id="A83706-0710" facs="A83706-001-a-0690">28.</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-0720" facs="A83706-001-a-0700">of</w>
          <c> </c>
          <hi xml:id="A83706e-60">
            <w lemma="August" ana="#n1-nn" reg="August" xml:id="A83706-0730" facs="A83706-001-a-0710">August</w>
            <pc xml:id="A83706-0740" facs="A83706-001-a-0720">,</pc>
            <c> </c>
            <w lemma="1644." ana="#crd" reg="1644." xml:id="A83706-0750" facs="A83706-001-a-0730">1644.</w>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0760" facs="A83706-001-a-0740">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0770" facs="A83706-001-a-0750">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83706-0780" facs="A83706-001-a-0760">severall</w>
          <c> </c>
          <w lemma="church" ana="#n2" reg="churches" xml:id="A83706-0790" facs="A83706-001-a-0770">Churches</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0800" facs="A83706-001-a-0780">in</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83706-0810" facs="A83706-001-a-0790">and</w>
          <c> </c>
          <w lemma="about" ana="#acp-p" reg="about" xml:id="A83706-0820" facs="A83706-001-a-0800">about</w>
          <c> </c>
          <hi xml:id="A83706e-70">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A83706-0830" facs="A83706-001-a-0810">London</w>
            <pc xml:id="A83706-0840" facs="A83706-001-a-0820">,</pc>
          </hi>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83706-0850" facs="A83706-001-a-0830">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0860" facs="A83706-001-a-0840">the</w>
          <c> </c>
          <w lemma="adventurer" ana="#n2" reg="adventurers" xml:id="A83706-0870" facs="A83706-001-a-0850">Adventurers</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A83706-0880" facs="A83706-001-a-0860">do</w>
          <c> </c>
          <w lemma="meet" ana="#vvi" reg="meet" xml:id="A83706-0890" facs="A83706-001-a-0870">meet</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A83706-0900" facs="A83706-001-a-0880">on</w>
          <c> </c>
          <w lemma="Friday" ana="#n1-nn" reg="Friday" xml:id="A83706-0910" facs="A83706-001-a-0890">Friday</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83706-0920" facs="A83706-001-a-0900">at</w>
          <c> </c>
          <w lemma="two" ana="#crd" reg="two" xml:id="A83706-0930" facs="A83706-001-a-0910">Two</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-0940" facs="A83706-001-a-0920">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0950" facs="A83706-001-a-0930">the</w>
          <c> </c>
          <w lemma="clock" ana="#n1" reg="clock" xml:id="A83706-0960" facs="A83706-001-a-0940">clock</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-0970" facs="A83706-001-a-0950">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-0980" facs="A83706-001-a-0960">the</w>
          <c> </c>
          <w lemma="afternoon" ana="#n1" reg="afternoon" xml:id="A83706-0990" facs="A83706-001-a-0970">afternoon</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83706-1000" facs="A83706-001-a-0980">at</w>
          <c> </c>
          <hi xml:id="A83706e-80">
            <w lemma="grocers-hall" ana="#n1-nn" reg="Grocers-hall" xml:id="A83706-1010" facs="A83706-001-a-0990">Grocers-Hall</w>
            <pc xml:id="A83706-1020" facs="A83706-001-a-1000">,</pc>
          </hi>
          <c> </c>
          <w lemma="where" ana="#crq-cs" reg="where" xml:id="A83706-1030" facs="A83706-001-a-1010">where</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83706-1040" facs="A83706-001-a-1020">a</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A83706-1050" facs="A83706-001-a-1030">Committee</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1060" facs="A83706-001-a-1040">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1070" facs="A83706-001-a-1050">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83706-1080" facs="A83706-001-a-1060">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1090" facs="A83706-001-a-1070">of</w>
          <c> </c>
          <w lemma="Commons" ana="#n2-nn" reg="Commons" xml:id="A83706-1100" facs="A83706-001-a-1080">Commons</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A83706-1110" facs="A83706-001-a-1090">is</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A83706-1120" facs="A83706-001-a-1100">Ordered</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83706-1130" facs="A83706-001-a-1110">to</w>
          <c> </c>
          <w lemma="meet" ana="#vvi" reg="meet" xml:id="A83706-1140" facs="A83706-001-a-1120">meet</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A83706-1150" facs="A83706-001-a-1130">them</w>
          <pc xml:id="A83706-1160" facs="A83706-001-a-1140">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-1170" facs="A83706-001-a-1150">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1180" facs="A83706-001-a-1160">the</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A83706-1190" facs="A83706-001-a-1170">present</w>
          <c> </c>
          <w lemma="raise" ana="#n1_vg" reg="raising" xml:id="A83706-1200" facs="A83706-001-a-1180">raising</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1210" facs="A83706-001-a-1190">of</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A83706-1220" facs="A83706-001-a-1200">some</w>
          <c> </c>
          <w lemma="provision" ana="#n1" reg="provision" xml:id="A83706-1230" facs="A83706-001-a-1210">provision</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A83706-1240" facs="A83706-001-a-1220">on</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83706-1250" facs="A83706-001-a-1230">this</w>
          <c> </c>
          <w lemma="foundation" ana="#n1" reg="foundation" xml:id="A83706-1260" facs="A83706-001-a-1240">foundation</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1270" facs="A83706-001-a-1250">of</w>
          <c> </c>
          <w lemma="credit" ana="#n1" reg="credit" xml:id="A83706-1280" facs="A83706-001-a-1260">Credit</w>
          <pc xml:id="A83706-1290" facs="A83706-001-a-1270">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-1300" facs="A83706-001-a-1280">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1310" facs="A83706-001-a-1290">the</w>
          <c> </c>
          <w lemma="relief" ana="#n1" reg="relief" xml:id="A83706-1320" facs="A83706-001-a-1300">relief</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1330" facs="A83706-001-a-1310">of</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A83706-1340" facs="A83706-001-a-1320">those</w>
          <c> </c>
          <w lemma="who" ana="#crq-r" reg="who" xml:id="A83706-1350" facs="A83706-001-a-1330">who</w>
          <c> </c>
          <w lemma="stand" ana="#vvb" reg="stand" xml:id="A83706-1360" facs="A83706-001-a-1340">stand</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A83706-1370" facs="A83706-001-a-1350">so</w>
          <c> </c>
          <w lemma="resolute" ana="#av_j" reg="resolutely" xml:id="A83706-1380" facs="A83706-001-a-1360">resolutely</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-1390" facs="A83706-001-a-1370">for</w>
          <c> </c>
          <w lemma="maintenance" ana="#n1" reg="maintenance" xml:id="A83706-1400" facs="A83706-001-a-1380">maintenance</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1410" facs="A83706-001-a-1390">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1420" facs="A83706-001-a-1400">the</w>
          <c> </c>
          <w lemma="common" ana="#j" reg="common" xml:id="A83706-1430" facs="A83706-001-a-1410">common</w>
          <c> </c>
          <w lemma="cause" ana="#n1" reg="cause" xml:id="A83706-1440" facs="A83706-001-a-1420">Cause</w>
          <pc xml:id="A83706-1450" facs="A83706-001-a-1430">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83706-1460" facs="A83706-001-a-1440">And</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83706-1470" facs="A83706-001-a-1450">all</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1480" facs="A83706-001-a-1460">the</w>
          <c> </c>
          <w lemma="minister" ana="#n2" reg="ministers" xml:id="A83706-1490" facs="A83706-001-a-1470">Ministers</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-1500" facs="A83706-001-a-1480">in</w>
          <c> </c>
          <hi xml:id="A83706e-90">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A83706-1510" facs="A83706-001-a-1490">London</w>
            <pc xml:id="A83706-1520" facs="A83706-001-a-1500">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83706-1530" facs="A83706-001-a-1510">and</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A83706-1540" facs="A83706-001-a-1520">within</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1550" facs="A83706-001-a-1530">the</w>
          <c> </c>
          <w lemma="line" ana="#n2" reg="lines" xml:id="A83706-1560" facs="A83706-001-a-1540">Lines</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1570" facs="A83706-001-a-1550">of</w>
          <c> </c>
          <w lemma="communication" ana="#n1" reg="communication" xml:id="A83706-1580" facs="A83706-001-a-1560">Communication</w>
          <pc xml:id="A83706-1590" facs="A83706-001-a-1570">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83706-1600" facs="A83706-001-a-1580">are</w>
          <c> </c>
          <w lemma="then" ana="#av" reg="then" xml:id="A83706-1610" facs="A83706-001-a-1590">then</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83706-1620" facs="A83706-001-a-1600">to</w>
          <c> </c>
          <w lemma="recommend" ana="#vvi" reg="recommend" xml:id="A83706-1630" facs="A83706-001-a-1610">recommend</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1640" facs="A83706-001-a-1620">the</w>
          <c> </c>
          <w lemma="success" ana="#n1" reg="success" xml:id="A83706-1650" facs="A83706-001-a-1630">success</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1660" facs="A83706-001-a-1640">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83706-1670" facs="A83706-001-a-1650">this</w>
          <c> </c>
          <w lemma="affair" ana="#n1" reg="affair" xml:id="A83706-1680" facs="A83706-001-a-1660">affair</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A83706-1690" facs="A83706-001-a-1670">unto</w>
          <c> </c>
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A83706-1700" facs="A83706-001-a-1680">God</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83706-1710" facs="A83706-001-a-1690">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83706-1720" facs="A83706-001-a-1700">their</w>
          <c> </c>
          <w lemma="prayer" ana="#n2" reg="prayers" xml:id="A83706-1730" facs="A83706-001-a-1710">prayers</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A83706-1740" facs="A83706-001-a-1720">with</w>
          <c> </c>
          <w lemma="thanksgiving" ana="#n1" reg="thanksgiving" xml:id="A83706-1750" facs="A83706-001-a-1730">Thanksgiving</w>
          <pc xml:id="A83706-1760" facs="A83706-001-a-1740">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-1770" facs="A83706-001-a-1750">for</w>
          <c> </c>
          <w lemma="God" ana="#n1g-nn" reg="God's" xml:id="A83706-1780" facs="A83706-001-a-1760">Gods</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A83706-1790" facs="A83706-001-a-1770">great</w>
          <c> </c>
          <w lemma="blessing" ana="#n1" reg="blessing" xml:id="A83706-1800" facs="A83706-001-a-1780">Blessing</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A83706-1810" facs="A83706-001-a-1790">on</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83706-1820" facs="A83706-001-a-1800">their</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A83706-1830" facs="A83706-001-a-1810">late</w>
          <c> </c>
          <w lemma="endeavour" ana="#n2" reg="endeavours" xml:id="A83706-1840" facs="A83706-001-a-1820">endeavours</w>
          <pc xml:id="A83706-1850" facs="A83706-001-a-1830">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83706-1860" facs="A83706-001-a-1840">And</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83706-1870" facs="A83706-001-a-1850">to</w>
          <c> </c>
          <w lemma="stir" ana="#vvi" reg="stir" xml:id="A83706-1880" facs="A83706-001-a-1860">stir</w>
          <c> </c>
          <w lemma="up" ana="#acp-av" reg="up" xml:id="A83706-1890" facs="A83706-001-a-1870">up</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-1900" facs="A83706-001-a-1880">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83706-1910" facs="A83706-001-a-1890">severall</w>
          <c> </c>
          <w lemma="adventurer" ana="#n2" reg="adventurers" xml:id="A83706-1920" facs="A83706-001-a-1900">Adventurers</w>
          <pc xml:id="A83706-1930" facs="A83706-001-a-1910">,</pc>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83706-1940" facs="A83706-001-a-1920">not</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83706-1950" facs="A83706-001-a-1930">to</w>
          <c> </c>
          <w lemma="fail" ana="#vvi" reg="fail" xml:id="A83706-1960" facs="A83706-001-a-1940">fail</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-1970" facs="A83706-001-a-1950">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83706-1980" facs="A83706-001-a-1960">this</w>
          <c> </c>
          <w lemma="meeting" ana="#n1" reg="meeting" xml:id="A83706-1990" facs="A83706-001-a-1970">meeting</w>
          <pc xml:id="A83706-2000" facs="A83706-001-a-1980">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83706-2010" facs="A83706-001-a-1990">as</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83706-2020" facs="A83706-001-a-2000">they</w>
          <c> </c>
          <w lemma="tender" ana="#vvb" reg="tender" xml:id="A83706-2030" facs="A83706-001-a-2010">tender</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83706-2040" facs="A83706-001-a-2020">the</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A83706-2050" facs="A83706-001-a-2030">good</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83706-2060" facs="A83706-001-a-2040">of</w>
          <c> </c>
          <hi xml:id="A83706e-100">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A83706-2070" facs="A83706-001-a-2050">Ireland</w>
            <pc unit="sentence" xml:id="A83706-2080" facs="A83706-001-a-2060">.</pc>
            <c> </c>
          </hi>
        </p>
        <div type="license" xml:id="A83706e-110">
          <head xml:id="A83706e-120">
            <w lemma="27." ana="#crd" reg="27." xml:id="A83706-2120" facs="A83706-001-a-2070">27.</w>
            <pc unit="sentence" xml:id="A83706-2120-eos" facs="A83706-001-a-2080"/>
            <c> </c>
            <w lemma="August" ana="#n1-nn" reg="August" xml:id="A83706-2130" facs="A83706-001-a-2090">August</w>
            <pc unit="sentence" xml:id="A83706-2140" facs="A83706-001-a-2100">.</pc>
            <c> </c>
            <w lemma="1644." ana="#crd" reg="1644." xml:id="A83706-2150" facs="A83706-001-a-2110">1644.</w>
            <pc unit="sentence" xml:id="A83706-2150-eos" facs="A83706-001-a-2120"/>
          </head>
          <p xml:id="A83706e-130">
            <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A83706-2180" facs="A83706-001-a-2130">ORdered</w>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A83706-2190" facs="A83706-001-a-2140">that</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A83706-2200" facs="A83706-001-a-2150">this</w>
            <c> </c>
            <w lemma="order" ana="#n1" reg="order" xml:id="A83706-2210" facs="A83706-001-a-2160">Order</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="be" xml:id="A83706-2220" facs="A83706-001-a-2170">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A83706-2230" facs="A83706-001-a-2180">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A83706-2240" facs="A83706-001-a-2190">printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A83706-2250" facs="A83706-001-a-2200">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A83706-2260" facs="A83706-001-a-2210">published</w>
            <pc xml:id="A83706-2270" facs="A83706-001-a-2220">,</pc>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A83706-2280" facs="A83706-001-a-2230">and</w>
            <c> </c>
            <w lemma="careful" ana="#av_j" reg="carefully" xml:id="A83706-2290" facs="A83706-001-a-2240">carefully</w>
            <c> </c>
            <w lemma="disperse" ana="#vvn" reg="dispersed" xml:id="A83706-2300" facs="A83706-001-a-2250">dispersed</w>
            <pc xml:id="A83706-2310" facs="A83706-001-a-2260">:</pc>
          </p>
          <closer xml:id="A83706e-140">
            <signed xml:id="A83706e-150">
              <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A83706-2350" facs="A83706-001-a-2270">Hen.</w>
              <c> </c>
              <w lemma="Elsing" ana="#n1-nn" reg="Elsing" xml:id="A83706-2370" facs="A83706-001-a-2290">Elsynge</w>
              <pc xml:id="A83706-2380" facs="A83706-001-a-2300">,</pc>
              <c> </c>
              <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A83706-2390" facs="A83706-001-a-2310">Cler.</w>
              <c> </c>
              <w lemma="parl." ana="#n-ab" reg="parl." xml:id="A83706-2400" facs="A83706-001-a-2320">Parl.</w>
              <c> </c>
              <w lemma="d." ana="#n-ab" reg="d." xml:id="A83706-2410" facs="A83706-001-a-2330">D.</w>
              <c> </c>
              <w lemma="com." ana="#n-ab" reg="com." xml:id="A83706-2420" facs="A83706-001-a-2340">Com.</w>
              <pc unit="sentence" xml:id="A83706-2420-eos" facs="A83706-001-a-2350"/>
            </signed>
          </closer>
        </div>
      </div>
    </body>
    <back xml:id="A83706e-160">
      <div type="colophon" xml:id="A83706e-170">
        <p xml:id="A83706e-180">
          <hi xml:id="A83706e-190">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A83706-2470" facs="A83706-001-a-2360">London</w>
            <pc xml:id="A83706-2480" facs="A83706-001-a-2370">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83706-2490" facs="A83706-001-a-2380">Printed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83706-2500" facs="A83706-001-a-2390">for</w>
          <c> </c>
          <hi xml:id="A83706e-200">
            <w lemma="Edward" ana="#n1-nn" reg="Edward" xml:id="A83706-2510" facs="A83706-001-a-2400">Edward</w>
            <c> </c>
            <w lemma="husband" ana="#n2" reg="husbands" xml:id="A83706-2520" facs="A83706-001-a-2410">Husbands</w>
            <pc unit="sentence" xml:id="A83706-2530" facs="A83706-001-a-2420">.</pc>
            <c> </c>
          </hi>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>