<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Veneris, Decemb. 16. 1642. Whereas severall ordinances of both houses of Parliament of the 29. of November last, and the seventh and 14. of this instant December are passed, concerning assessing such persons as are of ability ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83618</idno>
        <idno type="STC">Wing E2487</idno>
        <idno type="STC">ESTC R211552</idno>
        <idno type="EEBO-CITATION">45097742</idno>
        <idno type="OCLC">ocm 45097742</idno>
        <idno type="VID">171335</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83618)</note>
        <note>Transcribed from: (Early English Books Online ; image set 171335)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2571:49)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Veneris, Decemb. 16. 1642. Whereas severall ordinances of both houses of Parliament of the 29. of November last, and the seventh and 14. of this instant December are passed, concerning assessing such persons as are of ability ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1642]</date>
          </publicationStmt>
          <notesStmt>
            <note>Imprint suggested by Wing.</note>
            <note>Title from caption and first lines of text.</note>
            <note>Reproduction of original in the Henry E. Huntington Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Internal revenue -- Great Britain.</term>
          <term>Great Britain -- Politics and government -- 1642-1649.</term>
          <term>Broadsides -- England -- 17th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83618e-10">
    <body xml:id="A83618e-20">
      <div type="order_of_Parliament" xml:id="A83618e-30">
        <pb facs="tcp:171335:1" rendition="simple:additions" xml:id="A83618-001-a"/>
        <opener xml:id="A83618e-40">
          <dateline xml:id="A83618e-50">
            <date xml:id="A83618e-60">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A83618-0050" facs="A83618-001-a-0010">Die</w>
              <c> </c>
              <w lemma="veneris" ana="#fw-la" reg="veneris" xml:id="A83618-0060" facs="A83618-001-a-0020">Veneris</w>
              <pc xml:id="A83618-0070" facs="A83618-001-a-0030">,</pc>
              <c> </c>
              <w lemma="decemb." ana="#n-ab" reg="decemb." xml:id="A83618-0080" facs="A83618-001-a-0040">Decemb.</w>
              <pc unit="sentence" xml:id="A83618-0080-eos" facs="A83618-001-a-0050"/>
              <c> </c>
              <w lemma="16." ana="#crd" reg="16." xml:id="A83618-0090" facs="A83618-001-a-0060">16.</w>
              <c> </c>
              <w lemma="1642." ana="#crd" reg="1642." xml:id="A83618-0100" facs="A83618-001-a-0070">1642.</w>
              <pc unit="sentence" xml:id="A83618-0100-eos" facs="A83618-001-a-0080"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A83618e-70">
          <w lemma="whereas" ana="#cs" reg="Whereas" rend="initialcharacterdecorated" xml:id="A83618-0130" facs="A83618-001-a-0090">WHereas</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83618-0140" facs="A83618-001-a-0100">severall</w>
          <c> </c>
          <w lemma="ordinance" ana="#n2" reg="ordinances" xml:id="A83618-0150" facs="A83618-001-a-0110">Ordinances</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0160" facs="A83618-001-a-0120">of</w>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A83618-0170" facs="A83618-001-a-0130">both</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A83618-0180" facs="A83618-001-a-0140">Houses</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0190" facs="A83618-001-a-0150">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83618-0200" facs="A83618-001-a-0160">Parliament</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0210" facs="A83618-001-a-0170">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0220" facs="A83618-001-a-0180">the</w>
          <c> </c>
          <w lemma="29." ana="#crd" reg="29." xml:id="A83618-0230" facs="A83618-001-a-0190">29.</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0240" facs="A83618-001-a-0200">of</w>
          <c> </c>
          <hi xml:id="A83618e-80">
            <w lemma="November" ana="#n1-nn" reg="November" xml:id="A83618-0250" facs="A83618-001-a-0210">November</w>
          </hi>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A83618-0260" facs="A83618-001-a-0220">last</w>
          <pc xml:id="A83618-0270" facs="A83618-001-a-0230">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0280" facs="A83618-001-a-0240">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0290" facs="A83618-001-a-0250">the</w>
          <c> </c>
          <w lemma="seven" ana="#ord" reg="seventh" xml:id="A83618-0300" facs="A83618-001-a-0260">seventh</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0310" facs="A83618-001-a-0270">and</w>
          <c> </c>
          <w lemma="14." ana="#crd" reg="14." xml:id="A83618-0320" facs="A83618-001-a-0280">14.</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0330" facs="A83618-001-a-0290">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83618-0340" facs="A83618-001-a-0300">this</w>
          <c> </c>
          <w lemma="instant" ana="#j" reg="instant" xml:id="A83618-0350" facs="A83618-001-a-0310">instant</w>
          <c> </c>
          <hi xml:id="A83618e-90">
            <w lemma="December" ana="#n1-nn" reg="December" xml:id="A83618-0360" facs="A83618-001-a-0320">December</w>
          </hi>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83618-0370" facs="A83618-001-a-0330">are</w>
          <c> </c>
          <w lemma="pass" ana="#vvn" reg="passed" xml:id="A83618-0380" facs="A83618-001-a-0340">passed</w>
          <pc xml:id="A83618-0390" facs="A83618-001-a-0350">,</pc>
          <c> </c>
          <w lemma="concerning" ana="#acp-p" reg="concerning" xml:id="A83618-0400" facs="A83618-001-a-0360">concerning</w>
          <c> </c>
          <w lemma="assess" ana="#vvg" reg="assessing" xml:id="A83618-0410" facs="A83618-001-a-0370">Assessing</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A83618-0420" facs="A83618-001-a-0380">such</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83618-0430" facs="A83618-001-a-0390">persons</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83618-0440" facs="A83618-001-a-0400">as</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83618-0450" facs="A83618-001-a-0410">are</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0460" facs="A83618-001-a-0420">of</w>
          <c> </c>
          <w lemma="ability" ana="#n1" reg="ability" xml:id="A83618-0470" facs="A83618-001-a-0430">ability</w>
          <pc xml:id="A83618-0480" facs="A83618-001-a-0440">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0490" facs="A83618-001-a-0450">and</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A83618-0500" facs="A83618-001-a-0460">have</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83618-0510" facs="A83618-001-a-0470">not</w>
          <c> </c>
          <w lemma="contribute" ana="#vvn" reg="contributed" xml:id="A83618-0520" facs="A83618-001-a-0480">contributed</w>
          <pc xml:id="A83618-0530" facs="A83618-001-a-0490">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83618-0540" facs="A83618-001-a-0500">or</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83618-0550" facs="A83618-001-a-0510">not</w>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A83618-0560" facs="A83618-001-a-0520">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83618-0570" facs="A83618-001-a-0530">to</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83618-0580" facs="A83618-001-a-0540">their</w>
          <c> </c>
          <w lemma="ability" ana="#n1" reg="ability" xml:id="A83618-0590" facs="A83618-001-a-0550">abilitie</w>
          <pc xml:id="A83618-0600" facs="A83618-001-a-0560">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83618-0610" facs="A83618-001-a-0570">as</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83618-0620" facs="A83618-001-a-0580">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0630" facs="A83618-001-a-0590">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83618-0640" facs="A83618-001-a-0600">said</w>
          <c> </c>
          <w lemma="ordinance" ana="#n2" reg="ordinances" xml:id="A83618-0650" facs="A83618-001-a-0610">Ordinances</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A83618-0660" facs="A83618-001-a-0620">doe</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0670" facs="A83618-001-a-0630">and</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A83618-0680" facs="A83618-001-a-0640">may</w>
          <c> </c>
          <w lemma="appear" ana="#vvi" reg="appear" xml:id="A83618-0690" facs="A83618-001-a-0650">appeare</w>
          <pc xml:id="A83618-0700" facs="A83618-001-a-0660">;</pc>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83618-0710" facs="A83618-001-a-0670">It</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A83618-0720" facs="A83618-001-a-0680">is</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A83618-0730" facs="A83618-001-a-0690">now</w>
          <c> </c>
          <w lemma="further" ana="#av_j" reg="further" xml:id="A83618-0740" facs="A83618-001-a-0700">further</w>
          <c> </c>
          <w lemma="ordain" ana="#vvn" reg="ordained" xml:id="A83618-0750" facs="A83618-001-a-0710">Ordained</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0760" facs="A83618-001-a-0720">and</w>
          <c> </c>
          <w lemma="declare" ana="#vvn" reg="declared" xml:id="A83618-0770" facs="A83618-001-a-0730">Declared</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83618-0780" facs="A83618-001-a-0740">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0790" facs="A83618-001-a-0750">the</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A83618-0800" facs="A83618-001-a-0760">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0810" facs="A83618-001-a-0770">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A83618-0820" facs="A83618-001-a-0780">Commons</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83618-0830" facs="A83618-001-a-0790">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83618-0840" facs="A83618-001-a-0800">Parliament</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A83618-0850" facs="A83618-001-a-0810">assembled</w>
          <pc xml:id="A83618-0860" facs="A83618-001-a-0820">:</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83618-0870" facs="A83618-001-a-0830">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0880" facs="A83618-001-a-0840">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83618-0890" facs="A83618-001-a-0850">severall</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-0900" facs="A83618-001-a-0860">and</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A83618-0910" facs="A83618-001-a-0870">respective</w>
          <c> </c>
          <w lemma="assessor" ana="#n2" reg="assessors" xml:id="A83618-0920" facs="A83618-001-a-0880">Assessors</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83618-0930" facs="A83618-001-a-0890">shall</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83618-0940" facs="A83618-001-a-0900">not</w>
          <c> </c>
          <w lemma="assess" ana="#vvi" reg="assess" xml:id="A83618-0950" facs="A83618-001-a-0910">assesse</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A83618-0960" facs="A83618-001-a-0920">any</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-0970" facs="A83618-001-a-0930">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-0980" facs="A83618-001-a-0940">the</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A83618-0990" facs="A83618-001-a-0950">Members</w>
          <pc xml:id="A83618-1000" facs="A83618-001-a-0960">,</pc>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1010" facs="A83618-001-a-0970">of</w>
          <c> </c>
          <w lemma="either" ana="#d" reg="either" xml:id="A83618-1020" facs="A83618-001-a-0980">either</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1030" facs="A83618-001-a-0990">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1040" facs="A83618-001-a-1000">the</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A83618-1050" facs="A83618-001-a-1010">Houses</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1060" facs="A83618-001-a-1020">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83618-1070" facs="A83618-001-a-1030">Parliament</w>
          <pc xml:id="A83618-1080" facs="A83618-001-a-1040">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83618-1090" facs="A83618-001-a-1050">or</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1100" facs="A83618-001-a-1060">the</w>
          <c> </c>
          <w lemma="assistant" ana="#n2" reg="assistants" xml:id="A83618-1110" facs="A83618-001-a-1070">Assistants</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1120" facs="A83618-001-a-1080">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1130" facs="A83618-001-a-1090">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83618-1140" facs="A83618-001-a-1100">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1150" facs="A83618-001-a-1110">of</w>
          <c> </c>
          <w lemma="peer" ana="#n2" reg="peers" xml:id="A83618-1160" facs="A83618-001-a-1120">Peeres</w>
          <pc xml:id="A83618-1170" facs="A83618-001-a-1130">,</pc>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A83618-1180" facs="A83618-001-a-1140">any</w>
          <c> </c>
          <w lemma="thing" ana="#n1" reg="thing" xml:id="A83618-1190" facs="A83618-001-a-1150">thing</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83618-1200" facs="A83618-001-a-1160">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1210" facs="A83618-001-a-1170">the</w>
          <c> </c>
          <w lemma="precede" ana="#j_vg" reg="preceding" xml:id="A83618-1220" facs="A83618-001-a-1180">preceding</w>
          <c> </c>
          <w lemma="ordinance" ana="#n2" reg="ordinances" xml:id="A83618-1230" facs="A83618-001-a-1190">Ordinances</w>
          <pc xml:id="A83618-1240" facs="A83618-001-a-1200">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83618-1250" facs="A83618-001-a-1210">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A83618-1260" facs="A83618-001-a-1220">any</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1270" facs="A83618-001-a-1230">of</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A83618-1280" facs="A83618-001-a-1240">them</w>
          <pc xml:id="A83618-1290" facs="A83618-001-a-1250">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83618-1300" facs="A83618-001-a-1260">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1310" facs="A83618-001-a-1270">the</w>
          <c> </c>
          <w lemma="contrary" ana="#j" reg="contrary" xml:id="A83618-1320" facs="A83618-001-a-1280">contrary</w>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-av" reg="notwithstanding" xml:id="A83618-1330" facs="A83618-001-a-1290">notwithstanding</w>
          <pc unit="sentence" xml:id="A83618-1340" facs="A83618-001-a-1300">.</pc>
          <c> </c>
          <w lemma="but" ana="#acp-cc" reg="But" xml:id="A83618-1350" facs="A83618-001-a-1310">But</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83618-1360" facs="A83618-001-a-1320">that</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1370" facs="A83618-001-a-1330">the</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A83618-1380" facs="A83618-001-a-1340">Members</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1390" facs="A83618-001-a-1350">of</w>
          <c> </c>
          <w lemma="either" ana="#d" reg="either" xml:id="A83618-1400" facs="A83618-001-a-1360">either</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83618-1410" facs="A83618-001-a-1370">House</w>
          <pc xml:id="A83618-1420" facs="A83618-001-a-1380">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83618-1430" facs="A83618-001-a-1390">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83618-1440" facs="A83618-001-a-1400">be</w>
          <c> </c>
          <w lemma="assess" ana="#vvn" reg="assessed" xml:id="A83618-1450" facs="A83618-001-a-1410">assessed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83618-1460" facs="A83618-001-a-1420">by</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A83618-1470" facs="A83618-001-a-1430">that</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83618-1480" facs="A83618-001-a-1440">House</w>
          <pc xml:id="A83618-1490" facs="A83618-001-a-1450">,</pc>
          <c> </c>
          <w lemma="whereof" ana="#crq-cs" reg="whereof" xml:id="A83618-1500" facs="A83618-001-a-1460">whereof</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83618-1510" facs="A83618-001-a-1470">they</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83618-1520" facs="A83618-001-a-1480">are</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A83618-1530" facs="A83618-001-a-1490">Members</w>
          <pc xml:id="A83618-1540" facs="A83618-001-a-1500">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83618-1550" facs="A83618-001-a-1510">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1560" facs="A83618-001-a-1520">the</w>
          <c> </c>
          <w lemma="assistant" ana="#n2" reg="assistants" xml:id="A83618-1570" facs="A83618-001-a-1530">Assistants</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1580" facs="A83618-001-a-1540">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1590" facs="A83618-001-a-1550">the</w>
          <c> </c>
          <w lemma="peer" ana="#n2" reg="peers" xml:id="A83618-1600" facs="A83618-001-a-1560">Peeres</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83618-1610" facs="A83618-001-a-1570">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83618-1620" facs="A83618-001-a-1580">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83618-1630" facs="A83618-001-a-1590">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83618-1640" facs="A83618-001-a-1600">of</w>
          <c> </c>
          <w lemma="peer" ana="#n2" reg="peers" xml:id="A83618-1650" facs="A83618-001-a-1610">Peeres</w>
          <pc unit="sentence" xml:id="A83618-1660" facs="A83618-001-a-1620">.</pc>
        </p>
        <closer xml:id="A83618e-100">
          <hi xml:id="A83618e-110">
            <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A83618-1690" facs="A83618-001-a-1630">Ordered</w>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A83618-1700" facs="A83618-001-a-1640">that</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A83618-1710" facs="A83618-001-a-1650">this</w>
            <c> </c>
            <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A83618-1720" facs="A83618-001-a-1660">ordinance</w>
            <c> </c>
            <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83618-1730" facs="A83618-001-a-1670">shall</w>
            <c> </c>
            <w lemma="be" ana="#vvi" reg="be" xml:id="A83618-1740" facs="A83618-001-a-1680">be</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A83618-1750" facs="A83618-001-a-1690">Printed</w>
            <pc unit="sentence" xml:id="A83618-1760" facs="A83618-001-a-1700">.</pc>
            <c> </c>
          </hi>
          <signed xml:id="A83618e-120">
            <w lemma="io." ana="#n-ab" reg="Io." xml:id="A83618-1780" facs="A83618-001-a-1710">Io.</w>
            <c> </c>
            <w lemma="Browne" ana="#n1-nn" reg="Browne" xml:id="A83618-1790" facs="A83618-001-a-1720">Browne</w>
          </signed>
          <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A83618-1810" facs="A83618-001-a-1730">Cler.</w>
          <c> </c>
          <w lemma="parliamentorum" ana="#fw-la" reg="parliamentorum" xml:id="A83618-1820" facs="A83618-001-a-1740">Parliamentorum</w>
          <pc unit="sentence" xml:id="A83618-1830" facs="A83618-001-a-1750">.</pc>
        </closer>
      </div>
    </body>
    <back xml:id="A83618e-130">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>