<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Lunæ 7[mo] November, 1642. Whereas in these times of publique danger and distraction, there is a recourse unto the city of London of divers persons ...</title>
        <author>England and Wales. Parliament. House of Commons.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83872</idno>
        <idno type="STC">Wing E2779</idno>
        <idno type="STC">ESTC R211190</idno>
        <idno type="EEBO-CITATION">47683435</idno>
        <idno type="OCLC">ocm 47683435</idno>
        <idno type="VID">172900</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83872)</note>
        <note>Transcribed from: (Early English Books Online ; image set 172900)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2655:12)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Lunæ 7[mo] November, 1642. Whereas in these times of publique danger and distraction, there is a recourse unto the city of London of divers persons ...</title>
            <author>England and Wales. Parliament. House of Commons.</author>
            <author>Elsynge, Henry, 1598-1654.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>Printed for R. Oulton &amp; G. Dexter,</publisher>
            <pubPlace>London, :</pubPlace>
            <date>1642.</date>
          </publicationStmt>
          <notesStmt>
            <note>Headpiece; initial.</note>
            <note>At foot of sheet: Hen: Elsynge Cleri. Parl. D. Com.</note>
            <note>Reproduction of original in: Eton College. Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Civil War, 1642-1649.</term>
          <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
          <term>Broadsides -- England -- London -- 17th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-11</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-01</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
      <change><date>2008-01</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83872e-10">
    <body xml:id="A83872e-20">
      <div type="text" xml:id="A83872e-30">
        <pb facs="tcp:172900:1" rendition="simple:additions" xml:id="A83872-001-a"/>
        <opener xml:id="A83872e-40">
          <dateline xml:id="A83872e-50">
            <date xml:id="A83872e-60">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A83872-0050" facs="A83872-001-a-0010">Die</w>
              <c> </c>
              <w lemma="lunae" ana="#fw-la" reg="lunae" xml:id="A83872-0060" facs="A83872-001-a-0020">Lunae</w>
              <c> </c>
              <w lemma="7mo" part="I" ana="#fw-la" reg="7mo" xml:id="A83872-0070.1" facs="A83872-001-a-0030" rendition="hi-mid-2">7mo</w>
              <hi rend="sup" xml:id="A83872e-70">
                <pc unit="sentence" xml:id="A83872-0080" facs="A83872-001-a-0050">.</pc>
                <c> </c>
              </hi>
              <w lemma="November" ana="#n1-nn" reg="November" xml:id="A83872-0090" facs="A83872-001-a-0060">November</w>
              <pc xml:id="A83872-0100" facs="A83872-001-a-0070">,</pc>
              <c> </c>
              <w lemma="1642." ana="#crd" reg="1642." xml:id="A83872-0110" facs="A83872-001-a-0080">1642.</w>
              <pc unit="sentence" xml:id="A83872-0110-eos" facs="A83872-001-a-0090"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A83872e-80">
          <w lemma="whereas" ana="#cs" reg="Whereas" rend="initialcharacterdecorated" xml:id="A83872-0140" facs="A83872-001-a-0100">WHereas</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83872-0150" facs="A83872-001-a-0110">in</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A83872-0160" facs="A83872-001-a-0120">these</w>
          <c> </c>
          <w lemma="time" ana="#n2" reg="times" xml:id="A83872-0170" facs="A83872-001-a-0130">times</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-0180" facs="A83872-001-a-0140">of</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A83872-0190" facs="A83872-001-a-0150">publique</w>
          <c> </c>
          <w lemma="danger" ana="#n1" reg="danger" xml:id="A83872-0200" facs="A83872-001-a-0160">danger</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-0210" facs="A83872-001-a-0170">and</w>
          <c> </c>
          <w lemma="distraction" ana="#n1" reg="distraction" xml:id="A83872-0220" facs="A83872-001-a-0180">distraction</w>
          <pc xml:id="A83872-0230" facs="A83872-001-a-0190">,</pc>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A83872-0240" facs="A83872-001-a-0200">there</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A83872-0250" facs="A83872-001-a-0210">is</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83872-0260" facs="A83872-001-a-0220">a</w>
          <c> </c>
          <w lemma="recourse" ana="#n1" reg="recourse" xml:id="A83872-0270" facs="A83872-001-a-0230">recourse</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A83872-0280" facs="A83872-001-a-0240">unto</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-0290" facs="A83872-001-a-0250">the</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-0300" facs="A83872-001-a-0260">City</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-0310" facs="A83872-001-a-0270">of</w>
          <c> </c>
          <hi xml:id="A83872e-90">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A83872-0320" facs="A83872-001-a-0280">London</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-0330" facs="A83872-001-a-0290">of</w>
          <c> </c>
          <w lemma="divers" ana="#j" reg="divers" xml:id="A83872-0340" facs="A83872-001-a-0300">divers</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83872-0350" facs="A83872-001-a-0310">persons</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83872-0360" facs="A83872-001-a-0320">that</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83872-0370" facs="A83872-001-a-0330">are</w>
          <c> </c>
          <w lemma="come" ana="#vvn" reg="come" xml:id="A83872-0380" facs="A83872-001-a-0340">come</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A83872-0390" facs="A83872-001-a-0350">into</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-0400" facs="A83872-001-a-0360">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-0410" facs="A83872-001-a-0370">said</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-0420" facs="A83872-001-a-0380">City</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-0430" facs="A83872-001-a-0390">and</w>
          <c> </c>
          <w lemma="suburb" ana="#n2" reg="suburbs" xml:id="A83872-0440" facs="A83872-001-a-0400">Suburbs</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A83872-0450" facs="A83872-001-a-0410">thereof</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-0460" facs="A83872-001-a-0420">to</w>
          <c> </c>
          <w lemma="reside" ana="#vvi" reg="reside" xml:id="A83872-0470" facs="A83872-001-a-0430">reside</w>
          <pc xml:id="A83872-0480" facs="A83872-001-a-0440">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-0490" facs="A83872-001-a-0450">and</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A83872-0500" facs="A83872-001-a-0460">have</w>
          <c> </c>
          <w lemma="take" ana="#vvn" reg="taken" xml:id="A83872-0510" facs="A83872-001-a-0470">taken</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A83872-0520" facs="A83872-001-a-0480">Houses</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83872-0530" facs="A83872-001-a-0490">or</w>
          <c> </c>
          <w lemma="lodging" ana="#n2" reg="lodgings" xml:id="A83872-0540" facs="A83872-001-a-0500">Lodgings</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83872-0550" facs="A83872-001-a-0510">for</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-0560" facs="A83872-001-a-0520">their</w>
          <c> </c>
          <w lemma="habitation" ana="#n2" reg="habitations" xml:id="A83872-0570" facs="A83872-001-a-0530">habitations</w>
          <pc xml:id="A83872-0580" facs="A83872-001-a-0540">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A83872-0590" facs="A83872-001-a-0550">being</w>
          <c> </c>
          <w lemma="stranger" ana="#n2" reg="strangers" xml:id="A83872-0600" facs="A83872-001-a-0560">strangers</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-0610" facs="A83872-001-a-0570">and</w>
          <c> </c>
          <w lemma="altogether" ana="#av" reg="altogether" xml:id="A83872-0620" facs="A83872-001-a-0580">altogether</w>
          <c> </c>
          <w lemma="unknown" ana="#j" reg="unknown" xml:id="A83872-0630" facs="A83872-001-a-0590">unknown</w>
          <c> </c>
          <w lemma="how" ana="#crq-cs" reg="how" xml:id="A83872-0640" facs="A83872-001-a-0600">how</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83872-0650" facs="A83872-001-a-0610">they</w>
          <c> </c>
          <w lemma="stand" ana="#vvb" reg="stand" xml:id="A83872-0660" facs="A83872-001-a-0620">stand</w>
          <c> </c>
          <w lemma="affect" ana="#vvn" reg="affected" xml:id="A83872-0670" facs="A83872-001-a-0630">affected</w>
          <pc xml:id="A83872-0680" facs="A83872-001-a-0640">;</pc>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83872-0690" facs="A83872-001-a-0650">It</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A83872-0700" facs="A83872-001-a-0660">is</w>
          <c> </c>
          <w lemma="therefore" ana="#av" reg="therefore" xml:id="A83872-0710" facs="A83872-001-a-0670">therefore</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A83872-0720" facs="A83872-001-a-0680">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83872-0730" facs="A83872-001-a-0690">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-0740" facs="A83872-001-a-0700">the</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A83872-0750" facs="A83872-001-a-0710">Commons</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83872-0760" facs="A83872-001-a-0720">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83872-0770" facs="A83872-001-a-0730">Parliament</w>
          <pc xml:id="A83872-0780" facs="A83872-001-a-0740">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83872-0790" facs="A83872-001-a-0750">that</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-0800" facs="A83872-001-a-0760">the</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A83872-0810" facs="A83872-001-a-0770">Lord</w>
          <c> </c>
          <w lemma="major" ana="#j" reg="major" xml:id="A83872-0820" facs="A83872-001-a-0780">Major</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-0830" facs="A83872-001-a-0790">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-0840" facs="A83872-001-a-0800">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-0850" facs="A83872-001-a-0810">said</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-0860" facs="A83872-001-a-0820">City</w>
          <pc xml:id="A83872-0870" facs="A83872-001-a-0830">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83872-0880" facs="A83872-001-a-0840">shall</w>
          <c> </c>
          <w lemma="cause" ana="#vvi" reg="cause" xml:id="A83872-0890" facs="A83872-001-a-0850">cause</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83872-0900" facs="A83872-001-a-0860">a</w>
          <c> </c>
          <w lemma="general" ana="#j" reg="general" xml:id="A83872-0910" facs="A83872-001-a-0870">generall</w>
          <c> </c>
          <w lemma="search" ana="#n1" reg="search" xml:id="A83872-0920" facs="A83872-001-a-0880">search</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-0930" facs="A83872-001-a-0890">and</w>
          <c> </c>
          <w lemma="inquiry" ana="#n1" reg="inquiry" xml:id="A83872-0940" facs="A83872-001-a-0900">enquirie</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-0950" facs="A83872-001-a-0910">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83872-0960" facs="A83872-001-a-0920">be</w>
          <c> </c>
          <w lemma="make" ana="#vvn" reg="made" xml:id="A83872-0970" facs="A83872-001-a-0930">made</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A83872-0980" facs="A83872-001-a-0940">from</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A83872-0990" facs="A83872-001-a-0950">time</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83872-1000" facs="A83872-001-a-0960">to</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A83872-1010" facs="A83872-001-a-0970">time</w>
          <c> </c>
          <w lemma="throughout" ana="#acp-p" reg="throughout" xml:id="A83872-1020" facs="A83872-001-a-0980">throughout</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1030" facs="A83872-001-a-0990">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-1040" facs="A83872-001-a-1000">said</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-1050" facs="A83872-001-a-1010">City</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1060" facs="A83872-001-a-1020">and</w>
          <c> </c>
          <w lemma="suburb" ana="#n2" reg="suburbs" xml:id="A83872-1070" facs="A83872-001-a-1030">Suburbs</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A83872-1080" facs="A83872-001-a-1040">thereof</w>
          <pc xml:id="A83872-1090" facs="A83872-001-a-1050">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1100" facs="A83872-001-a-1060">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-1110" facs="A83872-001-a-1070">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A83872-1120" facs="A83872-001-a-1080">take</w>
          <c> </c>
          <w lemma="••eciall" ana="#j" reg="••eciall" xml:id="A83872-1130" facs="A83872-001-a-1090">••eciall</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A83872-1140" facs="A83872-001-a-1100">notice</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-1150" facs="A83872-001-a-1110">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1160" facs="A83872-001-a-1120">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-1170" facs="A83872-001-a-1130">said</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83872-1180" facs="A83872-001-a-1140">persons</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1190" facs="A83872-001-a-1150">and</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-1200" facs="A83872-001-a-1160">their</w>
          <c> </c>
          <w lemma="attendant" ana="#n2_j" reg="attendants" xml:id="A83872-1210" facs="A83872-001-a-1170">attendants</w>
          <pc xml:id="A83872-1220" facs="A83872-001-a-1180">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1230" facs="A83872-001-a-1190">and</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-1240" facs="A83872-001-a-1200">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-1250" facs="A83872-001-a-1210">their</w>
          <c> </c>
          <w lemma="name" ana="#n2" reg="names" xml:id="A83872-1260" facs="A83872-001-a-1220">names</w>
          <pc xml:id="A83872-1270" facs="A83872-001-a-1230">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1280" facs="A83872-001-a-1240">and</w>
          <c> </c>
          <w lemma="••r" ana="#n1" reg="••r" xml:id="A83872-1290" facs="A83872-001-a-1250">••r</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1300" facs="A83872-001-a-1260">the</w>
          <c> </c>
          <w lemma="better" ana="#j-c" reg="better" xml:id="A83872-1310" facs="A83872-001-a-1270">better</w>
          <c> </c>
          <w lemma="discovery" ana="#n1" reg="discovery" xml:id="A83872-1320" facs="A83872-001-a-1280">discovery</w>
          <c> </c>
          <w lemma="how" ana="#crq-cs" reg="how" xml:id="A83872-1330" facs="A83872-001-a-1290">how</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83872-1340" facs="A83872-001-a-1300">they</w>
          <c> </c>
          <w lemma="stand" ana="#vvb" reg="stand" xml:id="A83872-1350" facs="A83872-001-a-1310">stand</w>
          <c> </c>
          <w lemma="affect" ana="#vvn" reg="affected" xml:id="A83872-1360" facs="A83872-001-a-1320">affected</w>
          <pc xml:id="A83872-1370" facs="A83872-001-a-1330">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83872-1380" facs="A83872-001-a-1340">shall</w>
          <c> </c>
          <w lemma="tender" ana="#vvi" reg="tender" xml:id="A83872-1390" facs="A83872-001-a-1350">tender</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A83872-1400" facs="A83872-001-a-1360">unto</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A83872-1410" facs="A83872-001-a-1370">them</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1420" facs="A83872-001-a-1380">the</w>
          <c> </c>
          <w lemma="•ropositions" ana="#n2" reg="•ropositionss" xml:id="A83872-1430" facs="A83872-001-a-1390">•ropositions</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83872-1440" facs="A83872-001-a-1400">for</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A83872-1450" facs="A83872-001-a-1410">Horse</w>
          <c> </c>
          <w lemma="money" ana="#n1" reg="money" xml:id="A83872-1460" facs="A83872-001-a-1420">Money</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83872-1470" facs="A83872-001-a-1430">or</w>
          <c> </c>
          <w lemma="plate" ana="#n1" reg="plate" xml:id="A83872-1480" facs="A83872-001-a-1440">Plate</w>
          <pc xml:id="A83872-1490" facs="A83872-001-a-1450">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1500" facs="A83872-001-a-1460">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-1510" facs="A83872-001-a-1470">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A83872-1520" facs="A83872-001-a-1480">take</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-1530" facs="A83872-001-a-1490">their</w>
          <c> </c>
          <w lemma="subscription" ana="#n2" reg="subscriptions" xml:id="A83872-1540" facs="A83872-001-a-1500">subscriptions</w>
          <pc xml:id="A83872-1550" facs="A83872-001-a-1510">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1560" facs="A83872-001-a-1520">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-1570" facs="A83872-001-a-1530">to</w>
          <c> </c>
          <w lemma="in•orme" ana="#vvi" reg="in•orme" xml:id="A83872-1580" facs="A83872-001-a-1540">in•orme</w>
          <c> </c>
          <w lemma="himself" ana="#px" reg="himself" xml:id="A83872-1590" facs="A83872-001-a-1550">himselfe</w>
          <c> </c>
          <w lemma="whether" ana="#cs" reg="whether" xml:id="A83872-1600" facs="A83872-001-a-1560">whether</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83872-1610" facs="A83872-001-a-1570">they</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A83872-1620" facs="A83872-001-a-1580">doe</w>
          <c> </c>
          <w lemma="make" ana="#vvi" reg="make" xml:id="A83872-1630" facs="A83872-001-a-1590">make</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-1640" facs="A83872-001-a-1600">their</w>
          <c> </c>
          <w lemma="payment" ana="#n2" reg="payments" xml:id="A83872-1650" facs="A83872-001-a-1610">paiements</w>
          <c> </c>
          <w lemma="accord" ana="#av_vg" reg="accordingly" xml:id="A83872-1660" facs="A83872-001-a-1620">accordingly</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A83872-1670" facs="A83872-001-a-1630">into</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1680" facs="A83872-001-a-1640">the</w>
          <c> </c>
          <hi xml:id="A83872e-100">
            <w lemma="guild" ana="#n1" reg="guild" xml:id="A83872-1690" facs="A83872-001-a-1650">Guild</w>
            <c> </c>
            <w lemma="hall" ana="#n1" reg="hall" xml:id="A83872-1700" facs="A83872-001-a-1660">Hall</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-1710" facs="A83872-001-a-1670">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1720" facs="A83872-001-a-1680">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-1730" facs="A83872-001-a-1690">said</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-1740" facs="A83872-001-a-1700">City</w>
          <pc xml:id="A83872-1750" facs="A83872-001-a-1710">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-1760" facs="A83872-001-a-1720">And</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-1770" facs="A83872-001-a-1730">to</w>
          <c> </c>
          <w lemma="return" ana="#vvi" reg="return" xml:id="A83872-1780" facs="A83872-001-a-1740">returne</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1790" facs="A83872-001-a-1750">the</w>
          <c> </c>
          <w lemma="name" ana="#n2" reg="names" xml:id="A83872-1800" facs="A83872-001-a-1760">names</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-1810" facs="A83872-001-a-1770">of</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A83872-1820" facs="A83872-001-a-1780">such</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83872-1830" facs="A83872-001-a-1790">as</w>
          <c> </c>
          <w lemma="can" ana="#vmb-x" reg="cannot" xml:id="A83872-1840" facs="A83872-001-a-1800">cannot</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A83872-1850" facs="A83872-001-a-1810">give</w>
          <c> </c>
          <w xml:id="A83872-1860" lemma="•" ana="#zz" facs="A83872-001-a-1820">•</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A83872-1870" facs="A83872-001-a-1830">good</w>
          <c> </c>
          <w lemma="account" ana="#n1" reg="account" xml:id="A83872-1880" facs="A83872-001-a-1840">account</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83872-1890" facs="A83872-001-a-1850">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-1900" facs="A83872-001-a-1860">their</w>
          <c> </c>
          <w lemma="come" ana="#n1_vg" reg="coming" xml:id="A83872-1910" facs="A83872-001-a-1870">coming</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83872-1920" facs="A83872-001-a-1880">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-1930" facs="A83872-001-a-1890">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-1940" facs="A83872-001-a-1900">said</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A83872-1950" facs="A83872-001-a-1910">City</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83872-1960" facs="A83872-001-a-1920">or</w>
          <c> </c>
          <w lemma="suburb" ana="#n2" reg="suburbs" xml:id="A83872-1970" facs="A83872-001-a-1930">suburbs</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A83872-1980" facs="A83872-001-a-1940">thereof</w>
          <pc xml:id="A83872-1990" facs="A83872-001-a-1950">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83872-2000" facs="A83872-001-a-1960">or</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83872-2010" facs="A83872-001-a-1970">that</w>
          <c> </c>
          <w lemma="••ould" ana="#vmd" reg="••ould" xml:id="A83872-2020" facs="A83872-001-a-1980">••ould</w>
          <c> </c>
          <w lemma="refuse" ana="#vvi" reg="refuse" xml:id="A83872-2030" facs="A83872-001-a-1990">refuse</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83872-2040" facs="A83872-001-a-2000">to</w>
          <c> </c>
          <w lemma="subscribe" ana="#vvi" reg="subscribe" xml:id="A83872-2050" facs="A83872-001-a-2010">subscribe</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83872-2060" facs="A83872-001-a-2020">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83872-2070" facs="A83872-001-a-2030">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A83872-2080" facs="A83872-001-a-2040">said</w>
          <c> </c>
          <w lemma="proposition" ana="#n2" reg="propositions" xml:id="A83872-2090" facs="A83872-001-a-2050">Propositions</w>
          <pc xml:id="A83872-2100" facs="A83872-001-a-2060">,</pc>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A83872-2110" facs="A83872-001-a-2070">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83872-2120" facs="A83872-001-a-2080">to</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83872-2130" facs="A83872-001-a-2090">their</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83872-2140" facs="A83872-001-a-2100">severall</w>
          <c> </c>
          <w lemma="•states" ana="#n2" reg="•statess" xml:id="A83872-2150" facs="A83872-001-a-2110">•states</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83872-2160" facs="A83872-001-a-2120">and</w>
          <c> </c>
          <w lemma="quality" ana="#n2" reg="qualities" xml:id="A83872-2170" facs="A83872-001-a-2130">qualities</w>
          <pc xml:id="A83872-2180" facs="A83872-001-a-2140">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83872-2190" facs="A83872-001-a-2150">that</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A83872-2200" facs="A83872-001-a-2160">some</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A83872-2210" facs="A83872-001-a-2170">further</w>
          <c> </c>
          <w lemma="course" ana="#n1" reg="course" xml:id="A83872-2220" facs="A83872-001-a-2180">course</w>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A83872-2230" facs="A83872-001-a-2190">may</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83872-2240" facs="A83872-001-a-2200">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A83872-2250" facs="A83872-001-a-2210">forthwith</w>
          <c> </c>
          <w lemma="take" ana="#vvn" reg="taken" xml:id="A83872-2260" facs="A83872-001-a-2220">taken</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83872-2270" facs="A83872-001-a-2230">by</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83872-2280" facs="A83872-001-a-2240">this</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A83872-2290" facs="A83872-001-a-2250">House</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83872-2300" facs="A83872-001-a-2260">in</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A83872-2310" facs="A83872-001-a-2270">that</w>
          <c> </c>
          <w lemma="behalf" ana="#n1" reg="behalf" xml:id="A83872-2320" facs="A83872-001-a-2280">behalfe</w>
          <pc unit="sentence" xml:id="A83872-2330" facs="A83872-001-a-2290">.</pc>
        </p>
        <closer xml:id="A83872e-110">
          <signed xml:id="A83872e-120">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A83872-2370" facs="A83872-001-a-2300">Hen</w>
            <pc xml:id="A83872-2380" facs="A83872-001-a-2310">:</pc>
            <c> </c>
            <w lemma="Elsing" ana="#n1-nn" reg="Elsing" xml:id="A83872-2390" facs="A83872-001-a-2320">Elsynge</w>
            <c> </c>
            <w lemma="cleri" ana="#fw-la" reg="cleri" xml:id="A83872-2400" facs="A83872-001-a-2330">Cleri</w>
            <pc unit="sentence" xml:id="A83872-2410" facs="A83872-001-a-2340">.</pc>
            <c> </c>
            <w lemma="parl." ana="#n-ab" reg="Parl." xml:id="A83872-2420" facs="A83872-001-a-2350">Parl.</w>
            <c> </c>
            <w lemma="d." ana="#n-ab" reg="d." xml:id="A83872-2430" facs="A83872-001-a-2360">D.</w>
            <c> </c>
            <w lemma="com." ana="#n-ab" reg="com." xml:id="A83872-2440" facs="A83872-001-a-2370">Com.</w>
            <pc unit="sentence" xml:id="A83872-2440-eos" facs="A83872-001-a-2380"/>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A83872e-130">
      <div type="colophon" xml:id="A83872e-140">
        <p xml:id="A83872e-150">
          <w lemma="london" ana="#n1-nn" reg="London" xml:id="A83872-2490" facs="A83872-001-a-2390">London</w>
          <pc xml:id="A83872-2500" facs="A83872-001-a-2400">,</pc>
          <c> </c>
          <hi xml:id="A83872e-160">
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A83872-2510" facs="A83872-001-a-2410">Printed</w>
            <c> </c>
            <w lemma="for" ana="#acp-p" reg="for" xml:id="A83872-2520" facs="A83872-001-a-2420">for</w>
          </hi>
          <c> </c>
          <w lemma="r." ana="#n-ab" reg="r." xml:id="A83872-2530" facs="A83872-001-a-2430">R.</w>
          <c> </c>
          <w lemma="Oulton" ana="#n1-nn" reg="Oulton" xml:id="A83872-2540" facs="A83872-001-a-2440">Oulton</w>
          <c> </c>
          <hi xml:id="A83872e-170">
            <pc xml:id="A83872-2550" facs="A83872-001-a-2450">&amp;</pc>
          </hi>
          <c> </c>
          <w lemma="g." ana="#n-ab" reg="g." xml:id="A83872-2560" facs="A83872-001-a-2460">G.</w>
          <c> </c>
          <w lemma="dexter" ana="#n1-nn" reg="Dexter" xml:id="A83872-2570" facs="A83872-001-a-2470">Dexter</w>
          <pc xml:id="A83872-2580" facs="A83872-001-a-2480">,</pc>
          <c> </c>
          <w lemma="1642." ana="#crd" reg="1642." xml:id="A83872-2590" facs="A83872-001-a-2490">1642.</w>
          <pc unit="sentence" xml:id="A83872-2590-eos" facs="A83872-001-a-2500"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>