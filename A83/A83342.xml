<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Tuesday, May 10th. 1659. The Parliament doth resolve and declare, that all persons whatsoever shall pay, and hereby are required to pay in all arrears and growing duties, for customs, excise and new impost, monethly taxes, and all other moneys due and payable to the Common-wealth. ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1659</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83342</idno>
        <idno type="STC">Wing E2121</idno>
        <idno type="STC">Thomason 669.f.21[31]</idno>
        <idno type="STC">ESTC R211169</idno>
        <idno type="EEBO-CITATION">99869901</idno>
        <idno type="PROQUEST">99869901</idno>
        <idno type="VID">163526</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83342)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163526)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f21[31])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Tuesday, May 10th. 1659. The Parliament doth resolve and declare, that all persons whatsoever shall pay, and hereby are required to pay in all arrears and growing duties, for customs, excise and new impost, monethly taxes, and all other moneys due and payable to the Common-wealth. ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by John Field, Printer to the Parliament. And are to be sold at the seven Stars in Fleetstreet, over against Dunstans Church,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1659.</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from first lines of text.</note>
            <note>A resolution of Parliament, calling in all arrears of duties for customs, excise, etc.</note>
            <note>Order to print signed: Jo. Phelpes, Clerk of the Parliament pro tempore.</note>
            <note>Annotation on Thomason copy: "May. 11".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Taxation -- England -- Early works to 1800.</term>
          <term>Excise tax -- England -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
      <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83342e-10">
    <body xml:id="A83342e-20">
      <div type="text" xml:id="A83342e-30">
        <pb facs="tcp:163526:1" rendition="simple:additions" xml:id="A83342-001-a"/>
        <head xml:id="A83342e-40">
          <date xml:id="A83342e-50">
            <w lemma="tuesday" ana="#n1-nn" reg="Tuesday" xml:id="A83342-0040" facs="A83342-001-a-0010">Tuesday</w>
            <c> </c>
            <w lemma="may" ana="#vmb" reg="May" xml:id="A83342-0050" facs="A83342-001-a-0020">May</w>
            <c> </c>
            <w lemma="10th" part="I" ana="#ord" reg="10th" xml:id="A83342-0060.1" facs="A83342-001-a-0030" rendition="hi-mid-3">10th</w>
            <hi rend="sup" xml:id="A83342e-60"/>
            <c> </c>
            <w lemma="1659." ana="#crd" reg="1659." xml:id="A83342-0070" facs="A83342-001-a-0050">1659.</w>
            <pc unit="sentence" xml:id="A83342-0070-eos" facs="A83342-001-a-0060"/>
          </date>
        </head>
        <p xml:id="A83342e-70">
          <hi xml:id="A83342e-80">
            <w lemma="the" ana="#d" reg="the" xml:id="A83342-0100" facs="A83342-001-a-0070">THe</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83342-0110" facs="A83342-001-a-0080">Parliament</w>
            <c> </c>
            <w lemma="do" ana="#vvz" reg="doth" xml:id="A83342-0120" facs="A83342-001-a-0090">doth</w>
            <c> </c>
            <w lemma="resolve" ana="#vvi" reg="resolve" xml:id="A83342-0130" facs="A83342-001-a-0100">Resolve</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0140" facs="A83342-001-a-0110">and</w>
            <c> </c>
            <w lemma="declare" ana="#vvi" reg="declare" xml:id="A83342-0150" facs="A83342-001-a-0120">Declare</w>
            <pc xml:id="A83342-0160" facs="A83342-001-a-0130">,</pc>
          </hi>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83342-0170" facs="A83342-001-a-0140">That</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83342-0180" facs="A83342-001-a-0150">all</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83342-0190" facs="A83342-001-a-0160">persons</w>
          <c> </c>
          <w lemma="whatsoever" ana="#crq-r" reg="whatsoever" xml:id="A83342-0200" facs="A83342-001-a-0170">whatsoever</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83342-0210" facs="A83342-001-a-0180">shall</w>
          <c> </c>
          <w lemma="pay" ana="#vvi" reg="pay" xml:id="A83342-0220" facs="A83342-001-a-0190">pay</w>
          <pc xml:id="A83342-0230" facs="A83342-001-a-0200">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0240" facs="A83342-001-a-0210">and</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A83342-0250" facs="A83342-001-a-0220">hereby</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83342-0260" facs="A83342-001-a-0230">are</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A83342-0270" facs="A83342-001-a-0240">required</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83342-0280" facs="A83342-001-a-0250">to</w>
          <c> </c>
          <w lemma="pay" ana="#vvi" reg="pay" xml:id="A83342-0290" facs="A83342-001-a-0260">pay</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83342-0300" facs="A83342-001-a-0270">in</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83342-0310" facs="A83342-001-a-0280">all</w>
          <c> </c>
          <w lemma="arrear" ana="#n2" reg="arrears" xml:id="A83342-0320" facs="A83342-001-a-0290">Arrears</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0330" facs="A83342-001-a-0300">and</w>
          <c> </c>
          <w lemma="grow" ana="#vvg" reg="growing" xml:id="A83342-0340" facs="A83342-001-a-0310">Growing</w>
          <c> </c>
          <w lemma="duty" ana="#n2" reg="duties" xml:id="A83342-0350" facs="A83342-001-a-0320">Duties</w>
          <pc xml:id="A83342-0360" facs="A83342-001-a-0330">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83342-0370" facs="A83342-001-a-0340">for</w>
          <c> </c>
          <w lemma="custom" ana="#n2" reg="customs" xml:id="A83342-0380" facs="A83342-001-a-0350">Customs</w>
          <pc xml:id="A83342-0390" facs="A83342-001-a-0360">,</pc>
          <c> </c>
          <w lemma="excise" ana="#n1" reg="excise" xml:id="A83342-0400" facs="A83342-001-a-0370">Excise</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0410" facs="A83342-001-a-0380">and</w>
          <c> </c>
          <w lemma="new" ana="#j" reg="new" xml:id="A83342-0420" facs="A83342-001-a-0390">new</w>
          <c> </c>
          <w lemma="impost" ana="#n1" reg="impost" xml:id="A83342-0430" facs="A83342-001-a-0400">Impost</w>
          <pc xml:id="A83342-0440" facs="A83342-001-a-0410">,</pc>
          <c> </c>
          <w lemma="monthly" ana="#j" reg="monthly" xml:id="A83342-0450" facs="A83342-001-a-0420">Monethly</w>
          <c> </c>
          <w lemma="tax" ana="#n2" reg="taxes" xml:id="A83342-0460" facs="A83342-001-a-0430">Taxes</w>
          <pc xml:id="A83342-0470" facs="A83342-001-a-0440">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0480" facs="A83342-001-a-0450">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83342-0490" facs="A83342-001-a-0460">all</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A83342-0500" facs="A83342-001-a-0470">other</w>
          <c> </c>
          <w lemma="money" ana="#n2" reg="monies" xml:id="A83342-0510" facs="A83342-001-a-0480">Moneys</w>
          <c> </c>
          <w lemma="due" ana="#j" reg="due" xml:id="A83342-0520" facs="A83342-001-a-0490">due</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0530" facs="A83342-001-a-0500">and</w>
          <c> </c>
          <w lemma="payable" ana="#j" reg="payable" xml:id="A83342-0540" facs="A83342-001-a-0510">payable</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83342-0550" facs="A83342-001-a-0520">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-0560" facs="A83342-001-a-0530">the</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A83342-0570" facs="A83342-001-a-0540">Common-wealth</w>
          <pc unit="sentence" xml:id="A83342-0580" facs="A83342-001-a-0550">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A83342-0590" facs="A83342-001-a-0560">And</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83342-0600" facs="A83342-001-a-0570">all</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83342-0610" facs="A83342-001-a-0580">persons</w>
          <c> </c>
          <w lemma="employ" ana="#vvn" reg="employed" xml:id="A83342-0620" facs="A83342-001-a-0590">imployed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83342-0630" facs="A83342-001-a-0600">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-0640" facs="A83342-001-a-0610">the</w>
          <c> </c>
          <w lemma="receive" ana="#vvg" reg="receiving" xml:id="A83342-0650" facs="A83342-001-a-0620">Receiving</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0660" facs="A83342-001-a-0630">and</w>
          <c> </c>
          <w lemma="collect" ana="#vvg" reg="collecting" xml:id="A83342-0670" facs="A83342-001-a-0640">Collecting</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-0680" facs="A83342-001-a-0650">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A83342-0690" facs="A83342-001-a-0660">same</w>
          <pc xml:id="A83342-0700" facs="A83342-001-a-0670">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83342-0710" facs="A83342-001-a-0680">are</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A83342-0720" facs="A83342-001-a-0690">hereby</w>
          <c> </c>
          <w lemma="empower" ana="#vvn" reg="empowered" xml:id="A83342-0730" facs="A83342-001-a-0700">impowred</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0740" facs="A83342-001-a-0710">and</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A83342-0750" facs="A83342-001-a-0720">required</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83342-0760" facs="A83342-001-a-0730">to</w>
          <c> </c>
          <w lemma="act" ana="#vvi" reg="act" xml:id="A83342-0770" facs="A83342-001-a-0740">act</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83342-0780" facs="A83342-001-a-0750">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83342-0790" facs="A83342-001-a-0760">their</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A83342-0800" facs="A83342-001-a-0770">several</w>
          <c> </c>
          <w lemma="place" ana="#n2" reg="places" xml:id="A83342-0810" facs="A83342-001-a-0780">places</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83342-0820" facs="A83342-001-a-0790">for</w>
          <c> </c>
          <w lemma="receive" ana="#vvg" reg="receiving" xml:id="A83342-0830" facs="A83342-001-a-0800">Receiving</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-0840" facs="A83342-001-a-0810">and</w>
          <c> </c>
          <w lemma="collect" ana="#vvg" reg="collecting" xml:id="A83342-0850" facs="A83342-001-a-0820">Collecting</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-0860" facs="A83342-001-a-0830">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A83342-0870" facs="A83342-001-a-0840">same</w>
          <pc xml:id="A83342-0880" facs="A83342-001-a-0850">,</pc>
          <c> </c>
          <w lemma="until" ana="#acp-cs" reg="until" xml:id="A83342-0890" facs="A83342-001-a-0860">until</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-0900" facs="A83342-001-a-0870">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83342-0910" facs="A83342-001-a-0880">Parliament</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83342-0920" facs="A83342-001-a-0890">shall</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A83342-0930" facs="A83342-001-a-0900">take</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A83342-0940" facs="A83342-001-a-0910">further</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A83342-0950" facs="A83342-001-a-0920">Order</w>
          <pc unit="sentence" xml:id="A83342-0960" facs="A83342-001-a-0930">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A83342e-90">
      <div type="license" xml:id="A83342e-100">
        <p xml:id="A83342e-110">
          <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A83342-1010" facs="A83342-001-a-0940">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83342-1020" facs="A83342-001-a-0950">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-1030" facs="A83342-001-a-0960">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83342-1040" facs="A83342-001-a-0970">Parliament</w>
          <pc xml:id="A83342-1050" facs="A83342-001-a-0980">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83342-1060" facs="A83342-001-a-0990">That</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83342-1070" facs="A83342-001-a-1000">this</w>
          <c> </c>
          <w lemma="vote" ana="#n1" reg="vote" xml:id="A83342-1080" facs="A83342-001-a-1010">Vote</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83342-1090" facs="A83342-001-a-1020">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A83342-1100" facs="A83342-001-a-1030">forthwith</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83342-1110" facs="A83342-001-a-1040">Printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83342-1120" facs="A83342-001-a-1050">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A83342-1130" facs="A83342-001-a-1060">Published</w>
          <pc unit="sentence" xml:id="A83342-1140" facs="A83342-001-a-1070">.</pc>
        </p>
        <closer xml:id="A83342e-120">
          <signed xml:id="A83342e-130">
            <w lemma="JO" ana="#n1-nn" reg="Jo" xml:id="A83342-1180" facs="A83342-001-a-1080">JO</w>
            <pc unit="sentence" xml:id="A83342-1190" facs="A83342-001-a-1090">.</pc>
            <c> </c>
            <w lemma="phelpes" ana="#n1-nn" reg="Phelpes" xml:id="A83342-1200" facs="A83342-001-a-1100">PHELPES</w>
            <c> </c>
            <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A83342-1210" facs="A83342-001-a-1110">Clerk</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A83342-1220" facs="A83342-001-a-1120">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A83342-1230" facs="A83342-001-a-1130">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83342-1240" facs="A83342-001-a-1140">Parliament</w>
            <c> </c>
            <hi xml:id="A83342e-140">
              <w lemma="pro" ana="#fw-la" reg="pro" xml:id="A83342-1250" facs="A83342-001-a-1150">pro</w>
              <c> </c>
              <w lemma="tempore" ana="#fw-la" reg="tempore" xml:id="A83342-1260" facs="A83342-001-a-1160">tempore</w>
              <pc unit="sentence" xml:id="A83342-1270" facs="A83342-001-a-1170">.</pc>
              <c> </c>
            </hi>
          </signed>
        </closer>
      </div>
      <div type="colophon" xml:id="A83342e-150">
        <p xml:id="A83342e-160">
          <hi xml:id="A83342e-170">
            <w lemma="LONDON" ana="#n1-nn" reg="London" xml:id="A83342-1310" facs="A83342-001-a-1180">LONDON</w>
            <pc xml:id="A83342-1320" facs="A83342-001-a-1190">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83342-1330" facs="A83342-001-a-1200">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83342-1340" facs="A83342-001-a-1210">by</w>
          <c> </c>
          <w lemma="JOHN" ana="#n1-nn" reg="John" xml:id="A83342-1350" facs="A83342-001-a-1220">JOHN</w>
          <c> </c>
          <w lemma="field" ana="#n1" reg="field" xml:id="A83342-1360" facs="A83342-001-a-1230">FIELD</w>
          <pc xml:id="A83342-1370" facs="A83342-001-a-1240">,</pc>
          <c> </c>
          <w lemma="printer" ana="#n1" reg="printer" xml:id="A83342-1380" facs="A83342-001-a-1250">Printer</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83342-1390" facs="A83342-001-a-1260">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-1400" facs="A83342-001-a-1270">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83342-1410" facs="A83342-001-a-1280">Parliament</w>
          <pc unit="sentence" xml:id="A83342-1420" facs="A83342-001-a-1290">.</pc>
        </p>
        <p xml:id="A83342e-180">
          <w lemma="and" ana="#cc" reg="And" xml:id="A83342-1450" facs="A83342-001-a-1300">And</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83342-1460" facs="A83342-001-a-1310">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83342-1470" facs="A83342-001-a-1320">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83342-1480" facs="A83342-001-a-1330">be</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A83342-1490" facs="A83342-001-a-1340">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83342-1500" facs="A83342-001-a-1350">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83342-1510" facs="A83342-001-a-1360">the</w>
          <c> </c>
          <w lemma="seven" ana="#crd" reg="seven" xml:id="A83342-1520" facs="A83342-001-a-1370">seven</w>
          <c> </c>
          <w lemma="star" ana="#n2" reg="stars" xml:id="A83342-1530" facs="A83342-001-a-1380">Stars</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83342-1540" facs="A83342-001-a-1390">in</w>
          <c> </c>
          <hi xml:id="A83342e-190">
            <w lemma="Fleetstreet" ana="#n1-nn" reg="Fleetstreet" xml:id="A83342-1550" facs="A83342-001-a-1400">Fleetstreet</w>
            <pc xml:id="A83342-1560" facs="A83342-001-a-1410">,</pc>
          </hi>
          <c> </c>
          <w lemma="over" ana="#acp-av" reg="over" xml:id="A83342-1570" facs="A83342-001-a-1420">over</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A83342-1580" facs="A83342-001-a-1430">against</w>
          <c> </c>
          <hi xml:id="A83342e-200">
            <w lemma="Dunstan" ana="#n1g-nn" reg="Dunstan's" xml:id="A83342-1590" facs="A83342-001-a-1440">Dunstans</w>
          </hi>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A83342-1600" facs="A83342-001-a-1450">Church</w>
          <pc xml:id="A83342-1610" facs="A83342-001-a-1460">,</pc>
          <c> </c>
          <w lemma="1659." ana="#crd" reg="1659." xml:id="A83342-1620" facs="A83342-001-a-1470">1659.</w>
          <pc unit="sentence" xml:id="A83342-1620-eos" facs="A83342-001-a-1480"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>