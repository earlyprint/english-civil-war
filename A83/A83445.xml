<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Martis, 9[o] Aprilis, 1650. Resolved by the Parliament, that the arms of the late King be taken down in all ships of and belonging to the Commonwealth; ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1650</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83445</idno>
        <idno type="STC">Wing E2257</idno>
        <idno type="STC">Thomason 669.f.15[25]</idno>
        <idno type="STC">ESTC R211374</idno>
        <idno type="EEBO-CITATION">99870102</idno>
        <idno type="PROQUEST">99870102</idno>
        <idno type="VID">163100</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83445)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163100)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f15[25])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Martis, 9[o] Aprilis, 1650. Resolved by the Parliament, that the arms of the late King be taken down in all ships of and belonging to the Commonwealth; ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by Edward Husband and John Field, Printers to the Parliament of England,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1650.</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from caption title and first lines of text.</note>
            <note>Order to print signed: Hen: Scobell, Cleric. Parliamenti.</note>
            <note>The bracketed "o" in title is in superscript on t.p.</note>
            <note>Annotation on Thomason copy: "May. 3".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Commonwealth and Protectorate 1649-1660 -- Early works to 1800.</term>
          <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83445e-10">
    <body xml:id="A83445e-20">
      <div type="Parliamentary_resolution" xml:id="A83445e-30">
        <pb facs="tcp:163100:1" rendition="simple:additions" xml:id="A83445-001-a"/>
        <head xml:id="A83445e-40">
          <figure xml:id="A83445e-50">
            <figDesc xml:id="A83445e-60">blazon or coat of arms</figDesc>
          </figure>
        </head>
        <opener xml:id="A83445e-70">
          <dateline xml:id="A83445e-80">
            <date xml:id="A83445e-90">
              <hi xml:id="A83445e-100">
                <w lemma="die" ana="#fw-la" reg="die" xml:id="A83445-0140" facs="A83445-001-a-0010">Die</w>
                <c> </c>
                <w lemma="Martis" ana="#n1-nn" reg="Martis" xml:id="A83445-0150" facs="A83445-001-a-0020">Martis</w>
                <pc xml:id="A83445-0160" facs="A83445-001-a-0030">,</pc>
              </hi>
              <c> </c>
              <w lemma="9o" part="I" ana="#fw-la" reg="9o" xml:id="A83445-0170.1" facs="A83445-001-a-0040" rendition="hi-mid-2">9o</w>
              <hi rend="sup" xml:id="A83445e-110"/>
              <c> </c>
              <hi xml:id="A83445e-120">
                <w lemma="aprilis" ana="#fw-la" reg="aprilis" xml:id="A83445-0180" facs="A83445-001-a-0060">Aprilis</w>
                <pc xml:id="A83445-0190" facs="A83445-001-a-0070">,</pc>
              </hi>
              <c> </c>
              <w lemma="1650." ana="#crd" reg="1650." xml:id="A83445-0200" facs="A83445-001-a-0080">1650.</w>
              <pc unit="sentence" xml:id="A83445-0200-eos" facs="A83445-001-a-0090"/>
            </date>
          </dateline>
          <lb xml:id="A83445e-130"/>
          <hi xml:id="A83445e-140">
            <w lemma="resolve" ana="#j_vn" reg="resolved" xml:id="A83445-0220" facs="A83445-001-a-0100">Resolved</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A83445-0230" facs="A83445-001-a-0110">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A83445-0240" facs="A83445-001-a-0120">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83445-0250" facs="A83445-001-a-0130">Parliament</w>
            <pc xml:id="A83445-0260" facs="A83445-001-a-0140">,</pc>
          </hi>
        </opener>
        <p xml:id="A83445e-150">
          <w lemma="that" ana="#cs" reg="That" rend="initialcharacterdecorated" xml:id="A83445-0290" facs="A83445-001-a-0150">THat</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0300" facs="A83445-001-a-0160">the</w>
          <c> </c>
          <w lemma="arm" ana="#n2" reg="arms" xml:id="A83445-0310" facs="A83445-001-a-0170">Arms</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-0320" facs="A83445-001-a-0180">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0330" facs="A83445-001-a-0190">the</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A83445-0340" facs="A83445-001-a-0200">late</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A83445-0350" facs="A83445-001-a-0210">King</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83445-0360" facs="A83445-001-a-0220">be</w>
          <c> </c>
          <w lemma="take" ana="#vvn" reg="taken" xml:id="A83445-0370" facs="A83445-001-a-0230">taken</w>
          <c> </c>
          <w lemma="down" ana="#acp-av" reg="down" xml:id="A83445-0380" facs="A83445-001-a-0240">down</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83445-0390" facs="A83445-001-a-0250">in</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-0400" facs="A83445-001-a-0260">all</w>
          <c> </c>
          <w lemma="ship" ana="#n2" reg="ships" xml:id="A83445-0410" facs="A83445-001-a-0270">Ships</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-0420" facs="A83445-001-a-0280">of</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-0430" facs="A83445-001-a-0290">and</w>
          <c> </c>
          <w lemma="belong" ana="#vvg" reg="belonging" xml:id="A83445-0440" facs="A83445-001-a-0300">belonging</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83445-0450" facs="A83445-001-a-0310">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0460" facs="A83445-001-a-0320">the</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A83445-0470" facs="A83445-001-a-0330">Commonwealth</w>
          <pc xml:id="A83445-0480" facs="A83445-001-a-0340">;</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83445-0490" facs="A83445-001-a-0350">as</w>
          <c> </c>
          <w lemma="also" ana="#av" reg="also" xml:id="A83445-0500" facs="A83445-001-a-0360">also</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-0510" facs="A83445-001-a-0370">of</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-0520" facs="A83445-001-a-0380">all</w>
          <c> </c>
          <w lemma="merchant" ana="#n2" reg="merchants" xml:id="A83445-0530" facs="A83445-001-a-0390">Merchants</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83445-0540" facs="A83445-001-a-0400">or</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A83445-0550" facs="A83445-001-a-0410">others</w>
          <c> </c>
          <w lemma="inhabit" ana="#vvg" reg="inhabiting" xml:id="A83445-0560" facs="A83445-001-a-0420">inhabiting</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A83445-0570" facs="A83445-001-a-0430">within</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0580" facs="A83445-001-a-0440">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A83445-0590" facs="A83445-001-a-0450">same</w>
          <pc xml:id="A83445-0600" facs="A83445-001-a-0460">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-0610" facs="A83445-001-a-0470">and</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83445-0620" facs="A83445-001-a-0480">that</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0630" facs="A83445-001-a-0490">the</w>
          <c> </c>
          <w lemma="general" ana="#n2" reg="generals" xml:id="A83445-0640" facs="A83445-001-a-0500">Generals</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83445-0650" facs="A83445-001-a-0510">at</w>
          <c> </c>
          <w lemma="sea" ana="#n1" reg="sea" xml:id="A83445-0660" facs="A83445-001-a-0520">Sea</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83445-0670" facs="A83445-001-a-0530">be</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A83445-0680" facs="A83445-001-a-0540">required</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83445-0690" facs="A83445-001-a-0550">to</w>
          <c> </c>
          <w lemma="see" ana="#vvi" reg="see" xml:id="A83445-0700" facs="A83445-001-a-0560">see</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0710" facs="A83445-001-a-0570">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A83445-0720" facs="A83445-001-a-0580">same</w>
          <c> </c>
          <w lemma="do" ana="#vvn" reg="done" xml:id="A83445-0730" facs="A83445-001-a-0590">done</w>
          <c> </c>
          <w lemma="accord" ana="#av_vg" reg="accordingly" xml:id="A83445-0740" facs="A83445-001-a-0600">accordingly</w>
          <pc unit="sentence" xml:id="A83445-0750" facs="A83445-001-a-0610">.</pc>
        </p>
        <p xml:id="A83445e-160">
          <hi xml:id="A83445e-170">
            <w lemma="resolve" ana="#j_vn" reg="resolved" xml:id="A83445-0780" facs="A83445-001-a-0620">Resolved</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A83445-0790" facs="A83445-001-a-0630">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A83445-0800" facs="A83445-001-a-0640">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83445-0810" facs="A83445-001-a-0650">Parliament</w>
            <pc xml:id="A83445-0820" facs="A83445-001-a-0660">,</pc>
          </hi>
        </p>
        <p xml:id="A83445e-180">
          <w lemma="that" ana="#cs" reg="That" xml:id="A83445-0850" facs="A83445-001-a-0670">THat</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-0860" facs="A83445-001-a-0680">all</w>
          <c> </c>
          <w lemma="justice" ana="#n2" reg="justices" xml:id="A83445-0870" facs="A83445-001-a-0690">Justices</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-0880" facs="A83445-001-a-0700">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0890" facs="A83445-001-a-0710">the</w>
          <c> </c>
          <w lemma="peace" ana="#n1" reg="peace" xml:id="A83445-0900" facs="A83445-001-a-0720">Peace</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83445-0910" facs="A83445-001-a-0730">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-0920" facs="A83445-001-a-0740">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A83445-0930" facs="A83445-001-a-0750">respective</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A83445-0940" facs="A83445-001-a-0760">Counties</w>
          <pc xml:id="A83445-0950" facs="A83445-001-a-0770">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-0960" facs="A83445-001-a-0780">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-0970" facs="A83445-001-a-0790">all</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A83445-0980" facs="A83445-001-a-0800">other</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A83445-0990" facs="A83445-001-a-0810">publique</w>
          <c> </c>
          <w lemma="magistrate" ana="#n2" reg="magistrates" xml:id="A83445-1000" facs="A83445-001-a-0820">Magistrates</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1010" facs="A83445-001-a-0830">and</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A83445-1020" facs="A83445-001-a-0840">Officers</w>
          <pc xml:id="A83445-1030" facs="A83445-001-a-0850">,</pc>
          <c> </c>
          <w lemma="churchwarden" ana="#n2" reg="churchwardens" xml:id="A83445-1040" facs="A83445-001-a-0860">Church-wardens</w>
          <pc xml:id="A83445-1050" facs="A83445-001-a-0870">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1060" facs="A83445-001-a-0880">and</w>
          <c> </c>
          <w lemma="warden" ana="#n2" reg="wardens" xml:id="A83445-1070" facs="A83445-001-a-0890">VVardens</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-1080" facs="A83445-001-a-0900">of</w>
          <c> </c>
          <w lemma="company" ana="#n2" reg="companies" xml:id="A83445-1090" facs="A83445-001-a-0910">Companies</w>
          <pc xml:id="A83445-1100" facs="A83445-001-a-0920">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83445-1110" facs="A83445-001-a-0930">be</w>
          <c> </c>
          <w lemma="authorize" ana="#vvn" reg="authorised" xml:id="A83445-1120" facs="A83445-001-a-0940">authorized</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1130" facs="A83445-001-a-0950">and</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A83445-1140" facs="A83445-001-a-0960">required</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83445-1150" facs="A83445-001-a-0970">to</w>
          <c> </c>
          <w lemma="cause" ana="#vvi" reg="cause" xml:id="A83445-1160" facs="A83445-001-a-0980">cause</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-1170" facs="A83445-001-a-0990">the</w>
          <c> </c>
          <w lemma="arm" ana="#n2" reg="arms" xml:id="A83445-1180" facs="A83445-001-a-1000">Arms</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-1190" facs="A83445-001-a-1010">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-1200" facs="A83445-001-a-1020">the</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A83445-1210" facs="A83445-001-a-1030">late</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A83445-1220" facs="A83445-001-a-1040">King</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83445-1230" facs="A83445-001-a-1050">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83445-1240" facs="A83445-001-a-1060">be</w>
          <c> </c>
          <w lemma="take" ana="#vvn" reg="taken" xml:id="A83445-1250" facs="A83445-001-a-1070">taken</w>
          <c> </c>
          <w lemma="down" ana="#acp-av" reg="down" xml:id="A83445-1260" facs="A83445-001-a-1080">down</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1270" facs="A83445-001-a-1090">and</w>
          <c> </c>
          <w lemma="deface" ana="#vvn" reg="defaced" xml:id="A83445-1280" facs="A83445-001-a-1100">defaced</w>
          <pc xml:id="A83445-1290" facs="A83445-001-a-1110">,</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83445-1300" facs="A83445-001-a-1120">in</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-1310" facs="A83445-001-a-1130">all</w>
          <c> </c>
          <w lemma="church" ana="#n2" reg="churches" xml:id="A83445-1320" facs="A83445-001-a-1140">Churches</w>
          <pc xml:id="A83445-1330" facs="A83445-001-a-1150">,</pc>
          <c> </c>
          <w lemma="chapel" ana="#n2" reg="chapels" xml:id="A83445-1340" facs="A83445-001-a-1160">Chappels</w>
          <pc xml:id="A83445-1350" facs="A83445-001-a-1170">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1360" facs="A83445-001-a-1180">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A83445-1370" facs="A83445-001-a-1190">all</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A83445-1380" facs="A83445-001-a-1200">other</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A83445-1390" facs="A83445-001-a-1210">publique</w>
          <c> </c>
          <w lemma="place" ana="#n2" reg="places" xml:id="A83445-1400" facs="A83445-001-a-1220">places</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A83445-1410" facs="A83445-001-a-1230">within</w>
          <c> </c>
          <hi xml:id="A83445e-190">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A83445-1420" facs="A83445-001-a-1240">England</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1430" facs="A83445-001-a-1250">and</w>
          <c> </c>
          <hi xml:id="A83445e-200">
            <w lemma="wales" ana="#n1-nn" reg="Wales" xml:id="A83445-1440" facs="A83445-001-a-1260">Wales</w>
            <pc xml:id="A83445-1450" facs="A83445-001-a-1270">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1460" facs="A83445-001-a-1280">and</w>
          <c> </c>
          <w lemma="town" ana="#n1" reg="town" xml:id="A83445-1470" facs="A83445-001-a-1290">Town</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-1480" facs="A83445-001-a-1300">of</w>
          <c> </c>
          <hi xml:id="A83445e-210">
            <w lemma="Berwick" ana="#n1-nn" reg="Berwick" xml:id="A83445-1490" facs="A83445-001-a-1310">Berwick</w>
            <pc unit="sentence" xml:id="A83445-1500" facs="A83445-001-a-1320">.</pc>
            <c> </c>
          </hi>
        </p>
        <div type="license" xml:id="A83445e-220">
          <p xml:id="A83445e-230">
            <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A83445-1540" facs="A83445-001-a-1330">ORdered</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A83445-1550" facs="A83445-001-a-1340">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A83445-1560" facs="A83445-001-a-1350">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83445-1570" facs="A83445-001-a-1360">Parliament</w>
            <pc xml:id="A83445-1580" facs="A83445-001-a-1370">,</pc>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A83445-1590" facs="A83445-001-a-1380">That</w>
            <c> </c>
            <w lemma="these" ana="#d" reg="these" xml:id="A83445-1600" facs="A83445-001-a-1390">these</w>
            <c> </c>
            <w lemma="vote" ana="#n2" reg="votes" xml:id="A83445-1610" facs="A83445-001-a-1400">Votes</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="be" xml:id="A83445-1620" facs="A83445-001-a-1410">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A83445-1630" facs="A83445-001-a-1420">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A83445-1640" facs="A83445-001-a-1430">printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1650" facs="A83445-001-a-1440">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A83445-1660" facs="A83445-001-a-1450">published</w>
            <pc unit="sentence" xml:id="A83445-1670" facs="A83445-001-a-1460">.</pc>
          </p>
          <closer xml:id="A83445e-240">
            <signed xml:id="A83445e-250">
              <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A83445-1710" facs="A83445-001-a-1470">Hen</w>
              <pc xml:id="A83445-1720" facs="A83445-001-a-1480">:</pc>
              <c> </c>
              <w lemma="Scobell" ana="#n1-nn" reg="Scobell" xml:id="A83445-1730" facs="A83445-001-a-1490">Scobell</w>
              <pc xml:id="A83445-1740" facs="A83445-001-a-1500">,</pc>
              <c> </c>
              <w lemma="cleric" ana="#j" reg="cleric" xml:id="A83445-1750" facs="A83445-001-a-1510">Cleric</w>
              <pc unit="sentence" xml:id="A83445-1760" facs="A83445-001-a-1520">.</pc>
              <c> </c>
              <w lemma="parliamenti" ana="#fw-la" reg="Parliamenti" xml:id="A83445-1770" facs="A83445-001-a-1530">Parliamenti</w>
              <pc unit="sentence" xml:id="A83445-1780" facs="A83445-001-a-1540">.</pc>
            </signed>
          </closer>
        </div>
      </div>
    </body>
    <back xml:id="A83445e-260">
      <div type="colophon" xml:id="A83445e-270">
        <p xml:id="A83445e-280">
          <hi xml:id="A83445e-290">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A83445-1830" facs="A83445-001-a-1550">London</w>
            <pc xml:id="A83445-1840" facs="A83445-001-a-1560">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83445-1850" facs="A83445-001-a-1570">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83445-1860" facs="A83445-001-a-1580">by</w>
          <c> </c>
          <hi xml:id="A83445e-300">
            <w lemma="Edward" ana="#n1-nn" reg="Edward" xml:id="A83445-1870" facs="A83445-001-a-1590">Edward</w>
            <c> </c>
            <w lemma="husband" ana="#n1" reg="husband" xml:id="A83445-1880" facs="A83445-001-a-1600">Husband</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83445-1890" facs="A83445-001-a-1610">and</w>
          <c> </c>
          <hi xml:id="A83445e-310">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A83445-1900" facs="A83445-001-a-1620">John</w>
            <c> </c>
            <w lemma="field" ana="#n1" reg="field" xml:id="A83445-1910" facs="A83445-001-a-1630">Field</w>
            <pc xml:id="A83445-1920" facs="A83445-001-a-1640">,</pc>
          </hi>
          <c> </c>
          <w lemma="printer" ana="#n2" reg="printers" xml:id="A83445-1930" facs="A83445-001-a-1650">Printers</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83445-1940" facs="A83445-001-a-1660">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83445-1950" facs="A83445-001-a-1670">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83445-1960" facs="A83445-001-a-1680">Parliament</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83445-1970" facs="A83445-001-a-1690">of</w>
          <c> </c>
          <hi xml:id="A83445e-320">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A83445-1980" facs="A83445-001-a-1700">England</w>
            <pc xml:id="A83445-1990" facs="A83445-001-a-1710">,</pc>
          </hi>
          <c> </c>
          <w lemma="1650." ana="#crd" reg="1650." xml:id="A83445-2000" facs="A83445-001-a-1720">1650.</w>
          <pc unit="sentence" xml:id="A83445-2000-eos" facs="A83445-001-a-1730"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>