<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Sabbatti. September, 24. 1642. Whereas, this kingdome and Common wealth hath beene put to a great and vast charge by delinquents ...</title>
        <author>England and Wales. Parliament. House of Commons.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A83879</idno>
        <idno type="STC">Wing E2785A</idno>
        <idno type="STC">Thomason 669.f.5[80]</idno>
        <idno type="STC">ESTC R29787</idno>
        <idno type="EEBO-CITATION">99872242</idno>
        <idno type="PROQUEST">99872242</idno>
        <idno type="VID">160793</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A83879)</note>
        <note>Transcribed from: (Early English Books Online ; image set 160793)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[80])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Sabbatti. September, 24. 1642. Whereas, this kingdome and Common wealth hath beene put to a great and vast charge by delinquents ...</title>
            <author>England and Wales. Parliament. House of Commons.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>by L.N. for E. Husbands and Iohn Frank, and are to bee sold at their shops in the Middle Temple, and next dore to the Kings Haed [sic] in Fleetstreete,</publisher>
            <pubPlace>Imprinted at London :</pubPlace>
            <date>MDCXLII [1642]</date>
          </publicationStmt>
          <notesStmt>
            <note>Orders that the houses of delinquents and other ill-affected persons shall be preserved for the use and profit of the commonwealth--cf. NUC pre-1956 imprints.</note>
            <note>Reproductions of the originals in the Harvard University Library and the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Repossession -- Great Britain -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Confiscations and contributions -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-09</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-11</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-12</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A83879e-10">
    <body xml:id="A83879e-20">
      <div type="document" xml:id="A83879e-30">
        <pb facs="tcp:160793:1" rendition="simple:additions" xml:id="A83879-001-a"/>
        <head xml:id="A83879e-40">
          <w lemma="die" ana="#vvb" reg="Die" xml:id="A83879-0030" facs="A83879-001-a-0010">DIE</w>
          <c> </c>
          <w lemma="SABBATTI" ana="#n1-nn" reg="Sabbatti" xml:id="A83879-0040" facs="A83879-001-a-0020">SABBATTI</w>
          <pc unit="sentence" xml:id="A83879-0050" facs="A83879-001-a-0030">.</pc>
          <c> </c>
          <date xml:id="A83879e-50">
            <w lemma="September" ana="#n1-nn" reg="September" xml:id="A83879-0060" facs="A83879-001-a-0040">SEPTEMBER</w>
            <pc xml:id="A83879-0070" facs="A83879-001-a-0050">,</pc>
            <c> </c>
            <w lemma="24." ana="#crd" reg="24." xml:id="A83879-0080" facs="A83879-001-a-0060">24.</w>
            <c> </c>
            <w lemma="1642." ana="#crd" reg="1642." xml:id="A83879-0090" facs="A83879-001-a-0070">1642.</w>
            <pc unit="sentence" xml:id="A83879-0090-eos" facs="A83879-001-a-0080"/>
          </date>
        </head>
        <p xml:id="A83879e-60">
          <w lemma="whereas" ana="#cs" reg="Whereas" rend="initialcharacterdecorated" xml:id="A83879-0120" facs="A83879-001-a-0090">WHereas</w>
          <pc xml:id="A83879-0130" facs="A83879-001-a-0100">,</pc>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83879-0140" facs="A83879-001-a-0110">this</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A83879-0150" facs="A83879-001-a-0120">Kingdome</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-0160" facs="A83879-001-a-0130">and</w>
          <c> </c>
          <w lemma="common" ana="#j" reg="common" xml:id="A83879-0170" facs="A83879-001-a-0140">Common</w>
          <c> </c>
          <w lemma="wealth" ana="#n1" reg="wealth" xml:id="A83879-0180" facs="A83879-001-a-0150">wealth</w>
          <c> </c>
          <w lemma="have" ana="#vvz" reg="hath" xml:id="A83879-0190" facs="A83879-001-a-0160">hath</w>
          <c> </c>
          <w lemma="be" ana="#vvn" reg="been" xml:id="A83879-0200" facs="A83879-001-a-0170">beene</w>
          <c> </c>
          <w lemma="put" ana="#vvn" reg="put" xml:id="A83879-0210" facs="A83879-001-a-0180">put</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83879-0220" facs="A83879-001-a-0190">to</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83879-0230" facs="A83879-001-a-0200">a</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A83879-0240" facs="A83879-001-a-0210">great</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-0250" facs="A83879-001-a-0220">and</w>
          <c> </c>
          <w lemma="vast" ana="#j" reg="vast" xml:id="A83879-0260" facs="A83879-001-a-0230">vast</w>
          <c> </c>
          <w lemma="charge" ana="#n1" reg="charge" xml:id="A83879-0270" facs="A83879-001-a-0240">Charge</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83879-0280" facs="A83879-001-a-0250">by</w>
          <c> </c>
          <w lemma="delinquent" ana="#n2_j" reg="delinquents" xml:id="A83879-0290" facs="A83879-001-a-0260">Delinquents</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-0300" facs="A83879-001-a-0270">and</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="an" xml:id="A83879-0310" facs="A83879-001-a-0280">an</w>
          <c> </c>
          <w lemma="ill" ana="#j" reg="ill" xml:id="A83879-0320" facs="A83879-001-a-0290">ill</w>
          <c> </c>
          <w lemma="affect" ana="#j_vn" reg="affected" xml:id="A83879-0330" facs="A83879-001-a-0300">affected</w>
          <c> </c>
          <w lemma="party" ana="#n1" reg="party" xml:id="A83879-0340" facs="A83879-001-a-0310">Partie</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A83879-0350" facs="A83879-001-a-0320">which</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A83879-0360" facs="A83879-001-a-0330">if</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83879-0370" facs="A83879-001-a-0340">it</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83879-0380" facs="A83879-001-a-0350">be</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83879-0390" facs="A83879-001-a-0360">not</w>
          <c> </c>
          <w lemma="discharge" ana="#vvn" reg="discharged" xml:id="A83879-0400" facs="A83879-001-a-0370">discharged</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83879-0410" facs="A83879-001-a-0380">by</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A83879-0420" facs="A83879-001-a-0390">them</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-0430" facs="A83879-001-a-0400">and</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83879-0440" facs="A83879-001-a-0410">their</w>
          <c> </c>
          <w lemma="estate" ana="#n2" reg="estates" xml:id="A83879-0450" facs="A83879-001-a-0420">Estates</w>
          <pc xml:id="A83879-0460" facs="A83879-001-a-0430">,</pc>
          <c> </c>
          <w lemma="must" ana="#vmb" reg="must" xml:id="A83879-0470" facs="A83879-001-a-0440">must</w>
          <c> </c>
          <w lemma="necessary" ana="#av_j" reg="necessarily" xml:id="A83879-0480" facs="A83879-001-a-0450">necessarily</w>
          <c> </c>
          <w lemma="lie" ana="#vvi" reg="lie" xml:id="A83879-0490" facs="A83879-001-a-0460">lye</w>
          <c> </c>
          <w lemma="as" ana="#acp-p" reg="as" xml:id="A83879-0500" facs="A83879-001-a-0470">as</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A83879-0510" facs="A83879-001-a-0480">a</w>
          <c> </c>
          <w lemma="burden" ana="#n1" reg="burden" xml:id="A83879-0520" facs="A83879-001-a-0490">Burthen</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A83879-0530" facs="A83879-001-a-0500">upon</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-0540" facs="A83879-001-a-0510">the</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A83879-0550" facs="A83879-001-a-0520">good</w>
          <c> </c>
          <w lemma="subject" ana="#n2" reg="subjects" xml:id="A83879-0560" facs="A83879-001-a-0530">Subjects</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83879-0570" facs="A83879-001-a-0540">that</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A83879-0580" facs="A83879-001-a-0550">have</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A83879-0590" facs="A83879-001-a-0560">no</w>
          <c> </c>
          <w lemma="way" ana="#n1" reg="way" xml:id="A83879-0600" facs="A83879-001-a-0570">way</w>
          <c> </c>
          <w lemma="deserve" ana="#vvd" reg="deserved" xml:id="A83879-0610" facs="A83879-001-a-0580">deserved</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83879-0620" facs="A83879-001-a-0590">it</w>
          <pc unit="sentence" xml:id="A83879-0630" facs="A83879-001-a-0600">.</pc>
          <c> </c>
          <w lemma="it" ana="#pn" reg="It" xml:id="A83879-0640" facs="A83879-001-a-0610">It</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A83879-0650" facs="A83879-001-a-0620">is</w>
          <c> </c>
          <w lemma="therefore" ana="#av" reg="therefore" xml:id="A83879-0660" facs="A83879-001-a-0630">therefore</w>
          <c> </c>
          <w lemma="think" ana="#vvn" reg="thought" xml:id="A83879-0670" facs="A83879-001-a-0640">thought</w>
          <c> </c>
          <w lemma="fit" ana="#j" reg="fit" xml:id="A83879-0680" facs="A83879-001-a-0650">fit</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-0690" facs="A83879-001-a-0660">and</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A83879-0700" facs="A83879-001-a-0670">ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83879-0710" facs="A83879-001-a-0680">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-0720" facs="A83879-001-a-0690">the</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A83879-0730" facs="A83879-001-a-0700">Commons</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A83879-0740" facs="A83879-001-a-0710">assembled</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83879-0750" facs="A83879-001-a-0720">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A83879-0760" facs="A83879-001-a-0730">PARLIAMENT</w>
          <pc xml:id="A83879-0770" facs="A83879-001-a-0740">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83879-0780" facs="A83879-001-a-0750">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-0790" facs="A83879-001-a-0760">the</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A83879-0800" facs="A83879-001-a-0770">Houses</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83879-0810" facs="A83879-001-a-0780">of</w>
          <c> </c>
          <w lemma="delinquent" ana="#n2_j" reg="delinquents" xml:id="A83879-0820" facs="A83879-001-a-0790">Delinquents</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83879-0830" facs="A83879-001-a-0800">or</w>
          <c> </c>
          <w lemma="ill" ana="#j" reg="ill" xml:id="A83879-0840" facs="A83879-001-a-0810">ill</w>
          <c> </c>
          <w lemma="affect" ana="#j_vn" reg="affected" xml:id="A83879-0850" facs="A83879-001-a-0820">affected</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A83879-0860" facs="A83879-001-a-0830">Persons</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A83879-0870" facs="A83879-001-a-0840">shall</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A83879-0880" facs="A83879-001-a-0850">not</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83879-0890" facs="A83879-001-a-0860">be</w>
          <c> </c>
          <w lemma="plunder" ana="#vvn" reg="plundered" xml:id="A83879-0900" facs="A83879-001-a-0870">plunderd</w>
          <pc xml:id="A83879-0910" facs="A83879-001-a-0880">,</pc>
          <c> </c>
          <w lemma="pull" ana="#vvd" reg="pulled" xml:id="A83879-0920" facs="A83879-001-a-0890">pulled</w>
          <c> </c>
          <w lemma="down" ana="#acp-av" reg="down" xml:id="A83879-0930" facs="A83879-001-a-0900">downe</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A83879-0940" facs="A83879-001-a-0910">or</w>
          <c> </c>
          <w lemma="destroy" ana="#vvn" reg="destroyed" xml:id="A83879-0950" facs="A83879-001-a-0920">destroyed</w>
          <pc xml:id="A83879-0960" facs="A83879-001-a-0930">;</pc>
          <c> </c>
          <w lemma="but" ana="#acp-cc" reg="but" xml:id="A83879-0970" facs="A83879-001-a-0940">but</w>
          <c> </c>
          <w lemma="reserve" ana="#vvn" reg="reserved" xml:id="A83879-0980" facs="A83879-001-a-0950">reserved</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83879-0990" facs="A83879-001-a-0960">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1000" facs="A83879-001-a-0970">the</w>
          <c> </c>
          <w lemma="benefit" ana="#n1" reg="benefit" xml:id="A83879-1010" facs="A83879-001-a-0980">benefit</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1020" facs="A83879-001-a-0990">and</w>
          <c> </c>
          <w lemma="advantage" ana="#n1" reg="advantage" xml:id="A83879-1030" facs="A83879-001-a-1000">advantage</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83879-1040" facs="A83879-001-a-1010">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1050" facs="A83879-001-a-1020">the</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A83879-1060" facs="A83879-001-a-1030">Common-wealth</w>
          <pc xml:id="A83879-1070" facs="A83879-001-a-1040">,</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83879-1080" facs="A83879-001-a-1050">they</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A83879-1090" facs="A83879-001-a-1060">being</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A83879-1100" facs="A83879-001-a-1070">now</w>
          <c> </c>
          <w lemma="consider" ana="#vvn" reg="considered" xml:id="A83879-1110" facs="A83879-001-a-1080">considered</w>
          <c> </c>
          <w lemma="rather" ana="#av-c" reg="rather" xml:id="A83879-1120" facs="A83879-001-a-1090">rather</w>
          <c> </c>
          <w lemma="as" ana="#acp-p" reg="as" xml:id="A83879-1130" facs="A83879-001-a-1100">as</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1140" facs="A83879-001-a-1110">the</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A83879-1150" facs="A83879-001-a-1120">Houses</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83879-1160" facs="A83879-001-a-1130">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1170" facs="A83879-001-a-1140">the</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A83879-1180" facs="A83879-001-a-1150">Common-wealth</w>
          <c> </c>
          <w lemma="then" ana="#av" reg="then" xml:id="A83879-1190" facs="A83879-001-a-1160">then</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83879-1200" facs="A83879-001-a-1170">of</w>
          <c> </c>
          <w lemma="delinquent" ana="#n2_j" reg="delinquents" xml:id="A83879-1210" facs="A83879-001-a-1180">Delinquents</w>
          <pc xml:id="A83879-1220" facs="A83879-001-a-1190">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1230" facs="A83879-001-a-1200">and</w>
          <c> </c>
          <w lemma="accord" ana="#av_vg" reg="accordingly" xml:id="A83879-1240" facs="A83879-001-a-1210">accordingly</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83879-1250" facs="A83879-001-a-1220">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83879-1260" facs="A83879-001-a-1230">be</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A83879-1270" facs="A83879-001-a-1240">so</w>
          <c> </c>
          <w lemma="preserve" ana="#vvn" reg="preserved" xml:id="A83879-1280" facs="A83879-001-a-1250">preserved</w>
          <pc xml:id="A83879-1290" facs="A83879-001-a-1260">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A83879-1300" facs="A83879-001-a-1270">as</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A83879-1310" facs="A83879-001-a-1280">they</w>
          <c> </c>
          <w lemma="yield" ana="#vvb" reg="yield" xml:id="A83879-1320" facs="A83879-001-a-1290">yeeld</w>
          <c> </c>
          <w lemma="more" ana="#d-c" reg="more" xml:id="A83879-1330" facs="A83879-001-a-1300">more</w>
          <c> </c>
          <w lemma="profit" ana="#n1" reg="profit" xml:id="A83879-1340" facs="A83879-001-a-1310">profit</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1350" facs="A83879-001-a-1320">and</w>
          <c> </c>
          <w lemma="advantage" ana="#n1" reg="advantage" xml:id="A83879-1360" facs="A83879-001-a-1330">advantage</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83879-1370" facs="A83879-001-a-1340">to</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A83879-1380" facs="A83879-001-a-1350">it</w>
          <pc unit="sentence" xml:id="A83879-1390" facs="A83879-001-a-1360">.</pc>
        </p>
      </div>
      <div type="license" xml:id="A83879e-70">
        <p xml:id="A83879e-80">
          <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A83879-1430" facs="A83879-001-a-1370">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83879-1440" facs="A83879-001-a-1380">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1450" facs="A83879-001-a-1390">the</w>
          <c> </c>
          <hi xml:id="A83879e-90">
            <w lemma="house" ana="#n1" reg="house" xml:id="A83879-1460" facs="A83879-001-a-1400">House</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A83879-1470" facs="A83879-001-a-1410">of</w>
          <c> </c>
          <hi xml:id="A83879e-100">
            <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A83879-1480" facs="A83879-001-a-1420">Commons</w>
          </hi>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A83879-1490" facs="A83879-001-a-1430">that</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A83879-1500" facs="A83879-001-a-1440">this</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A83879-1510" facs="A83879-001-a-1450">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A83879-1520" facs="A83879-001-a-1460">forthwith</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A83879-1530" facs="A83879-001-a-1470">printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1540" facs="A83879-001-a-1480">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A83879-1550" facs="A83879-001-a-1490">published</w>
          <pc unit="sentence" xml:id="A83879-1560" facs="A83879-001-a-1500">.</pc>
        </p>
        <closer xml:id="A83879e-110">
          <signed xml:id="A83879e-120">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A83879-1600" facs="A83879-001-a-1510">Hen</w>
            <pc xml:id="A83879-1610" facs="A83879-001-a-1520">:</pc>
            <c> </c>
            <w lemma="Elsinge" ana="#n1-nn" reg="Elsinge" xml:id="A83879-1620" facs="A83879-001-a-1530">Elsinge</w>
            <pc xml:id="A83879-1630" facs="A83879-001-a-1540">,</pc>
            <c> </c>
            <w lemma="cleric" ana="#j" reg="cleric" xml:id="A83879-1640" facs="A83879-001-a-1550">Cleric</w>
            <pc unit="sentence" xml:id="A83879-1650" facs="A83879-001-a-1560">.</pc>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="Parliament" xml:id="A83879-1660" facs="A83879-001-a-1570">Parliament</w>
            <pc xml:id="A83879-1670" facs="A83879-001-a-1580">.</pc>
            <c> </c>
            <w lemma="d." ana="#n-ab" reg="d." xml:id="A83879-1680" facs="A83879-001-a-1590">D.</w>
            <c> </c>
            <w lemma="com." ana="#n-ab" reg="com." xml:id="A83879-1690" facs="A83879-001-a-1600">Com.</w>
            <pc unit="sentence" xml:id="A83879-1690-eos" facs="A83879-001-a-1610"/>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A83879e-130">
      <div type="colophon" xml:id="A83879e-140">
        <p xml:id="A83879e-150">
          <w lemma="imprint" ana="#vvn" reg="Imprinted" xml:id="A83879-1740" facs="A83879-001-a-1620">Imprinted</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83879-1750" facs="A83879-001-a-1630">at</w>
          <c> </c>
          <w lemma="LONDON" ana="#n1-nn" reg="London" xml:id="A83879-1760" facs="A83879-001-a-1640">LONDON</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A83879-1770" facs="A83879-001-a-1650">by</w>
          <c> </c>
          <hi xml:id="A83879e-160">
            <w lemma="l." ana="#n-ab" reg="l." xml:id="A83879-1780" facs="A83879-001-a-1660">L.</w>
            <c> </c>
            <w lemma="n." ana="#n-ab" reg="n." xml:id="A83879-1790" facs="A83879-001-a-1670">N.</w>
          </hi>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A83879-1800" facs="A83879-001-a-1680">for</w>
          <c> </c>
          <hi xml:id="A83879e-170">
            <w lemma="e." ana="#n-ab" reg="e." xml:id="A83879-1810" facs="A83879-001-a-1690">E.</w>
            <c> </c>
            <w lemma="husband" ana="#n2" reg="husbands" xml:id="A83879-1820" facs="A83879-001-a-1700">Husbands</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1830" facs="A83879-001-a-1710">and</w>
          <c> </c>
          <hi xml:id="A83879e-180">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A83879-1840" facs="A83879-001-a-1720">Iohn</w>
            <c> </c>
            <w lemma="Frank" ana="#n1-nn" reg="Frank" xml:id="A83879-1850" facs="A83879-001-a-1730">Frank</w>
            <pc xml:id="A83879-1860" facs="A83879-001-a-1740">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-1870" facs="A83879-001-a-1750">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A83879-1880" facs="A83879-001-a-1760">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A83879-1890" facs="A83879-001-a-1770">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A83879-1900" facs="A83879-001-a-1780">bee</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A83879-1910" facs="A83879-001-a-1790">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A83879-1920" facs="A83879-001-a-1800">at</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A83879-1930" facs="A83879-001-a-1810">their</w>
          <c> </c>
          <w lemma="shop" ana="#n2" reg="shops" xml:id="A83879-1940" facs="A83879-001-a-1820">Shops</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83879-1950" facs="A83879-001-a-1830">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-1960" facs="A83879-001-a-1840">the</w>
          <c> </c>
          <w lemma="middle" ana="#j" reg="middle" xml:id="A83879-1970" facs="A83879-001-a-1850">Middle</w>
          <c> </c>
          <w lemma="temple" ana="#n1" reg="temple" xml:id="A83879-1980" facs="A83879-001-a-1860">Temple</w>
          <pc xml:id="A83879-1990" facs="A83879-001-a-1870">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A83879-2000" facs="A83879-001-a-1880">and</w>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A83879-2010" facs="A83879-001-a-1890">next</w>
          <c> </c>
          <w lemma="door" ana="#n1" reg="door" xml:id="A83879-2020" facs="A83879-001-a-1900">dore</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A83879-2030" facs="A83879-001-a-1910">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A83879-2040" facs="A83879-001-a-1920">the</w>
          <c> </c>
          <w lemma="king" ana="#n1g" reg="King's" xml:id="A83879-2050" facs="A83879-001-a-1930">Kings</w>
          <c> </c>
          <w lemma="head" ana="#n1" reg="head" xml:id="A83879-2060" facs="A83879-001-a-1940">Head</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A83879-2070" facs="A83879-001-a-1950">in</w>
          <c> </c>
          <w lemma="Fleetstreet" ana="#n1-nn" reg="Fleetstreet" xml:id="A83879-2080" facs="A83879-001-a-1960">Fleetstreete</w>
          <pc unit="sentence" xml:id="A83879-2090" facs="A83879-001-a-1970">.</pc>
          <c> </c>
          <w lemma="mdcxlii" ana="#crd" reg="Mdcxlii" xml:id="A83879-2100" facs="A83879-001-a-1980">MDCXLII</w>
          <pc unit="sentence" xml:id="A83879-2110" facs="A83879-001-a-1990">.</pc>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>