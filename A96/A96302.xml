<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Whereas a printed paper was lately put forth in the names of some of the trustees for ministers maintenance wherein amongst other things they take upon them without warrant to discharge all incumbents from whom any first-fruits are due, from paying the same unto Mr. Thomas Baker at the First-fruits Office in the Strand in the county of Middlesex, ...</title>
        <author>Baker, Thomas, of the First-fruits office.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1655</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A96302</idno>
        <idno type="STC">Wing W1611</idno>
        <idno type="STC">Thomason 669.f.20[18]</idno>
        <idno type="STC">ESTC R211671</idno>
        <idno type="EEBO-CITATION">99870377</idno>
        <idno type="PROQUEST">99870377</idno>
        <idno type="VID">163435</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A96302)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163435)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f20[18])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Whereas a printed paper was lately put forth in the names of some of the trustees for ministers maintenance wherein amongst other things they take upon them without warrant to discharge all incumbents from whom any first-fruits are due, from paying the same unto Mr. Thomas Baker at the First-fruits Office in the Strand in the county of Middlesex, ...</title>
            <author>Baker, Thomas, of the First-fruits office.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1655]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from opening lines of text.</note>
            <note>Imprint from Wing.</note>
            <note>Dated and signed at end: Dated at the First-fruits Office aforesaid, the eighteenth day of November 1655. Thomas Baker.</note>
            <note>Annotation on Thomason copy: "A pay from ye first-fruits office [illegible] paying ye first fruits".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Taxation -- England -- Middlesex -- Early works to 1800.</term>
          <term>Middlesex (England) -- History -- 17th century -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-06</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-07</date><label>Pip Willcox</label>
        Sampled and proofread
      </change>
      <change><date>2007-07</date><label>Pip Willcox</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A96302e-10">
    <body xml:id="A96302e-20">
      <div type="text" xml:id="A96302e-30">
        <pb facs="tcp:163435:1" rendition="simple:additions" xml:id="A96302-001-a"/>
        <p xml:id="A96302e-40">
          <w lemma="whereas" ana="#cs" reg="Whereas" rend="initialcharacterdecorated" xml:id="A96302-0030" facs="A96302-001-a-0010">WHereas</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A96302-0040" facs="A96302-001-a-0020">a</w>
          <c> </c>
          <w lemma="print" ana="#j_vn" reg="printed" xml:id="A96302-0050" facs="A96302-001-a-0030">printed</w>
          <c> </c>
          <w lemma="paper" ana="#n1" reg="paper" xml:id="A96302-0060" facs="A96302-001-a-0040">paper</w>
          <c> </c>
          <w lemma="be" ana="#vvd" reg="was" xml:id="A96302-0070" facs="A96302-001-a-0050">was</w>
          <c> </c>
          <w lemma="late" ana="#av_j" reg="lately" xml:id="A96302-0080" facs="A96302-001-a-0060">lately</w>
          <c> </c>
          <w lemma="put" ana="#vvn" reg="put" xml:id="A96302-0090" facs="A96302-001-a-0070">put</w>
          <c> </c>
          <w lemma="forth" ana="#av" reg="forth" xml:id="A96302-0100" facs="A96302-001-a-0080">forth</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A96302-0110" facs="A96302-001-a-0090">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0120" facs="A96302-001-a-0100">the</w>
          <c> </c>
          <w lemma="name" ana="#n2" reg="names" xml:id="A96302-0130" facs="A96302-001-a-0110">names</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-0140" facs="A96302-001-a-0120">of</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A96302-0150" facs="A96302-001-a-0130">some</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-0160" facs="A96302-001-a-0140">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0170" facs="A96302-001-a-0150">the</w>
          <c> </c>
          <w lemma="trustee" ana="#n2" reg="trustees" xml:id="A96302-0180" facs="A96302-001-a-0160">Trustees</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A96302-0190" facs="A96302-001-a-0170">for</w>
          <c> </c>
          <w lemma="minister" ana="#n2" reg="ministers" xml:id="A96302-0200" facs="A96302-001-a-0180">Ministers</w>
          <c> </c>
          <w lemma="maintenance" ana="#n1" reg="maintenance" xml:id="A96302-0210" facs="A96302-001-a-0190">maintenance</w>
          <pc xml:id="A96302-0220" facs="A96302-001-a-0200">,</pc>
          <c> </c>
          <w lemma="wherein" ana="#crq-cs" reg="wherein" xml:id="A96302-0230" facs="A96302-001-a-0210">wherein</w>
          <c> </c>
          <w lemma="among" ana="#acp-p" reg="amongst" xml:id="A96302-0240" facs="A96302-001-a-0220">amongst</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A96302-0250" facs="A96302-001-a-0230">other</w>
          <c> </c>
          <w lemma="thing" ana="#n2" reg="things" xml:id="A96302-0260" facs="A96302-001-a-0240">things</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A96302-0270" facs="A96302-001-a-0250">they</w>
          <c> </c>
          <w lemma="take" ana="#vvb" reg="take" xml:id="A96302-0280" facs="A96302-001-a-0260">take</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A96302-0290" facs="A96302-001-a-0270">upon</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A96302-0300" facs="A96302-001-a-0280">them</w>
          <c> </c>
          <w lemma="without" ana="#acp-p" reg="without" xml:id="A96302-0310" facs="A96302-001-a-0290">without</w>
          <c> </c>
          <w lemma="warrant" ana="#n1" reg="warrant" xml:id="A96302-0320" facs="A96302-001-a-0300">warrant</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A96302-0330" facs="A96302-001-a-0310">to</w>
          <c> </c>
          <w lemma="discharge" ana="#vvi" reg="discharge" xml:id="A96302-0340" facs="A96302-001-a-0320">discharge</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A96302-0350" facs="A96302-001-a-0330">all</w>
          <c> </c>
          <w lemma="incumbent" ana="#n2" reg="incumbents" xml:id="A96302-0360" facs="A96302-001-a-0340">Incumbents</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A96302-0370" facs="A96302-001-a-0350">from</w>
          <c> </c>
          <w lemma="who" ana="#crq-ro" reg="whom" xml:id="A96302-0380" facs="A96302-001-a-0360">whom</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A96302-0390" facs="A96302-001-a-0370">any</w>
          <c> </c>
          <w lemma="first-fruit" ana="#n2" reg="first-fruits" xml:id="A96302-0400" facs="A96302-001-a-0380">First-fruits</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A96302-0410" facs="A96302-001-a-0390">are</w>
          <c> </c>
          <w lemma="due" ana="#j" reg="due" xml:id="A96302-0420" facs="A96302-001-a-0400">due</w>
          <pc xml:id="A96302-0430" facs="A96302-001-a-0410">,</pc>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A96302-0440" facs="A96302-001-a-0420">from</w>
          <c> </c>
          <w lemma="pay" ana="#vvg" reg="paying" xml:id="A96302-0450" facs="A96302-001-a-0430">paying</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0460" facs="A96302-001-a-0440">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A96302-0470" facs="A96302-001-a-0450">same</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A96302-0480" facs="A96302-001-a-0460">unto</w>
          <c> </c>
          <w lemma="m" part="I" ana="#sy" reg="M" xml:id="A96302-0490.1" facs="A96302-001-a-0470">Mr.</w>
          <pc unit="sentence" xml:id="A96302-0490.1-eos" facs="A96302-001-a-0480"/>
          <hi rend="sup" xml:id="A96302e-50">
            <pc unit="sentence" xml:id="A96302-0490.2-eos" facs="A96302-001-a-0500"/>
            <c> </c>
          </hi>
          <hi xml:id="A96302e-60">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A96302-0500" facs="A96302-001-a-0510">Thomas</w>
            <c> </c>
            <w lemma="baker" ana="#n1" reg="baker" xml:id="A96302-0510" facs="A96302-001-a-0520">Baker</w>
          </hi>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A96302-0520" facs="A96302-001-a-0530">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0530" facs="A96302-001-a-0540">the</w>
          <c> </c>
          <w lemma="first-fruit" ana="#n2" reg="first-fruits" xml:id="A96302-0540" facs="A96302-001-a-0550">First-fruits</w>
          <c> </c>
          <w lemma="office" ana="#n1" reg="office" xml:id="A96302-0550" facs="A96302-001-a-0560">Office</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A96302-0560" facs="A96302-001-a-0570">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0570" facs="A96302-001-a-0580">the</w>
          <c> </c>
          <hi xml:id="A96302e-70">
            <w lemma="strand" ana="#n1" reg="strand" xml:id="A96302-0580" facs="A96302-001-a-0590">Strand</w>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A96302-0590" facs="A96302-001-a-0600">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0600" facs="A96302-001-a-0610">the</w>
          <c> </c>
          <w lemma="county" ana="#n1" reg="county" xml:id="A96302-0610" facs="A96302-001-a-0620">County</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-0620" facs="A96302-001-a-0630">of</w>
          <c> </c>
          <hi xml:id="A96302e-80">
            <w lemma="Middlesex" ana="#n1-nn" reg="Middlesex" xml:id="A96302-0630" facs="A96302-001-a-0640">Middlesex</w>
            <pc xml:id="A96302-0640" facs="A96302-001-a-0650">,</pc>
          </hi>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A96302-0650" facs="A96302-001-a-0660">These</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A96302-0660" facs="A96302-001-a-0670">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A96302-0670" facs="A96302-001-a-0680">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A96302-0680" facs="A96302-001-a-0690">give</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A96302-0690" facs="A96302-001-a-0700">notice</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A96302-0700" facs="A96302-001-a-0710">to</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A96302-0710" facs="A96302-001-a-0720">all</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A96302-0720" facs="A96302-001-a-0730">persons</w>
          <c> </c>
          <w lemma="concern" ana="#vvd" reg="concerned" xml:id="A96302-0730" facs="A96302-001-a-0740">concernd</w>
          <pc xml:id="A96302-0740" facs="A96302-001-a-0750">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A96302-0750" facs="A96302-001-a-0760">that</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A96302-0760" facs="A96302-001-a-0770">they</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A96302-0770" facs="A96302-001-a-0780">are</w>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-av" reg="notwithstanding" xml:id="A96302-0780" facs="A96302-001-a-0790">notwithstanding</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A96302-0790" facs="A96302-001-a-0800">to</w>
          <c> </c>
          <w lemma="apply" ana="#vvi" reg="apply" xml:id="A96302-0800" facs="A96302-001-a-0810">apply</w>
          <c> </c>
          <w lemma="themselves" ana="#px" reg="themselves" xml:id="A96302-0810" facs="A96302-001-a-0820">themselves</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A96302-0820" facs="A96302-001-a-0830">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-0830" facs="A96302-001-a-0840">the</w>
          <c> </c>
          <w lemma="accustom" ana="#j_vn" reg="accustomed" xml:id="A96302-0840" facs="A96302-001-a-0850">accustomed</w>
          <c> </c>
          <w lemma="place" ana="#n1" reg="place" xml:id="A96302-0850" facs="A96302-001-a-0860">place</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A96302-0860" facs="A96302-001-a-0870">and</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A96302-0870" facs="A96302-001-a-0880">persons</w>
          <pc xml:id="A96302-0880" facs="A96302-001-a-0890">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-cs" reg="for" xml:id="A96302-0890" facs="A96302-001-a-0900">for</w>
          <c> </c>
          <w lemma="dispatch" ana="#vvi" reg="dispatch" xml:id="A96302-0900" facs="A96302-001-a-0910">dispatch</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-0910" facs="A96302-001-a-0920">of</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A96302-0920" facs="A96302-001-a-0930">all</w>
          <c> </c>
          <w lemma="business" ana="#n2" reg="businesses" xml:id="A96302-0930" facs="A96302-001-a-0940">businesses</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A96302-0940" facs="A96302-001-a-0950">which</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A96302-0950" facs="A96302-001-a-0960">have</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A96302-0960" facs="A96302-001-a-0970">or</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A96302-0970" facs="A96302-001-a-0980">do</w>
          <c> </c>
          <w lemma="belong" ana="#vvi" reg="belong" xml:id="A96302-0980" facs="A96302-001-a-0990">belong</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A96302-0990" facs="A96302-001-a-1000">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1000" facs="A96302-001-a-1010">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A96302-1010" facs="A96302-001-a-1020">said</w>
          <c> </c>
          <w lemma="office" ana="#n1" reg="office" xml:id="A96302-1020" facs="A96302-001-a-1030">Office</w>
          <pc xml:id="A96302-1030" facs="A96302-001-a-1040">,</pc>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A96302-1040" facs="A96302-001-a-1050">it</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A96302-1050" facs="A96302-001-a-1060">being</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="an" xml:id="A96302-1060" facs="A96302-001-a-1070">an</w>
          <c> </c>
          <w lemma="office" ana="#n1" reg="office" xml:id="A96302-1070" facs="A96302-001-a-1080">Office</w>
          <c> </c>
          <w lemma="establish" ana="#vvn" reg="established" xml:id="A96302-1080" facs="A96302-001-a-1090">established</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A96302-1090" facs="A96302-001-a-1100">by</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A96302-1100" facs="A96302-001-a-1110">several</w>
          <c> </c>
          <w lemma="act" ana="#n2" reg="acts" xml:id="A96302-1110" facs="A96302-001-a-1120">Acts</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-1120" facs="A96302-001-a-1130">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A96302-1130" facs="A96302-001-a-1140">Parliament</w>
          <c> </c>
          <w lemma="never" ana="#av-x" reg="never" xml:id="A96302-1140" facs="A96302-001-a-1150">never</w>
          <c> </c>
          <w lemma="yet" ana="#av" reg="yet" xml:id="A96302-1150" facs="A96302-001-a-1160">yet</w>
          <c> </c>
          <w lemma="repeal" ana="#vvn" reg="repealed" xml:id="A96302-1160" facs="A96302-001-a-1170">repealed</w>
          <c> </c>
          <w lemma="nor" ana="#cc-x" reg="nor" xml:id="A96302-1170" facs="A96302-001-a-1180">nor</w>
          <c> </c>
          <w lemma="alter" ana="#vvn" reg="altered" xml:id="A96302-1180" facs="A96302-001-a-1190">altered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A96302-1190" facs="A96302-001-a-1200">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1200" facs="A96302-001-a-1210">the</w>
          <c> </c>
          <w lemma="present" ana="#j" reg="present" xml:id="A96302-1210" facs="A96302-001-a-1220">present</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A96302-1220" facs="A96302-001-a-1230">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A96302-1230" facs="A96302-001-a-1240">any</w>
          <c> </c>
          <w lemma="former" ana="#j" reg="former" xml:id="A96302-1240" facs="A96302-001-a-1250">former</w>
          <c> </c>
          <w lemma="government" ana="#n1" reg="government" xml:id="A96302-1250" facs="A96302-001-a-1260">Government</w>
          <pc xml:id="A96302-1260" facs="A96302-001-a-1270">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A96302-1270" facs="A96302-001-a-1280">and</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A96302-1280" facs="A96302-001-a-1290">that</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A96302-1290" facs="A96302-001-a-1300">they</w>
          <c> </c>
          <w lemma="pay" ana="#vvb" reg="pay" xml:id="A96302-1300" facs="A96302-001-a-1310">pay</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A96302-1310" facs="A96302-001-a-1320">their</w>
          <c> </c>
          <w lemma="money" ana="#n2" reg="monies" xml:id="A96302-1320" facs="A96302-001-a-1330">monies</w>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A96302-1330" facs="A96302-001-a-1340">there</w>
          <pc xml:id="A96302-1340" facs="A96302-001-a-1350">,</pc>
          <c> </c>
          <w lemma="where" ana="#crq-cs" reg="where" xml:id="A96302-1350" facs="A96302-001-a-1360">where</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A96302-1360" facs="A96302-001-a-1370">their</w>
          <c> </c>
          <w lemma="security" ana="#n2" reg="securities" xml:id="A96302-1370" facs="A96302-001-a-1380">securities</w>
          <c> </c>
          <w lemma="remain" ana="#vvi" reg="remain" xml:id="A96302-1380" facs="A96302-001-a-1390">remain</w>
          <pc xml:id="A96302-1390" facs="A96302-001-a-1400">,</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A96302-1400" facs="A96302-001-a-1410">which</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A96302-1410" facs="A96302-001-a-1420">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A96302-1420" facs="A96302-001-a-1430">be</w>
          <c> </c>
          <w lemma="deliver" ana="#vvn" reg="delivered" xml:id="A96302-1430" facs="A96302-001-a-1440">delivered</w>
          <c> </c>
          <w lemma="up" ana="#acp-av" reg="up" xml:id="A96302-1440" facs="A96302-001-a-1450">up</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A96302-1450" facs="A96302-001-a-1460">to</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A96302-1460" facs="A96302-001-a-1470">them</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A96302-1470" facs="A96302-001-a-1480">upon</w>
          <c> </c>
          <w lemma="payment" ana="#n1" reg="payment" xml:id="A96302-1480" facs="A96302-001-a-1490">payment</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-1490" facs="A96302-001-a-1500">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1500" facs="A96302-001-a-1510">the</w>
          <c> </c>
          <w lemma="same" ana="#d" reg="same" xml:id="A96302-1510" facs="A96302-001-a-1520">same</w>
          <pc xml:id="A96302-1520" facs="A96302-001-a-1530">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A96302-1530" facs="A96302-001-a-1540">and</w>
          <c> </c>
          <w lemma="without" ana="#acp-p" reg="without" xml:id="A96302-1540" facs="A96302-001-a-1550">without</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A96302-1550" facs="A96302-001-a-1560">which</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A96302-1560" facs="A96302-001-a-1570">they</w>
          <c> </c>
          <w lemma="can" ana="#vmb" reg="can" xml:id="A96302-1570" facs="A96302-001-a-1580">can</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A96302-1580" facs="A96302-001-a-1590">no</w>
          <c> </c>
          <w lemma="way" ana="#n1" reg="way" xml:id="A96302-1590" facs="A96302-001-a-1600">way</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A96302-1600" facs="A96302-001-a-1610">be</w>
          <c> </c>
          <w lemma="legal" ana="#av_j" reg="legally" xml:id="A96302-1610" facs="A96302-001-a-1620">legally</w>
          <c> </c>
          <w lemma="discharge" ana="#vvn" reg="discharged" xml:id="A96302-1620" facs="A96302-001-a-1630">discharged</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A96302-1630" facs="A96302-001-a-1640">or</w>
          <c> </c>
          <w lemma="secure" ana="#vvn" reg="secured" xml:id="A96302-1640" facs="A96302-001-a-1650">secured</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A96302-1650" facs="A96302-001-a-1660">by</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A96302-1660" facs="A96302-001-a-1670">any</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A96302-1670" facs="A96302-001-a-1680">other</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A96302-1680" facs="A96302-001-a-1690">persons</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A96302-1690" facs="A96302-001-a-1700">upon</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A96302-1700" facs="A96302-001-a-1710">any</w>
          <c> </c>
          <w lemma="pretence" ana="#n1" reg="pretence" xml:id="A96302-1710" facs="A96302-001-a-1720">pretence</w>
          <c> </c>
          <w lemma="whatsoever" ana="#crq-r" reg="whatsoever" xml:id="A96302-1720" facs="A96302-001-a-1730">whatsoever</w>
          <pc xml:id="A96302-1730" facs="A96302-001-a-1740">,</pc>
          <c> </c>
          <w lemma="nor" ana="#cc-x" reg="nor" xml:id="A96302-1740" facs="A96302-001-a-1750">nor</w>
          <c> </c>
          <w lemma="free" ana="#vvi" reg="free" xml:id="A96302-1750" facs="A96302-001-a-1760">free</w>
          <c> </c>
          <w lemma="themselves" ana="#px" reg="themselves" xml:id="A96302-1760" facs="A96302-001-a-1770">themselves</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A96302-1770" facs="A96302-001-a-1780">from</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1780" facs="A96302-001-a-1790">the</w>
          <c> </c>
          <w lemma="process" ana="#n1" reg="process" xml:id="A96302-1790" facs="A96302-001-a-1800">process</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-1800" facs="A96302-001-a-1810">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1810" facs="A96302-001-a-1820">the</w>
          <c> </c>
          <w lemma="court" ana="#n1" reg="court" xml:id="A96302-1820" facs="A96302-001-a-1830">Court</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-1830" facs="A96302-001-a-1840">of</w>
          <c> </c>
          <w lemma="exchequer" ana="#n1" reg="exchequer" xml:id="A96302-1840" facs="A96302-001-a-1850">Exchequer</w>
          <pc xml:id="A96302-1850" facs="A96302-001-a-1860">,</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A96302-1860" facs="A96302-001-a-1870">which</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A96302-1870" facs="A96302-001-a-1880">in</w>
          <c> </c>
          <w lemma="discharge" ana="#n1" reg="discharge" xml:id="A96302-1880" facs="A96302-001-a-1890">discharge</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-1890" facs="A96302-001-a-1900">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-1900" facs="A96302-001-a-1910">the</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A96302-1910" facs="A96302-001-a-1920">Officers</w>
          <c> </c>
          <w lemma="duty" ana="#n1" reg="duty" xml:id="A96302-1920" facs="A96302-001-a-1930">duty</w>
          <c> </c>
          <w lemma="must" ana="#vmb" reg="must" xml:id="A96302-1930" facs="A96302-001-a-1940">must</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A96302-1940" facs="A96302-001-a-1950">be</w>
          <c> </c>
          <w lemma="issue" ana="#vvn" reg="issued" xml:id="A96302-1950" facs="A96302-001-a-1960">issued</w>
          <pc xml:id="A96302-1960" facs="A96302-001-a-1970">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A96302-1970" facs="A96302-001-a-1980">to</w>
          <c> </c>
          <w lemma="bring" ana="#vvi" reg="bring" xml:id="A96302-1980" facs="A96302-001-a-1990">bring</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A96302-1990" facs="A96302-001-a-2000">all</w>
          <c> </c>
          <w lemma="person" ana="#n2" reg="persons" xml:id="A96302-2000" facs="A96302-001-a-2010">persons</w>
          <c> </c>
          <w lemma="concern" ana="#vvn" reg="concerned" xml:id="A96302-2010" facs="A96302-001-a-2020">concerned</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A96302-2020" facs="A96302-001-a-2030">to</w>
          <c> </c>
          <w lemma="conformity" ana="#n1" reg="conformity" xml:id="A96302-2030" facs="A96302-001-a-2040">conformity</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A96302-2040" facs="A96302-001-a-2050">in</w>
          <c> </c>
          <w lemma="payment" ana="#n1" reg="payment" xml:id="A96302-2050" facs="A96302-001-a-2060">payment</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-2060" facs="A96302-001-a-2070">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A96302-2070" facs="A96302-001-a-2080">the</w>
          <c> </c>
          <w lemma="state" ana="#n1g" reg="state's" xml:id="A96302-2080" facs="A96302-001-a-2090">States</w>
          <c> </c>
          <w lemma="revenue" ana="#n1" reg="revenue" xml:id="A96302-2090" facs="A96302-001-a-2100">Revenew</w>
          <pc unit="sentence" xml:id="A96302-2100" facs="A96302-001-a-2110">.</pc>
        </p>
        <closer xml:id="A96302e-90">
          <dateline xml:id="A96302e-100">
            <w lemma="date" ana="#j_vn" reg="dated" xml:id="A96302-2140" facs="A96302-001-a-2120">Dated</w>
            <c> </c>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A96302-2150" facs="A96302-001-a-2130">at</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A96302-2160" facs="A96302-001-a-2140">the</w>
            <c> </c>
            <w lemma="first-fruit" ana="#n2" reg="first-fruits" xml:id="A96302-2170" facs="A96302-001-a-2150">First-fruits</w>
            <c> </c>
            <w lemma="office" ana="#n1" reg="office" xml:id="A96302-2180" facs="A96302-001-a-2160">Office</w>
            <c> </c>
            <w lemma="aforesaid" ana="#j" reg="aforesaid" xml:id="A96302-2190" facs="A96302-001-a-2170">aforesaid</w>
            <pc xml:id="A96302-2200" facs="A96302-001-a-2180">,</pc>
            <date xml:id="A96302e-110">
              <w lemma="the" ana="#d" reg="the" xml:id="A96302-2210" facs="A96302-001-a-2190">the</w>
              <c> </c>
              <w lemma="eighteen" ana="#ord" reg="eighteenth" xml:id="A96302-2220" facs="A96302-001-a-2200">Eighteenth</w>
              <c> </c>
              <w lemma="day" ana="#n1" reg="day" xml:id="A96302-2230" facs="A96302-001-a-2210">day</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A96302-2240" facs="A96302-001-a-2220">of</w>
              <c> </c>
              <hi xml:id="A96302e-120">
                <w lemma="November" ana="#n1-nn" reg="November" xml:id="A96302-2250" facs="A96302-001-a-2230">November</w>
              </hi>
              <c> </c>
              <w lemma="1655." ana="#crd" reg="1655." xml:id="A96302-2260" facs="A96302-001-a-2240">1655.</w>
              <pc unit="sentence" xml:id="A96302-2260-eos" facs="A96302-001-a-2250"/>
            </date>
          </dateline>
          <signed xml:id="A96302e-130">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A96302-2290" facs="A96302-001-a-2260">Thomas</w>
            <c> </c>
            <w lemma="baker" ana="#n1" reg="baker" xml:id="A96302-2300" facs="A96302-001-a-2270">Baker</w>
            <pc unit="sentence" xml:id="A96302-2310" facs="A96302-001-a-2280">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A96302e-140">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>