<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>All gentlemen merchants, and others, may please to take notice, that if they send their letters by the old post, ...</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1653</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A76043</idno>
        <idno type="STC">Wing A933</idno>
        <idno type="STC">Thomason 669.f.16[92]</idno>
        <idno type="STC">ESTC R211682</idno>
        <idno type="EEBO-CITATION">99870388</idno>
        <idno type="PROQUEST">99870388</idno>
        <idno type="VID">163245</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A76043)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163245)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[92])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>All gentlemen merchants, and others, may please to take notice, that if they send their letters by the old post, ...</title>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1653]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from opening words of text.</note>
            <note>Imprint from Wing.</note>
            <note>Annotation on Thomason copy: "The same 2d of April this was cast about upon the Exchange".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Postal service -- England -- Early works to 1800.</term>
          <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A76043e-10">
    <body xml:id="A76043e-20">
      <div type="text" xml:id="A76043e-30">
        <pb facs="tcp:163245:1" rendition="simple:additions" xml:id="A76043-001-a"/>
        <p xml:id="A76043e-40">
          <w lemma="all" ana="#av_d" reg="All" xml:id="A76043-030" facs="A76043-001-a-0010">ALL</w>
          <c> </c>
          <w lemma="gentleman" ana="#n2" reg="gentlemen" xml:id="A76043-040" facs="A76043-001-a-0020">Gentlemen</w>
          <c> </c>
          <w lemma="merchant" ana="#n2" reg="merchants" xml:id="A76043-050" facs="A76043-001-a-0030">Merchants</w>
          <pc xml:id="A76043-060" facs="A76043-001-a-0040">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A76043-070" facs="A76043-001-a-0050">and</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A76043-080" facs="A76043-001-a-0060">others</w>
          <pc xml:id="A76043-090" facs="A76043-001-a-0070">,</pc>
          <c> </c>
          <w lemma="may" ana="#vmb" reg="May" xml:id="A76043-100" facs="A76043-001-a-0080">may</w>
          <c> </c>
          <w lemma="please" ana="#vvi" reg="please" xml:id="A76043-110" facs="A76043-001-a-0090">please</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A76043-120" facs="A76043-001-a-0100">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A76043-130" facs="A76043-001-a-0110">take</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A76043-140" facs="A76043-001-a-0120">notice</w>
          <pc xml:id="A76043-150" facs="A76043-001-a-0130">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A76043-160" facs="A76043-001-a-0140">That</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A76043-170" facs="A76043-001-a-0150">if</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A76043-180" facs="A76043-001-a-0160">they</w>
          <c> </c>
          <w lemma="send" ana="#vvb" reg="send" xml:id="A76043-190" facs="A76043-001-a-0170">send</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A76043-200" facs="A76043-001-a-0180">their</w>
          <c> </c>
          <hi xml:id="A76043e-50">
            <w lemma="letter" ana="#n2" reg="letters" xml:id="A76043-210" facs="A76043-001-a-0190">Letters</w>
          </hi>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A76043-220" facs="A76043-001-a-0200">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A76043-230" facs="A76043-001-a-0210">the</w>
          <c> </c>
          <w lemma="old" ana="#j" reg="old" xml:id="A76043-240" facs="A76043-001-a-0220">Old</w>
          <c> </c>
          <w lemma="post" ana="#n1" reg="post" xml:id="A76043-250" facs="A76043-001-a-0230">Post</w>
          <pc xml:id="A76043-260" facs="A76043-001-a-0240">,</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A76043-270" facs="A76043-001-a-0250">they</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A76043-280" facs="A76043-001-a-0260">will</w>
          <c> </c>
          <w lemma="have" ana="#vvi" reg="have" xml:id="A76043-290" facs="A76043-001-a-0270">have</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A76043-300" facs="A76043-001-a-0280">a</w>
          <c> </c>
          <w lemma="free" ana="#j" reg="free" xml:id="A76043-310" facs="A76043-001-a-0290">Free</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A76043-320" facs="A76043-001-a-0300">and</w>
          <c> </c>
          <w lemma="safe" ana="#j" reg="safe" xml:id="A76043-330" facs="A76043-001-a-0310">Safe</w>
          <c> </c>
          <w lemma="go" ana="#vvg" reg="going" xml:id="A76043-340" facs="A76043-001-a-0320">Going</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A76043-350" facs="A76043-001-a-0330">and</w>
          <c> </c>
          <w lemma="come" ana="#vvg" reg="coming" xml:id="A76043-360" facs="A76043-001-a-0340">Comming</w>
          <c> </c>
          <w lemma="as" ana="#acp-av" reg="as" xml:id="A76043-370" facs="A76043-001-a-0350">as</w>
          <c> </c>
          <w lemma="former" ana="#av_j" reg="formerly" xml:id="A76043-380" facs="A76043-001-a-0360">formerly</w>
          <pc xml:id="A76043-390" facs="A76043-001-a-0370">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A76043-400" facs="A76043-001-a-0380">and</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A76043-410" facs="A76043-001-a-0390">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A76043-420" facs="A76043-001-a-0400">the</w>
          <c> </c>
          <w lemma="low" ana="#j-s" reg="lowest" xml:id="A76043-430" facs="A76043-001-a-0410">Lowest</w>
          <c> </c>
          <w lemma="rate" ana="#n2" reg="rates" xml:id="A76043-440" facs="A76043-001-a-0420">Rates</w>
          <pc xml:id="A76043-450" facs="A76043-001-a-0430">:</pc>
          <c> </c>
          <w lemma="but" ana="#acp-cc" reg="but" xml:id="A76043-460" facs="A76043-001-a-0440">But</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A76043-470" facs="A76043-001-a-0450">if</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A76043-480" facs="A76043-001-a-0460">they</w>
          <c> </c>
          <w lemma="send" ana="#vvb" reg="send" xml:id="A76043-490" facs="A76043-001-a-0470">send</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A76043-500" facs="A76043-001-a-0480">by</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A76043-510" facs="A76043-001-a-0490">those</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A76043-520" facs="A76043-001-a-0500">that</w>
          <c> </c>
          <w lemma="stile" ana="#n1" reg="stile" xml:id="A76043-530" facs="A76043-001-a-0510">stile</w>
          <c> </c>
          <w lemma="themselves" ana="#px" reg="themselves" xml:id="A76043-540" facs="A76043-001-a-0520">themselves</w>
          <pc xml:id="A76043-550" facs="A76043-001-a-0530">,</pc>
          <c> </c>
          <hi xml:id="A76043e-60">
            <w lemma="the" ana="#d" reg="the" xml:id="A76043-560" facs="A76043-001-a-0540">The</w>
            <c> </c>
            <w lemma="new-vndertaker" ana="#n2" reg="new-vndertakers" xml:id="A76043-570" facs="A76043-001-a-0550">New-Vndertakers</w>
            <pc xml:id="A76043-580" facs="A76043-001-a-0560">,</pc>
          </hi>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A76043-590" facs="A76043-001-a-0570">Their</w>
          <c> </c>
          <w lemma="passage" ana="#n1" reg="passage" xml:id="A76043-600" facs="A76043-001-a-0580">Passage</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A76043-610" facs="A76043-001-a-0590">will</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A76043-620" facs="A76043-001-a-0600">be</w>
          <c> </c>
          <w lemma="interrupt" ana="#vvn" reg="interrupted" xml:id="A76043-630" facs="A76043-001-a-0610">interrupted</w>
          <pc unit="sentence" xml:id="A76043-640" facs="A76043-001-a-0620">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A76043e-70">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>