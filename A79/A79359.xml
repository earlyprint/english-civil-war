<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A proclamation proclaming [sic] Charls Prince of Wales, King of Great Brittaine, France, and Ireland.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1649</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A79359</idno>
        <idno type="STC">Wing C3525</idno>
        <idno type="STC">Thomason 669.f.13[79]</idno>
        <idno type="STC">ESTC R211200</idno>
        <idno type="EEBO-CITATION">99869932</idno>
        <idno type="PROQUEST">99869932</idno>
        <idno type="VID">162975</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A79359)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162975)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f13[79])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A proclamation proclaming [sic] Charls Prince of Wales, King of Great Brittaine, France, and Ireland.</title>
          </titleStmt>
          <extent>1 sheet ([1] p.) : ill. (royal arms)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London? :</pubPlace>
            <date>1649]</date>
          </publicationStmt>
          <notesStmt>
            <note>Issuing body, if any, unknown. Suggested imprint from Wing.</note>
            <note>Dated at end: "the first day of February in the first yeare of his Majesties Raigne. God save King Charl the Second".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Charles -- II, -- King of England, 1630-1685 -- Early works to 1800.</term>
          <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A79359e-10">
    <body xml:id="A79359e-20">
      <div type="royal_proclamation" xml:id="A79359e-30">
        <pb facs="tcp:162975:1" xml:id="A79359-001-a"/>
        <head xml:id="A79359e-40">
          <figure xml:id="A79359e-50">
            <p xml:id="A79359e-60">
              <w lemma="diev" ana="#fw-fr" reg="Diev" xml:id="A79359-0050" facs="A79359-001-a-0010">DIEV</w>
              <c> </c>
              <w lemma="et" ana="#fw-fr" reg="et" xml:id="A79359-0060" facs="A79359-001-a-0020">ET</w>
              <c> </c>
              <w lemma="mon" ana="#fw-fr" reg="mon" xml:id="A79359-0070" facs="A79359-001-a-0030">MON</w>
              <c> </c>
              <w lemma="droit" ana="#fw-fr" reg="droit" xml:id="A79359-0080" facs="A79359-001-a-0040">DROIT</w>
            </p>
            <p xml:id="A79359e-70">
              <w lemma="honi" ana="#fw-fr" reg="Honi" xml:id="A79359-0110" facs="A79359-001-a-0050">HONI</w>
              <c> </c>
              <w lemma="soit" ana="#fw-fr" reg="soit" xml:id="A79359-0120" facs="A79359-001-a-0060">SOIT</w>
              <c> </c>
              <w lemma="qvi" ana="#fw-fr" reg="qvi" xml:id="A79359-0130" facs="A79359-001-a-0070">QVI</w>
              <c> </c>
              <w lemma="mal" ana="#fw-fr" reg="mal" xml:id="A79359-0140" facs="A79359-001-a-0080">MAL</w>
              <c> </c>
              <w lemma="y" ana="#fw-fr" reg="y" xml:id="A79359-0150" facs="A79359-001-a-0090">Y</w>
              <c> </c>
              <w lemma="PENSE" ana="#fw-fr" reg="pense" xml:id="A79359-0160" facs="A79359-001-a-0100">PENSE</w>
            </p>
            <figDesc xml:id="A79359e-80">royal blazon or coat of arms</figDesc>
          </figure>
        </head>
        <head xml:id="A79359e-90">
          <w lemma="a" ana="#d" reg="A" xml:id="A79359-0270" facs="A79359-001-a-0110">A</w>
          <c> </c>
          <w lemma="proclamation" ana="#n1" reg="proclamation" xml:id="A79359-0280" facs="A79359-001-a-0120">PROCLAMATION</w>
          <c> </c>
          <w lemma="proclaim" ana="#vvg" reg="proclaiming" xml:id="A79359-0290" facs="A79359-001-a-0130">PROCLAMING</w>
          <c> </c>
          <w lemma="CHARLES" ana="#n1-nn" reg="Charles" xml:id="A79359-0300" facs="A79359-001-a-0140">CHARLS</w>
          <c> </c>
          <w lemma="prince" ana="#n1" reg="Prince" xml:id="A79359-0310" facs="A79359-001-a-0150">Prince</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-0320" facs="A79359-001-a-0160">of</w>
          <c> </c>
          <w lemma="Wales" ana="#n1-nn" reg="Wales" xml:id="A79359-0330" facs="A79359-001-a-0170">Wales</w>
          <pc xml:id="A79359-0340" facs="A79359-001-a-0180">,</pc>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A79359-0350" facs="A79359-001-a-0190">King</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-0360" facs="A79359-001-a-0200">of</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A79359-0370" facs="A79359-001-a-0210">Great</w>
          <c> </c>
          <w lemma="Britain" ana="#n1-nn" reg="Britain" xml:id="A79359-0380" facs="A79359-001-a-0220">Brittaine</w>
          <pc xml:id="A79359-0390" facs="A79359-001-a-0230">,</pc>
          <c> </c>
          <w lemma="France" ana="#n1-nn" reg="France" xml:id="A79359-0400" facs="A79359-001-a-0240">France</w>
          <pc xml:id="A79359-0410" facs="A79359-001-a-0250">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-0420" facs="A79359-001-a-0260">and</w>
          <c> </c>
          <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A79359-0430" facs="A79359-001-a-0270">Ireland</w>
          <pc unit="sentence" xml:id="A79359-0440" facs="A79359-001-a-0280">.</pc>
        </head>
        <p xml:id="A79359e-100">
          <w lemma="we" ana="#pns" reg="We" xml:id="A79359-0470" facs="A79359-001-a-0290">WEE</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-0480" facs="A79359-001-a-0300">the</w>
          <c> </c>
          <w lemma="nobleman" ana="#n2" reg="noblemen" xml:id="A79359-0490" facs="A79359-001-a-0310">Noblemen</w>
          <pc xml:id="A79359-0500" facs="A79359-001-a-0320">,</pc>
          <c> </c>
          <w lemma="judge" ana="#n2" reg="judges" xml:id="A79359-0510" facs="A79359-001-a-0330">Iudges</w>
          <pc xml:id="A79359-0520" facs="A79359-001-a-0340">,</pc>
          <c> </c>
          <w lemma="knight" ana="#n2" reg="knights" xml:id="A79359-0530" facs="A79359-001-a-0350">Knights</w>
          <pc xml:id="A79359-0540" facs="A79359-001-a-0360">,</pc>
          <c> </c>
          <w lemma="lawyer" ana="#n2" reg="lawyers" xml:id="A79359-0550" facs="A79359-001-a-0370">Lawyers</w>
          <pc xml:id="A79359-0560" facs="A79359-001-a-0380">,</pc>
          <c> </c>
          <w lemma="gentleman" ana="#n2" reg="gentlemen" xml:id="A79359-0570" facs="A79359-001-a-0390">Gentlemen</w>
          <pc xml:id="A79359-0580" facs="A79359-001-a-0400">,</pc>
          <c> </c>
          <w lemma="freeholder" ana="#n2" reg="freeholders" xml:id="A79359-0590" facs="A79359-001-a-0410">Freeholders</w>
          <pc xml:id="A79359-0600" facs="A79359-001-a-0420">,</pc>
          <c> </c>
          <w lemma="merchant" ana="#n2" reg="merchants" xml:id="A79359-0610" facs="A79359-001-a-0430">Merchants</w>
          <pc xml:id="A79359-0620" facs="A79359-001-a-0440">,</pc>
          <c> </c>
          <w lemma="citizen" ana="#n2" reg="citizens" xml:id="A79359-0630" facs="A79359-001-a-0450">Citizens</w>
          <pc xml:id="A79359-0640" facs="A79359-001-a-0460">,</pc>
          <c> </c>
          <w lemma="yeoman" ana="#n2" reg="yeomen" xml:id="A79359-0650" facs="A79359-001-a-0470">Yeomen</w>
          <pc xml:id="A79359-0660" facs="A79359-001-a-0480">,</pc>
          <c> </c>
          <w lemma="seaman" ana="#n2" reg="seamen" xml:id="A79359-0670" facs="A79359-001-a-0490">Seamen</w>
          <pc xml:id="A79359-0680" facs="A79359-001-a-0500">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-0690" facs="A79359-001-a-0510">and</w>
          <c> </c>
          <w lemma="other" ana="#d" reg="other" xml:id="A79359-0700" facs="A79359-001-a-0520">other</w>
          <c> </c>
          <w lemma="freeman" ana="#n2" reg="freemen" xml:id="A79359-0710" facs="A79359-001-a-0530">freemen</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-0720" facs="A79359-001-a-0540">of</w>
          <c> </c>
          <w lemma="England" ana="#n1-nn" reg="England" xml:id="A79359-0730" facs="A79359-001-a-0550">England</w>
          <pc xml:id="A79359-0740" facs="A79359-001-a-0560">,</pc>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A79359-0750" facs="A79359-001-a-0570">doe</w>
          <pc xml:id="A79359-0760" facs="A79359-001-a-0580">,</pc>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A79359-0770" facs="A79359-001-a-0590">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79359-0780" facs="A79359-001-a-0600">to</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79359-0790" facs="A79359-001-a-0610">our</w>
          <c> </c>
          <w lemma="allegiance" ana="#n1" reg="allegiance" xml:id="A79359-0800" facs="A79359-001-a-0620">Allegiance</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-0810" facs="A79359-001-a-0630">and</w>
          <c> </c>
          <w lemma="covenant" ana="#n1" reg="covenant" xml:id="A79359-0820" facs="A79359-001-a-0640">Covenant</w>
          <pc xml:id="A79359-0830" facs="A79359-001-a-0650">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A79359-0840" facs="A79359-001-a-0660">by</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A79359-0850" facs="A79359-001-a-0670">these</w>
          <c> </c>
          <w lemma="present" ana="#n2" reg="presents" xml:id="A79359-0860" facs="A79359-001-a-0680">presents</w>
          <c> </c>
          <w lemma="hearty" ana="#av_j" reg="heartily" xml:id="A79359-0870" facs="A79359-001-a-0690">hartily</w>
          <pc xml:id="A79359-0880" facs="A79359-001-a-0700">,</pc>
          <c> </c>
          <w lemma="joyful" ana="#av_j" reg="joyfully" xml:id="A79359-0890" facs="A79359-001-a-0710">ioyfully</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-0900" facs="A79359-001-a-0720">and</w>
          <c> </c>
          <w lemma="unanimous" ana="#av_j" reg="unanimously" xml:id="A79359-0910" facs="A79359-001-a-0730">unanimously</w>
          <c> </c>
          <w lemma="acknowledge" ana="#vvi" reg="acknowledge" xml:id="A79359-0920" facs="A79359-001-a-0740">acknowledg</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-0930" facs="A79359-001-a-0750">and</w>
          <c> </c>
          <w lemma="proclaim" ana="#vvi" reg="proclaim" xml:id="A79359-0940" facs="A79359-001-a-0760">proclaime</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-0950" facs="A79359-001-a-0770">the</w>
          <c> </c>
          <w lemma="illustrious" ana="#j" reg="illustrious" xml:id="A79359-0960" facs="A79359-001-a-0780">Illustrious</w>
          <c> </c>
          <hi xml:id="A79359e-110">
            <w lemma="Charles" ana="#n1-nn" reg="Charles" xml:id="A79359-0970" facs="A79359-001-a-0790">Charls</w>
          </hi>
          <c> </c>
          <w lemma="prince" ana="#n1" reg="Prince" xml:id="A79359-0980" facs="A79359-001-a-0800">Prince</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-0990" facs="A79359-001-a-0810">of</w>
          <c> </c>
          <hi xml:id="A79359e-120">
            <w lemma="Wales" ana="#n1-nn" reg="Wales" xml:id="A79359-1000" facs="A79359-001-a-0820">Wales</w>
            <pc xml:id="A79359-1010" facs="A79359-001-a-0830">,</pc>
          </hi>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A79359-1020" facs="A79359-001-a-0840">next</w>
          <c> </c>
          <w lemma="heir" ana="#n1" reg="heir" xml:id="A79359-1030" facs="A79359-001-a-0850">heire</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-1040" facs="A79359-001-a-0860">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-1050" facs="A79359-001-a-0870">the</w>
          <c> </c>
          <w lemma="blood" ana="#n1" reg="blood" xml:id="A79359-1060" facs="A79359-001-a-0880">blood</w>
          <c> </c>
          <w lemma="royal" ana="#j" reg="royal" xml:id="A79359-1070" facs="A79359-001-a-0890">Royall</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79359-1080" facs="A79359-001-a-0900">to</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79359-1090" facs="A79359-001-a-0910">his</w>
          <c> </c>
          <w lemma="father" ana="#n1" reg="father" xml:id="A79359-1100" facs="A79359-001-a-0920">Father</w>
          <c> </c>
          <hi xml:id="A79359e-130">
            <w lemma="king" ana="#n1" reg="King" xml:id="A79359-1110" facs="A79359-001-a-0930">King</w>
            <c> </c>
            <w lemma="Charles" ana="#n1-nn" reg="Charles" xml:id="A79359-1120" facs="A79359-001-a-0940">Charls</w>
          </hi>
          <c> </c>
          <pc xml:id="A79359-1130" facs="A79359-001-a-0950">(</pc>
          <w lemma="who" ana="#crq-rg" reg="Whose" xml:id="A79359-1140" facs="A79359-001-a-0960">whose</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A79359-1150" facs="A79359-001-a-0970">late</w>
          <c> </c>
          <w lemma="wicked" ana="#j" reg="wicked" xml:id="A79359-1160" facs="A79359-001-a-0980">wicked</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1170" facs="A79359-001-a-0990">and</w>
          <c> </c>
          <w lemma="traitorous" ana="#j" reg="traitorous" xml:id="A79359-1180" facs="A79359-001-a-1000">trayterous</w>
          <c> </c>
          <w lemma="murder" ana="#n1" reg="murder" xml:id="A79359-1190" facs="A79359-001-a-1010">murther</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79359-1200" facs="A79359-001-a-1020">wee</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A79359-1210" facs="A79359-001-a-1030">doe</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A79359-1220" facs="A79359-001-a-1040">from</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79359-1230" facs="A79359-001-a-1050">our</w>
          <c> </c>
          <w lemma="soul" ana="#n2" reg="souls" xml:id="A79359-1240" facs="A79359-001-a-1060">soules</w>
          <c> </c>
          <w lemma="abominate" ana="#j" reg="abominate" xml:id="A79359-1250" facs="A79359-001-a-1070">abominate</w>
          <pc xml:id="A79359-1260" facs="A79359-001-a-1080">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1270" facs="A79359-001-a-1090">and</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A79359-1280" facs="A79359-001-a-1100">all</w>
          <c> </c>
          <w lemma="party" ana="#n2" reg="parties" xml:id="A79359-1290" facs="A79359-001-a-1110">parties</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1300" facs="A79359-001-a-1120">and</w>
          <c> </c>
          <w lemma="consenter" ana="#n2" reg="consenters" xml:id="A79359-1310" facs="A79359-001-a-1130">consenters</w>
          <c> </c>
          <w lemma="thereunto" ana="#av" reg="thereunto" xml:id="A79359-1320" facs="A79359-001-a-1140">thereunto</w>
          <pc xml:id="A79359-1330" facs="A79359-001-a-1150">)</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79359-1340" facs="A79359-001-a-1160">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A79359-1350" facs="A79359-001-a-1170">be</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A79359-1360" facs="A79359-001-a-1180">by</w>
          <c> </c>
          <w lemma="hereditary" ana="#j" reg="hereditary" xml:id="A79359-1370" facs="A79359-001-a-1190">hereditary</w>
          <c> </c>
          <w lemma="birthright" ana="#n1" reg="birthright" xml:id="A79359-1380" facs="A79359-001-a-1200">Birthright</w>
          <pc xml:id="A79359-1390" facs="A79359-001-a-1210">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1400" facs="A79359-001-a-1220">and</w>
          <c> </c>
          <w lemma="lawful" ana="#j" reg="lawful" xml:id="A79359-1410" facs="A79359-001-a-1230">lawfull</w>
          <c> </c>
          <w lemma="succession" ana="#n1" reg="succession" xml:id="A79359-1420" facs="A79359-001-a-1240">succession</w>
          <pc xml:id="A79359-1430" facs="A79359-001-a-1250">,</pc>
          <c> </c>
          <w lemma="rightful" ana="#j" reg="rightful" xml:id="A79359-1440" facs="A79359-001-a-1260">rightfull</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1450" facs="A79359-001-a-1270">and</w>
          <c> </c>
          <w lemma="undoubted" ana="#j" reg="undoubted" xml:id="A79359-1460" facs="A79359-001-a-1280">undoubted</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A79359-1470" facs="A79359-001-a-1290">King</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-1480" facs="A79359-001-a-1300">of</w>
          <c> </c>
          <hi xml:id="A79359e-140">
            <w lemma="great" ana="#j" reg="great" xml:id="A79359-1490" facs="A79359-001-a-1310">Great</w>
            <c> </c>
            <w lemma="Britain" ana="#n1-nn" reg="Britain" xml:id="A79359-1500" facs="A79359-001-a-1320">Brittaine</w>
            <pc xml:id="A79359-1510" facs="A79359-001-a-1330">,</pc>
            <c> </c>
            <w lemma="France" ana="#n1-nn" reg="France" xml:id="A79359-1520" facs="A79359-001-a-1340">France</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1530" facs="A79359-001-a-1350">and</w>
          <c> </c>
          <hi xml:id="A79359e-150">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A79359-1540" facs="A79359-001-a-1360">Ireland</w>
            <pc xml:id="A79359-1550" facs="A79359-001-a-1370">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1560" facs="A79359-001-a-1380">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-1570" facs="A79359-001-a-1390">the</w>
          <c> </c>
          <w lemma="dominion" ana="#n2" reg="dominions" xml:id="A79359-1580" facs="A79359-001-a-1400">dominions</w>
          <c> </c>
          <w lemma="thereunto" ana="#av" reg="thereunto" xml:id="A79359-1590" facs="A79359-001-a-1410">thereunto</w>
          <c> </c>
          <w lemma="belong" ana="#vvg" reg="belonging" xml:id="A79359-1600" facs="A79359-001-a-1420">belonging</w>
          <pc unit="sentence" xml:id="A79359-1610" facs="A79359-001-a-1430">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A79359-1620" facs="A79359-001-a-1440">And</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A79359-1630" facs="A79359-001-a-1450">that</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79359-1640" facs="A79359-001-a-1460">we</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A79359-1650" facs="A79359-001-a-1470">will</w>
          <c> </c>
          <w lemma="faithful" ana="#av_j" reg="faithfully" xml:id="A79359-1660" facs="A79359-001-a-1480">faithfully</w>
          <pc xml:id="A79359-1670" facs="A79359-001-a-1490">,</pc>
          <c> </c>
          <w lemma="constant" ana="#av_j" reg="constantly" xml:id="A79359-1680" facs="A79359-001-a-1500">constantly</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1690" facs="A79359-001-a-1510">and</w>
          <c> </c>
          <w lemma="sincere" ana="#av_j" reg="sincerely" xml:id="A79359-1700" facs="A79359-001-a-1520">sinceerely</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A79359-1710" facs="A79359-001-a-1530">in</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79359-1720" facs="A79359-001-a-1540">our</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A79359-1730" facs="A79359-001-a-1550">severall</w>
          <c> </c>
          <w lemma="place" ana="#n2" reg="places" xml:id="A79359-1740" facs="A79359-001-a-1560">places</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1750" facs="A79359-001-a-1570">and</w>
          <c> </c>
          <w lemma="calling" ana="#n2" reg="callings" xml:id="A79359-1760" facs="A79359-001-a-1580">callings</w>
          <c> </c>
          <w lemma="defend" ana="#vvi" reg="defend" xml:id="A79359-1770" facs="A79359-001-a-1590">defend</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1780" facs="A79359-001-a-1600">and</w>
          <c> </c>
          <w lemma="maintain" ana="#vvi" reg="maintain" xml:id="A79359-1790" facs="A79359-001-a-1610">maintaine</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79359-1800" facs="A79359-001-a-1620">his</w>
          <c> </c>
          <w lemma="royal" ana="#j" reg="royal" xml:id="A79359-1810" facs="A79359-001-a-1630">Royall</w>
          <c> </c>
          <w lemma="person" ana="#n1" reg="person" xml:id="A79359-1820" facs="A79359-001-a-1640">Person</w>
          <pc xml:id="A79359-1830" facs="A79359-001-a-1650">,</pc>
          <c> </c>
          <w lemma="crown" ana="#n1" reg="crown" xml:id="A79359-1840" facs="A79359-001-a-1660">Crowne</w>
          <pc xml:id="A79359-1850" facs="A79359-001-a-1670">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1860" facs="A79359-001-a-1680">and</w>
          <c> </c>
          <w lemma="dignity" ana="#n1" reg="dignity" xml:id="A79359-1870" facs="A79359-001-a-1690">Dignity</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A79359-1880" facs="A79359-001-a-1700">with</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79359-1890" facs="A79359-001-a-1710">our</w>
          <c> </c>
          <w lemma="estate" ana="#n2" reg="estates" xml:id="A79359-1900" facs="A79359-001-a-1720">Estates</w>
          <pc unit="sentence" xml:id="A79359-1910" facs="A79359-001-a-1730">.</pc>
          <c> </c>
          <w lemma="life" ana="#n2" reg="Lives" xml:id="A79359-1920" facs="A79359-001-a-1740">Lives</w>
          <pc xml:id="A79359-1930" facs="A79359-001-a-1750">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-1940" facs="A79359-001-a-1760">and</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A79359-1950" facs="A79359-001-a-1770">last</w>
          <c> </c>
          <w lemma="drop" ana="#n1" reg="drop" xml:id="A79359-1960" facs="A79359-001-a-1780">drop</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-1970" facs="A79359-001-a-1790">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79359-1980" facs="A79359-001-a-1800">our</w>
          <c> </c>
          <w lemma="•loods" ana="#n2" reg="•loodss" xml:id="A79359-1990" facs="A79359-001-a-1810">•loods</w>
          <pc xml:id="A79359-2000" facs="A79359-001-a-1820">,</pc>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A79359-2010" facs="A79359-001-a-1830">against</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A79359-2020" facs="A79359-001-a-1840">all</w>
          <c> </c>
          <w lemma="opposer" ana="#n2" reg="opposers" xml:id="A79359-2030" facs="A79359-001-a-1850">opposers</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A79359-2040" facs="A79359-001-a-1860">thereof</w>
          <pc xml:id="A79359-2050" facs="A79359-001-a-1870">;</pc>
          <c> </c>
          <w lemma="who" ana="#crq-ro" reg="whom" xml:id="A79359-2060" facs="A79359-001-a-1880">whom</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79359-2070" facs="A79359-001-a-1890">wee</w>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A79359-2080" facs="A79359-001-a-1900">doe</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A79359-2090" facs="A79359-001-a-1910">hereby</w>
          <c> </c>
          <w lemma="declare" ana="#vvi" reg="declare" xml:id="A79359-2100" facs="A79359-001-a-1920">declare</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79359-2110" facs="A79359-001-a-1930">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A79359-2120" facs="A79359-001-a-1940">bee</w>
          <c> </c>
          <w lemma="traitor" ana="#n2" reg="traitors" xml:id="A79359-2130" facs="A79359-001-a-1950">Traytors</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-2140" facs="A79359-001-a-1960">and</w>
          <c> </c>
          <w lemma="enemy" ana="#n2" reg="enemies" xml:id="A79359-2150" facs="A79359-001-a-1970">Enemies</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79359-2160" facs="A79359-001-a-1980">to</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79359-2170" facs="A79359-001-a-1990">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A79359-2180" facs="A79359-001-a-2000">Maiesty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-2190" facs="A79359-001-a-2010">and</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79359-2200" facs="A79359-001-a-2020">his</w>
          <c> </c>
          <w lemma="kingdom" ana="#n2" reg="kingdoms" xml:id="A79359-2210" facs="A79359-001-a-2030">Kingdoms</w>
          <pc unit="sentence" xml:id="A79359-2220" facs="A79359-001-a-2040">.</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="In" xml:id="A79359-2230" facs="A79359-001-a-2050">In</w>
          <c> </c>
          <w lemma="testimony" ana="#n1" reg="testimony" xml:id="A79359-2240" facs="A79359-001-a-2060">testimony</w>
          <c> </c>
          <w lemma="whereof" ana="#crq-cs" reg="whereof" xml:id="A79359-2250" facs="A79359-001-a-2070">whereof</w>
          <pc xml:id="A79359-2260" facs="A79359-001-a-2080">,</pc>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79359-2270" facs="A79359-001-a-2090">wee</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A79359-2280" facs="A79359-001-a-2100">have</w>
          <c> </c>
          <w lemma="cause" ana="#vvn" reg="caused" xml:id="A79359-2290" facs="A79359-001-a-2110">caused</w>
          <c> </c>
          <w lemma="these" ana="#d" reg="these" xml:id="A79359-2300" facs="A79359-001-a-2120">these</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79359-2310" facs="A79359-001-a-2130">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A79359-2320" facs="A79359-001-a-2140">be</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A79359-2330" facs="A79359-001-a-2150">published</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-2340" facs="A79359-001-a-2160">and</w>
          <c> </c>
          <w lemma="proclaim" ana="#vvn" reg="proclaimed" xml:id="A79359-2350" facs="A79359-001-a-2170">proclamed</w>
          <c> </c>
          <w lemma="throughout" ana="#acp-p" reg="throughout" xml:id="A79359-2360" facs="A79359-001-a-2180">throughout</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A79359-2370" facs="A79359-001-a-2190">al</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A79359-2380" facs="A79359-001-a-2200">Counties</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79359-2390" facs="A79359-001-a-2210">and</w>
          <c> </c>
          <w lemma="corporation" ana="#n2" reg="corporations" xml:id="A79359-2400" facs="A79359-001-a-2220">Corporations</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-2410" facs="A79359-001-a-2230">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A79359-2420" facs="A79359-001-a-2240">this</w>
          <c> </c>
          <w lemma="realm" ana="#n1" reg="realm" xml:id="A79359-2430" facs="A79359-001-a-2250">Realm</w>
          <pc xml:id="A79359-2440" facs="A79359-001-a-2260">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-2450" facs="A79359-001-a-2270">the</w>
          <c> </c>
          <w lemma="first" ana="#ord" reg="first" xml:id="A79359-2460" facs="A79359-001-a-2280">first</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A79359-2470" facs="A79359-001-a-2290">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-2480" facs="A79359-001-a-2300">of</w>
          <c> </c>
          <hi xml:id="A79359e-160">
            <w lemma="February" ana="#n1-nn" reg="February" xml:id="A79359-2490" facs="A79359-001-a-2310">February</w>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A79359-2500" facs="A79359-001-a-2320">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-2510" facs="A79359-001-a-2330">the</w>
          <c> </c>
          <w lemma="first" ana="#ord" reg="first" xml:id="A79359-2520" facs="A79359-001-a-2340">first</w>
          <c> </c>
          <w lemma="year" ana="#n1" reg="year" xml:id="A79359-2530" facs="A79359-001-a-2350">yeare</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79359-2540" facs="A79359-001-a-2360">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79359-2550" facs="A79359-001-a-2370">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1g" reg="majesty's" xml:id="A79359-2560" facs="A79359-001-a-2380">Maiesties</w>
          <c> </c>
          <w lemma="reign" ana="#n1" reg="reign" xml:id="A79359-2570" facs="A79359-001-a-2390">Raigne</w>
          <pc unit="sentence" xml:id="A79359-2580" facs="A79359-001-a-2400">.</pc>
        </p>
        <closer xml:id="A79359e-170">
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A79359-2610" facs="A79359-001-a-2410">God</w>
          <c> </c>
          <w lemma="save" ana="#acp-p" reg="save" xml:id="A79359-2620" facs="A79359-001-a-2420">save</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A79359-2630" facs="A79359-001-a-2430">KING</w>
          <c> </c>
          <w lemma="CHARLES" ana="#n1-nn" reg="Charles" xml:id="A79359-2640" facs="A79359-001-a-2440">CHARLS</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79359-2650" facs="A79359-001-a-2450">the</w>
          <c> </c>
          <w lemma="second" ana="#ord" reg="second" xml:id="A79359-2660" facs="A79359-001-a-2460">Second</w>
          <pc unit="sentence" xml:id="A79359-2670" facs="A79359-001-a-2470">.</pc>
        </closer>
      </div>
    </body>
    <back xml:id="A79359e-180">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>