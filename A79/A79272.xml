<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Most Reverend Father in God, We greet you well, being tender of our engagement to have a care for the reasonable satisfaction of the tenants and purchasers of church lands. ...</title>
        <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1660</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A79272</idno>
        <idno type="STC">Wing C3189</idno>
        <idno type="STC">Thomason 669.f.26[21]</idno>
        <idno type="STC">ESTC R210817</idno>
        <idno type="EEBO-CITATION">99869574</idno>
        <idno type="PROQUEST">99869574</idno>
        <idno type="VID">163903</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A79272)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163903)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f26[21])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Most Reverend Father in God, We greet you well, being tender of our engagement to have a care for the reasonable satisfaction of the tenants and purchasers of church lands. ...</title>
            <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
            <author>Charles II, King of England, 1630-1685.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1660]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from caption and first line of text.</note>
            <note>Imprint from from Wing.</note>
            <note>An order from the King, addressed to the Archbishop of Canterbury, concerning church lands.</note>
            <note>Dated at end: Given at our Court at White-hall the 13. of Octob. in the twelfth year of our Reign. By His Majesties command. E.N.</note>
            <note>Annotation on Thomason copy: "nou 16 1660".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Church lands -- England -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-06</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-07</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2008-07</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A79272e-10">
    <body xml:id="A79272e-20">
      <div type="letter" xml:id="A79272e-30">
        <pb facs="tcp:163903:1" rendition="simple:additions" xml:id="A79272-001-a"/>
        <opener xml:id="A79272e-40">
          <signed xml:id="A79272e-50">
            <w lemma="c." ana="#n-ab" reg="C." xml:id="A79272-0040" facs="A79272-001-a-0010">C.</w>
            <c> </c>
            <w lemma="r." ana="#n-ab" reg="r." xml:id="A79272-0050" facs="A79272-001-a-0020">R.</w>
            <pc unit="sentence" xml:id="A79272-0050-eos" facs="A79272-001-a-0030"/>
          </signed>
          <lb xml:id="A79272e-60"/>
          <salute xml:id="A79272e-70">
            <hi xml:id="A79272e-80">
              <w lemma="most" ana="#d-s" reg="most" xml:id="A79272-0080" facs="A79272-001-a-0040">Most</w>
            </hi>
            <c> </c>
            <w lemma="reverend" ana="#j" reg="reverend" xml:id="A79272-0090" facs="A79272-001-a-0050">Reverend</w>
            <c> </c>
            <hi xml:id="A79272e-90">
              <w lemma="father" ana="#n1" reg="father" xml:id="A79272-0100" facs="A79272-001-a-0060">Father</w>
              <c> </c>
              <w lemma="in" ana="#acp-p" reg="in" xml:id="A79272-0110" facs="A79272-001-a-0070">in</w>
              <c> </c>
              <w lemma="God" ana="#n1-nn" reg="God" xml:id="A79272-0120" facs="A79272-001-a-0080">God</w>
              <pc xml:id="A79272-0130" facs="A79272-001-a-0090">,</pc>
            </hi>
          </salute>
        </opener>
        <p xml:id="A79272e-100">
          <w lemma="we" ana="#pns" reg="We" rend="initialcharacterdecorated" xml:id="A79272-0160" facs="A79272-001-a-0100">WE</w>
          <c> </c>
          <w lemma="greet" ana="#vvb" reg="greet" xml:id="A79272-0170" facs="A79272-001-a-0110">greet</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A79272-0180" facs="A79272-001-a-0120">you</w>
          <c> </c>
          <w lemma="well" ana="#av" reg="well" xml:id="A79272-0190" facs="A79272-001-a-0130">well</w>
          <pc xml:id="A79272-0200" facs="A79272-001-a-0140">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A79272-0210" facs="A79272-001-a-0150">being</w>
          <c> </c>
          <w lemma="tender" ana="#j" reg="tender" xml:id="A79272-0220" facs="A79272-001-a-0160">tender</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-0230" facs="A79272-001-a-0170">of</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79272-0240" facs="A79272-001-a-0180">our</w>
          <c> </c>
          <w lemma="engagement" ana="#n1" reg="engagement" xml:id="A79272-0250" facs="A79272-001-a-0190">Engagement</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79272-0260" facs="A79272-001-a-0200">to</w>
          <c> </c>
          <w lemma="have" ana="#vvi" reg="have" xml:id="A79272-0270" facs="A79272-001-a-0210">have</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A79272-0280" facs="A79272-001-a-0220">a</w>
          <c> </c>
          <w lemma="care" ana="#n1" reg="care" xml:id="A79272-0290" facs="A79272-001-a-0230">care</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A79272-0300" facs="A79272-001-a-0240">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-0310" facs="A79272-001-a-0250">the</w>
          <c> </c>
          <w lemma="reasonable" ana="#j" reg="reasonable" xml:id="A79272-0320" facs="A79272-001-a-0260">reasonable</w>
          <c> </c>
          <w lemma="satisfaction" ana="#n1" reg="satisfaction" xml:id="A79272-0330" facs="A79272-001-a-0270">satisfaction</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-0340" facs="A79272-001-a-0280">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-0350" facs="A79272-001-a-0290">the</w>
          <c> </c>
          <w lemma="tenant" ana="#n2" reg="tenants" xml:id="A79272-0360" facs="A79272-001-a-0300">Tenants</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-0370" facs="A79272-001-a-0310">and</w>
          <c> </c>
          <w lemma="purchaser" ana="#n2" reg="purchasers" xml:id="A79272-0380" facs="A79272-001-a-0320">Purchasers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-0390" facs="A79272-001-a-0330">of</w>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A79272-0400" facs="A79272-001-a-0340">Church</w>
          <c> </c>
          <w lemma="land" ana="#n2" reg="lands" xml:id="A79272-0410" facs="A79272-001-a-0350">Lands</w>
          <pc unit="sentence" xml:id="A79272-0420" facs="A79272-001-a-0360">.</pc>
          <c> </c>
          <w lemma="our" ana="#po" reg="Our" xml:id="A79272-0430" facs="A79272-001-a-0370">Our</w>
          <c> </c>
          <w lemma="will" ana="#n1" reg="will" xml:id="A79272-0440" facs="A79272-001-a-0380">Will</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-0450" facs="A79272-001-a-0390">and</w>
          <c> </c>
          <w lemma="pleasure" ana="#n1" reg="pleasure" xml:id="A79272-0460" facs="A79272-001-a-0400">Pleasure</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A79272-0470" facs="A79272-001-a-0410">is</w>
          <pc xml:id="A79272-0480" facs="A79272-001-a-0420">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A79272-0490" facs="A79272-001-a-0430">That</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A79272-0500" facs="A79272-001-a-0440">you</w>
          <c> </c>
          <w lemma="give" ana="#vvb" reg="give" xml:id="A79272-0510" facs="A79272-001-a-0450">give</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A79272-0520" facs="A79272-001-a-0460">Order</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79272-0530" facs="A79272-001-a-0470">to</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A79272-0540" facs="A79272-001-a-0480">all</w>
          <c> </c>
          <hi xml:id="A79272e-110">
            <w lemma="bishop" ana="#n2" reg="Bishops" xml:id="A79272-0550" facs="A79272-001-a-0490">Bishops</w>
            <pc xml:id="A79272-0560" facs="A79272-001-a-0500">,</pc>
            <c> </c>
            <w lemma="dean" ana="#n2" reg="Deans" xml:id="A79272-0570" facs="A79272-001-a-0510">Deans</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-0580" facs="A79272-001-a-0520">and</w>
          <c> </c>
          <hi xml:id="A79272e-120">
            <w lemma="chapter" ana="#n2" reg="chapters" xml:id="A79272-0590" facs="A79272-001-a-0530">Chapters</w>
          </hi>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A79272-0600" facs="A79272-001-a-0540">within</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A79272-0610" facs="A79272-001-a-0550">your</w>
          <c> </c>
          <w lemma="province" ana="#n2" reg="provinces" xml:id="A79272-0620" facs="A79272-001-a-0560">Provinces</w>
          <pc xml:id="A79272-0630" facs="A79272-001-a-0570">:</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A79272-0640" facs="A79272-001-a-0580">That</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A79272-0650" facs="A79272-001-a-0590">in</w>
          <c> </c>
          <w lemma="let" ana="#vvg" reg="letting" xml:id="A79272-0660" facs="A79272-001-a-0600">letting</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-0670" facs="A79272-001-a-0610">the</w>
          <c> </c>
          <w lemma="land" ana="#n2" reg="lands" xml:id="A79272-0680" facs="A79272-001-a-0620">Lands</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-0690" facs="A79272-001-a-0630">and</w>
          <c> </c>
          <w lemma="revenue" ana="#n2" reg="revenues" xml:id="A79272-0700" facs="A79272-001-a-0640">Revenues</w>
          <c> </c>
          <w lemma="belong" ana="#vvg" reg="belonging" xml:id="A79272-0710" facs="A79272-001-a-0650">belonging</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79272-0720" facs="A79272-001-a-0660">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-0730" facs="A79272-001-a-0670">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A79272-0740" facs="A79272-001-a-0680">respective</w>
          <c> </c>
          <w lemma="church" ana="#n2" reg="churches" xml:id="A79272-0750" facs="A79272-001-a-0690">Churches</w>
          <pc xml:id="A79272-0760" facs="A79272-001-a-0700">,</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A79272-0770" facs="A79272-001-a-0710">they</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A79272-0780" facs="A79272-001-a-0720">have</w>
          <c> </c>
          <w lemma="regard" ana="#n1" reg="regard" xml:id="A79272-0790" facs="A79272-001-a-0730">regard</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79272-0800" facs="A79272-001-a-0740">to</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A79272-0810" facs="A79272-001-a-0750">such</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A79272-0820" facs="A79272-001-a-0760">as</w>
          <c> </c>
          <w lemma="be" ana="#vvd" reg="were" xml:id="A79272-0830" facs="A79272-001-a-0770">were</w>
          <c> </c>
          <w lemma="tenant" ana="#n2" reg="tenants" xml:id="A79272-0840" facs="A79272-001-a-0780">Tenants</w>
          <c> </c>
          <w lemma="before" ana="#acp-p" reg="before" xml:id="A79272-0850" facs="A79272-001-a-0790">before</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-0860" facs="A79272-001-a-0800">the</w>
          <c> </c>
          <w lemma="late" ana="#j" reg="late" xml:id="A79272-0870" facs="A79272-001-a-0810">late</w>
          <c> </c>
          <w lemma="trouble" ana="#n2" reg="troubles" xml:id="A79272-0880" facs="A79272-001-a-0820">Troubles</w>
          <pc xml:id="A79272-0890" facs="A79272-001-a-0830">:</pc>
          <c> </c>
          <w lemma="where" ana="#crq-cs" reg="where" xml:id="A79272-0900" facs="A79272-001-a-0840">Where</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A79272-0910" facs="A79272-001-a-0850">they</w>
          <c> </c>
          <w lemma="have" ana="#vvb" reg="have" xml:id="A79272-0920" facs="A79272-001-a-0860">have</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A79272-0930" facs="A79272-001-a-0870">not</w>
          <c> </c>
          <w lemma="part" ana="#vvn" reg="parted" xml:id="A79272-0940" facs="A79272-001-a-0880">parted</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A79272-0950" facs="A79272-001-a-0890">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A79272-0960" facs="A79272-001-a-0900">their</w>
          <c> </c>
          <w lemma="lease" ana="#n2" reg="leases" xml:id="A79272-0970" facs="A79272-001-a-0910">Leases</w>
          <pc unit="sentence" xml:id="A79272-0980" facs="A79272-001-a-0920">.</pc>
          <c> </c>
          <w lemma="give" ana="#vvg" reg="Giving" xml:id="A79272-0990" facs="A79272-001-a-0930">Giving</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A79272-1000" facs="A79272-001-a-0940">them</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A79272-1010" facs="A79272-001-a-0950">not</w>
          <c> </c>
          <w lemma="only" ana="#av_j" reg="only" xml:id="A79272-1020" facs="A79272-001-a-0960">only</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-1030" facs="A79272-001-a-0970">the</w>
          <c> </c>
          <w lemma="privilege" ana="#n1" reg="privilege" xml:id="A79272-1040" facs="A79272-001-a-0980">priviledge</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1050" facs="A79272-001-a-0990">of</w>
          <c> </c>
          <w lemma="preemption" ana="#n1" reg="preemption" xml:id="A79272-1060" facs="A79272-001-a-1000">preemption</w>
          <c> </c>
          <w lemma="before" ana="#acp-p" reg="before" xml:id="A79272-1070" facs="A79272-001-a-1010">before</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A79272-1080" facs="A79272-001-a-1020">any</w>
          <c> </c>
          <w lemma="other" ana="#pi_d" reg="other" xml:id="A79272-1090" facs="A79272-001-a-1030">other</w>
          <pc xml:id="A79272-1100" facs="A79272-001-a-1040">,</pc>
          <c> </c>
          <w lemma="but" ana="#acp-cc" reg="but" xml:id="A79272-1110" facs="A79272-001-a-1050">but</w>
          <c> </c>
          <w lemma="use" ana="#vvg" reg="using" xml:id="A79272-1120" facs="A79272-001-a-1060">using</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A79272-1130" facs="A79272-001-a-1070">them</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A79272-1140" facs="A79272-001-a-1080">with</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A79272-1150" facs="A79272-001-a-1090">all</w>
          <c> </c>
          <w lemma="favour" ana="#n1" reg="favour" xml:id="A79272-1160" facs="A79272-001-a-1100">favour</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-1170" facs="A79272-001-a-1110">and</w>
          <c> </c>
          <w lemma="kindness" ana="#n1" reg="kindness" xml:id="A79272-1180" facs="A79272-001-a-1120">kindness</w>
          <pc unit="sentence" xml:id="A79272-1190" facs="A79272-001-a-1130">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A79272-1200" facs="A79272-001-a-1140">And</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A79272-1210" facs="A79272-001-a-1150">you</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A79272-1220" facs="A79272-001-a-1160">are</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A79272-1230" facs="A79272-001-a-1170">forthwith</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79272-1240" facs="A79272-001-a-1180">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A79272-1250" facs="A79272-001-a-1190">give</w>
          <c> </c>
          <w lemma="direction" ana="#n2" reg="directions" xml:id="A79272-1260" facs="A79272-001-a-1200">directions</w>
          <pc xml:id="A79272-1270" facs="A79272-001-a-1210">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A79272-1280" facs="A79272-001-a-1220">that</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A79272-1290" facs="A79272-001-a-1230">no</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A79272-1300" facs="A79272-001-a-1240">such</w>
          <c> </c>
          <w lemma="ancient" ana="#j" reg="ancient" xml:id="A79272-1310" facs="A79272-001-a-1250">ancient</w>
          <c> </c>
          <w lemma="tenant" ana="#n1" reg="tenant" xml:id="A79272-1320" facs="A79272-001-a-1260">Tenant</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A79272-1330" facs="A79272-001-a-1270">be</w>
          <c> </c>
          <w lemma="put" ana="#vvn" reg="put" xml:id="A79272-1340" facs="A79272-001-a-1280">put</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A79272-1350" facs="A79272-001-a-1290">out</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1360" facs="A79272-001-a-1300">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79272-1370" facs="A79272-001-a-1310">his</w>
          <c> </c>
          <w lemma="possession" ana="#n1" reg="possession" xml:id="A79272-1380" facs="A79272-001-a-1320">possession</w>
          <pc xml:id="A79272-1390" facs="A79272-001-a-1330">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-1400" facs="A79272-001-a-1340">and</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A79272-1410" facs="A79272-001-a-1350">that</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A79272-1420" facs="A79272-001-a-1360">no</w>
          <c> </c>
          <w lemma="grant" ana="#n1" reg="grant" xml:id="A79272-1430" facs="A79272-001-a-1370">Grant</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1440" facs="A79272-001-a-1380">of</w>
          <c> </c>
          <w lemma="lease" ana="#n1" reg="lease" xml:id="A79272-1450" facs="A79272-001-a-1390">Lease</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A79272-1460" facs="A79272-001-a-1400">be</w>
          <c> </c>
          <w lemma="make" ana="#vvn" reg="made" xml:id="A79272-1470" facs="A79272-001-a-1410">made</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1480" facs="A79272-001-a-1420">of</w>
          <c> </c>
          <w lemma="thing" ana="#n2" reg="things" xml:id="A79272-1490" facs="A79272-001-a-1430">things</w>
          <c> </c>
          <w lemma="purchase" ana="#vvn" reg="purchased" xml:id="A79272-1500" facs="A79272-001-a-1440">purchased</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A79272-1510" facs="A79272-001-a-1450">by</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A79272-1520" facs="A79272-001-a-1460">any</w>
          <c> </c>
          <w lemma="officer" ana="#n1" reg="officer" xml:id="A79272-1530" facs="A79272-001-a-1470">Officer</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A79272-1540" facs="A79272-001-a-1480">or</w>
          <c> </c>
          <w lemma="soldier" ana="#n1" reg="soldier" xml:id="A79272-1550" facs="A79272-001-a-1490">Souldier</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1560" facs="A79272-001-a-1500">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-1570" facs="A79272-001-a-1510">the</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A79272-1580" facs="A79272-001-a-1520">Army</w>
          <pc xml:id="A79272-1590" facs="A79272-001-a-1530">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A79272-1600" facs="A79272-001-a-1540">and</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A79272-1610" facs="A79272-001-a-1550">others</w>
          <pc xml:id="A79272-1620" facs="A79272-001-a-1560">,</pc>
          <c> </c>
          <w lemma="unless" ana="#cs" reg="unless" xml:id="A79272-1630" facs="A79272-001-a-1570">unless</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A79272-1640" facs="A79272-001-a-1580">it</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A79272-1650" facs="A79272-001-a-1590">be</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A79272-1660" facs="A79272-001-a-1600">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-1670" facs="A79272-001-a-1610">the</w>
          <c> </c>
          <w lemma="purchaser" ana="#n1" reg="purchaser" xml:id="A79272-1680" facs="A79272-001-a-1620">Purchaser</w>
          <pc xml:id="A79272-1690" facs="A79272-001-a-1630">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A79272-1700" facs="A79272-001-a-1640">or</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A79272-1710" facs="A79272-001-a-1650">by</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A79272-1720" facs="A79272-001-a-1660">his</w>
          <c> </c>
          <w lemma="consent" ana="#n1" reg="consent" xml:id="A79272-1730" facs="A79272-001-a-1670">consent</w>
          <pc xml:id="A79272-1740" facs="A79272-001-a-1680">,</pc>
          <c> </c>
          <w lemma="until" ana="#acp-cs" reg="until" xml:id="A79272-1750" facs="A79272-001-a-1690">untill</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79272-1760" facs="A79272-001-a-1700">we</w>
          <c> </c>
          <w lemma="take" ana="#vvb" reg="take" xml:id="A79272-1770" facs="A79272-001-a-1710">take</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A79272-1780" facs="A79272-001-a-1720">further</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A79272-1790" facs="A79272-001-a-1730">Order</w>
          <pc xml:id="A79272-1800" facs="A79272-001-a-1740">,</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A79272-1810" facs="A79272-001-a-1750">which</w>
          <c> </c>
          <w lemma="we" ana="#pns" reg="we" xml:id="A79272-1820" facs="A79272-001-a-1760">we</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A79272-1830" facs="A79272-001-a-1770">shall</w>
          <c> </c>
          <w lemma="do" ana="#vvi" reg="do" xml:id="A79272-1840" facs="A79272-001-a-1780">do</w>
          <c> </c>
          <w lemma="speedy" ana="#av_j" reg="speedily" xml:id="A79272-1850" facs="A79272-001-a-1790">speedily</w>
          <pc xml:id="A79272-1860" facs="A79272-001-a-1800">,</pc>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A79272-1870" facs="A79272-001-a-1810">it</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A79272-1880" facs="A79272-001-a-1820">being</w>
          <c> </c>
          <w lemma="our" ana="#po" reg="our" xml:id="A79272-1890" facs="A79272-001-a-1830">our</w>
          <c> </c>
          <w lemma="intention" ana="#n1" reg="intention" xml:id="A79272-1900" facs="A79272-001-a-1840">intention</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A79272-1910" facs="A79272-001-a-1850">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A79272-1920" facs="A79272-001-a-1860">be</w>
          <c> </c>
          <w lemma="careful" ana="#j" reg="careful" xml:id="A79272-1930" facs="A79272-001-a-1870">carefull</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-1940" facs="A79272-001-a-1880">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-1950" facs="A79272-001-a-1890">the</w>
          <c> </c>
          <w lemma="church" ana="#n1g" reg="church's" xml:id="A79272-1960" facs="A79272-001-a-1900">Churches</w>
          <c> </c>
          <w lemma="interest" ana="#n1" reg="interest" xml:id="A79272-1970" facs="A79272-001-a-1910">Interest</w>
          <pc unit="sentence" xml:id="A79272-1980" facs="A79272-001-a-1920">.</pc>
        </p>
        <closer xml:id="A79272e-130">
          <dateline xml:id="A79272e-140">
            <hi xml:id="A79272e-150">
              <w lemma="give" ana="#vvn" reg="given" xml:id="A79272-2020" facs="A79272-001-a-1930">Given</w>
              <c> </c>
              <w lemma="at" ana="#acp-p" reg="at" xml:id="A79272-2030" facs="A79272-001-a-1940">at</w>
              <c> </c>
              <w lemma="our" ana="#po" reg="our" xml:id="A79272-2040" facs="A79272-001-a-1950">our</w>
              <c> </c>
              <w lemma="court" ana="#n1" reg="court" xml:id="A79272-2050" facs="A79272-001-a-1960">Court</w>
              <c> </c>
              <w lemma="at" ana="#acp-p" reg="at" xml:id="A79272-2060" facs="A79272-001-a-1970">at</w>
            </hi>
            <c> </c>
            <w lemma="Whitehall" ana="#n1-nn" reg="Whitehall" xml:id="A79272-2070" facs="A79272-001-a-1980">White-hall</w>
            <date xml:id="A79272e-160">
              <hi xml:id="A79272e-170">
                <w lemma="the" ana="#d" reg="the" xml:id="A79272-2090" facs="A79272-001-a-1990">the</w>
              </hi>
              <c> </c>
              <w lemma="13." ana="#crd" reg="13." xml:id="A79272-2100" facs="A79272-001-a-2000">13.</w>
              <c> </c>
              <hi xml:id="A79272e-180">
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-2110" facs="A79272-001-a-2010">of</w>
              </hi>
              <c> </c>
              <w lemma="octob." ana="#n-ab" reg="octob." xml:id="A79272-2120" facs="A79272-001-a-2020">Octob.</w>
              <c> </c>
              <hi xml:id="A79272e-190">
                <w lemma="in" ana="#acp-p" reg="in" xml:id="A79272-2130" facs="A79272-001-a-2030">in</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A79272-2140" facs="A79272-001-a-2040">the</w>
                <c> </c>
                <w lemma="twelve" ana="#ord" reg="twelfth" xml:id="A79272-2150" facs="A79272-001-a-2050">twelfth</w>
                <c> </c>
                <w lemma="year" ana="#n1" reg="year" xml:id="A79272-2160" facs="A79272-001-a-2060">year</w>
                <c> </c>
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-2170" facs="A79272-001-a-2070">of</w>
                <c> </c>
                <w lemma="our" ana="#po" reg="our" xml:id="A79272-2180" facs="A79272-001-a-2080">our</w>
                <c> </c>
                <w lemma="reign" ana="#n1" reg="reign" xml:id="A79272-2190" facs="A79272-001-a-2090">Reign</w>
                <pc unit="sentence" xml:id="A79272-2200" facs="A79272-001-a-2100">.</pc>
                <c> </c>
              </hi>
            </date>
          </dateline>
          <signed xml:id="A79272e-200">
            <w lemma="by" ana="#acp-p" reg="By" xml:id="A79272-2230" facs="A79272-001-a-2110">By</w>
            <c> </c>
            <w lemma="his" ana="#po" reg="his" xml:id="A79272-2240" facs="A79272-001-a-2120">His</w>
            <c> </c>
            <w lemma="majesty" ana="#n1g" reg="majesty's" xml:id="A79272-2250" facs="A79272-001-a-2130">Majesties</w>
            <c> </c>
            <w lemma="command" ana="#n1" reg="command" xml:id="A79272-2260" facs="A79272-001-a-2140">Command</w>
            <pc unit="sentence" xml:id="A79272-2270" facs="A79272-001-a-2150">.</pc>
            <c> </c>
            <hi xml:id="A79272e-210">
              <w lemma="e." ana="#n-ab" reg="e." xml:id="A79272-2280" facs="A79272-001-a-2160">E.</w>
              <c> </c>
              <w lemma="n." ana="#n-ab" reg="n." xml:id="A79272-2290" facs="A79272-001-a-2170">N.</w>
              <pc unit="sentence" xml:id="A79272-2290-eos" facs="A79272-001-a-2180"/>
              <c> </c>
            </hi>
          </signed>
        </closer>
        <trailer xml:id="A79272e-220">
          <w lemma="to" ana="#acp-p" reg="To" xml:id="A79272-2320" facs="A79272-001-a-2190">To</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A79272-2330" facs="A79272-001-a-2200">the</w>
          <c> </c>
          <w lemma="most" ana="#av-s_d" reg="most" xml:id="A79272-2340" facs="A79272-001-a-2210">Most</w>
          <c> </c>
          <w lemma="reverend" ana="#j" reg="reverend" xml:id="A79272-2350" facs="A79272-001-a-2220">Reverend</w>
          <c> </c>
          <w lemma="father" ana="#n1" reg="father" xml:id="A79272-2360" facs="A79272-001-a-2230">Father</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A79272-2370" facs="A79272-001-a-2240">in</w>
          <c> </c>
          <w lemma="God" ana="#n1-nn" reg="God" xml:id="A79272-2380" facs="A79272-001-a-2250">God</w>
          <c> </c>
          <hi xml:id="A79272e-230">
            <w lemma="William" ana="#n1-nn" reg="William" xml:id="A79272-2390" facs="A79272-001-a-2260">William</w>
          </hi>
          <c> </c>
          <w lemma="archbishop" ana="#n1" reg="Archbishop" xml:id="A79272-2400" facs="A79272-001-a-2270">Archbishop</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A79272-2410" facs="A79272-001-a-2280">of</w>
          <c> </c>
          <hi xml:id="A79272e-240">
            <w lemma="Canterbury" ana="#n1-nn" reg="Canterbury" xml:id="A79272-2420" facs="A79272-001-a-2290">Canterbury</w>
            <pc unit="sentence" xml:id="A79272-2430" facs="A79272-001-a-2300">.</pc>
            <c> </c>
          </hi>
        </trailer>
      </div>
    </body>
    <back xml:id="A79272e-250">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>