<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Sir, You are desired to send in your horse &amp; armes required of you by warrant from the Militia of London, to the new artillary-ground, upon Monday next being the nine and twentieth of this instant Iuly, by eight of the clock in the forenoone. Dated this 26 of Iuly 1650. Your reall freind[sic] Jacob Strange.</title>
        <author>Strange, Jacob.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1650</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A93957</idno>
        <idno type="STC">Wing S5809</idno>
        <idno type="STC">Thomason E608_14</idno>
        <idno type="STC">ESTC R205913</idno>
        <idno type="EEBO-CITATION">99865141</idno>
        <idno type="PROQUEST">99865141</idno>
        <idno type="VID">165699</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A93957)</note>
        <note>Transcribed from: (Early English Books Online ; image set 165699)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 93:E608[14])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Sir, You are desired to send in your horse &amp; armes required of you by warrant from the Militia of London, to the new artillary-ground, upon Monday next being the nine and twentieth of this instant Iuly, by eight of the clock in the forenoone. Dated this 26 of Iuly 1650. Your reall freind[sic] Jacob Strange.</title>
            <author>Strange, Jacob.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1650]</date>
          </publicationStmt>
          <notesStmt>
            <note>A form sent to George Thomason and Philemon Stevens by the Militia of London.</note>
            <note>Date and place of publication from Wing.</note>
            <note>Annotation on Thomason copy: "vide 4th June 1650 in ye follio collection.".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>City of London (England). -- Committee for the Militia.</term>
          <term>London (England) -- History, Military -- 17th century.</term>
          <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-06</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-07</date><label>Robyn Anspach</label>
        Sampled and proofread
      </change>
      <change><date>2007-07</date><label>Robyn Anspach</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A93957e-10">
    <body xml:id="A93957e-20">
      <div type="letter" xml:id="A93957e-30">
        <pb facs="tcp:165699:1" rendition="simple:additions" xml:id="A93957-001-a"/>
        <opener xml:id="A93957e-40">
          <salute xml:id="A93957e-50">
            <w lemma="sir" ana="#n1" reg="Sir" xml:id="A93957-040" facs="A93957-001-a-0010">SIR</w>
            <pc xml:id="A93957-050" facs="A93957-001-a-0020">,</pc>
          </salute>
        </opener>
        <p xml:id="A93957e-60">
          <hi xml:id="A93957e-70">
            <w lemma="you" ana="#pn" reg="you" rend="initialcharacterdecorated" xml:id="A93957-080" facs="A93957-001-a-0030">YOU</w>
          </hi>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A93957-090" facs="A93957-001-a-0040">are</w>
          <c> </c>
          <w lemma="desire" ana="#vvn" reg="desired" xml:id="A93957-100" facs="A93957-001-a-0050">desired</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A93957-110" facs="A93957-001-a-0060">to</w>
          <c> </c>
          <w lemma="send" ana="#vvi" reg="send" xml:id="A93957-120" facs="A93957-001-a-0070">send</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A93957-130" facs="A93957-001-a-0080">in</w>
          <c> </c>
          <w lemma="your" ana="#po" reg="your" xml:id="A93957-140" facs="A93957-001-a-0090">your</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A93957-150" facs="A93957-001-a-0100">Horse</w>
          <c> </c>
          <pc xml:id="A93957-160" facs="A93957-001-a-0110">&amp;</pc>
          <c> </c>
          <w lemma="arm" ana="#n2" reg="arms" xml:id="A93957-170" facs="A93957-001-a-0120">Armes</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A93957-180" facs="A93957-001-a-0130">required</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A93957-190" facs="A93957-001-a-0140">of</w>
          <c> </c>
          <w lemma="you" ana="#pn" reg="you" xml:id="A93957-200" facs="A93957-001-a-0150">you</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A93957-210" facs="A93957-001-a-0160">by</w>
          <c> </c>
          <w lemma="warrant" ana="#n1" reg="warrant" xml:id="A93957-220" facs="A93957-001-a-0170">warrant</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A93957-230" facs="A93957-001-a-0180">from</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A93957-240" facs="A93957-001-a-0190">the</w>
          <c> </c>
          <hi xml:id="A93957e-80">
            <w lemma="militia" ana="#n1" reg="militia" xml:id="A93957-250" facs="A93957-001-a-0200">Militia</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A93957-260" facs="A93957-001-a-0210">of</w>
          <c> </c>
          <hi xml:id="A93957e-90">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A93957-270" facs="A93957-001-a-0220">London</w>
            <pc xml:id="A93957-280" facs="A93957-001-a-0230">,</pc>
          </hi>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A93957-290" facs="A93957-001-a-0240">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A93957-300" facs="A93957-001-a-0250">the</w>
          <c> </c>
          <w lemma="new" ana="#j" reg="new" xml:id="A93957-310" facs="A93957-001-a-0260">new</w>
          <c> </c>
          <w lemma="artillery-ground" ana="#n1" reg="artillery-ground" xml:id="A93957-320" facs="A93957-001-a-0270">Artillary-Ground</w>
          <pc xml:id="A93957-330" facs="A93957-001-a-0280">,</pc>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A93957-340" facs="A93957-001-a-0290">upon</w>
          <c> </c>
          <w lemma="Monday" ana="#n1-nn" reg="Monday" xml:id="A93957-350" facs="A93957-001-a-0300">Monday</w>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A93957-360" facs="A93957-001-a-0310">next</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A93957-370" facs="A93957-001-a-0320">being</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A93957-380" facs="A93957-001-a-0330">the</w>
          <c> </c>
          <w lemma="nine" ana="#crd" reg="nine" xml:id="A93957-390" facs="A93957-001-a-0340">nine</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A93957-400" facs="A93957-001-a-0350">and</w>
          <c> </c>
          <w lemma="twenty" ana="#ord" reg="twentieth" xml:id="A93957-410" facs="A93957-001-a-0360">twentieth</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A93957-420" facs="A93957-001-a-0370">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A93957-430" facs="A93957-001-a-0380">this</w>
          <c> </c>
          <w lemma="instant" ana="#j" reg="instant" xml:id="A93957-440" facs="A93957-001-a-0390">instant</w>
          <c> </c>
          <hi xml:id="A93957e-100">
            <w lemma="july" ana="#n1-nn" reg="July" xml:id="A93957-450" facs="A93957-001-a-0400">Iuly</w>
            <pc xml:id="A93957-460" facs="A93957-001-a-0410">,</pc>
          </hi>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A93957-470" facs="A93957-001-a-0420">by</w>
          <c> </c>
          <w lemma="eight" ana="#crd" reg="eight" xml:id="A93957-480" facs="A93957-001-a-0430">eight</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A93957-490" facs="A93957-001-a-0440">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A93957-500" facs="A93957-001-a-0450">the</w>
          <c> </c>
          <w lemma="clock" ana="#n1" reg="clock" xml:id="A93957-510" facs="A93957-001-a-0460">clock</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A93957-520" facs="A93957-001-a-0470">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A93957-530" facs="A93957-001-a-0480">the</w>
          <c> </c>
          <w lemma="forenoon" ana="#n1" reg="forenoon" xml:id="A93957-540" facs="A93957-001-a-0490">forenoone</w>
          <pc unit="sentence" xml:id="A93957-550" facs="A93957-001-a-0500">.</pc>
        </p>
        <closer xml:id="A93957e-110">
          <dateline xml:id="A93957e-120">
            <w lemma="date" ana="#j_vn" reg="dated" xml:id="A93957-590" facs="A93957-001-a-0510">Dated</w>
            <date xml:id="A93957e-130">
              <w lemma="this" ana="#d" reg="this" xml:id="A93957-600" facs="A93957-001-a-0520">this</w>
              <c> </c>
              <w lemma="26" ana="#crd" reg="26" xml:id="A93957-610" facs="A93957-001-a-0530">26</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A93957-620" facs="A93957-001-a-0540">of</w>
              <c> </c>
              <hi xml:id="A93957e-140">
                <w lemma="july" ana="#n1-nn" reg="July" xml:id="A93957-630" facs="A93957-001-a-0550">Iuly</w>
              </hi>
              <c> </c>
              <w lemma="1650." ana="#crd" reg="1650." xml:id="A93957-640" facs="A93957-001-a-0560">1650.</w>
              <pc unit="sentence" xml:id="A93957-640-eos" facs="A93957-001-a-0570"/>
            </date>
          </dateline>
          <signed xml:id="A93957e-150">
            <w lemma="your" ana="#po" reg="Your" xml:id="A93957-670" facs="A93957-001-a-0580">Your</w>
            <c> </c>
            <w lemma="real" ana="#j" reg="real" xml:id="A93957-680" facs="A93957-001-a-0590">reall</w>
            <c> </c>
            <w lemma="friend" ana="#n1" reg="friend" xml:id="A93957-690" facs="A93957-001-a-0600">freind</w>
            <c> </c>
            <hi xml:id="A93957e-160">
              <w lemma="Jacob" ana="#n1-nn" reg="Jacob" xml:id="A93957-700" facs="A93957-001-a-0610">Jacob</w>
              <c> </c>
              <w lemma="strange" ana="#j" reg="strange" xml:id="A93957-710" facs="A93957-001-a-0620">Strange</w>
              <pc unit="sentence" xml:id="A93957-720" facs="A93957-001-a-0630">.</pc>
              <c> </c>
            </hi>
          </signed>
        </closer>
        <trailer xml:id="A93957e-170">
          <w lemma="to" ana="#acp-cs" reg="To" xml:id="A93957-750" facs="A93957-001-a-0640">To</w>
          <c> </c>
          <w lemma="mr." ana="#n-ab" reg="Mr." xml:id="A93957-760" facs="A93957-001-a-0650">Mr.</w>
          <c> </c>
          <pc xml:id="A93957-770" facs="A93957-001-a-0660">_____</pc>
        </trailer>
      </div>
    </body>
    <back xml:id="A93957e-180">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>