<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>At the counsell at White-Hall ordered by His Highness with the consent of his counsel, that the commissioners for the excise ...</title>
        <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1654</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A35101</idno>
        <idno type="STC">Wing C7124</idno>
        <idno type="STC">ESTC R37694</idno>
        <idno type="EEBO-CITATION">17002638</idno>
        <idno type="OCLC">ocm 17002638</idno>
        <idno type="VID">105710</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A35101)</note>
        <note>Transcribed from: (Early English Books Online ; image set 105710)</note>
        <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1613:3)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>At the counsell at White-Hall ordered by His Highness with the consent of his counsel, that the commissioners for the excise ...</title>
            <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
            <author>Cromwell, Oliver, 1599-1658.</author>
          </titleStmt>
          <extent>1 broadside.</extent>
          <publicationStmt>
            <publisher>s.n.],</publisher>
            <pubPlace>[S.l. :</pubPlace>
            <date>1654.</date>
          </publicationStmt>
          <notesStmt>
            <note>"Thursday 11th of May, 1654."</note>
            <note>"Ex. Wm. Iessop, clerk of the Counsell."</note>
            <note>Reproduction of original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Taxation -- England.</term>
          <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2009-01</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
      <change><date>2009-01</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A35101e-10">
    <body xml:id="A35101e-20">
      <div type="text" xml:id="A35101e-30">
        <pb facs="tcp:105710:1" rendition="simple:additions" xml:id="A35101-001-a"/>
        <opener xml:id="A35101e-40">
          <dateline xml:id="A35101e-50">
            <date xml:id="A35101e-60">
              <w lemma="thursday" ana="#n1-nn" reg="Thursday" xml:id="A35101-0050" facs="A35101-001-a-0010">Thursday</w>
              <c> </c>
              <w lemma="11th" part="I" ana="#ord" reg="11th" xml:id="A35101-0060.1" facs="A35101-001-a-0020" rendition="hi-mid-3">11th</w>
              <hi rend="sup" xml:id="A35101e-70"/>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0070" facs="A35101-001-a-0040">of</w>
              <c> </c>
              <w lemma="May" ana="#n1-nn" reg="May" xml:id="A35101-0080" facs="A35101-001-a-0050">May</w>
              <pc xml:id="A35101-0090" facs="A35101-001-a-0060">,</pc>
              <c> </c>
              <w lemma="1654." ana="#crd" reg="1654." xml:id="A35101-0100" facs="A35101-001-a-0070">1654</w>
            </date>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A35101-0120" facs="A35101-001-a-0080">At</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A35101-0130" facs="A35101-001-a-0090">the</w>
            <c> </c>
            <w lemma="counsel" ana="#n1" reg="counsel" xml:id="A35101-0140" facs="A35101-001-a-0100">Counsell</w>
            <c> </c>
            <w lemma="at" ana="#acp-p" reg="at" xml:id="A35101-0150" facs="A35101-001-a-0110">at</w>
            <c> </c>
            <w lemma="White-hall" ana="#n1-nn" reg="White-hall" xml:id="A35101-0160" facs="A35101-001-a-0120">White-Hall</w>
            <pc unit="sentence" xml:id="A35101-0170" facs="A35101-001-a-0130">.</pc>
          </dateline>
        </opener>
        <p xml:id="A35101e-80">
          <w lemma="order" ana="#j_vn" reg="Ordered" rend="initialcharacterdecorated" xml:id="A35101-0200" facs="A35101-001-a-0140">ORdered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A35101-0210" facs="A35101-001-a-0150">by</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A35101-0220" facs="A35101-001-a-0160">his</w>
          <c> </c>
          <w lemma="highness" ana="#n1" reg="highness" xml:id="A35101-0230" facs="A35101-001-a-0170">Highness</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A35101-0240" facs="A35101-001-a-0180">with</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0250" facs="A35101-001-a-0190">the</w>
          <c> </c>
          <w lemma="consent" ana="#n1" reg="consent" xml:id="A35101-0260" facs="A35101-001-a-0200">consent</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0270" facs="A35101-001-a-0210">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A35101-0280" facs="A35101-001-a-0220">his</w>
          <c> </c>
          <w lemma="counsel" ana="#n1" reg="counsel" xml:id="A35101-0290" facs="A35101-001-a-0230">Counsel</w>
          <pc xml:id="A35101-0300" facs="A35101-001-a-0240">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A35101-0310" facs="A35101-001-a-0250">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0320" facs="A35101-001-a-0260">the</w>
          <c> </c>
          <w lemma="commissioner" ana="#n2" reg="commissioners" xml:id="A35101-0330" facs="A35101-001-a-0270">Commissioners</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A35101-0340" facs="A35101-001-a-0280">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0350" facs="A35101-001-a-0290">the</w>
          <c> </c>
          <w lemma="excise" ana="#n1" reg="excise" xml:id="A35101-0360" facs="A35101-001-a-0300">Excise</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A35101-0370" facs="A35101-001-a-0310">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0380" facs="A35101-001-a-0320">the</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A35101-0390" facs="A35101-001-a-0330">time</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A35101-0400" facs="A35101-001-a-0340">being</w>
          <pc xml:id="A35101-0410" facs="A35101-001-a-0350">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A35101-0420" facs="A35101-001-a-0360">and</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A35101-0430" facs="A35101-001-a-0370">their</w>
          <c> </c>
          <w lemma="subcommissioner" ana="#n2" reg="subcommissioners" xml:id="A35101-0440" facs="A35101-001-a-0380">Sub-Commissioners</w>
          <pc xml:id="A35101-0450" facs="A35101-001-a-0390">,</pc>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A35101-0460" facs="A35101-001-a-0400">doe</w>
          <c> </c>
          <w lemma="demand" ana="#n1" reg="demand" xml:id="A35101-0470" facs="A35101-001-a-0410">demand</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A35101-0480" facs="A35101-001-a-0420">and</w>
          <c> </c>
          <w lemma="receive" ana="#vvi" reg="receive" xml:id="A35101-0490" facs="A35101-001-a-0430">receive</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A35101-0500" facs="A35101-001-a-0440">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0510" facs="A35101-001-a-0450">the</w>
          <c> </c>
          <w lemma="duty" ana="#n1" reg="duty" xml:id="A35101-0520" facs="A35101-001-a-0460">duty</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0530" facs="A35101-001-a-0470">of</w>
          <c> </c>
          <w lemma="excise" ana="#n1" reg="excise" xml:id="A35101-0540" facs="A35101-001-a-0480">Excise</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A35101-0550" facs="A35101-001-a-0490">from</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A35101-0560" facs="A35101-001-a-0500">and</w>
          <c> </c>
          <w lemma="after" ana="#acp-p" reg="after" xml:id="A35101-0570" facs="A35101-001-a-0510">after</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0580" facs="A35101-001-a-0520">the</w>
          <c> </c>
          <w lemma="25" part="I" ana="#ord" reg="25th" xml:id="A35101-0590.1" facs="A35101-001-a-0530" rendition="hi-mid-3">25th</w>
          <hi rend="sup" xml:id="A35101e-90"/>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0600" facs="A35101-001-a-0550">of</w>
          <c> </c>
          <hi xml:id="A35101e-100">
            <w lemma="march" ana="#n1" reg="March" xml:id="A35101-0610" facs="A35101-001-a-0560">March</w>
          </hi>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A35101-0620" facs="A35101-001-a-0570">last</w>
          <pc xml:id="A35101-0630" facs="A35101-001-a-0580">,</pc>
          <c> </c>
          <w lemma="six" ana="#crd" reg="six" xml:id="A35101-0640" facs="A35101-001-a-0590">six</w>
          <c> </c>
          <w lemma="penny" ana="#n2" reg="pence" xml:id="A35101-0650" facs="A35101-001-a-0600">pence</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A35101-0660" facs="A35101-001-a-0610">upon</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A35101-0670" facs="A35101-001-a-0620">every</w>
          <c> </c>
          <w lemma="hundred" ana="#crd" reg="hundred" xml:id="A35101-0680" facs="A35101-001-a-0630">hundred</w>
          <c> </c>
          <w lemma="weight" ana="#n1" reg="weight" xml:id="A35101-0690" facs="A35101-001-a-0640">weight</w>
          <c> </c>
          <w lemma="contain" ana="#vvg" reg="containing" xml:id="A35101-0700" facs="A35101-001-a-0650">conteining</w>
          <c> </c>
          <w lemma="one" ana="#crd" reg="one" xml:id="A35101-0710" facs="A35101-001-a-0660">one</w>
          <c> </c>
          <w lemma="hundred" ana="#crd" reg="hundred" xml:id="A35101-0720" facs="A35101-001-a-0670">hundred</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A35101-0730" facs="A35101-001-a-0680">and</w>
          <c> </c>
          <w lemma="twelve" ana="#crd" reg="twelve" xml:id="A35101-0740" facs="A35101-001-a-0690">twelve</w>
          <c> </c>
          <w lemma="pound" ana="#n1" reg="pound" xml:id="A35101-0750" facs="A35101-001-a-0700">pound</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0760" facs="A35101-001-a-0710">of</w>
          <c> </c>
          <w lemma="iron" ana="#n1" reg="iron" xml:id="A35101-0770" facs="A35101-001-a-0720">Iron</w>
          <c> </c>
          <w lemma="English" ana="#j-nn" reg="English" xml:id="A35101-0780" facs="A35101-001-a-0730">English</w>
          <pc xml:id="A35101-0790" facs="A35101-001-a-0740">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A35101-0800" facs="A35101-001-a-0750">and</w>
          <c> </c>
          <w lemma="no" ana="#d-x" reg="no" xml:id="A35101-0810" facs="A35101-001-a-0760">no</w>
          <c> </c>
          <w lemma="more" ana="#av-c_d" reg="more" xml:id="A35101-0820" facs="A35101-001-a-0770">more</w>
          <pc xml:id="A35101-0830" facs="A35101-001-a-0780">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A35101-0840" facs="A35101-001-a-0790">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A35101-0850" facs="A35101-001-a-0800">be</w>
          <c> </c>
          <w lemma="pay" ana="#vvn" reg="paid" xml:id="A35101-0860" facs="A35101-001-a-0810">paid</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A35101-0870" facs="A35101-001-a-0820">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0880" facs="A35101-001-a-0830">the</w>
          <c> </c>
          <w lemma="maker" ana="#n1" reg="maker" xml:id="A35101-0890" facs="A35101-001-a-0840">Maker</w>
          <pc xml:id="A35101-0900" facs="A35101-001-a-0850">;</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0910" facs="A35101-001-a-0860">the</w>
          <c> </c>
          <w lemma="rate" ana="#n1" reg="rate" xml:id="A35101-0920" facs="A35101-001-a-0870">rate</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A35101-0930" facs="A35101-001-a-0880">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0940" facs="A35101-001-a-0890">the</w>
          <c> </c>
          <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A35101-0950" facs="A35101-001-a-0900">Ordinance</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0960" facs="A35101-001-a-0910">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A35101-0970" facs="A35101-001-a-0920">the</w>
          <c> </c>
          <w lemma="17th" part="I" ana="#ord" reg="17th" xml:id="A35101-0980.1" facs="A35101-001-a-0930" rendition="hi-mid-3">17th</w>
          <hi rend="sup" xml:id="A35101e-110"/>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A35101-0990" facs="A35101-001-a-0950">of</w>
          <c> </c>
          <hi xml:id="A35101e-120">
            <w lemma="march" ana="#n1" reg="March" xml:id="A35101-1000" facs="A35101-001-a-0960">March</w>
            <pc xml:id="A35101-1010" facs="A35101-001-a-0970">,</pc>
          </hi>
          <c> </c>
          <w lemma="1653." ana="#crd" reg="1653." xml:id="A35101-1020" facs="A35101-001-a-0980">1653.</w>
          <c> </c>
          <w lemma="notwithstanding" ana="#acp-p" reg="notwithstanding" xml:id="A35101-1030" facs="A35101-001-a-0990">notwithstanding</w>
          <pc unit="sentence" xml:id="A35101-1040" facs="A35101-001-a-1000">.</pc>
        </p>
        <closer xml:id="A35101e-130">
          <signed xml:id="A35101e-140">
            <hi xml:id="A35101e-150">
              <w lemma="ex." ana="#n-ab" reg="ex." xml:id="A35101-1080" facs="A35101-001-a-1010">Ex.</w>
              <c> </c>
              <w lemma="Wm." part="I" ana="#n1-nn" reg="Wm." xml:id="A35101-1090.1" facs="A35101-001-a-1020">Wm Iessop Clerk of the Councell </w>
              <hi rend="sup" xml:id="A35101e-160"/>
              <pc part="F" xml:id="A35101-1090.3" facs="A35101-001-a-1040">.</pc>
            </hi>
            <hi xml:id="A35101e-170">
              <pc unit="sentence" xml:id="A35101-1150" facs="A35101-001-a-1100">.</pc>
            </hi>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A35101e-180">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>