<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Vicessimo nono Julij, 1645. By the Committee of Grocers-Hall for Irish affaires.</title>
        <author>Committee of Adventurers in London for Lands in Ireland.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1645</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A78078</idno>
        <idno type="STC">Wing B6361</idno>
        <idno type="STC">Thomason E294_7</idno>
        <idno type="STC">ESTC R200180</idno>
        <idno type="EEBO-CITATION">99860985</idno>
        <idno type="PROQUEST">99860985</idno>
        <idno type="VID">113112</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A78078)</note>
        <note>Transcribed from: (Early English Books Online ; image set 113112)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 48:E294[7])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Vicessimo nono Julij, 1645. By the Committee of Grocers-Hall for Irish affaires.</title>
            <author>Committee of Adventurers in London for Lands in Ireland.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1645]</date>
          </publicationStmt>
          <notesStmt>
            <note>Place and date of publication from Wing.</note>
            <note>Calls for a meeting to be held "Thursday next".</note>
            <note>Annotation on Thomason copy: "an order".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Committee of Adventurers in London for Lands in Ireland. -- Early works to 1800.</term>
          <term>Ireland -- History -- 1625-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A78078e-10">
    <body xml:id="A78078e-20">
      <div type="text" xml:id="A78078e-30">
        <pb facs="tcp:113112:1" rendition="simple:additions" xml:id="A78078-001-a"/>
        <head xml:id="A78078e-40">
          <hi xml:id="A78078e-50">
            <w lemma="vicessimo" ana="#fw-la" reg="vicessimo" xml:id="A78078-0030" facs="A78078-001-a-0010">Vicessimo</w>
            <c> </c>
            <w lemma="nono" ana="#fw-la" reg="nono" xml:id="A78078-0040" facs="A78078-001-a-0020">nono</w>
            <c> </c>
            <w lemma="julij" ana="#fw-la" reg="julij" xml:id="A78078-0050" facs="A78078-001-a-0030">Julij</w>
            <pc xml:id="A78078-0060" facs="A78078-001-a-0040">,</pc>
          </hi>
          <c> </c>
          <w lemma="1645." ana="#crd" reg="1645." xml:id="A78078-0070" facs="A78078-001-a-0050">1645.</w>
          <pc unit="sentence" xml:id="A78078-0070-eos" facs="A78078-001-a-0060"/>
        </head>
        <head xml:id="A78078e-60">
          <w lemma="by" ana="#acp-p" reg="By" xml:id="A78078-0100" facs="A78078-001-a-0070">By</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-0110" facs="A78078-001-a-0080">the</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A78078-0120" facs="A78078-001-a-0090">Committee</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-0130" facs="A78078-001-a-0100">of</w>
          <c> </c>
          <w lemma="grocers-hall" ana="#n1-nn" reg="Grocers-hall" xml:id="A78078-0140" facs="A78078-001-a-0110">Grocers-Hall</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-0150" facs="A78078-001-a-0120">for</w>
          <c> </c>
          <w lemma="irish" ana="#j-nn" reg="Irish" xml:id="A78078-0160" facs="A78078-001-a-0130">Irish</w>
          <c> </c>
          <w lemma="affair" ana="#n2" reg="affairs" xml:id="A78078-0170" facs="A78078-001-a-0140">affaires</w>
          <pc unit="sentence" xml:id="A78078-0180" facs="A78078-001-a-0150">.</pc>
        </head>
        <p xml:id="A78078e-70">
          <w lemma="the" ana="#d" reg="The" xml:id="A78078-0210" facs="A78078-001-a-0160">THe</w>
          <c> </c>
          <w lemma="minister" ana="#n2" reg="ministers" xml:id="A78078-0220" facs="A78078-001-a-0170">Ministers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-0230" facs="A78078-001-a-0180">of</w>
          <c> </c>
          <w lemma="every" ana="#d" reg="every" xml:id="A78078-0240" facs="A78078-001-a-0190">every</w>
          <c> </c>
          <w lemma="parish" ana="#n1" reg="parish" xml:id="A78078-0250" facs="A78078-001-a-0200">Parish</w>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A78078-0260" facs="A78078-001-a-0210">Church</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-0270" facs="A78078-001-a-0220">and</w>
          <c> </c>
          <w lemma="chapel" ana="#n1" reg="chapel" xml:id="A78078-0280" facs="A78078-001-a-0230">Chappell</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A78078-0290" facs="A78078-001-a-0240">within</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-0300" facs="A78078-001-a-0250">the</w>
          <c> </c>
          <w lemma="city" ana="#n2" reg="cities" xml:id="A78078-0310" facs="A78078-001-a-0260">Cities</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-0320" facs="A78078-001-a-0270">of</w>
          <c> </c>
          <hi xml:id="A78078e-80">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A78078-0330" facs="A78078-001-a-0280">London</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-0340" facs="A78078-001-a-0290">and</w>
          <c> </c>
          <hi xml:id="A78078e-90">
            <w lemma="Westminster" ana="#n1-nn" reg="Westminster" xml:id="A78078-0350" facs="A78078-001-a-0300">Westminster</w>
            <pc xml:id="A78078-0360" facs="A78078-001-a-0310">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-0370" facs="A78078-001-a-0320">and</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A78078-0380" facs="A78078-001-a-0330">within</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-0390" facs="A78078-001-a-0340">the</w>
          <c> </c>
          <w lemma="line" ana="#n2" reg="lines" xml:id="A78078-0400" facs="A78078-001-a-0350">Lines</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-0410" facs="A78078-001-a-0360">of</w>
          <c> </c>
          <w lemma="communication" ana="#n1" reg="communication" xml:id="A78078-0420" facs="A78078-001-a-0370">Communication</w>
          <pc xml:id="A78078-0430" facs="A78078-001-a-0380">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A78078-0440" facs="A78078-001-a-0390">are</w>
          <c> </c>
          <w lemma="earnest" ana="#av_j" reg="earnestly" xml:id="A78078-0450" facs="A78078-001-a-0400">earnestly</w>
          <c> </c>
          <w lemma="desire" ana="#vvn" reg="desired" xml:id="A78078-0460" facs="A78078-001-a-0410">desired</w>
          <pc xml:id="A78078-0470" facs="A78078-001-a-0420">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-0480" facs="A78078-001-a-0430">that</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A78078-0490" facs="A78078-001-a-0440">upon</w>
          <c> </c>
          <w lemma="Wednesday" ana="#n1-nn" reg="Wednesday" xml:id="A78078-0500" facs="A78078-001-a-0450">Wednesday</w>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A78078-0510" facs="A78078-001-a-0460">next</w>
          <pc xml:id="A78078-0520" facs="A78078-001-a-0470">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A78078-0530" facs="A78078-001-a-0480">being</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-0540" facs="A78078-001-a-0490">the</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A78078-0550" facs="A78078-001-a-0500">publike</w>
          <c> </c>
          <w lemma="fast" ana="#n1" reg="fast" xml:id="A78078-0560" facs="A78078-001-a-0510">Fast</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-0570" facs="A78078-001-a-0520">and</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A78078-0580" facs="A78078-001-a-0530">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-0590" facs="A78078-001-a-0540">of</w>
          <c> </c>
          <w lemma="humiliation" ana="#n1" reg="humiliation" xml:id="A78078-0600" facs="A78078-001-a-0550">humiliation</w>
          <pc xml:id="A78078-0610" facs="A78078-001-a-0560">,</pc>
          <c> </c>
          <hi xml:id="A78078e-100">
            <w lemma="etc." ana="#av" reg="&amp;c." xml:id="A78078-0620" facs="A78078-001-a-0570">&amp;c.</w>
            <pc unit="sentence" xml:id="A78078-0620-eos" facs="A78078-001-a-0580"/>
            <c> </c>
          </hi>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-0630" facs="A78078-001-a-0590">That</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A78078-0640" facs="A78078-001-a-0600">they</w>
          <c> </c>
          <w lemma="will" ana="#vmd" reg="would" xml:id="A78078-0650" facs="A78078-001-a-0610">would</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A78078-0660" facs="A78078-001-a-0620">give</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A78078-0670" facs="A78078-001-a-0630">notice</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A78078-0680" facs="A78078-001-a-0640">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-0690" facs="A78078-001-a-0650">their</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A78078-0700" facs="A78078-001-a-0660">severall</w>
          <c> </c>
          <w lemma="congregation" ana="#n2" reg="congregations" xml:id="A78078-0710" facs="A78078-001-a-0670">Congregations</w>
          <pc xml:id="A78078-0720" facs="A78078-001-a-0680">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-0730" facs="A78078-001-a-0690">that</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A78078-0740" facs="A78078-001-a-0700">it</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A78078-0750" facs="A78078-001-a-0710">is</w>
          <c> </c>
          <w lemma="earnest" ana="#av_j" reg="earnestly" xml:id="A78078-0760" facs="A78078-001-a-0720">earnestly</w>
          <c> </c>
          <w lemma="desire" ana="#vvn" reg="desired" xml:id="A78078-0770" facs="A78078-001-a-0730">desired</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-0780" facs="A78078-001-a-0740">that</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A78078-0790" facs="A78078-001-a-0750">all</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-0800" facs="A78078-001-a-0760">the</w>
          <c> </c>
          <w lemma="adventurer" ana="#n2" reg="adventurers" xml:id="A78078-0810" facs="A78078-001-a-0770">Adventurers</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A78078-0820" facs="A78078-001-a-0780">in</w>
          <c> </c>
          <hi xml:id="A78078e-110">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A78078-0830" facs="A78078-001-a-0790">London</w>
          </hi>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-0840" facs="A78078-001-a-0800">for</w>
          <c> </c>
          <w lemma="land" ana="#n2" reg="lands" xml:id="A78078-0850" facs="A78078-001-a-0810">Lands</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A78078-0860" facs="A78078-001-a-0820">in</w>
          <c> </c>
          <hi xml:id="A78078e-120">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A78078-0870" facs="A78078-001-a-0830">Ireland</w>
            <pc xml:id="A78078-0880" facs="A78078-001-a-0840">,</pc>
          </hi>
          <c> </c>
          <w lemma="will" ana="#vmd" reg="would" xml:id="A78078-0890" facs="A78078-001-a-0850">would</w>
          <c> </c>
          <w lemma="meet" ana="#vvi" reg="meet" xml:id="A78078-0900" facs="A78078-001-a-0860">meete</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A78078-0910" facs="A78078-001-a-0870">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-0920" facs="A78078-001-a-0880">their</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A78078-0930" facs="A78078-001-a-0890">Committee</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A78078-0940" facs="A78078-001-a-0900">at</w>
          <c> </c>
          <w lemma="grocers-hall" ana="#n1-nn" reg="Grocers-hall" xml:id="A78078-0950" facs="A78078-001-a-0910">Grocers-Hall</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A78078-0960" facs="A78078-001-a-0920">upon</w>
          <c> </c>
          <w lemma="Thursday" ana="#n1-nn" reg="Thursday" xml:id="A78078-0970" facs="A78078-001-a-0930">Thursday</w>
          <c> </c>
          <w lemma="next" ana="#ord" reg="next" xml:id="A78078-0980" facs="A78078-001-a-0940">next</w>
          <pc xml:id="A78078-0990" facs="A78078-001-a-0950">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A78078-1000" facs="A78078-001-a-0960">by</w>
          <c> </c>
          <w lemma="eight" ana="#crd" reg="eight" xml:id="A78078-1010" facs="A78078-001-a-0970">eight</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1020" facs="A78078-001-a-0980">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1030" facs="A78078-001-a-0990">the</w>
          <c> </c>
          <w lemma="clock" ana="#n1" reg="clock" xml:id="A78078-1040" facs="A78078-001-a-1000">clock</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A78078-1050" facs="A78078-001-a-1010">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1060" facs="A78078-001-a-1020">the</w>
          <c> </c>
          <w lemma="forenoon" ana="#n1" reg="forenoon" xml:id="A78078-1070" facs="A78078-001-a-1030">fore-noon</w>
          <pc xml:id="A78078-1080" facs="A78078-001-a-1040">,</pc>
          <c> </c>
          <w lemma="then" ana="#av" reg="then" xml:id="A78078-1090" facs="A78078-001-a-1050">then</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1100" facs="A78078-001-a-1060">and</w>
          <c> </c>
          <w lemma="there" ana="#av" reg="there" xml:id="A78078-1110" facs="A78078-001-a-1070">there</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78078-1120" facs="A78078-001-a-1080">to</w>
          <c> </c>
          <w lemma="resolve" ana="#vvi" reg="resolve" xml:id="A78078-1130" facs="A78078-001-a-1090">resolve</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A78078-1140" facs="A78078-001-a-1100">upon</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A78078-1150" facs="A78078-001-a-1110">some</w>
          <c> </c>
          <w lemma="matter" ana="#n2" reg="matters" xml:id="A78078-1160" facs="A78078-001-a-1120">matters</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1170" facs="A78078-001-a-1130">and</w>
          <c> </c>
          <w lemma="thing" ana="#n2" reg="things" xml:id="A78078-1180" facs="A78078-001-a-1140">things</w>
          <c> </c>
          <w lemma="desire" ana="#vvd" reg="desired" xml:id="A78078-1190" facs="A78078-001-a-1150">desired</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78078-1200" facs="A78078-001-a-1160">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A78078-1210" facs="A78078-001-a-1170">be</w>
          <c> </c>
          <w lemma="consider" ana="#vvn" reg="considered" xml:id="A78078-1220" facs="A78078-001-a-1180">considered</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1230" facs="A78078-001-a-1190">of</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A78078-1240" facs="A78078-001-a-1200">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1250" facs="A78078-001-a-1210">the</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A78078-1260" facs="A78078-001-a-1220">Committee</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1270" facs="A78078-001-a-1230">of</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A78078-1280" facs="A78078-001-a-1240">LORDS</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1290" facs="A78078-001-a-1250">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A78078-1300" facs="A78078-001-a-1260">COMMONS</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-1310" facs="A78078-001-a-1270">for</w>
          <c> </c>
          <w lemma="proposition" ana="#n2" reg="propositions" xml:id="A78078-1320" facs="A78078-001-a-1280">Propositions</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-1330" facs="A78078-001-a-1290">for</w>
          <c> </c>
          <hi xml:id="A78078e-130">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A78078-1340" facs="A78078-001-a-1300">Ireland</w>
            <pc xml:id="A78078-1350" facs="A78078-001-a-1310">,</pc>
          </hi>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A78078-1360" facs="A78078-001-a-1320">which</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A78078-1370" facs="A78078-001-a-1330">by</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-1380" facs="A78078-001-a-1340">their</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A78078-1390" facs="A78078-001-a-1350">Order</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1400" facs="A78078-001-a-1360">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1410" facs="A78078-001-a-1370">the</w>
          <c> </c>
          <w lemma="26" part="I" ana="#ord" reg="26th" xml:id="A78078-1420.1" facs="A78078-001-a-1380" rendition="hi-mid-3">26th</w>
          <hi rend="sup" xml:id="A78078e-140"/>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1430" facs="A78078-001-a-1400">of</w>
          <c> </c>
          <hi xml:id="A78078e-150">
            <w lemma="july" ana="#n1-nn" reg="July" xml:id="A78078-1440" facs="A78078-001-a-1410">Iuly</w>
            <pc xml:id="A78078-1450" facs="A78078-001-a-1420">,</pc>
          </hi>
          <c> </c>
          <w lemma="1645." ana="#crd" reg="1645." xml:id="A78078-1460" facs="A78078-001-a-1430">1645.</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A78078-1470" facs="A78078-001-a-1440">they</w>
          <c> </c>
          <w lemma="have" ana="#vvg" reg="having" xml:id="A78078-1480" facs="A78078-001-a-1450">having</w>
          <c> </c>
          <w lemma="recommend" ana="#vvn" reg="recommended" xml:id="A78078-1490" facs="A78078-001-a-1460">recommended</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A78078-1500" facs="A78078-001-a-1470">unto</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-1510" facs="A78078-001-a-1480">their</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A78078-1520" facs="A78078-001-a-1490">said</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A78078-1530" facs="A78078-001-a-1500">Committee</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1540" facs="A78078-001-a-1510">and</w>
          <c> </c>
          <w lemma="body" ana="#n1" reg="body" xml:id="A78078-1550" facs="A78078-001-a-1520">body</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1560" facs="A78078-001-a-1530">of</w>
          <c> </c>
          <w lemma="adventurer" ana="#n2" reg="adventurers" xml:id="A78078-1570" facs="A78078-001-a-1540">Adventurers</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78078-1580" facs="A78078-001-a-1550">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A78078-1590" facs="A78078-001-a-1560">be</w>
          <c> </c>
          <w lemma="advise" ana="#vvn" reg="advised" xml:id="A78078-1600" facs="A78078-001-a-1570">advised</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A78078-1610" facs="A78078-001-a-1580">upon</w>
          <pc xml:id="A78078-1620" facs="A78078-001-a-1590">,</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A78078-1630" facs="A78078-001-a-1600">which</w>
          <c> </c>
          <w lemma="much" ana="#d" reg="much" xml:id="A78078-1640" facs="A78078-001-a-1610">much</w>
          <c> </c>
          <w lemma="concern" ana="#vvz" reg="concerns" xml:id="A78078-1650" facs="A78078-001-a-1620">concernes</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1660" facs="A78078-001-a-1630">the</w>
          <c> </c>
          <w lemma="save" ana="#n1_vg" reg="saving" xml:id="A78078-1670" facs="A78078-001-a-1640">saving</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1680" facs="A78078-001-a-1650">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-1690" facs="A78078-001-a-1660">the</w>
          <c> </c>
          <w lemma="province" ana="#n1" reg="province" xml:id="A78078-1700" facs="A78078-001-a-1670">Province</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1710" facs="A78078-001-a-1680">of</w>
          <c> </c>
          <hi xml:id="A78078e-160">
            <w lemma="Munster" ana="#n1-nn" reg="Munster" xml:id="A78078-1720" facs="A78078-001-a-1690">Munster</w>
            <pc xml:id="A78078-1730" facs="A78078-001-a-1700">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1740" facs="A78078-001-a-1710">and</w>
          <c> </c>
          <w lemma="consequent" ana="#av_j" reg="consequently" xml:id="A78078-1750" facs="A78078-001-a-1720">consequently</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-1760" facs="A78078-001-a-1730">of</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A78078-1770" facs="A78078-001-a-1740">that</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A78078-1780" facs="A78078-001-a-1750">Kingdome</w>
          <c> </c>
          <w lemma="be" ana="#vvg" reg="being" xml:id="A78078-1790" facs="A78078-001-a-1760">being</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A78078-1800" facs="A78078-001-a-1770">now</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A78078-1810" facs="A78078-001-a-1780">in</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A78078-1820" facs="A78078-001-a-1790">great</w>
          <c> </c>
          <w lemma="danger" ana="#n1" reg="danger" xml:id="A78078-1830" facs="A78078-001-a-1800">danger</w>
          <pc xml:id="A78078-1840" facs="A78078-001-a-1810">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-1850" facs="A78078-001-a-1820">and</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A78078-1860" facs="A78078-001-a-1830">at</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A78078-1870" facs="A78078-001-a-1840">which</w>
          <c> </c>
          <w lemma="meeting" ana="#n1" reg="meeting" xml:id="A78078-1880" facs="A78078-001-a-1850">meeting</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A78078-1890" facs="A78078-001-a-1860">it</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A78078-1900" facs="A78078-001-a-1870">is</w>
          <c> </c>
          <w lemma="very" ana="#av" reg="very" xml:id="A78078-1910" facs="A78078-001-a-1880">very</w>
          <c> </c>
          <w lemma="probable" ana="#j" reg="probable" xml:id="A78078-1920" facs="A78078-001-a-1890">probable</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-1930" facs="A78078-001-a-1900">that</w>
          <c> </c>
          <w lemma="some" ana="#d" reg="some" xml:id="A78078-1940" facs="A78078-001-a-1910">some</w>
          <c> </c>
          <w lemma="good" ana="#j" reg="good" xml:id="A78078-1950" facs="A78078-001-a-1920">good</w>
          <c> </c>
          <w lemma="conclusion" ana="#n1" reg="conclusion" xml:id="A78078-1960" facs="A78078-001-a-1930">conclusion</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A78078-1970" facs="A78078-001-a-1940">is</w>
          <c> </c>
          <w lemma="likely" ana="#j" reg="likely" xml:id="A78078-1980" facs="A78078-001-a-1950">likely</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78078-1990" facs="A78078-001-a-1960">to</w>
          <c> </c>
          <w lemma="follow" ana="#vvi" reg="follow" xml:id="A78078-2000" facs="A78078-001-a-1970">follow</w>
          <pc xml:id="A78078-2010" facs="A78078-001-a-1980">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-2020" facs="A78078-001-a-1990">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-2030" facs="A78078-001-a-2000">the</w>
          <c> </c>
          <w lemma="relieve" ana="#n1_vg" reg="relieving" xml:id="A78078-2040" facs="A78078-001-a-2010">relieving</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-2050" facs="A78078-001-a-2020">of</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A78078-2060" facs="A78078-001-a-2030">those</w>
          <c> </c>
          <w lemma="force" ana="#n2" reg="forces" xml:id="A78078-2070" facs="A78078-001-a-2040">Forces</w>
          <pc xml:id="A78078-2080" facs="A78078-001-a-2050">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-2090" facs="A78078-001-a-2060">and</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-2100" facs="A78078-001-a-2070">for</w>
          <c> </c>
          <w lemma="secure" ana="#vvg" reg="securing" xml:id="A78078-2110" facs="A78078-001-a-2080">securing</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-2120" facs="A78078-001-a-2090">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-2130" facs="A78078-001-a-2100">their</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A78078-2140" facs="A78078-001-a-2110">said</w>
          <c> </c>
          <w lemma="adventure" ana="#n2" reg="adventures" xml:id="A78078-2150" facs="A78078-001-a-2120">Adventures</w>
          <pc xml:id="A78078-2160" facs="A78078-001-a-2130">:</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78078-2170" facs="A78078-001-a-2140">And</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-2180" facs="A78078-001-a-2150">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A78078-2190" facs="A78078-001-a-2160">said</w>
          <c> </c>
          <w lemma="adventurer" ana="#n2" reg="adventurers" xml:id="A78078-2200" facs="A78078-001-a-2170">Adventurers</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A78078-2210" facs="A78078-001-a-2180">are</w>
          <c> </c>
          <w lemma="earnest" ana="#av_j" reg="earnestly" xml:id="A78078-2220" facs="A78078-001-a-2190">earnestly</w>
          <c> </c>
          <w lemma="desire" ana="#vvn" reg="desired" xml:id="A78078-2230" facs="A78078-001-a-2200">desired</w>
          <pc xml:id="A78078-2240" facs="A78078-001-a-2210">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-2250" facs="A78078-001-a-2220">that</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A78078-2260" facs="A78078-001-a-2230">they</w>
          <c> </c>
          <w lemma="will" ana="#vmd" reg="would" xml:id="A78078-2270" facs="A78078-001-a-2240">would</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A78078-2280" facs="A78078-001-a-2250">not</w>
          <c> </c>
          <w lemma="fail" ana="#vvi" reg="fail" xml:id="A78078-2290" facs="A78078-001-a-2260">fail</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78078-2300" facs="A78078-001-a-2270">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A78078-2310" facs="A78078-001-a-2280">give</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78078-2320" facs="A78078-001-a-2290">their</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A78078-2330" facs="A78078-001-a-2300">said</w>
          <c> </c>
          <w lemma="committee" ana="#n1" reg="committee" xml:id="A78078-2340" facs="A78078-001-a-2310">Committee</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A78078-2350" facs="A78078-001-a-2320">a</w>
          <c> </c>
          <w lemma="meeting" ana="#n1" reg="meeting" xml:id="A78078-2360" facs="A78078-001-a-2330">meeting</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A78078-2370" facs="A78078-001-a-2340">at</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A78078-2380" facs="A78078-001-a-2350">this</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A78078-2390" facs="A78078-001-a-2360">time</w>
          <pc xml:id="A78078-2400" facs="A78078-001-a-2370">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-cs" reg="for" xml:id="A78078-2410" facs="A78078-001-a-2380">for</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78078-2420" facs="A78078-001-a-2390">that</w>
          <c> </c>
          <w lemma="very" ana="#av" reg="very" xml:id="A78078-2430" facs="A78078-001-a-2400">very</w>
          <c> </c>
          <w lemma="much" ana="#av_d" reg="much" xml:id="A78078-2440" facs="A78078-001-a-2410">much</w>
          <c> </c>
          <w lemma="depend" ana="#vvz" reg="depends" xml:id="A78078-2450" facs="A78078-001-a-2420">depends</w>
          <c> </c>
          <w lemma="thereupon" ana="#av" reg="thereupon" xml:id="A78078-2460" facs="A78078-001-a-2430">thereupon</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A78078-2470" facs="A78078-001-a-2440">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78078-2480" facs="A78078-001-a-2450">the</w>
          <c> </c>
          <w lemma="safety" ana="#n1" reg="safety" xml:id="A78078-2490" facs="A78078-001-a-2460">safety</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78078-2500" facs="A78078-001-a-2470">of</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A78078-2510" facs="A78078-001-a-2480">that</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A78078-2520" facs="A78078-001-a-2490">Kingdome</w>
          <pc unit="sentence" xml:id="A78078-2530" facs="A78078-001-a-2500">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A78078e-170">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>