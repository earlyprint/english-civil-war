<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A speech spoken by a worthy and learned gentleman, Master Thomas Chadlicot, Esquire: in the behalfe of the King and Parliament, at Bishops Cannings in Wiltshire; before one Master Blithe a minister of Gods Word, and Master Lewin a councellour at law. And now published for the publique good for the cure of the miserable distempers of this distracted kingdome. With the Parliaments resolution concerning the Kings most Excellent Majesty.</title>
        <author>Chadlicot, Thomas.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A78511</idno>
        <idno type="STC">Wing C1786</idno>
        <idno type="STC">Thomason E200_57</idno>
        <idno type="STC">Thomason E200_58</idno>
        <idno type="STC">ESTC R212614</idno>
        <idno type="EEBO-CITATION">99871218</idno>
        <idno type="PROQUEST">99871218</idno>
        <idno type="VID">157592</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A78511)</note>
        <note>Transcribed from: (Early English Books Online ; image set 157592)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 35:E200[57] or 35:E200[58])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A speech spoken by a worthy and learned gentleman, Master Thomas Chadlicot, Esquire: in the behalfe of the King and Parliament, at Bishops Cannings in Wiltshire; before one Master Blithe a minister of Gods Word, and Master Lewin a councellour at law. And now published for the publique good for the cure of the miserable distempers of this distracted kingdome. With the Parliaments resolution concerning the Kings most Excellent Majesty.</title>
            <author>Chadlicot, Thomas.</author>
            <author>Charles I, King of England, 1600-1649.</author>
          </titleStmt>
          <extent>[8] p.</extent>
          <publicationStmt>
            <publisher>printed for Tho. Banks,</publisher>
            <pubPlace>London :</pubPlace>
            <date>August 26. 1642.</date>
          </publicationStmt>
          <notesStmt>
            <note>Signatures: A⁴.</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Speeches, addresses, etc., English -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2009-01</date><label>Judith Siefring</label>
        Sampled and proofread
      </change>
      <change><date>2009-01</date><label>Judith Siefring</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A78511e-10">
    <body xml:id="A78511e-20">
      <div type="text" xml:id="A78511e-30">
        <pb facs="tcp:157592:1" xml:id="A78511-001-a"/>
        <head xml:id="A78511e-40">
          <w lemma="the" ana="#d" reg="The" xml:id="A78511-0030" facs="A78511-001-a-0010">The</w>
          <c> </c>
          <w lemma="parliament" ana="#n2" reg="parliaments" xml:id="A78511-0040" facs="A78511-001-a-0020">PARLIAMENTS</w>
          <c> </c>
          <w lemma="resolution" ana="#n1" reg="resolution" xml:id="A78511-0050" facs="A78511-001-a-0030">Resolution</w>
          <c> </c>
          <w lemma="concern" ana="#n2_vg" reg="concernings" xml:id="A78511-0060" facs="A78511-001-a-0040">Concernings</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78511-0070" facs="A78511-001-a-0050">the</w>
          <c> </c>
          <w lemma="king" ana="#n2" reg="Kings" xml:id="A78511-0080" facs="A78511-001-a-0060">KINGS</w>
          <c> </c>
          <hi xml:id="A78511e-50">
            <w lemma="most" ana="#av-s_d" reg="most" xml:id="A78511-0090" facs="A78511-001-a-0070">Most</w>
            <c> </c>
            <w lemma="excellent" ana="#j" reg="excellent" xml:id="A78511-0100" facs="A78511-001-a-0080">Excellent</w>
          </hi>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A78511-0110" facs="A78511-001-a-0090">MAIESTY</w>
          <pc unit="sentence" xml:id="A78511-0120" facs="A78511-001-a-0100">.</pc>
        </head>
        <p xml:id="A78511e-60">
          <w lemma="the" ana="#d" reg="The" xml:id="A78511-0150" facs="A78511-001-a-0110">THE</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A78511-0160" facs="A78511-001-a-0120">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0170" facs="A78511-001-a-0130">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A78511-0180" facs="A78511-001-a-0140">Commons</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78511-0190" facs="A78511-001-a-0150">to</w>
          <c> </c>
          <w lemma="witness" ana="#vvi" reg="witness" xml:id="A78511-0200" facs="A78511-001-a-0160">witnesse</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78511-0210" facs="A78511-001-a-0170">their</w>
          <c> </c>
          <w lemma="constant" ana="#j" reg="constant" xml:id="A78511-0220" facs="A78511-001-a-0180">constant</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0230" facs="A78511-001-a-0190">and</w>
          <c> </c>
          <w lemma="unshaken" ana="#j" reg="unshaken" xml:id="A78511-0240" facs="A78511-001-a-0200">unshaken</w>
          <c> </c>
          <w lemma="loyalty" ana="#n1" reg="loyalty" xml:id="A78511-0250" facs="A78511-001-a-0210">loyalty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0260" facs="A78511-001-a-0220">and</w>
          <c> </c>
          <w lemma="affection" ana="#n1" reg="affection" xml:id="A78511-0270" facs="A78511-001-a-0230">affection</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A78511-0280" facs="A78511-001-a-0240">to</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0290" facs="A78511-001-a-0250">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A78511-0300" facs="A78511-001-a-0260">Majesty</w>
          <pc xml:id="A78511-0310" facs="A78511-001-a-0270">;</pc>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A78511-0320" facs="A78511-001-a-0280">doe</w>
          <c> </c>
          <w lemma="solemn" ana="#av_j" reg="solemnly" xml:id="A78511-0330" facs="A78511-001-a-0290">solemnly</w>
          <c> </c>
          <w lemma="declare" ana="#vvi" reg="declare" xml:id="A78511-0340" facs="A78511-001-a-0300">declare</w>
          <pc xml:id="A78511-0350" facs="A78511-001-a-0310">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78511-0360" facs="A78511-001-a-0320">that</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A78511-0370" facs="A78511-001-a-0330">if</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0380" facs="A78511-001-a-0340">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A78511-0390" facs="A78511-001-a-0350">Majesty</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A78511-0400" facs="A78511-001-a-0360">shall</w>
          <c> </c>
          <w lemma="immediate" ana="#av_j" reg="immediately" xml:id="A78511-0410" facs="A78511-001-a-0370">immediately</w>
          <c> </c>
          <w lemma="disband" ana="#vvi" reg="disband" xml:id="A78511-0420" facs="A78511-001-a-0380">disband</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A78511-0430" facs="A78511-001-a-0390">all</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0440" facs="A78511-001-a-0400">his</w>
          <c> </c>
          <w lemma="force" ana="#n2" reg="forces" xml:id="A78511-0450" facs="A78511-001-a-0410">Forces</w>
          <pc xml:id="A78511-0460" facs="A78511-001-a-0420">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0470" facs="A78511-001-a-0430">and</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A78511-0480" facs="A78511-001-a-0440">be</w>
          <c> </c>
          <w lemma="please" ana="#vvn" reg="pleased" xml:id="A78511-0490" facs="A78511-001-a-0450">pleased</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78511-0500" facs="A78511-001-a-0460">to</w>
          <c> </c>
          <w lemma="abandon" ana="#vvi" reg="abandon" xml:id="A78511-0510" facs="A78511-001-a-0470">abandon</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A78511-0520" facs="A78511-001-a-0480">all</w>
          <c> </c>
          <w lemma="those" ana="#d" reg="those" xml:id="A78511-0530" facs="A78511-001-a-0490">those</w>
          <c> </c>
          <w lemma="wicked" ana="#j" reg="wicked" xml:id="A78511-0540" facs="A78511-001-a-0500">wicked</w>
          <c> </c>
          <w lemma="counsel" ana="#n2" reg="counsels" xml:id="A78511-0550" facs="A78511-001-a-0510">Counsels</w>
          <pc xml:id="A78511-0560" facs="A78511-001-a-0520">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0570" facs="A78511-001-a-0530">and</w>
          <c> </c>
          <w lemma="leave" ana="#vvb" reg="leave" xml:id="A78511-0580" facs="A78511-001-a-0540">leave</w>
          <c> </c>
          <w lemma="they" ana="#pno" reg="them" xml:id="A78511-0590" facs="A78511-001-a-0550">them</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A78511-0600" facs="A78511-001-a-0560">to</w>
          <c> </c>
          <w lemma="condign" ana="#j" reg="condign" xml:id="A78511-0610" facs="A78511-001-a-0570">condigne</w>
          <c> </c>
          <w lemma="punishment" ana="#n1" reg="punishment" xml:id="A78511-0620" facs="A78511-001-a-0580">punishment</w>
          <pc xml:id="A78511-0630" facs="A78511-001-a-0590">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0640" facs="A78511-001-a-0600">and</w>
          <c> </c>
          <w lemma="return" ana="#vvi" reg="return" xml:id="A78511-0650" facs="A78511-001-a-0610">returne</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0660" facs="A78511-001-a-0620">and</w>
          <c> </c>
          <w lemma="hearken" ana="#vvi" reg="hearken" xml:id="A78511-0670" facs="A78511-001-a-0630">harken</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A78511-0680" facs="A78511-001-a-0640">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A78511-0690" facs="A78511-001-a-0650">the</w>
          <c> </c>
          <w lemma="wholesome" ana="#j" reg="wholesome" xml:id="A78511-0700" facs="A78511-001-a-0660">wholesome</w>
          <c> </c>
          <w lemma="advice" ana="#n1" reg="advice" xml:id="A78511-0710" facs="A78511-001-a-0670">advice</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A78511-0720" facs="A78511-001-a-0680">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0730" facs="A78511-001-a-0690">his</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A78511-0740" facs="A78511-001-a-0700">great</w>
          <c> </c>
          <w lemma="council" ana="#n1" reg="council" xml:id="A78511-0750" facs="A78511-001-a-0710">Councell</w>
          <pc xml:id="A78511-0760" facs="A78511-001-a-0720">;</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A78511-0770" facs="A78511-001-a-0730">they</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A78511-0780" facs="A78511-001-a-0740">will</w>
          <c> </c>
          <w lemma="real" ana="#av_j" reg="really" xml:id="A78511-0790" facs="A78511-001-a-0750">really</w>
          <c> </c>
          <w lemma="endeavour" ana="#vvi" reg="endeavour" xml:id="A78511-0800" facs="A78511-001-a-0760">endevour</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A78511-0810" facs="A78511-001-a-0770">to</w>
          <c> </c>
          <w lemma="make" ana="#vvi" reg="make" xml:id="A78511-0820" facs="A78511-001-a-0780">make</w>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A78511-0830" facs="A78511-001-a-0790">both</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0840" facs="A78511-001-a-0800">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A78511-0850" facs="A78511-001-a-0810">Majesty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0860" facs="A78511-001-a-0820">and</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A78511-0870" facs="A78511-001-a-0830">his</w>
          <c> </c>
          <w lemma="posterity" ana="#n1" reg="posterity" xml:id="A78511-0880" facs="A78511-001-a-0840">Posterity</w>
          <pc xml:id="A78511-0890" facs="A78511-001-a-0850">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A78511-0900" facs="A78511-001-a-0860">as</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A78511-0910" facs="A78511-001-a-0870">great</w>
          <pc xml:id="A78511-0920" facs="A78511-001-a-0880">,</pc>
          <c> </c>
          <w lemma="rich" ana="#j" reg="rich" xml:id="A78511-0930" facs="A78511-001-a-0890">rich</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-0940" facs="A78511-001-a-0900">and</w>
          <c> </c>
          <w lemma="potent" ana="#j" reg="potent" xml:id="A78511-0950" facs="A78511-001-a-0910">potent</w>
          <pc xml:id="A78511-0960" facs="A78511-001-a-0920">;</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A78511-0970" facs="A78511-001-a-0930">as</w>
          <c> </c>
          <w lemma="much" ana="#av_d" reg="much" xml:id="A78511-0980" facs="A78511-001-a-0940">much</w>
          <c> </c>
          <w lemma="belove" ana="#vvn" reg="beloved" xml:id="A78511-0990" facs="A78511-001-a-0950">beloved</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A78511-1000" facs="A78511-001-a-0960">at</w>
          <c> </c>
          <w lemma="home" ana="#n1" reg="home" xml:id="A78511-1010" facs="A78511-001-a-0970">home</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-1020" facs="A78511-001-a-0980">and</w>
          <c> </c>
          <w lemma="fear" ana="#vvd" reg="feared" xml:id="A78511-1030" facs="A78511-001-a-0990">feared</w>
          <c> </c>
          <w lemma="abroad" ana="#av" reg="abroad" xml:id="A78511-1040" facs="A78511-001-a-1000">abroad</w>
          <pc xml:id="A78511-1050" facs="A78511-001-a-1010">,</pc>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A78511-1060" facs="A78511-001-a-1020">as</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A78511-1070" facs="A78511-001-a-1030">any</w>
          <c> </c>
          <w lemma="prince" ana="#n1" reg="Prince" xml:id="A78511-1080" facs="A78511-001-a-1040">Prince</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A78511-1090" facs="A78511-001-a-1050">that</w>
          <c> </c>
          <w lemma="ever" ana="#av" reg="ever" xml:id="A78511-1100" facs="A78511-001-a-1060">ever</w>
          <c> </c>
          <w lemma="sway" ana="#vvd" reg="swayed" xml:id="A78511-1110" facs="A78511-001-a-1070">swayed</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A78511-1120" facs="A78511-001-a-1080">this</w>
          <c> </c>
          <w lemma="sceptre" ana="#n1" reg="sceptre" xml:id="A78511-1130" facs="A78511-001-a-1090">Scepter</w>
          <pc xml:id="A78511-1140" facs="A78511-001-a-1100">;</pc>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A78511-1150" facs="A78511-001-a-1110">which</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A78511-1160" facs="A78511-001-a-1120">is</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A78511-1170" facs="A78511-001-a-1130">their</w>
          <c> </c>
          <w lemma="firm" ana="#j" reg="firm" xml:id="A78511-1180" facs="A78511-001-a-1140">firme</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A78511-1190" facs="A78511-001-a-1150">and</w>
          <c> </c>
          <w lemma="constant" ana="#j" reg="constant" xml:id="A78511-1200" facs="A78511-001-a-1160">constant</w>
          <c> </c>
          <w lemma="resolution" ana="#n1" reg="resolution" xml:id="A78511-1210" facs="A78511-001-a-1170">Resolution</w>
          <pc unit="sentence" xml:id="A78511-1220" facs="A78511-001-a-1180">.</pc>
        </p>
        <div type="license" xml:id="A78511e-70">
          <p xml:id="A78511e-80">
            <hi xml:id="A78511e-90">
              <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A78511-1260" facs="A78511-001-a-1190">Ordered</w>
              <c> </c>
              <w lemma="by" ana="#acp-p" reg="by" xml:id="A78511-1270" facs="A78511-001-a-1200">by</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A78511-1280" facs="A78511-001-a-1210">the</w>
              <c> </c>
              <w lemma="lord" ana="#n2" reg="Lords" xml:id="A78511-1290" facs="A78511-001-a-1220">Lords</w>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A78511-1300" facs="A78511-001-a-1230">and</w>
              <c> </c>
              <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A78511-1310" facs="A78511-001-a-1240">Commons</w>
              <c> </c>
              <w lemma="in" ana="#acp-p" reg="in" xml:id="A78511-1320" facs="A78511-001-a-1250">in</w>
              <c> </c>
              <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A78511-1330" facs="A78511-001-a-1260">Parliament</w>
              <pc xml:id="A78511-1340" facs="A78511-001-a-1270">,</pc>
              <c> </c>
              <w lemma="that" ana="#cs" reg="that" xml:id="A78511-1350" facs="A78511-001-a-1280">that</w>
              <c> </c>
              <w lemma="this" ana="#d" reg="this" xml:id="A78511-1360" facs="A78511-001-a-1290">this</w>
              <c> </c>
              <w lemma="declaration" ana="#n1" reg="declaration" xml:id="A78511-1370" facs="A78511-001-a-1300">Declaration</w>
              <c> </c>
              <w lemma="be" ana="#vvb" reg="be" xml:id="A78511-1380" facs="A78511-001-a-1310">be</w>
              <c> </c>
              <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A78511-1390" facs="A78511-001-a-1320">forthwith</w>
              <c> </c>
              <w lemma="print" ana="#vvn" reg="printed" xml:id="A78511-1400" facs="A78511-001-a-1330">Printed</w>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A78511-1410" facs="A78511-001-a-1340">and</w>
              <c> </c>
              <w lemma="publish" ana="#vvn" reg="published" xml:id="A78511-1420" facs="A78511-001-a-1350">published</w>
              <pc unit="sentence" xml:id="A78511-1430" facs="A78511-001-a-1360">.</pc>
              <c> </c>
            </hi>
          </p>
          <closer xml:id="A78511e-100">
            <signed xml:id="A78511e-110">
              <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A78511-1470" facs="A78511-001-a-1370">Hen.</w>
              <c> </c>
              <w lemma="Elsyng" ana="#n1-nn" reg="Elsing" xml:id="A78511-1490" facs="A78511-001-a-1390">Elsyng</w>
              <c> </c>
              <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A78511-1500" facs="A78511-001-a-1400">Cler.</w>
              <pc unit="sentence" xml:id="A78511-1500-eos" facs="A78511-001-a-1410"/>
              <c> </c>
              <w lemma="parl." ana="#n-ab" reg="Parl." xml:id="A78511-1510" facs="A78511-001-a-1420">Parl.</w>
              <c> </c>
              <w lemma="do." ana="#n-ab" reg="do." xml:id="A78511-1520" facs="A78511-001-a-1430">Do.</w>
              <c> </c>
              <w lemma="com." ana="#n-ab" reg="com." xml:id="A78511-1530" facs="A78511-001-a-1440">Com.</w>
              <pc unit="sentence" xml:id="A78511-1530-eos" facs="A78511-001-a-1450"/>
            </signed>
          </closer>
        </div>
      </div>
    </body>
    <back xml:id="A78511e-120">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>