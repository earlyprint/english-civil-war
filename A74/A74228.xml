<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Come freind, array your selfe, and never looke,</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74228</idno>
        <idno type="STC">Thomason 669.f.6[71]</idno>
        <idno type="STC">ESTC R212497</idno>
        <idno type="EEBO-CITATION">99871106</idno>
        <idno type="PROQUEST">99871106</idno>
        <idno type="VID">160932</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74228)</note>
        <note>Transcribed from: (Early English Books Online ; image set 160932)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f6[71])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Come freind, array your selfe, and never looke,</title>
          </titleStmt>
          <extent>1 sheet ([1] p.) : ill. (port.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London? :</pubPlace>
            <date>1642]</date>
          </publicationStmt>
          <notesStmt>
            <note>An engraved plate bearing the portraits of Sir Thomas Malet, Archbishop Williams and Sir Thomas Lunsford, with verses below each portrait.</note>
            <note>Below portrait of Malet, verse begins: "Come freind, array your selfe, and never looke,"; below Williams: "Oh Sr. Ime ready, did you never heare,"; below Lunsford: "I'le helpe to kill, to pillage and destroy".</note>
            <note>Title from first line of first verse.</note>
            <note>Imprint from Thomason.</note>
            <note>Annotation on Thomason copy: "Jud: mallet Abp Williams Col Lunsford".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Malet, Thomas, -- Sir, ca. 1582-1665 -- Early works to 1800.</term>
          <term>Williams, John, 1582-1650 -- Early works to 1800.</term>
          <term>Lunsford, Thomas, -- Sir, 1610?-1653? -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-09</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-11</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-11</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74228e-10">
    <body xml:id="A74228e-20">
      <div type="text" xml:id="A74228e-30">
        <pb facs="tcp:160932:1" rendition="simple:additions" xml:id="A74228-001-a"/>
        <p xml:id="A74228e-40">
          <figure xml:id="A74228e-50">
            <figDesc xml:id="A74228e-60">portrait of Thomas Malet</figDesc>
            <lg xml:id="A74228e-70">
              <l xml:id="A74228e-80">
                <w lemma="come" ana="#vvb" reg="Come" xml:id="A74228-0050" facs="A74228-001-a-0010">Come</w>
                <c> </c>
                <w lemma="friend" ana="#n1" reg="friend" xml:id="A74228-0060" facs="A74228-001-a-0020">Freind</w>
                <pc xml:id="A74228-0070" facs="A74228-001-a-0030">,</pc>
                <c> </c>
                <w lemma="array" ana="#n1" reg="array" xml:id="A74228-0080" facs="A74228-001-a-0040">ARRAY</w>
                <c> </c>
                <w lemma="yourself" part="I" ana="#px" reg="yourself" xml:id="A74228-0090.1" facs="A74228-001-a-0050">your selfe</w>
                <pc xml:id="A74228-0100" facs="A74228-001-a-0070">,</pc>
                <c> </c>
                <w lemma="and" ana="#cc" reg="and" xml:id="A74228-0110" facs="A74228-001-a-0080">and</w>
                <c> </c>
                <w lemma="never" ana="#av-x" reg="never" xml:id="A74228-0120" facs="A74228-001-a-0090">never</w>
                <c> </c>
                <w lemma="look" ana="#vvb" reg="look" xml:id="A74228-0130" facs="A74228-001-a-0100">looke</w>
                <pc xml:id="A74228-0140" facs="A74228-001-a-0110">,</pc>
              </l>
              <l xml:id="A74228e-90">
                <w lemma="to" ana="#acp-cs" reg="To" xml:id="A74228-0150" facs="A74228-001-a-0120">To</w>
                <c> </c>
                <w lemma="prosper" ana="#vvi" reg="prosper" xml:id="A74228-0160" facs="A74228-001-a-0130">prosper</w>
                <c> </c>
                <w lemma="in" ana="#acp-p" reg="in" xml:id="A74228-0170" facs="A74228-001-a-0140">in</w>
                <c> </c>
                <w lemma="your" ana="#po" reg="your" xml:id="A74228-0180" facs="A74228-001-a-0150">your</w>
                <c> </c>
                <w lemma="di-o-cease" ana="#n1" reg="di-o-cease" xml:id="A74228-0190" facs="A74228-001-a-0160">Di-o-cease</w>
                <c> </c>
                <w lemma="your" ana="#po" reg="your" xml:id="A74228-0200" facs="A74228-001-a-0170">your</w>
                <c> </c>
                <w lemma="book" ana="#n1" reg="book" xml:id="A74228-0210" facs="A74228-001-a-0180">Booke</w>
              </l>
              <l xml:id="A74228e-100">
                <w lemma="Medale" ana="#n1-nn" reg="Medale" xml:id="A74228-0220" facs="A74228-001-a-0190">Medale</w>
                <c> </c>
                <w lemma="with" ana="#acp-p" reg="with" xml:id="A74228-0230" facs="A74228-001-a-0200">with</w>
                <c> </c>
                <w lemma="it" ana="#pn" reg="it" xml:id="A74228-0240" facs="A74228-001-a-0210">it</w>
                <c> </c>
                <w lemma="less" ana="#av-c_d" reg="less" xml:id="A74228-0250" facs="A74228-001-a-0220">lesse</w>
                <pc xml:id="A74228-0260" facs="A74228-001-a-0230">:</pc>
                <c> </c>
                <w lemma="for" ana="#acp-cs" reg="for" xml:id="A74228-0270" facs="A74228-001-a-0240">for</w>
                <c> </c>
                <w lemma="you" ana="#pn" reg="you" xml:id="A74228-0280" facs="A74228-001-a-0250">you</w>
                <c> </c>
                <w lemma="must" ana="#vmb" reg="must" xml:id="A74228-0290" facs="A74228-001-a-0260">must</w>
                <c> </c>
                <w lemma="arm" ana="#vvi" reg="arm" xml:id="A74228-0300" facs="A74228-001-a-0270">Arme</w>
              </l>
              <l xml:id="A74228e-110">
                <w lemma="if" ana="#cs" reg="If" xml:id="A74228-0310" facs="A74228-001-a-0280">If</w>
                <c> </c>
                <w lemma="you" ana="#pn" reg="you" xml:id="A74228-0320" facs="A74228-001-a-0290">you</w>
                <c> </c>
                <w lemma="intend" ana="#vvb" reg="intend" xml:id="A74228-0330" facs="A74228-001-a-0300">intend</w>
                <c> </c>
                <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74228-0340" facs="A74228-001-a-0310">to</w>
                <c> </c>
                <w lemma="keep" ana="#vvi" reg="keep" xml:id="A74228-0350" facs="A74228-001-a-0320">keepe</w>
                <c> </c>
                <w lemma="yourself" part="I" ana="#px" reg="yourself" xml:id="A74228-0360.1" facs="A74228-001-a-0330">your selfe</w>
                <c> </c>
                <w lemma="from" ana="#acp-p" reg="from" xml:id="A74228-0370" facs="A74228-001-a-0350">from</w>
                <c> </c>
                <w lemma="harm" ana="#n1" reg="harm" xml:id="A74228-0380" facs="A74228-001-a-0360">harme</w>
                <pc xml:id="A74228-0390" facs="A74228-001-a-0370">:</pc>
              </l>
              <l xml:id="A74228e-120">
                <w lemma="Vsenow" ana="#n1-nn" reg="Vsenow" xml:id="A74228-0400" facs="A74228-001-a-0380">Vsenow</w>
                <c> </c>
                <w lemma="your" ana="#po" reg="your" xml:id="A74228-0410" facs="A74228-001-a-0390">your</w>
                <c> </c>
                <w lemma="power" ana="#n1" reg="power" xml:id="A74228-0420" facs="A74228-001-a-0400">power</w>
                <c> </c>
                <w lemma="only" ana="#av_j" reg="only" xml:id="A74228-0430" facs="A74228-001-a-0410">only</w>
                <c> </c>
                <w lemma="against" ana="#acp-p" reg="against" xml:id="A74228-0440" facs="A74228-001-a-0420">against</w>
                <c> </c>
                <w lemma="those" ana="#d" reg="those" xml:id="A74228-0450" facs="A74228-001-a-0430">those</w>
              </l>
              <l xml:id="A74228e-130">
                <w lemma="that" ana="#cs" reg="That" xml:id="A74228-0460" facs="A74228-001-a-0440">That</w>
                <c> </c>
                <w lemma="be" ana="#vvb" reg="are" xml:id="A74228-0470" facs="A74228-001-a-0450">are</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74228-0480" facs="A74228-001-a-0460">the</w>
                <c> </c>
                <w lemma="kingdom" ana="#n1g" reg="kingdom's" xml:id="A74228-0490" facs="A74228-001-a-0470">Kingdomes</w>
                <c> </c>
                <w lemma="friend" ana="#n2" reg="friends" xml:id="A74228-0500" facs="A74228-001-a-0480">freinds</w>
                <c> </c>
                <pc xml:id="A74228-0510" facs="A74228-001-a-0490">(</pc>
                <w lemma="but" ana="#acp-cc" reg="But" xml:id="A74228-0520" facs="A74228-001-a-0500">but</w>
                <c> </c>
                <w lemma="yet" ana="#av" reg="yet" xml:id="A74228-0530" facs="A74228-001-a-0510">yet</w>
                <c> </c>
                <w lemma="our" ana="#po" reg="our" xml:id="A74228-0540" facs="A74228-001-a-0520">our</w>
                <c> </c>
                <w lemma="foe" ana="#n2" reg="foes" xml:id="A74228-0550" facs="A74228-001-a-0530">foes</w>
                <pc unit="sentence" xml:id="A74228-0560" facs="A74228-001-a-0540">)</pc>
                <c> </c>
              </l>
            </lg>
          </figure>
        </p>
        <p xml:id="A74228e-140">
          <figure xml:id="A74228e-150">
            <figDesc xml:id="A74228e-160">portrait of Archbishop Williams</figDesc>
            <lg xml:id="A74228e-170">
              <l xml:id="A74228e-180">
                <w lemma="o" ana="#uh" reg="Oh" xml:id="A74228-0670" facs="A74228-001-a-0550">Oh</w>
                <c> </c>
                <w lemma="sr." part="I" ana="#n-ab" reg="sr." xml:id="A74228-0680.1" facs="A74228-001-a-0560" rendition="hi-mid-2">Sr.</w>
                <hi rend="sup" xml:id="A74228e-190"/>
                <c> </c>
                <w lemma="i" ana="#pns" reg="i" xml:id="A74228-0690" facs="A74228-001-a-0590">I</w>
                <c> </c>
                <w lemma="i" ana="#pno" reg="me" xml:id="A74228-0700" facs="A74228-001-a-0600">me</w>
                <c> </c>
                <w lemma="ready" ana="#j" reg="ready" xml:id="A74228-0710" facs="A74228-001-a-0610">ready</w>
                <pc xml:id="A74228-0720" facs="A74228-001-a-0620">,</pc>
                <c> </c>
                <w lemma="do" ana="#vvd" reg="did" xml:id="A74228-0730" facs="A74228-001-a-0630">did</w>
                <c> </c>
                <w lemma="you" ana="#pn" reg="you" xml:id="A74228-0740" facs="A74228-001-a-0640">you</w>
                <c> </c>
                <w lemma="never" ana="#av-x" reg="never" xml:id="A74228-0750" facs="A74228-001-a-0650">never</w>
                <c> </c>
                <w lemma="he" ana="#pns" reg="he" xml:id="A74228-0760" facs="A74228-001-a-0660">he</w>
                <c> </c>
                <w lemma="be" ana="#vvb" reg="are" xml:id="A74228-0770" facs="A74228-001-a-0670">are</w>
                <pc xml:id="A74228-0780" facs="A74228-001-a-0680">,</pc>
              </l>
              <l xml:id="A74228e-200">
                <w lemma="how" ana="#crq-cs" reg="How" xml:id="A74228-0790" facs="A74228-001-a-0690">How</w>
                <c> </c>
                <w lemma="forward" ana="#av_j" reg="forward" xml:id="A74228-0800" facs="A74228-001-a-0700">forward</w>
                <c> </c>
                <w lemma="i" ana="#pns" reg="i" xml:id="A74228-0810" facs="A74228-001-a-0710">I</w>
                <c> </c>
                <w lemma="have" ana="#vvb" reg="have" xml:id="A74228-0820" facs="A74228-001-a-0720">haue</w>
                <c> </c>
                <w lemma="be" ana="#vvn" reg="been" xml:id="A74228-0830" facs="A74228-001-a-0730">byn</w>
                <c> </c>
                <w lemma="it|be" ana="#pn|vvz" reg="'tis" xml:id="A74228-0840" facs="A74228-001-a-0740">tis</w>
                <c> </c>
                <w lemma="many" ana="#d" reg="many" xml:id="A74228-0850" facs="A74228-001-a-0750">many</w>
                <c> </c>
                <w lemma="a" ana="#d" reg="a" xml:id="A74228-0860" facs="A74228-001-a-0760">a</w>
                <c> </c>
                <w lemma="year" ana="#n1" reg="year" xml:id="A74228-0870" facs="A74228-001-a-0770">yeare</w>
                <pc xml:id="A74228-0880" facs="A74228-001-a-0780">,</pc>
              </l>
              <l xml:id="A74228e-210">
                <w lemma="toppose" ana="#vvb" reg="Toppose" xml:id="A74228-0890" facs="A74228-001-a-0790">Toppose</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74228-0900" facs="A74228-001-a-0800">the</w>
                <c> </c>
                <w lemma="practice" ana="#n1" reg="practice" xml:id="A74228-0910" facs="A74228-001-a-0810">practice</w>
                <c> </c>
                <w lemma="that" ana="#cs" reg="that" xml:id="A74228-0920" facs="A74228-001-a-0820">dat</w>
                <c> </c>
                <w lemma="be" ana="#vvz" reg="is" xml:id="A74228-0930" facs="A74228-001-a-0830">is</w>
                <c> </c>
                <w lemma="now" ana="#av" reg="now" xml:id="A74228-0940" facs="A74228-001-a-0840">now</w>
                <c> </c>
                <w lemma="on" ana="#acp-p" reg="on" xml:id="A74228-0950" facs="A74228-001-a-0850">on</w>
                <c> </c>
                <w lemma="foot" ana="#n1" reg="foot" xml:id="A74228-0960" facs="A74228-001-a-0860">foote</w>
              </l>
              <l xml:id="A74228e-220">
                <w lemma="which" ana="#crq-r" reg="Which" xml:id="A74228-0970" facs="A74228-001-a-0870">Which</w>
                <c> </c>
                <w lemma="pluck" ana="#vvz" reg="plucks" xml:id="A74228-0980" facs="A74228-001-a-0880">plucks</w>
                <c> </c>
                <w lemma="my" ana="#po" reg="my" xml:id="A74228-0990" facs="A74228-001-a-0890">my</w>
                <c> </c>
                <w lemma="pretnren" ana="#n2" reg="pretnrens" xml:id="A74228-1000" facs="A74228-001-a-0900">Pretnren</w>
                <c> </c>
                <w lemma="up" ana="#acp-av" reg="up" xml:id="A74228-1010" facs="A74228-001-a-0910">vp</w>
                <c> </c>
                <w lemma="poth" ana="#fw-mi" reg="poth" xml:id="A74228-1020" facs="A74228-001-a-0920">poth</w>
                <c> </c>
                <w lemma="pranch" ana="#n1" reg="pranch" xml:id="A74228-1030" facs="A74228-001-a-0930">pranch</w>
                <c> </c>
                <w lemma="and" ana="#cc" reg="and" xml:id="A74228-1040" facs="A74228-001-a-0940">and</w>
                <c> </c>
                <w lemma="root" ana="#n1" reg="root" xml:id="A74228-1050" facs="A74228-001-a-0950">roote</w>
                <pc xml:id="A74228-1060" facs="A74228-001-a-0960">:</pc>
              </l>
              <l xml:id="A74228e-230">
                <w lemma="my" ana="#po" reg="My" xml:id="A74228-1070" facs="A74228-001-a-0970">My</w>
                <c> </c>
                <w lemma="posture" ana="#n1" reg="posture" xml:id="A74228-1080" facs="A74228-001-a-0980">posture</w>
                <c> </c>
                <w lemma="and" ana="#cc" reg="and" xml:id="A74228-1090" facs="A74228-001-a-0990">and</w>
                <c> </c>
                <w lemma="my" ana="#po" reg="my" xml:id="A74228-1100" facs="A74228-001-a-1000">my</w>
                <c> </c>
                <w lemma="heart" ana="#n1" reg="heart" xml:id="A74228-1110" facs="A74228-001-a-1010">hart</w>
                <c> </c>
                <w lemma="do" ana="#vvz" reg="doth" xml:id="A74228-1120" facs="A74228-001-a-1020">doth</w>
                <c> </c>
                <w lemma="well" ana="#av" reg="well" xml:id="A74228-1130" facs="A74228-001-a-1030">well</w>
                <c> </c>
                <w lemma="agree" ana="#vvi" reg="agree" xml:id="A74228-1140" facs="A74228-001-a-1040">agree</w>
              </l>
              <l xml:id="A74228e-240">
                <w lemma="to" ana="#acp-cs" reg="To" xml:id="A74228-1150" facs="A74228-001-a-1050">To</w>
                <c> </c>
                <w lemma="fight" ana="#vvi" reg="fight" xml:id="A74228-1160" facs="A74228-001-a-1060">fight</w>
                <pc xml:id="A74228-1170" facs="A74228-001-a-1070">,</pc>
                <c> </c>
                <w lemma="now" ana="#av" reg="now" xml:id="A74228-1180" facs="A74228-001-a-1080">now</w>
                <c> </c>
                <w lemma="plyd" ana="#vvd" reg="plyd" xml:id="A74228-1190" facs="A74228-001-a-1090">plyd</w>
                <c> </c>
                <w lemma="be" ana="#vvz" reg="is" xml:id="A74228-1200" facs="A74228-001-a-1100">is</w>
                <c> </c>
                <w lemma="up" ana="#acp-av" reg="up" xml:id="A74228-1210" facs="A74228-001-a-1110">vp</w>
                <pc xml:id="A74228-1220" facs="A74228-001-a-1120">:</pc>
                <c> </c>
                <w lemma="come" ana="#vvb" reg="come" xml:id="A74228-1230" facs="A74228-001-a-1130">come</w>
                <c> </c>
                <w lemma="follow" ana="#vvb" reg="follow" xml:id="A74228-1240" facs="A74228-001-a-1140">follow</w>
                <c> </c>
                <w lemma="i" ana="#pno" reg="me" xml:id="A74228-1250" facs="A74228-001-a-1150">mee</w>
              </l>
            </lg>
          </figure>
        </p>
        <p xml:id="A74228e-250">
          <figure xml:id="A74228e-260">
            <figDesc xml:id="A74228e-270">portrait of Thomas Lunsford</figDesc>
            <lg xml:id="A74228e-280">
              <l xml:id="A74228e-290">
                <w lemma="I|will" ana="#pns|vmb" reg="I'll" xml:id="A74228-1360" facs="A74228-001-a-1160">I'le</w>
                <c> </c>
                <w lemma="help" ana="#vvi" reg="help" xml:id="A74228-1370" facs="A74228-001-a-1170">helpe</w>
                <c> </c>
                <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74228-1380" facs="A74228-001-a-1180">to</w>
                <c> </c>
                <w lemma="kill" ana="#vvi" reg="kill" xml:id="A74228-1390" facs="A74228-001-a-1190">kill</w>
                <pc xml:id="A74228-1400" facs="A74228-001-a-1200">,</pc>
                <c> </c>
                <w lemma="to" ana="#acp-p" reg="to" xml:id="A74228-1410" facs="A74228-001-a-1210">to</w>
                <c> </c>
                <w lemma="pillage" ana="#n1" reg="pillage" xml:id="A74228-1420" facs="A74228-001-a-1220">pillage</w>
                <c> </c>
                <w lemma="and" ana="#cc" reg="and" xml:id="A74228-1430" facs="A74228-001-a-1230">and</w>
                <c> </c>
                <w lemma="destroy" ana="#vvi" reg="destroy" xml:id="A74228-1440" facs="A74228-001-a-1240">destroy</w>
              </l>
              <l xml:id="A74228e-300">
                <w lemma="all" ana="#d" reg="All" xml:id="A74228-1450" facs="A74228-001-a-1250">All</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74228-1460" facs="A74228-001-a-1260">the</w>
                <c> </c>
                <w lemma="opposer" ana="#n2" reg="opposers" xml:id="A74228-1470" facs="A74228-001-a-1270">Opposers</w>
                <c> </c>
                <w lemma="of" ana="#acp-p" reg="of" xml:id="A74228-1480" facs="A74228-001-a-1280">of</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74228-1490" facs="A74228-001-a-1290">the</w>
                <c> </c>
                <w lemma="prelacy" ana="#n1" reg="prelacy" xml:id="A74228-1500" facs="A74228-001-a-1300">Prelacy</w>
                <pc unit="sentence" xml:id="A74228-1510" facs="A74228-001-a-1310">.</pc>
                <c> </c>
              </l>
              <l xml:id="A74228e-310">
                <w lemma="my" ana="#po" reg="My" xml:id="A74228-1520" facs="A74228-001-a-1320">My</w>
                <c> </c>
                <w lemma="fortune" ana="#n2" reg="fortunes" xml:id="A74228-1530" facs="A74228-001-a-1330">Fortunes</w>
                <c> </c>
                <w lemma="be" ana="#vvb" reg="are" xml:id="A74228-1540" facs="A74228-001-a-1340">are</w>
                <c> </c>
                <w lemma="growme" ana="#n1" reg="growme" xml:id="A74228-1550" facs="A74228-001-a-1350">growme</w>
                <c> </c>
                <w lemma="small" ana="#j" reg="small" xml:id="A74228-1560" facs="A74228-001-a-1360">small</w>
                <pc xml:id="A74228-1570" facs="A74228-001-a-1370">,</pc>
                <c> </c>
                <w lemma="my" ana="#po" reg="my" xml:id="A74228-1580" facs="A74228-001-a-1380">my</w>
                <c> </c>
                <w lemma="friend" ana="#n2" reg="friends" xml:id="A74228-1590" facs="A74228-001-a-1390">Freinds</w>
                <c> </c>
                <w lemma="be" ana="#vvb" reg="are" xml:id="A74228-1600" facs="A74228-001-a-1400">are</w>
                <c> </c>
                <w lemma="less" ana="#av-c_d" reg="less" xml:id="A74228-1610" facs="A74228-001-a-1410">less</w>
              </l>
              <l xml:id="A74228e-320">
                <w lemma="I|will" ana="#pns|vmb" reg="I'll" xml:id="A74228-1620" facs="A74228-001-a-1420">Ile</w>
                <c> </c>
                <w lemma="venture" ana="#vvi" reg="venture" xml:id="A74228-1630" facs="A74228-001-a-1430">venter</w>
                <c> </c>
                <w lemma="therefore" ana="#av" reg="therefore" xml:id="A74228-1640" facs="A74228-001-a-1440">therefore</w>
                <c> </c>
                <w lemma="life" ana="#n1" reg="life" xml:id="A74228-1650" facs="A74228-001-a-1450">life</w>
                <c> </c>
                <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74228-1660" facs="A74228-001-a-1460">to</w>
                <c> </c>
                <w lemma="have" ana="#vvi" reg="have" xml:id="A74228-1670" facs="A74228-001-a-1470">have</w>
                <c> </c>
                <w lemma="redress" ana="#n1" reg="redress" xml:id="A74228-1680" facs="A74228-001-a-1480">redress</w>
              </l>
              <l xml:id="A74228e-330">
                <w lemma="by" ana="#acp-p" reg="By" xml:id="A74228-1690" facs="A74228-001-a-1490">By</w>
                <c> </c>
                <w lemma="pick" ana="#vvg" reg="picking" xml:id="A74228-1700" facs="A74228-001-a-1500">picking</w>
                <pc xml:id="A74228-1710" facs="A74228-001-a-1510">,</pc>
                <c> </c>
                <w lemma="steal" ana="#vvg" reg="stealing" xml:id="A74228-1720" facs="A74228-001-a-1520">stealing</w>
                <pc xml:id="A74228-1730" facs="A74228-001-a-1530">,</pc>
                <c> </c>
                <w lemma="or" ana="#cc" reg="or" xml:id="A74228-1740" facs="A74228-001-a-1540">or</w>
                <c> </c>
                <w lemma="by" ana="#acp-p" reg="by" xml:id="A74228-1750" facs="A74228-001-a-1550">by</w>
                <c> </c>
                <w lemma="cut" ana="#vvg" reg="cutting" xml:id="A74228-1760" facs="A74228-001-a-1560">cutting</w>
                <c> </c>
                <w lemma="throat" ana="#n2" reg="throats" xml:id="A74228-1770" facs="A74228-001-a-1570">throates</w>
                <pc unit="sentence" xml:id="A74228-1780" facs="A74228-001-a-1580">.</pc>
                <c> </c>
              </l>
              <l xml:id="A74228e-340">
                <w lemma="although" ana="#cs" reg="Although" xml:id="A74228-1790" facs="A74228-001-a-1590">Although</w>
                <c> </c>
                <w lemma="my" ana="#po" reg="my" xml:id="A74228-1800" facs="A74228-001-a-1600">my</w>
                <c> </c>
                <w lemma="practice" ana="#n1" reg="practice" xml:id="A74228-1810" facs="A74228-001-a-1610">practice</w>
                <c> </c>
                <w lemma="cross" ana="#vvi" reg="cross" xml:id="A74228-1820" facs="A74228-001-a-1620">crosse</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74228-1830" facs="A74228-001-a-1630">the</w>
                <c> </c>
                <w lemma="kingdom" ana="#n2" reg="kingdoms" xml:id="A74228-1840" facs="A74228-001-a-1640">Kingdoms</w>
                <c> </c>
                <w lemma="vote" ana="#n2" reg="votes" xml:id="A74228-1850" facs="A74228-001-a-1650">votes</w>
                <pc unit="sentence" xml:id="A74228-1860" facs="A74228-001-a-1660">.</pc>
                <c> </c>
              </l>
            </lg>
          </figure>
        </p>
      </div>
    </body>
    <back xml:id="A74228e-350">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>