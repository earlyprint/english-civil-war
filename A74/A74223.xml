<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Martis, 20. Maii. 1642. It is this day ordered by the Lords and Commons in Parliament assembled, that the magazines of the severall counties in England and Wales, shall be forthwith put in the power of the lord lieutenants of the said counties, ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74223</idno>
        <idno type="STC">Wing E1605</idno>
        <idno type="STC">Thomason 669.f.5[28]</idno>
        <idno type="STC">ESTC R210540</idno>
        <idno type="EEBO-CITATION">99869327</idno>
        <idno type="PROQUEST">99869327</idno>
        <idno type="VID">160741</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74223)</note>
        <note>Transcribed from: (Early English Books Online ; image set 160741)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[28])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Martis, 20. Maii. 1642. It is this day ordered by the Lords and Commons in Parliament assembled, that the magazines of the severall counties in England and Wales, shall be forthwith put in the power of the lord lieutenants of the said counties, ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed for Joseph Hunscott,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1642.</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from caption and first lines of text.</note>
            <note>Reproduction of the original in the British Library.</note>
            <note>With engraved border.</note>
            <note>Order for printing signed: Joh. Brown, Cler. Parl.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>England and Wales. -- Parliament -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74223e-10">
    <body xml:id="A74223e-20">
      <div type="Parliamentary_ordinance" xml:id="A74223e-30">
        <pb facs="tcp:160741:1" rendition="simple:additions" xml:id="A74223-001-a"/>
        <opener xml:id="A74223e-40">
          <dateline xml:id="A74223e-50">
            <date xml:id="A74223e-60">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A74223-0050" facs="A74223-001-a-0010">Die</w>
              <c> </c>
              <w lemma="Martis" ana="#n1-nn" reg="Martis" xml:id="A74223-0060" facs="A74223-001-a-0020">Martis</w>
              <pc xml:id="A74223-0070" facs="A74223-001-a-0030">,</pc>
              <c> </c>
              <hi xml:id="A74223e-70">
                <w lemma="20." ana="#crd" reg="20." xml:id="A74223-0080" facs="A74223-001-a-0040">20.</w>
                <pc unit="sentence" xml:id="A74223-0080-eos" facs="A74223-001-a-0050"/>
                <c> </c>
              </hi>
              <w lemma="maii." ana="#n-ab" reg="maii." xml:id="A74223-0090" facs="A74223-001-a-0060">Maii.</w>
              <c> </c>
              <hi xml:id="A74223e-80">
                <w lemma="1642" ana="#crd" reg="1642" xml:id="A74223-0100" facs="A74223-001-a-0070">1642</w>
                <pc unit="sentence" xml:id="A74223-0110" facs="A74223-001-a-0080">.</pc>
                <c> </c>
              </hi>
            </date>
          </dateline>
        </opener>
        <p xml:id="A74223e-90">
          <w lemma="it" ana="#pn" reg="It" rend="initialcharacterdecorated" xml:id="A74223-0140" facs="A74223-001-a-0090">IT</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A74223-0150" facs="A74223-001-a-0100">is</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74223-0160" facs="A74223-001-a-0110">this</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A74223-0170" facs="A74223-001-a-0120">day</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A74223-0180" facs="A74223-001-a-0130">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74223-0190" facs="A74223-001-a-0140">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0200" facs="A74223-001-a-0150">the</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A74223-0210" facs="A74223-001-a-0160">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74223-0220" facs="A74223-001-a-0170">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74223-0230" facs="A74223-001-a-0180">Commons</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74223-0240" facs="A74223-001-a-0190">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74223-0250" facs="A74223-001-a-0200">Parliament</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A74223-0260" facs="A74223-001-a-0210">assembled</w>
          <pc xml:id="A74223-0270" facs="A74223-001-a-0220">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74223-0280" facs="A74223-001-a-0230">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0290" facs="A74223-001-a-0240">the</w>
          <c> </c>
          <w lemma="magazine" ana="#n2" reg="magazines" xml:id="A74223-0300" facs="A74223-001-a-0250">Magazines</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74223-0310" facs="A74223-001-a-0260">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0320" facs="A74223-001-a-0270">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A74223-0330" facs="A74223-001-a-0280">severall</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74223-0340" facs="A74223-001-a-0290">Counties</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74223-0350" facs="A74223-001-a-0300">in</w>
          <c> </c>
          <hi xml:id="A74223e-100">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A74223-0360" facs="A74223-001-a-0310">England</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74223-0370" facs="A74223-001-a-0320">and</w>
          <c> </c>
          <hi xml:id="A74223e-110">
            <w lemma="wales" ana="#n1-nn" reg="Wales" xml:id="A74223-0380" facs="A74223-001-a-0330">Wales</w>
            <pc xml:id="A74223-0390" facs="A74223-001-a-0340">,</pc>
          </hi>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74223-0400" facs="A74223-001-a-0350">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74223-0410" facs="A74223-001-a-0360">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A74223-0420" facs="A74223-001-a-0370">forthwith</w>
          <c> </c>
          <w lemma="put" ana="#vvn" reg="put" xml:id="A74223-0430" facs="A74223-001-a-0380">put</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A74223-0440" facs="A74223-001-a-0390">into</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0450" facs="A74223-001-a-0400">the</w>
          <c> </c>
          <w lemma="power" ana="#n1" reg="power" xml:id="A74223-0460" facs="A74223-001-a-0410">power</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74223-0470" facs="A74223-001-a-0420">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0480" facs="A74223-001-a-0430">the</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A74223-0490" facs="A74223-001-a-0440">Lord</w>
          <c> </c>
          <w lemma="lieutenant" ana="#n2" reg="lieutenants" xml:id="A74223-0500" facs="A74223-001-a-0450">Lievtenants</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74223-0510" facs="A74223-001-a-0460">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0520" facs="A74223-001-a-0470">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A74223-0530" facs="A74223-001-a-0480">said</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74223-0540" facs="A74223-001-a-0490">Counties</w>
          <pc xml:id="A74223-0550" facs="A74223-001-a-0500">,</pc>
          <c> </c>
          <w lemma="respective" ana="#av_j" reg="respectively" xml:id="A74223-0560" facs="A74223-001-a-0510">respectively</w>
          <pc xml:id="A74223-0570" facs="A74223-001-a-0520">,</pc>
          <c> </c>
          <pc xml:id="A74223-0580" facs="A74223-001-a-0530">(</pc>
          <w lemma="be" ana="#vvg" reg="Being" xml:id="A74223-0590" facs="A74223-001-a-0540">being</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74223-0600" facs="A74223-001-a-0550">such</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A74223-0610" facs="A74223-001-a-0560">as</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0620" facs="A74223-001-a-0570">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74223-0630" facs="A74223-001-a-0580">Parliament</w>
          <c> </c>
          <w lemma="do" ana="#vvz" reg="doth" xml:id="A74223-0640" facs="A74223-001-a-0590">doth</w>
          <c> </c>
          <w lemma="confide" ana="#vvi" reg="confide" xml:id="A74223-0650" facs="A74223-001-a-0600">confide</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74223-0660" facs="A74223-001-a-0610">in</w>
          <pc xml:id="A74223-0670" facs="A74223-001-a-0620">)</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74223-0680" facs="A74223-001-a-0630">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0690" facs="A74223-001-a-0640">the</w>
          <c> </c>
          <w lemma="service" ana="#n1" reg="service" xml:id="A74223-0700" facs="A74223-001-a-0650">Service</w>
          <pc xml:id="A74223-0710" facs="A74223-001-a-0660">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74223-0720" facs="A74223-001-a-0670">and</w>
          <c> </c>
          <w lemma="safety" ana="#n1" reg="safety" xml:id="A74223-0730" facs="A74223-001-a-0680">safety</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74223-0740" facs="A74223-001-a-0690">of</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74223-0750" facs="A74223-001-a-0700">His</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A74223-0760" facs="A74223-001-a-0710">Majesty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74223-0770" facs="A74223-001-a-0720">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0780" facs="A74223-001-a-0730">the</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A74223-0790" facs="A74223-001-a-0740">Kingdom</w>
          <pc unit="sentence" xml:id="A74223-0800" facs="A74223-001-a-0750">.</pc>
        </p>
        <p xml:id="A74223e-120">
          <w lemma="order" ana="#j_vn" reg="Ordered" xml:id="A74223-0830" facs="A74223-001-a-0760">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74223-0840" facs="A74223-001-a-0770">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74223-0850" facs="A74223-001-a-0780">the</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A74223-0860" facs="A74223-001-a-0790">Lords</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74223-0870" facs="A74223-001-a-0800">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74223-0880" facs="A74223-001-a-0810">Parliament</w>
          <pc xml:id="A74223-0890" facs="A74223-001-a-0820">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74223-0900" facs="A74223-001-a-0830">That</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74223-0910" facs="A74223-001-a-0840">this</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A74223-0920" facs="A74223-001-a-0850">Order</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74223-0930" facs="A74223-001-a-0860">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74223-0940" facs="A74223-001-a-0870">be</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A74223-0950" facs="A74223-001-a-0880">Printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74223-0960" facs="A74223-001-a-0890">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A74223-0970" facs="A74223-001-a-0900">Published</w>
          <pc unit="sentence" xml:id="A74223-0980" facs="A74223-001-a-0910">.</pc>
        </p>
        <closer xml:id="A74223e-130">
          <signed xml:id="A74223e-140">
            <w lemma="joh." ana="#n-ab" reg="Joh." xml:id="A74223-1020" facs="A74223-001-a-0920">Joh.</w>
            <c> </c>
            <w lemma="Brown" ana="#n1-nn" reg="Brown" xml:id="A74223-1030" facs="A74223-001-a-0930">Brown</w>
            <pc xml:id="A74223-1040" facs="A74223-001-a-0940">,</pc>
            <c> </c>
            <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A74223-1050" facs="A74223-001-a-0950">Cler.</w>
            <c> </c>
            <w lemma="parl." ana="#n-ab" reg="parl." xml:id="A74223-1060" facs="A74223-001-a-0960">Parl.</w>
            <pc unit="sentence" xml:id="A74223-1060-eos" facs="A74223-001-a-0970"/>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A74223e-150">
      <div type="colophon" xml:id="A74223e-160">
        <p xml:id="A74223e-170">
          <hi xml:id="A74223e-180">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A74223-1110" facs="A74223-001-a-0980">London</w>
            <pc xml:id="A74223-1120" facs="A74223-001-a-0990">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A74223-1130" facs="A74223-001-a-1000">Printed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74223-1140" facs="A74223-001-a-1010">for</w>
          <c> </c>
          <hi xml:id="A74223e-190">
            <w lemma="Joseph" ana="#n1-nn" reg="Joseph" xml:id="A74223-1150" facs="A74223-001-a-1020">Joseph</w>
            <c> </c>
            <w lemma="Hunscott" ana="#n1-nn" reg="Hunscott" xml:id="A74223-1160" facs="A74223-001-a-1030">Hunscott</w>
            <pc unit="sentence" xml:id="A74223-1170" facs="A74223-001-a-1040">.</pc>
            <c> </c>
          </hi>
          <w lemma="1642." ana="#crd" reg="1642." xml:id="A74223-1180" facs="A74223-001-a-1050">1642.</w>
          <pc unit="sentence" xml:id="A74223-1180-eos" facs="A74223-001-a-1060"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>