<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Die Jovis 2d. Junij, 1642. It is this day ordered by the Commons now assembled in Parliament, that the severall members of this house, doe forthwith give their attendance upon the publique service of this Commonwealth ...</title>
        <author>England and Wales. Parliament. House of Commons.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1642</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74224</idno>
        <idno type="STC">Thomason 669.f.5[32]</idno>
        <idno type="STC">ESTC R210585</idno>
        <idno type="EEBO-CITATION">99869369</idno>
        <idno type="PROQUEST">99869369</idno>
        <idno type="VID">160745</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74224)</note>
        <note>Transcribed from: (Early English Books Online ; image set 160745)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[32])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Die Jovis 2d. Junij, 1642. It is this day ordered by the Commons now assembled in Parliament, that the severall members of this house, doe forthwith give their attendance upon the publique service of this Commonwealth ...</title>
            <author>England and Wales. Parliament. House of Commons.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed for Joseph Hunscott,</publisher>
            <pubPlace>[London] :</pubPlace>
            <date>1642.</date>
          </publicationStmt>
          <notesStmt>
            <note>Place of publication from Steele.</note>
            <note>Reproduction of the original in the British Library.</note>
            <note>"Ordered that it be forthwith printed. H. Elsynge Cler:Parl:D:Com."</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>England and Wales. -- Parliament -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74224e-10">
    <body xml:id="A74224e-20">
      <div type="letter" xml:id="A74224e-30">
        <pb facs="tcp:160745:1" rendition="simple:additions" xml:id="A74224-001-a"/>
        <opener xml:id="A74224e-40">
          <dateline xml:id="A74224e-50">
            <date xml:id="A74224e-60">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A74224-0050" facs="A74224-001-a-0010">Die</w>
              <c> </c>
              <w lemma="jovis" ana="#fw-la" reg="jovis" xml:id="A74224-0060" facs="A74224-001-a-0020">Jovis</w>
              <c> </c>
              <w lemma="2d" part="I" ana="#crd" reg="2d" xml:id="A74224-0070.1" facs="A74224-001-a-0030" rendition="hi-mid-2">2d</w>
              <hi rend="sup" xml:id="A74224e-70"/>
              <pc unit="sentence" xml:id="A74224-0080" facs="A74224-001-a-0050">.</pc>
              <c> </c>
              <w lemma="Junij" ana="#fw-la" reg="Junij" xml:id="A74224-0090" facs="A74224-001-a-0060">Junij</w>
              <pc xml:id="A74224-0100" facs="A74224-001-a-0070">,</pc>
              <c> </c>
              <w lemma="1642." ana="#crd" reg="1642." xml:id="A74224-0110" facs="A74224-001-a-0080">1642.</w>
              <pc unit="sentence" xml:id="A74224-0110-eos" facs="A74224-001-a-0090"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A74224e-80">
          <w lemma="it" ana="#pn" reg="It" rend="initialcharacterdecorated" xml:id="A74224-0140" facs="A74224-001-a-0100">IT</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A74224-0150" facs="A74224-001-a-0110">is</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-0160" facs="A74224-001-a-0120">this</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A74224-0170" facs="A74224-001-a-0130">day</w>
          <c> </c>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A74224-0180" facs="A74224-001-a-0140">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74224-0190" facs="A74224-001-a-0150">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0200" facs="A74224-001-a-0160">the</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74224-0210" facs="A74224-001-a-0170">Commons</w>
          <c> </c>
          <w lemma="now" ana="#av" reg="now" xml:id="A74224-0220" facs="A74224-001-a-0180">now</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A74224-0230" facs="A74224-001-a-0190">assembled</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74224-0240" facs="A74224-001-a-0200">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74224-0250" facs="A74224-001-a-0210">Parliament</w>
          <pc xml:id="A74224-0260" facs="A74224-001-a-0220">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74224-0270" facs="A74224-001-a-0230">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0280" facs="A74224-001-a-0240">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A74224-0290" facs="A74224-001-a-0250">severall</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A74224-0300" facs="A74224-001-a-0260">Members</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0310" facs="A74224-001-a-0270">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-0320" facs="A74224-001-a-0280">this</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-0330" facs="A74224-001-a-0290">House</w>
          <pc xml:id="A74224-0340" facs="A74224-001-a-0300">,</pc>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A74224-0350" facs="A74224-001-a-0310">doe</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A74224-0360" facs="A74224-001-a-0320">forthwith</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A74224-0370" facs="A74224-001-a-0330">give</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-0380" facs="A74224-001-a-0340">their</w>
          <c> </c>
          <w lemma="attendance" ana="#n1" reg="attendance" xml:id="A74224-0390" facs="A74224-001-a-0350">Attendance</w>
          <c> </c>
          <w lemma="upon" ana="#acp-p" reg="upon" xml:id="A74224-0400" facs="A74224-001-a-0360">upon</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0410" facs="A74224-001-a-0370">the</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A74224-0420" facs="A74224-001-a-0380">publique</w>
          <c> </c>
          <w lemma="service" ana="#n1" reg="service" xml:id="A74224-0430" facs="A74224-001-a-0390">Service</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0440" facs="A74224-001-a-0400">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-0450" facs="A74224-001-a-0410">this</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A74224-0460" facs="A74224-001-a-0420">Commonwealth</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A74224-0470" facs="A74224-001-a-0430">with</w>
          <c> </c>
          <w lemma="which" ana="#crq-r" reg="which" xml:id="A74224-0480" facs="A74224-001-a-0440">which</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A74224-0490" facs="A74224-001-a-0450">they</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74224-0500" facs="A74224-001-a-0460">are</w>
          <c> </c>
          <w lemma="entrust" ana="#vvn" reg="entrusted" xml:id="A74224-0510" facs="A74224-001-a-0470">entrusted</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74224-0520" facs="A74224-001-a-0480">by</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-0530" facs="A74224-001-a-0490">their</w>
          <c> </c>
          <w lemma="country" ana="#n2" reg="countries" xml:id="A74224-0540" facs="A74224-001-a-0500">Countreys</w>
          <pc unit="sentence" xml:id="A74224-0550" facs="A74224-001-a-0510">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A74224-0560" facs="A74224-001-a-0520">And</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0570" facs="A74224-001-a-0530">the</w>
          <c> </c>
          <w lemma="sheriff" ana="#n2" reg="sheriffs" xml:id="A74224-0580" facs="A74224-001-a-0540">Sheriffs</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0590" facs="A74224-001-a-0550">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0600" facs="A74224-001-a-0560">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A74224-0610" facs="A74224-001-a-0570">severall</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74224-0620" facs="A74224-001-a-0580">Counties</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0630" facs="A74224-001-a-0590">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-0640" facs="A74224-001-a-0600">this</w>
          <c> </c>
          <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A74224-0650" facs="A74224-001-a-0610">Kingdom</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0660" facs="A74224-001-a-0620">of</w>
          <c> </c>
          <hi xml:id="A74224e-90">
            <w lemma="England" ana="#n1-nn" reg="England" xml:id="A74224-0670" facs="A74224-001-a-0630">England</w>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74224-0680" facs="A74224-001-a-0640">and</w>
          <c> </c>
          <w lemma="dominion" ana="#n1" reg="dominion" xml:id="A74224-0690" facs="A74224-001-a-0650">Dominion</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0700" facs="A74224-001-a-0660">of</w>
          <c> </c>
          <hi xml:id="A74224e-100">
            <w lemma="Wales" ana="#n1-nn" reg="Wales" xml:id="A74224-0710" facs="A74224-001-a-0670">Wales</w>
          </hi>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74224-0720" facs="A74224-001-a-0680">are</w>
          <c> </c>
          <w lemma="require" ana="#vvn" reg="required" xml:id="A74224-0730" facs="A74224-001-a-0690">required</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74224-0740" facs="A74224-001-a-0700">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A74224-0750" facs="A74224-001-a-0710">give</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A74224-0760" facs="A74224-001-a-0720">Notice</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0770" facs="A74224-001-a-0730">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-0780" facs="A74224-001-a-0740">this</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A74224-0790" facs="A74224-001-a-0750">Order</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A74224-0800" facs="A74224-001-a-0760">unto</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A74224-0810" facs="A74224-001-a-0770">all</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-0820" facs="A74224-001-a-0780">such</w>
          <c> </c>
          <w lemma="member" ana="#n2" reg="members" xml:id="A74224-0830" facs="A74224-001-a-0790">Members</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0840" facs="A74224-001-a-0800">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-0850" facs="A74224-001-a-0810">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-0860" facs="A74224-001-a-0820">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-0870" facs="A74224-001-a-0830">of</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74224-0880" facs="A74224-001-a-0840">Commons</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A74224-0890" facs="A74224-001-a-0850">as</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74224-0900" facs="A74224-001-a-0860">are</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A74224-0910" facs="A74224-001-a-0870">within</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-0920" facs="A74224-001-a-0880">their</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A74224-0930" facs="A74224-001-a-0890">respective</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74224-0940" facs="A74224-001-a-0900">Counties</w>
          <pc xml:id="A74224-0950" facs="A74224-001-a-0910">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74224-0960" facs="A74224-001-a-0920">and</w>
          <c> </c>
          <w lemma="speedy" ana="#av_j" reg="speedily" xml:id="A74224-0970" facs="A74224-001-a-0930">speedily</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74224-0980" facs="A74224-001-a-0940">to</w>
          <c> </c>
          <w lemma="make" ana="#vvi" reg="make" xml:id="A74224-0990" facs="A74224-001-a-0950">make</w>
          <c> </c>
          <w lemma="return" ana="#n1" reg="return" xml:id="A74224-1000" facs="A74224-001-a-0960">returne</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1010" facs="A74224-001-a-0970">of</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-1020" facs="A74224-001-a-0980">such</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-1030" facs="A74224-001-a-0990">their</w>
          <c> </c>
          <w lemma="do" ana="#n2_vg" reg="doings" xml:id="A74224-1040" facs="A74224-001-a-1000">doings</w>
          <pc xml:id="A74224-1050" facs="A74224-001-a-1010">,</pc>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A74224-1060" facs="A74224-001-a-1020">unto</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-1070" facs="A74224-001-a-1030">the</w>
          <c> </c>
          <hi xml:id="A74224e-110">
            <w lemma="speaker" ana="#n1" reg="speaker" xml:id="A74224-1080" facs="A74224-001-a-1040">Speaker</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1090" facs="A74224-001-a-1050">of</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A74224-1100" facs="A74224-001-a-1060">that</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-1110" facs="A74224-001-a-1070">House</w>
          <pc xml:id="A74224-1120" facs="A74224-001-a-1080">:</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74224-1130" facs="A74224-001-a-1090">And</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A74224-1140" facs="A74224-001-a-1100">all</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-1150" facs="A74224-001-a-1110">such</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A74224-1160" facs="A74224-001-a-1120">as</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74224-1170" facs="A74224-001-a-1130">shall</w>
          <c> </c>
          <w lemma="not" ana="#xx" reg="not" xml:id="A74224-1180" facs="A74224-001-a-1140">not</w>
          <c> </c>
          <w lemma="make" ana="#vvi" reg="make" xml:id="A74224-1190" facs="A74224-001-a-1150">make</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-1200" facs="A74224-001-a-1160">their</w>
          <c> </c>
          <w lemma="personal" ana="#j" reg="personal" xml:id="A74224-1210" facs="A74224-001-a-1170">personall</w>
          <c> </c>
          <w lemma="appearance" ana="#n1" reg="appearance" xml:id="A74224-1220" facs="A74224-001-a-1180">apperance</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74224-1230" facs="A74224-001-a-1190">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-1240" facs="A74224-001-a-1200">the</w>
          <c> </c>
          <w lemma="sixteen" ana="#ord" reg="sixteenth" xml:id="A74224-1250" facs="A74224-001-a-1210">sixteenth</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A74224-1260" facs="A74224-001-a-1220">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1270" facs="A74224-001-a-1230">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-1280" facs="A74224-001-a-1240">this</w>
          <c> </c>
          <w lemma="instant" ana="#j" reg="instant" xml:id="A74224-1290" facs="A74224-001-a-1250">instant</w>
          <c> </c>
          <hi xml:id="A74224e-120">
            <w lemma="june" ana="#n1-nn" reg="June" xml:id="A74224-1300" facs="A74224-001-a-1260">Iune</w>
            <pc xml:id="A74224-1310" facs="A74224-001-a-1270">,</pc>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74224-1320" facs="A74224-001-a-1280">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-1330" facs="A74224-001-a-1290">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-1340" facs="A74224-001-a-1300">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1350" facs="A74224-001-a-1310">of</w>
          <c> </c>
          <w lemma="Commons" ana="#n2-nn" reg="Commons" xml:id="A74224-1360" facs="A74224-001-a-1320">Commons</w>
          <pc xml:id="A74224-1370" facs="A74224-001-a-1330">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74224-1380" facs="A74224-001-a-1340">shall</w>
          <c> </c>
          <w lemma="each" ana="#d" reg="each" xml:id="A74224-1390" facs="A74224-001-a-1350">each</w>
          <c> </c>
          <w lemma="one" ana="#pi" reg="one" xml:id="A74224-1400" facs="A74224-001-a-1360">one</w>
          <c> </c>
          <w lemma="forfeit" ana="#vvb" reg="forfeit" xml:id="A74224-1410" facs="A74224-001-a-1370">forfeit</w>
          <c> </c>
          <w lemma="one" ana="#crd" reg="one" xml:id="A74224-1420" facs="A74224-001-a-1380">One</w>
          <c> </c>
          <w lemma="hundred" ana="#crd" reg="hundred" xml:id="A74224-1430" facs="A74224-001-a-1390">hundred</w>
          <c> </c>
          <w lemma="pound" ana="#n2" reg="pounds" xml:id="A74224-1440" facs="A74224-001-a-1400">pounds</w>
          <pc xml:id="A74224-1450" facs="A74224-001-a-1410">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74224-1460" facs="A74224-001-a-1420">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74224-1470" facs="A74224-001-a-1430">be</w>
          <c> </c>
          <w lemma="dispose" ana="#vvn" reg="disposed" xml:id="A74224-1480" facs="A74224-001-a-1440">disposed</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1490" facs="A74224-001-a-1450">of</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74224-1500" facs="A74224-001-a-1460">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-1510" facs="A74224-001-a-1470">the</w>
          <c> </c>
          <w lemma="war" ana="#n2" reg="wars" xml:id="A74224-1520" facs="A74224-001-a-1480">Warrs</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74224-1530" facs="A74224-001-a-1490">in</w>
          <c> </c>
          <hi xml:id="A74224e-130">
            <w lemma="Ireland" ana="#n1-nn" reg="Ireland" xml:id="A74224-1540" facs="A74224-001-a-1500">Ireland</w>
            <pc xml:id="A74224-1550" facs="A74224-001-a-1510">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74224-1560" facs="A74224-001-a-1520">and</w>
          <c> </c>
          <w lemma="undergo" ana="#vvi" reg="undergo" xml:id="A74224-1570" facs="A74224-001-a-1530">undergo</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-1580" facs="A74224-001-a-1540">such</w>
          <c> </c>
          <w lemma="further" ana="#j-c" reg="further" xml:id="A74224-1590" facs="A74224-001-a-1550">further</w>
          <c> </c>
          <w lemma="censure" ana="#n1" reg="censure" xml:id="A74224-1600" facs="A74224-001-a-1560">censure</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74224-1610" facs="A74224-001-a-1570">and</w>
          <c> </c>
          <w lemma="punishment" ana="#n1" reg="punishment" xml:id="A74224-1620" facs="A74224-001-a-1580">punishment</w>
          <c> </c>
          <w lemma="as" ana="#acp-p" reg="as" xml:id="A74224-1630" facs="A74224-001-a-1590">as</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74224-1640" facs="A74224-001-a-1600">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A74224-1650" facs="A74224-001-a-1610">said</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-1660" facs="A74224-001-a-1620">House</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74224-1670" facs="A74224-001-a-1630">shall</w>
          <c> </c>
          <w lemma="think" ana="#vvi" reg="think" xml:id="A74224-1680" facs="A74224-001-a-1640">thinke</w>
          <c> </c>
          <w lemma="fit" ana="#j" reg="fit" xml:id="A74224-1690" facs="A74224-001-a-1650">fit</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74224-1700" facs="A74224-001-a-1660">for</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A74224-1710" facs="A74224-001-a-1670">so</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74224-1720" facs="A74224-001-a-1680">great</w>
          <c> </c>
          <w lemma="neglect" ana="#n1" reg="neglect" xml:id="A74224-1730" facs="A74224-001-a-1690">neglect</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74224-1740" facs="A74224-001-a-1700">of</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-1750" facs="A74224-001-a-1710">their</w>
          <c> </c>
          <w lemma="duty" ana="#n1" reg="duty" xml:id="A74224-1760" facs="A74224-001-a-1720">duty</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74224-1770" facs="A74224-001-a-1730">in</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A74224-1780" facs="A74224-001-a-1740">a</w>
          <c> </c>
          <w lemma="time" ana="#n1" reg="time" xml:id="A74224-1790" facs="A74224-001-a-1750">time</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74224-1800" facs="A74224-001-a-1760">that</w>
          <c> </c>
          <w lemma="so" ana="#av" reg="so" xml:id="A74224-1810" facs="A74224-001-a-1770">so</w>
          <c> </c>
          <w lemma="necessary" ana="#av_j" reg="necessarily" xml:id="A74224-1820" facs="A74224-001-a-1780">necessarily</w>
          <c> </c>
          <w lemma="require" ana="#vvz" reg="requires" xml:id="A74224-1830" facs="A74224-001-a-1790">requires</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-1840" facs="A74224-001-a-1800">their</w>
          <c> </c>
          <w lemma="assistance" ana="#n1" reg="assistance" xml:id="A74224-1850" facs="A74224-001-a-1810">assistance</w>
          <pc unit="sentence" xml:id="A74224-1860" facs="A74224-001-a-1820">.</pc>
        </p>
        <p xml:id="A74224e-140">
          <w lemma="provide" ana="#vvn" reg="Provided" xml:id="A74224-1890" facs="A74224-001-a-1830">Provided</w>
          <c> </c>
          <w lemma="always" ana="#av" reg="always" xml:id="A74224-1900" facs="A74224-001-a-1840">alwayes</w>
          <pc xml:id="A74224-1910" facs="A74224-001-a-1850">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74224-1920" facs="A74224-001-a-1860">That</w>
          <c> </c>
          <w lemma="all" ana="#d" reg="all" xml:id="A74224-1930" facs="A74224-001-a-1870">all</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-1940" facs="A74224-001-a-1880">such</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A74224-1950" facs="A74224-001-a-1890">as</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74224-1960" facs="A74224-001-a-1900">are</w>
          <c> </c>
          <w lemma="special" ana="#av_j" reg="specially" xml:id="A74224-1970" facs="A74224-001-a-1910">specially</w>
          <c> </c>
          <w lemma="employ" ana="#vvn" reg="employed" xml:id="A74224-1980" facs="A74224-001-a-1920">imployed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74224-1990" facs="A74224-001-a-1930">by</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74224-2000" facs="A74224-001-a-1940">this</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74224-2010" facs="A74224-001-a-1950">House</w>
          <pc xml:id="A74224-2020" facs="A74224-001-a-1960">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74224-2030" facs="A74224-001-a-1970">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74224-2040" facs="A74224-001-a-1980">to</w>
          <c> </c>
          <w lemma="remain" ana="#vvi" reg="remain" xml:id="A74224-2050" facs="A74224-001-a-1990">remaine</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74224-2060" facs="A74224-001-a-2000">in</w>
          <c> </c>
          <w lemma="such" ana="#d" reg="such" xml:id="A74224-2070" facs="A74224-001-a-2010">such</w>
          <c> </c>
          <w lemma="employment" ana="#n2" reg="employments" xml:id="A74224-2080" facs="A74224-001-a-2020">imployments</w>
          <pc xml:id="A74224-2090" facs="A74224-001-a-2030">,</pc>
          <c> </c>
          <w lemma="until" ana="#acp-cs" reg="until" xml:id="A74224-2100" facs="A74224-001-a-2040">untill</w>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A74224-2110" facs="A74224-001-a-2050">they</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74224-2120" facs="A74224-001-a-2060">shall</w>
          <c> </c>
          <w lemma="have" ana="#vvi" reg="have" xml:id="A74224-2130" facs="A74224-001-a-2070">have</w>
          <c> </c>
          <w lemma="particular" ana="#j" reg="particular" xml:id="A74224-2140" facs="A74224-001-a-2080">particular</w>
          <c> </c>
          <w lemma="direction" ana="#n2" reg="directions" xml:id="A74224-2150" facs="A74224-001-a-2090">Directions</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74224-2160" facs="A74224-001-a-2100">for</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74224-2170" facs="A74224-001-a-2110">their</w>
          <c> </c>
          <w lemma="return" ana="#n1" reg="return" xml:id="A74224-2180" facs="A74224-001-a-2120">returne</w>
          <pc unit="sentence" xml:id="A74224-2190" facs="A74224-001-a-2130">.</pc>
        </p>
        <closer xml:id="A74224e-150">
          <hi xml:id="A74224e-160">
            <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A74224-2220" facs="A74224-001-a-2140">Ordered</w>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A74224-2230" facs="A74224-001-a-2150">that</w>
            <c> </c>
            <w lemma="it" ana="#pn" reg="it" xml:id="A74224-2240" facs="A74224-001-a-2160">it</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="be" xml:id="A74224-2250" facs="A74224-001-a-2170">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A74224-2260" facs="A74224-001-a-2180">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A74224-2270" facs="A74224-001-a-2190">Printed</w>
            <pc unit="sentence" xml:id="A74224-2280" facs="A74224-001-a-2200">.</pc>
            <c> </c>
          </hi>
          <signed xml:id="A74224e-170">
            <w lemma="h." ana="#n-ab" reg="H." xml:id="A74224-2300" facs="A74224-001-a-2210">H.</w>
            <c> </c>
            <w lemma="Elsing" ana="#n1-nn" reg="Elsing" xml:id="A74224-2310" facs="A74224-001-a-2220">Elsynge</w>
            <c> </c>
            <w lemma="Cler." ana="#n-ab" reg="cler." xml:id="A74224-2320" facs="A74224-001-a-2230">Cler</w>
            <pc xml:id="A74224-2330" facs="A74224-001-a-2240">:</pc>
            <c> </c>
            <w lemma="Parl." ana="#n-ab" reg="parl." xml:id="A74224-2340" facs="A74224-001-a-2250">Parl</w>
            <pc xml:id="A74224-2350" facs="A74224-001-a-2260">:</pc>
            <c> </c>
            <w lemma="d" ana="#sy" reg="d" xml:id="A74224-2360" facs="A74224-001-a-2270">D</w>
            <pc xml:id="A74224-2370" facs="A74224-001-a-2280">:</pc>
            <c> </c>
            <w lemma="com." ana="#n-ab" reg="com." xml:id="A74224-2380" facs="A74224-001-a-2290">Com.</w>
            <pc unit="sentence" xml:id="A74224-2380-eos" facs="A74224-001-a-2300"/>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A74224e-180">
      <div type="colophon" xml:id="A74224e-190">
        <p xml:id="A74224e-200">
          <w lemma="print" ana="#vvn" reg="Printed" xml:id="A74224-2430" facs="A74224-001-a-2310">Printed</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74224-2440" facs="A74224-001-a-2320">for</w>
          <c> </c>
          <hi xml:id="A74224e-210">
            <w lemma="joseph" ana="#n1-nn" reg="Joseph" xml:id="A74224-2450" facs="A74224-001-a-2330">Ioseph</w>
            <c> </c>
            <w lemma="Hunscott" ana="#n1-nn" reg="Hunscott" xml:id="A74224-2460" facs="A74224-001-a-2340">Hunscott</w>
            <pc unit="sentence" xml:id="A74224-2470" facs="A74224-001-a-2350">.</pc>
            <c> </c>
          </hi>
          <w lemma="1642." ana="#crd" reg="1642." xml:id="A74224-2480" facs="A74224-001-a-2360">1642.</w>
          <pc unit="sentence" xml:id="A74224-2480-eos" facs="A74224-001-a-2370"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>