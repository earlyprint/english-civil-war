<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The most excellent Sr. Thomas Firfax Captaine Generall of the armyes raysed for the defence of the King Parliament and Kingdome.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1645</date>
        </edition>
      </editionStmt>
      <extent>Approx. 0 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74089</idno>
        <idno type="STC">Thomason 669.f.10[25]</idno>
        <idno type="STC">ESTC R210359</idno>
        <idno type="EEBO-CITATION">99869167</idno>
        <idno type="PROQUEST">99869167</idno>
        <idno type="VID">162566</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74089)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162566)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f10[25])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The most excellent Sr. Thomas Firfax Captaine Generall of the armyes raysed for the defence of the King Parliament and Kingdome.</title>
            <author>Bressie, W.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.) : ill. (port.)</extent>
          <publicationStmt>
            <publisher>Sold by Peter Stent neare Newgate,</publisher>
            <pubPlace>[London] :</pubPlace>
            <date>[1645]</date>
          </publicationStmt>
          <notesStmt>
            <note>Signed: W. Bressie sculp:.</note>
            <note>A fully engraved equestrian portrait.</note>
            <note>Annotation on Thomason copy: "Aprill 1st 1645".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Fairfax, Thomas Fairfax, -- Baron, 1612-1671 -- Portraits -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-09</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-11</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-11</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74089e-10">
    <body xml:id="A74089e-20">
      <div type="portrait" xml:id="A74089e-30">
        <pb facs="tcp:162566:1" rendition="simple:additions" xml:id="A74089-001-a"/>
        <p xml:id="A74089e-40">
          <figure xml:id="A74089e-50">
            <figDesc xml:id="A74089e-60">portrait of Thomas Fairfax</figDesc>
            <p xml:id="A74089e-70">
              <w lemma="the" ana="#d" reg="The" xml:id="A74089-050" facs="A74089-001-a-0010">The</w>
              <c> </c>
              <w lemma="most" ana="#av-s_d" reg="most" xml:id="A74089-060" facs="A74089-001-a-0020">most</w>
              <c> </c>
              <w lemma="excellent" ana="#j" reg="excellent" xml:id="A74089-070" facs="A74089-001-a-0030">Excellent</w>
              <c> </c>
              <w lemma="sir" part="I" ana="#n1" reg="Sir" xml:id="A74089-080.1" facs="A74089-001-a-0040" rendition="hi-mid-2">Sr</w>
              <hi rend="sup" xml:id="A74089e-80"/>
              <c> </c>
              <pc xml:id="A74089-090" facs="A74089-001-a-0060">'</pc>
              <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A74089-100" facs="A74089-001-a-0070">Thomas</w>
              <c> </c>
              <w lemma="Firfax" ana="#n1-nn" reg="Firfax" xml:id="A74089-110" facs="A74089-001-a-0080">Firfax</w>
              <c> </c>
              <w lemma="captain" ana="#n1" reg="captain" xml:id="A74089-120" facs="A74089-001-a-0090">Captaine</w>
              <c> </c>
              <w lemma="general" ana="#n1" reg="general" xml:id="A74089-130" facs="A74089-001-a-0100">Generall</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A74089-140" facs="A74089-001-a-0110">of</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A74089-150" facs="A74089-001-a-0120">the</w>
              <c> </c>
              <w lemma="army" ana="#n2" reg="armies" xml:id="A74089-160" facs="A74089-001-a-0130">Armyes</w>
              <c> </c>
              <w lemma="raise" ana="#vvn" reg="raised" xml:id="A74089-170" facs="A74089-001-a-0140">raysed</w>
              <c> </c>
              <w lemma="for" ana="#acp-p" reg="for" xml:id="A74089-180" facs="A74089-001-a-0150">for</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A74089-190" facs="A74089-001-a-0160">the</w>
              <c> </c>
              <w lemma="defence" ana="#n1" reg="defence" xml:id="A74089-200" facs="A74089-001-a-0170">defence</w>
              <c> </c>
              <w lemma="of" ana="#acp-p" reg="of" xml:id="A74089-210" facs="A74089-001-a-0180">of</w>
              <c> </c>
              <w lemma="the" ana="#d" reg="the" xml:id="A74089-220" facs="A74089-001-a-0190">the</w>
              <c> </c>
              <w lemma="king" ana="#n1" reg="King" xml:id="A74089-230" facs="A74089-001-a-0200">King</w>
              <c> </c>
              <w lemma="parlnnent" ana="#fw-fr" reg="parlnnent" xml:id="A74089-240" facs="A74089-001-a-0210">Parlnnent</w>
              <c> </c>
              <w lemma="and" ana="#cc" reg="and" xml:id="A74089-250" facs="A74089-001-a-0220">and</w>
              <c> </c>
              <w lemma="kingdom" ana="#n1" reg="kingdom" xml:id="A74089-260" facs="A74089-001-a-0230">Kingdome</w>
              <pc unit="sentence" xml:id="A74089-270" facs="A74089-001-a-0240">.</pc>
            </p>
            <p xml:id="A74089e-90">
              <w lemma="sell" ana="#vvn" reg="Sold" xml:id="A74089-300" facs="A74089-001-a-0250">sold</w>
              <c> </c>
              <w lemma="by" ana="#acp-p" reg="by" xml:id="A74089-310" facs="A74089-001-a-0260">by</w>
              <c> </c>
              <w lemma="Peter" ana="#n1-nn" reg="Peter" xml:id="A74089-320" facs="A74089-001-a-0270">Peter</w>
              <c> </c>
              <w lemma="stint" ana="#vvb" reg="stint" xml:id="A74089-330" facs="A74089-001-a-0280">Stent</w>
              <c> </c>
              <w lemma="near" ana="#av_j" reg="near" xml:id="A74089-340" facs="A74089-001-a-0290">neare</w>
              <c> </c>
              <w lemma="ne••••te" ana="#j" reg="ne••••te" xml:id="A74089-350" facs="A74089-001-a-0300">Ne••••te</w>
            </p>
            <signed xml:id="A74089e-100">
              <w lemma="W" ana="#n1-nn" reg="W" xml:id="A74089-380" facs="A74089-001-a-0310">W</w>
              <pc xml:id="A74089-390" facs="A74089-001-a-0320">:</pc>
              <c> </c>
              <w lemma="Bressie" ana="#n1-nn" reg="Bressie" xml:id="A74089-400" facs="A74089-001-a-0330">Bressie</w>
              <c> </c>
              <w lemma="sculp" ana="#vvi" reg="sculp" xml:id="A74089-410" facs="A74089-001-a-0340">sculp</w>
              <pc xml:id="A74089-420" facs="A74089-001-a-0350">:</pc>
            </signed>
          </figure>
        </p>
      </div>
    </body>
    <back xml:id="A74089e-110">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>