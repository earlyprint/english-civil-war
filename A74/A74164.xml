<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Tuesday, September 20. 1659. Ordered by the Parliament, that all masters and governors of hospitals be, and are hereby prohibited to grant or renew any leases of any lands, tenements, or hereditaments belonging unto any of the said respective hospitals, until this House take further order</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1659</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74164</idno>
        <idno type="STC">Thomason 669.f.21[74]</idno>
        <idno type="STC">ESTC R211276</idno>
        <idno type="EEBO-CITATION">99870004</idno>
        <idno type="PROQUEST">99870004</idno>
        <idno type="VID">163569</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74164)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163569)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f21[74])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Tuesday, September 20. 1659. Ordered by the Parliament, that all masters and governors of hospitals be, and are hereby prohibited to grant or renew any leases of any lands, tenements, or hereditaments belonging unto any of the said respective hospitals, until this House take further order</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>Printed by John Field, Printer to the Parliament. And are to be sold at the seven Stars in Fleetstreet, over against Dunstans Church,</publisher>
            <pubPlace>London :</pubPlace>
            <date>1659.</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from caption and first lines of text.</note>
            <note>Order to print signed: Tho. St Nicholas, Clerk of the Parliament.</note>
            <note>Annotation on Thomason copy: "Sept. 21".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Hospitals -- England -- Early works to 1800.</term>
          <term>Land use -- Law and legislation -- England -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74164e-10">
    <body xml:id="A74164e-20">
      <div type="proclamation" xml:id="A74164e-30">
        <pb facs="tcp:163569:1" rendition="simple:additions" xml:id="A74164-001-a"/>
        <opener xml:id="A74164e-40">
          <dateline xml:id="A74164e-50">
            <date xml:id="A74164e-60">
              <w lemma="tuesday" ana="#n1-nn" reg="Tuesday" xml:id="A74164-0050" facs="A74164-001-a-0010">Tuesday</w>
              <pc xml:id="A74164-0060" facs="A74164-001-a-0020">,</pc>
              <c> </c>
              <w lemma="September" ana="#n1-nn" reg="September" xml:id="A74164-0070" facs="A74164-001-a-0030">September</w>
              <c> </c>
              <w lemma="20." ana="#crd" reg="20." xml:id="A74164-0080" facs="A74164-001-a-0040">20.</w>
              <c> </c>
              <w lemma="1659." ana="#crd" reg="1659." xml:id="A74164-0090" facs="A74164-001-a-0050">1659.</w>
              <pc unit="sentence" xml:id="A74164-0090-eos" facs="A74164-001-a-0060"/>
            </date>
          </dateline>
          <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A74164-0110" facs="A74164-001-a-0070">Ordered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74164-0120" facs="A74164-001-a-0080">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-0130" facs="A74164-001-a-0090">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74164-0140" facs="A74164-001-a-0100">Parliament</w>
          <pc xml:id="A74164-0150" facs="A74164-001-a-0110">,</pc>
        </opener>
        <p xml:id="A74164e-70">
          <hi xml:id="A74164e-80">
            <w lemma="that" ana="#cs" reg="that" xml:id="A74164-0180" facs="A74164-001-a-0120">That</w>
            <c> </c>
            <w lemma="all" ana="#d" reg="all" xml:id="A74164-0190" facs="A74164-001-a-0130">all</w>
            <c> </c>
            <w lemma="master" ana="#n2" reg="Masters" xml:id="A74164-0200" facs="A74164-001-a-0140">Masters</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A74164-0210" facs="A74164-001-a-0150">and</w>
            <c> </c>
            <w lemma="governor" ana="#n2" reg="governors" xml:id="A74164-0220" facs="A74164-001-a-0160">Governors</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-0230" facs="A74164-001-a-0170">of</w>
            <c> </c>
            <w lemma="hospital" ana="#n2" reg="hospitals" xml:id="A74164-0240" facs="A74164-001-a-0180">Hospitals</w>
            <c> </c>
            <w lemma="be" ana="#vvi" reg="be" xml:id="A74164-0250" facs="A74164-001-a-0190">be</w>
            <pc xml:id="A74164-0260" facs="A74164-001-a-0200">,</pc>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A74164-0270" facs="A74164-001-a-0210">and</w>
            <c> </c>
            <w lemma="be" ana="#vvb" reg="are" xml:id="A74164-0280" facs="A74164-001-a-0220">are</w>
            <c> </c>
            <w lemma="hereby" ana="#av" reg="hereby" xml:id="A74164-0290" facs="A74164-001-a-0230">hereby</w>
            <c> </c>
            <w lemma="prohibit" ana="#vvn" reg="prohibited" xml:id="A74164-0300" facs="A74164-001-a-0240">prohibited</w>
            <c> </c>
            <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74164-0310" facs="A74164-001-a-0250">to</w>
            <c> </c>
            <w lemma="grant" ana="#vvi" reg="grant" xml:id="A74164-0320" facs="A74164-001-a-0260">Grant</w>
            <c> </c>
            <w lemma="or" ana="#cc" reg="or" xml:id="A74164-0330" facs="A74164-001-a-0270">or</w>
            <c> </c>
            <w lemma="renew" ana="#vvb" reg="renew" xml:id="A74164-0340" facs="A74164-001-a-0280">Renew</w>
            <c> </c>
            <w lemma="any" ana="#d" reg="any" xml:id="A74164-0350" facs="A74164-001-a-0290">any</w>
            <c> </c>
            <w lemma="lease" ana="#n2" reg="leases" xml:id="A74164-0360" facs="A74164-001-a-0300">Leases</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-0370" facs="A74164-001-a-0310">of</w>
            <c> </c>
            <w lemma="any" ana="#d" reg="any" xml:id="A74164-0380" facs="A74164-001-a-0320">any</w>
            <c> </c>
            <w lemma="land" ana="#n2" reg="lands" xml:id="A74164-0390" facs="A74164-001-a-0330">Lands</w>
            <pc xml:id="A74164-0400" facs="A74164-001-a-0340">,</pc>
            <c> </c>
            <w lemma="tenement" ana="#n2" reg="tenements" xml:id="A74164-0410" facs="A74164-001-a-0350">Tenements</w>
            <pc xml:id="A74164-0420" facs="A74164-001-a-0360">,</pc>
            <c> </c>
            <w lemma="or" ana="#cc" reg="or" xml:id="A74164-0430" facs="A74164-001-a-0370">or</w>
            <c> </c>
            <w lemma="hereditament" ana="#n2" reg="hereditaments" xml:id="A74164-0440" facs="A74164-001-a-0380">Hereditaments</w>
            <c> </c>
            <w lemma="belong" ana="#vvg" reg="belonging" xml:id="A74164-0450" facs="A74164-001-a-0390">belonging</w>
            <c> </c>
            <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A74164-0460" facs="A74164-001-a-0400">unto</w>
            <c> </c>
            <w lemma="any" ana="#d" reg="any" xml:id="A74164-0470" facs="A74164-001-a-0410">any</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-0480" facs="A74164-001-a-0420">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A74164-0490" facs="A74164-001-a-0430">the</w>
            <c> </c>
            <w lemma="say" ana="#j_vn" reg="said" xml:id="A74164-0500" facs="A74164-001-a-0440">said</w>
            <c> </c>
            <w lemma="respective" ana="#j" reg="respective" xml:id="A74164-0510" facs="A74164-001-a-0450">respective</w>
            <c> </c>
            <w lemma="hospital" ana="#n2" reg="hospitals" xml:id="A74164-0520" facs="A74164-001-a-0460">Hospitals</w>
            <pc xml:id="A74164-0530" facs="A74164-001-a-0470">,</pc>
            <c> </c>
            <w lemma="until" ana="#acp-cs" reg="until" xml:id="A74164-0540" facs="A74164-001-a-0480">until</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A74164-0550" facs="A74164-001-a-0490">this</w>
            <c> </c>
            <w lemma="house" ana="#n1" reg="house" xml:id="A74164-0560" facs="A74164-001-a-0500">House</w>
            <c> </c>
            <w lemma="take" ana="#vvb" reg="take" xml:id="A74164-0570" facs="A74164-001-a-0510">take</w>
            <c> </c>
            <w lemma="further" ana="#j-c" reg="further" xml:id="A74164-0580" facs="A74164-001-a-0520">further</w>
            <c> </c>
            <w lemma="order" ana="#n1" reg="order" xml:id="A74164-0590" facs="A74164-001-a-0530">Order</w>
            <pc unit="sentence" xml:id="A74164-0600" facs="A74164-001-a-0540">.</pc>
            <c> </c>
          </hi>
        </p>
      </div>
      <div type="proclamation" xml:id="A74164e-90">
        <opener xml:id="A74164e-100">
          <dateline xml:id="A74164e-110">
            <date xml:id="A74164e-120">
              <w lemma="tuesday" ana="#n1-nn" reg="Tuesday" xml:id="A74164-0660" facs="A74164-001-a-0550">Tuesday</w>
              <pc xml:id="A74164-0670" facs="A74164-001-a-0560">,</pc>
              <c> </c>
              <w lemma="September" ana="#n1-nn" reg="September" xml:id="A74164-0680" facs="A74164-001-a-0570">September</w>
              <c> </c>
              <w lemma="20." ana="#crd" reg="20." xml:id="A74164-0690" facs="A74164-001-a-0580">20.</w>
              <c> </c>
              <w lemma="1659." ana="#crd" reg="1659." xml:id="A74164-0700" facs="A74164-001-a-0590">1659.</w>
              <pc unit="sentence" xml:id="A74164-0700-eos" facs="A74164-001-a-0600"/>
            </date>
          </dateline>
          <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A74164-0720" facs="A74164-001-a-0610">ORdered</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74164-0730" facs="A74164-001-a-0620">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-0740" facs="A74164-001-a-0630">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74164-0750" facs="A74164-001-a-0640">Parliament</w>
          <pc xml:id="A74164-0760" facs="A74164-001-a-0650">,</pc>
        </opener>
        <p xml:id="A74164e-130">
          <w lemma="that" ana="#cs" reg="That" xml:id="A74164-0790" facs="A74164-001-a-0660">That</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74164-0800" facs="A74164-001-a-0670">this</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A74164-0810" facs="A74164-001-a-0680">Order</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74164-0820" facs="A74164-001-a-0690">be</w>
          <c> </c>
          <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A74164-0830" facs="A74164-001-a-0700">forthwith</w>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A74164-0840" facs="A74164-001-a-0710">Printed</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74164-0850" facs="A74164-001-a-0720">and</w>
          <c> </c>
          <w lemma="publish" ana="#vvn" reg="published" xml:id="A74164-0860" facs="A74164-001-a-0730">Published</w>
          <pc unit="sentence" xml:id="A74164-0870" facs="A74164-001-a-0740">.</pc>
        </p>
        <closer xml:id="A74164e-140">
          <signed xml:id="A74164e-150">
            <w lemma="tho" ana="#av" reg="Tho" xml:id="A74164-0910" facs="A74164-001-a-0750">THO</w>
            <pc unit="sentence" xml:id="A74164-0920" facs="A74164-001-a-0760">.</pc>
            <c> </c>
            <w lemma="st" part="I" ana="#n-ab" reg="Saint" xml:id="A74164-0930.1" facs="A74164-001-a-0770" rendition="hi-mid-2">St</w>
            <hi rend="sup" xml:id="A74164e-160"/>
            <c> </c>
            <w lemma="NICHOLAS" ana="#n1-nn" reg="Nicholas" xml:id="A74164-0940" facs="A74164-001-a-0790">NICHOLAS</w>
            <pc xml:id="A74164-0950" facs="A74164-001-a-0800">,</pc>
            <c> </c>
            <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A74164-0960" facs="A74164-001-a-0810">Clerk</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-0970" facs="A74164-001-a-0820">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A74164-0980" facs="A74164-001-a-0830">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74164-0990" facs="A74164-001-a-0840">Parliament</w>
            <pc unit="sentence" xml:id="A74164-1000" facs="A74164-001-a-0850">.</pc>
          </signed>
        </closer>
      </div>
      <div type="proclamation" xml:id="A74164e-170">
        <opener xml:id="A74164e-180">
          <dateline xml:id="A74164e-190">
            <date xml:id="A74164e-200">
              <w lemma="tuesday" ana="#n1-nn" reg="Tuesday" xml:id="A74164-1060" facs="A74164-001-a-0860">Tuesday</w>
              <pc xml:id="A74164-1070" facs="A74164-001-a-0870">,</pc>
              <c> </c>
              <w lemma="September" ana="#n1-nn" reg="September" xml:id="A74164-1080" facs="A74164-001-a-0880">September</w>
              <c> </c>
              <w lemma="20." ana="#crd" reg="20." xml:id="A74164-1090" facs="A74164-001-a-0890">20.</w>
              <c> </c>
              <w lemma="1659." ana="#crd" reg="1659." xml:id="A74164-1100" facs="A74164-001-a-0900">1659.</w>
              <pc unit="sentence" xml:id="A74164-1100-eos" facs="A74164-001-a-0910"/>
            </date>
          </dateline>
          <w lemma="order" ana="#vvn" reg="ordered" xml:id="A74164-1120" facs="A74164-001-a-0920">Ordered</w>
          <pc xml:id="A74164-1130" facs="A74164-001-a-0930">,</pc>
        </opener>
        <p xml:id="A74164e-210">
          <w lemma="that" ana="#cs" reg="That" xml:id="A74164-1160" facs="A74164-001-a-0940">THat</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A74164-1170" facs="A74164-001-a-0950">it</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74164-1180" facs="A74164-001-a-0960">be</w>
          <c> </c>
          <w lemma="refer" ana="#vvn" reg="referred" xml:id="A74164-1190" facs="A74164-001-a-0970">referred</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74164-1200" facs="A74164-001-a-0980">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1210" facs="A74164-001-a-0990">the</w>
          <c> </c>
          <w lemma="council" ana="#n1" reg="council" xml:id="A74164-1220" facs="A74164-001-a-1000">Councel</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-1230" facs="A74164-001-a-1010">of</w>
          <c> </c>
          <w lemma="state" ana="#n1" reg="state" xml:id="A74164-1240" facs="A74164-001-a-1020">State</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74164-1250" facs="A74164-001-a-1030">to</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A74164-1260" facs="A74164-001-a-1040">take</w>
          <c> </c>
          <w lemma="care" ana="#n1" reg="care" xml:id="A74164-1270" facs="A74164-001-a-1050">care</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74164-1280" facs="A74164-001-a-1060">that</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74164-1290" facs="A74164-001-a-1070">this</w>
          <c> </c>
          <w lemma="order" ana="#n1" reg="order" xml:id="A74164-1300" facs="A74164-001-a-1080">Order</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74164-1310" facs="A74164-001-a-1090">be</w>
          <c> </c>
          <w lemma="send" ana="#vvn" reg="sent" xml:id="A74164-1320" facs="A74164-001-a-1100">sent</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74164-1330" facs="A74164-001-a-1110">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1340" facs="A74164-001-a-1120">the</w>
          <c> </c>
          <w lemma="sheriff" ana="#n2" reg="sheriffs" xml:id="A74164-1350" facs="A74164-001-a-1130">Sheriffs</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74164-1360" facs="A74164-001-a-1140">and</w>
          <c> </c>
          <w lemma="justice" ana="#n2" reg="justices" xml:id="A74164-1370" facs="A74164-001-a-1150">Justices</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-1380" facs="A74164-001-a-1160">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1390" facs="A74164-001-a-1170">the</w>
          <c> </c>
          <w lemma="peace" ana="#n1" reg="peace" xml:id="A74164-1400" facs="A74164-001-a-1180">Peace</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-1410" facs="A74164-001-a-1190">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1420" facs="A74164-001-a-1200">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A74164-1430" facs="A74164-001-a-1210">respective</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74164-1440" facs="A74164-001-a-1220">Counties</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A74164-1450" facs="A74164-001-a-1230">within</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74164-1460" facs="A74164-001-a-1240">this</w>
          <c> </c>
          <w lemma="commonwealth" ana="#n1" reg="commonwealth" xml:id="A74164-1470" facs="A74164-001-a-1250">Commonwealth</w>
          <pc xml:id="A74164-1480" facs="A74164-001-a-1260">,</pc>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74164-1490" facs="A74164-001-a-1270">to</w>
          <c> </c>
          <w lemma="give" ana="#vvi" reg="give" xml:id="A74164-1500" facs="A74164-001-a-1280">give</w>
          <c> </c>
          <w lemma="notice" ana="#n1" reg="notice" xml:id="A74164-1510" facs="A74164-001-a-1290">notice</w>
          <c> </c>
          <w lemma="thereof" ana="#av" reg="thereof" xml:id="A74164-1520" facs="A74164-001-a-1300">thereof</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74164-1530" facs="A74164-001-a-1310">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1540" facs="A74164-001-a-1320">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A74164-1550" facs="A74164-001-a-1330">respective</w>
          <c> </c>
          <w lemma="master" ana="#n2" reg="Masters" xml:id="A74164-1560" facs="A74164-001-a-1340">Masters</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74164-1570" facs="A74164-001-a-1350">and</w>
          <c> </c>
          <w lemma="governor" ana="#n2" reg="governors" xml:id="A74164-1580" facs="A74164-001-a-1360">Governors</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-1590" facs="A74164-001-a-1370">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1600" facs="A74164-001-a-1380">the</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A74164-1610" facs="A74164-001-a-1390">respective</w>
          <c> </c>
          <w lemma="hospital" ana="#n2" reg="hospitals" xml:id="A74164-1620" facs="A74164-001-a-1400">Hospitals</w>
          <c> </c>
          <w lemma="within" ana="#acp-p" reg="within" xml:id="A74164-1630" facs="A74164-001-a-1410">within</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1640" facs="A74164-001-a-1420">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A74164-1650" facs="A74164-001-a-1430">said</w>
          <c> </c>
          <w lemma="respective" ana="#j" reg="respective" xml:id="A74164-1660" facs="A74164-001-a-1440">respective</w>
          <c> </c>
          <w lemma="county" ana="#n2" reg="counties" xml:id="A74164-1670" facs="A74164-001-a-1450">Counties</w>
          <pc unit="sentence" xml:id="A74164-1680" facs="A74164-001-a-1460">.</pc>
        </p>
        <closer xml:id="A74164e-220">
          <signed xml:id="A74164e-230">
            <w lemma="tho" ana="#av" reg="Tho" xml:id="A74164-1720" facs="A74164-001-a-1470">THO</w>
            <pc unit="sentence" xml:id="A74164-1730" facs="A74164-001-a-1480">.</pc>
            <c> </c>
            <w lemma="st" part="I" ana="#n-ab" reg="Saint" xml:id="A74164-1740.1" facs="A74164-001-a-1490" rendition="hi-mid-2">St</w>
            <hi rend="sup" xml:id="A74164e-240"/>
            <c> </c>
            <w lemma="NICHOLAS" ana="#n1-nn" reg="Nicholas" xml:id="A74164-1750" facs="A74164-001-a-1510">NICHOLAS</w>
            <pc xml:id="A74164-1760" facs="A74164-001-a-1520">,</pc>
            <c> </c>
            <w lemma="clerk" ana="#n1" reg="clerk" xml:id="A74164-1770" facs="A74164-001-a-1530">Clerk</w>
            <c> </c>
            <w lemma="of" ana="#acp-p" reg="of" xml:id="A74164-1780" facs="A74164-001-a-1540">of</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A74164-1790" facs="A74164-001-a-1550">the</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74164-1800" facs="A74164-001-a-1560">Parliament</w>
            <pc unit="sentence" xml:id="A74164-1810" facs="A74164-001-a-1570">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A74164e-250">
      <div type="colophon" xml:id="A74164e-260">
        <p xml:id="A74164e-270">
          <hi xml:id="A74164e-280">
            <w lemma="london" ana="#n1-nn" reg="London" xml:id="A74164-1860" facs="A74164-001-a-1580">London</w>
            <pc xml:id="A74164-1870" facs="A74164-001-a-1590">,</pc>
          </hi>
          <c> </c>
          <w lemma="print" ana="#vvn" reg="printed" xml:id="A74164-1880" facs="A74164-001-a-1600">Printed</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74164-1890" facs="A74164-001-a-1610">by</w>
          <c> </c>
          <w lemma="JOHN" ana="#n1-nn" reg="John" xml:id="A74164-1900" facs="A74164-001-a-1620">JOHN</w>
          <c> </c>
          <w lemma="field" ana="#n1" reg="field" xml:id="A74164-1910" facs="A74164-001-a-1630">FIELD</w>
          <pc xml:id="A74164-1920" facs="A74164-001-a-1640">,</pc>
          <c> </c>
          <w lemma="printer" ana="#n1" reg="printer" xml:id="A74164-1930" facs="A74164-001-a-1650">Printer</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74164-1940" facs="A74164-001-a-1660">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-1950" facs="A74164-001-a-1670">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74164-1960" facs="A74164-001-a-1680">Parliament</w>
          <pc unit="sentence" xml:id="A74164-1970" facs="A74164-001-a-1690">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A74164-1980" facs="A74164-001-a-1700">And</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74164-1990" facs="A74164-001-a-1710">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74164-2000" facs="A74164-001-a-1720">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74164-2010" facs="A74164-001-a-1730">be</w>
          <c> </c>
          <w lemma="sell" ana="#vvn" reg="sold" xml:id="A74164-2020" facs="A74164-001-a-1740">sold</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A74164-2030" facs="A74164-001-a-1750">at</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74164-2040" facs="A74164-001-a-1760">the</w>
          <c> </c>
          <w lemma="seven" ana="#crd" reg="seven" xml:id="A74164-2050" facs="A74164-001-a-1770">seven</w>
          <c> </c>
          <w lemma="star" ana="#n2" reg="stars" xml:id="A74164-2060" facs="A74164-001-a-1780">Stars</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74164-2070" facs="A74164-001-a-1790">in</w>
          <c> </c>
          <hi xml:id="A74164e-290">
            <w lemma="Fleestreet" ana="#n1-nn" reg="Fleestreet" xml:id="A74164-2080" facs="A74164-001-a-1800">Fleestreet</w>
            <pc xml:id="A74164-2090" facs="A74164-001-a-1810">,</pc>
          </hi>
          <c> </c>
          <w lemma="over" ana="#acp-av" reg="over" xml:id="A74164-2100" facs="A74164-001-a-1820">over</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A74164-2110" facs="A74164-001-a-1830">against</w>
          <c> </c>
          <hi xml:id="A74164e-300">
            <w lemma="Dunstan" ana="#n1g-nn" reg="Dunstan's" xml:id="A74164-2120" facs="A74164-001-a-1840">Dunstans</w>
          </hi>
          <c> </c>
          <w lemma="church" ana="#n1" reg="church" xml:id="A74164-2130" facs="A74164-001-a-1850">Church</w>
          <pc xml:id="A74164-2140" facs="A74164-001-a-1860">,</pc>
          <c> </c>
          <w lemma="1659." ana="#crd" reg="1659." xml:id="A74164-2150" facs="A74164-001-a-1870">1659.</w>
          <pc unit="sentence" xml:id="A74164-2150-eos" facs="A74164-001-a-1880"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>