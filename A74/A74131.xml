<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Wednesday the 27 August, 1651. Mr. Speaker, by way of report acquaints the House of the great appearance of the militiaes of London, Westminster, Southwarke, and the hamblets of the Tower, on Monday last in Finsbury feilds, ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1651</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74131</idno>
        <idno type="STC">Thomason 669.f.16[22]</idno>
        <idno type="STC">ESTC R211350</idno>
        <idno type="EEBO-CITATION">99870080</idno>
        <idno type="PROQUEST">99870080</idno>
        <idno type="VID">163179</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74131)</note>
        <note>Transcribed from: (Early English Books Online ; image set 163179)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[22])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Wednesday the 27 August, 1651. Mr. Speaker, by way of report acquaints the House of the great appearance of the militiaes of London, Westminster, Southwarke, and the hamblets of the Tower, on Monday last in Finsbury feilds, ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>s.n.,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1651]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title from opening lines of text.</note>
            <note>Place of publication and publication date inferred.</note>
            <note>Signed: Hen. Scobell Cler. Parliament.</note>
            <note>The speaker reports the appearance of the militia at Finsbury Fields on Monday, 25th inst.: whereon a vote of thanks is passed to the Lord Mayor, &amp;c., of London, etc. for their affection to Parliament. Ald. Pennington, Sir John Bourchier, Ald. Atkin, and Ald. Allen to return thanks -- Cf. Steele.</note>
            <note>Annotation on Thomason copy: "Septemb. 1. 1651".</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
          <term>Great Britain -- Militia -- Early works to 1800.</term>
          <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2009-01</date><label>Judith Siefring</label>
        Sampled and proofread
      </change>
      <change><date>2009-01</date><label>Judith Siefring</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74131e-10">
    <body xml:id="A74131e-20">
      <div type="text" xml:id="A74131e-30">
        <pb facs="tcp:163179:1" rendition="simple:additions" xml:id="A74131-001-a"/>
        <opener xml:id="A74131e-40">
          <dateline xml:id="A74131e-50">
            <date xml:id="A74131e-60">
              <hi xml:id="A74131e-70">
                <w lemma="wednesday" ana="#n1-nn" reg="Wednesday" xml:id="A74131-0050" facs="A74131-001-a-0010">Wednesday</w>
                <c> </c>
                <w lemma="the" ana="#d" reg="the" xml:id="A74131-0060" facs="A74131-001-a-0020">the</w>
              </hi>
              <c> </c>
              <w lemma="27" ana="#crd" reg="27" xml:id="A74131-0070" facs="A74131-001-a-0030">27</w>
              <c> </c>
              <w lemma="August" ana="#n1-nn" reg="August" xml:id="A74131-0080" facs="A74131-001-a-0040">August</w>
              <pc xml:id="A74131-0090" facs="A74131-001-a-0050">,</pc>
              <c> </c>
              <w lemma="1651." ana="#crd" reg="1651." xml:id="A74131-0100" facs="A74131-001-a-0060">1651.</w>
              <pc unit="sentence" xml:id="A74131-0100-eos" facs="A74131-001-a-0070"/>
            </date>
          </dateline>
        </opener>
        <p xml:id="A74131e-80">
          <w lemma="Mr." ana="#n-ab" reg="Mr." rend="initialcharacterdecorated" xml:id="A74131-0130" facs="A74131-001-a-0080">MR.</w>
          <c> </c>
          <hi xml:id="A74131e-90">
            <w lemma="speaker" ana="#n1" reg="speaker" xml:id="A74131-0150" facs="A74131-001-a-0100">Speaker</w>
            <pc xml:id="A74131-0160" facs="A74131-001-a-0110">,</pc>
          </hi>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74131-0170" facs="A74131-001-a-0120">by</w>
          <c> </c>
          <w lemma="way" ana="#n1" reg="way" xml:id="A74131-0180" facs="A74131-001-a-0130">way</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0190" facs="A74131-001-a-0140">of</w>
          <c> </c>
          <w lemma="report" ana="#n1" reg="report" xml:id="A74131-0200" facs="A74131-001-a-0150">report</w>
          <c> </c>
          <w lemma="acquaint" ana="#vvz" reg="acquaints" xml:id="A74131-0210" facs="A74131-001-a-0160">acquaints</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0220" facs="A74131-001-a-0170">the</w>
          <c> </c>
          <w lemma="house" ana="#n1" reg="house" xml:id="A74131-0230" facs="A74131-001-a-0180">House</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0240" facs="A74131-001-a-0190">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0250" facs="A74131-001-a-0200">the</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74131-0260" facs="A74131-001-a-0210">great</w>
          <c> </c>
          <w lemma="appearance" ana="#n1" reg="appearance" xml:id="A74131-0270" facs="A74131-001-a-0220">appearance</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0280" facs="A74131-001-a-0230">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0290" facs="A74131-001-a-0240">the</w>
          <c> </c>
          <hi xml:id="A74131e-100">
            <w lemma="militia" ana="#n2" reg="militias" xml:id="A74131-0300" facs="A74131-001-a-0250">Militiaes</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0310" facs="A74131-001-a-0260">of</w>
          <c> </c>
          <hi xml:id="A74131e-110">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74131-0320" facs="A74131-001-a-0270">London</w>
            <pc xml:id="A74131-0330" facs="A74131-001-a-0280">,</pc>
            <c> </c>
            <w lemma="Westminster" ana="#n1-nn" reg="Westminster" xml:id="A74131-0340" facs="A74131-001-a-0290">Westminster</w>
            <pc xml:id="A74131-0350" facs="A74131-001-a-0300">,</pc>
            <c> </c>
            <w lemma="Southwark" ana="#n1-nn" reg="Southwark" xml:id="A74131-0360" facs="A74131-001-a-0310">Southwarke</w>
            <pc xml:id="A74131-0370" facs="A74131-001-a-0320">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0380" facs="A74131-001-a-0330">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0390" facs="A74131-001-a-0340">the</w>
          <c> </c>
          <w lemma="hamlet" ana="#n2" reg="hamlets" xml:id="A74131-0400" facs="A74131-001-a-0350">Hamblets</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0410" facs="A74131-001-a-0360">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0420" facs="A74131-001-a-0370">the</w>
          <c> </c>
          <hi xml:id="A74131e-120">
            <w lemma="tower" ana="#n1" reg="tower" xml:id="A74131-0430" facs="A74131-001-a-0380">Tower</w>
            <pc xml:id="A74131-0440" facs="A74131-001-a-0390">,</pc>
          </hi>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A74131-0450" facs="A74131-001-a-0400">on</w>
          <c> </c>
          <w lemma="Monday" ana="#n1-nn" reg="Monday" xml:id="A74131-0460" facs="A74131-001-a-0410">Monday</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A74131-0470" facs="A74131-001-a-0420">last</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74131-0480" facs="A74131-001-a-0430">in</w>
          <c> </c>
          <hi xml:id="A74131e-130">
            <w lemma="Finsbury" ana="#n1-nn" reg="Finsbury" xml:id="A74131-0490" facs="A74131-001-a-0440">Finsbury</w>
          </hi>
          <c> </c>
          <w lemma="field" ana="#n2" reg="fields" xml:id="A74131-0500" facs="A74131-001-a-0450">feilds</w>
          <pc xml:id="A74131-0510" facs="A74131-001-a-0460">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0520" facs="A74131-001-a-0470">and</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74131-0530" facs="A74131-001-a-0480">their</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74131-0540" facs="A74131-001-a-0490">great</w>
          <c> </c>
          <w lemma="cheerfulness" ana="#n1" reg="cheerfulness" xml:id="A74131-0550" facs="A74131-001-a-0500">Cheerefulnes</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0560" facs="A74131-001-a-0510">and</w>
          <c> </c>
          <w lemma="readiness" ana="#n1" reg="readiness" xml:id="A74131-0570" facs="A74131-001-a-0520">Readines</w>
          <c> </c>
          <w lemma="manifest" ana="#vvn" reg="manifested" xml:id="A74131-0580" facs="A74131-001-a-0530">manifested</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-0590" facs="A74131-001-a-0540">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0600" facs="A74131-001-a-0550">the</w>
          <c> </c>
          <w lemma="public" ana="#j" reg="public" xml:id="A74131-0610" facs="A74131-001-a-0560">publick</w>
          <c> </c>
          <w lemma="service" ana="#n1" reg="service" xml:id="A74131-0620" facs="A74131-001-a-0570">service</w>
          <pc unit="sentence" xml:id="A74131-0630" facs="A74131-001-a-0580">.</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="And" xml:id="A74131-0640" facs="A74131-001-a-0590">And</w>
          <c> </c>
          <w lemma="likewise" ana="#av" reg="likewise" xml:id="A74131-0650" facs="A74131-001-a-0600">likewise</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0660" facs="A74131-001-a-0610">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0670" facs="A74131-001-a-0620">the</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74131-0680" facs="A74131-001-a-0630">great</w>
          <c> </c>
          <w lemma="care" ana="#n1" reg="care" xml:id="A74131-0690" facs="A74131-001-a-0640">care</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0700" facs="A74131-001-a-0650">and</w>
          <c> </c>
          <w lemma="affection" ana="#n1" reg="affection" xml:id="A74131-0710" facs="A74131-001-a-0660">affection</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0720" facs="A74131-001-a-0670">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0730" facs="A74131-001-a-0680">the</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A74131-0740" facs="A74131-001-a-0690">Lord</w>
          <c> </c>
          <w lemma="Major" ana="#n1-nn" reg="Major" xml:id="A74131-0750" facs="A74131-001-a-0700">Major</w>
          <pc xml:id="A74131-0760" facs="A74131-001-a-0710">,</pc>
          <c> </c>
          <w lemma="alderman" ana="#n2" reg="aldermen" xml:id="A74131-0770" facs="A74131-001-a-0720">Aldermen</w>
          <pc xml:id="A74131-0780" facs="A74131-001-a-0730">,</pc>
          <c> </c>
          <w lemma="sheriff" ana="#n2" reg="sheriffs" xml:id="A74131-0790" facs="A74131-001-a-0740">Sheriffes</w>
          <pc xml:id="A74131-0800" facs="A74131-001-a-0750">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0810" facs="A74131-001-a-0760">and</w>
          <c> </c>
          <w lemma="common-council" ana="#n1" reg="common-council" xml:id="A74131-0820" facs="A74131-001-a-0770">Common-councell</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0830" facs="A74131-001-a-0780">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0840" facs="A74131-001-a-0790">the</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A74131-0850" facs="A74131-001-a-0800">City</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0860" facs="A74131-001-a-0810">of</w>
          <c> </c>
          <hi xml:id="A74131e-140">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74131-0870" facs="A74131-001-a-0820">London</w>
            <pc xml:id="A74131-0880" facs="A74131-001-a-0830">,</pc>
          </hi>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0890" facs="A74131-001-a-0840">the</w>
          <c> </c>
          <w lemma="colonel" ana="#n2" reg="colonels" xml:id="A74131-0900" facs="A74131-001-a-0850">Collonels</w>
          <pc xml:id="A74131-0910" facs="A74131-001-a-0860">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0920" facs="A74131-001-a-0870">and</w>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A74131-0930" facs="A74131-001-a-0880">Officers</w>
          <pc xml:id="A74131-0940" facs="A74131-001-a-0890">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-0950" facs="A74131-001-a-0900">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A74131-0960" facs="A74131-001-a-0910">Souldiers</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-0970" facs="A74131-001-a-0920">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-0980" facs="A74131-001-a-0930">the</w>
          <c> </c>
          <w lemma="train" ana="#vvn" reg="trained" xml:id="A74131-0990" facs="A74131-001-a-0940">Trained</w>
          <c> </c>
          <w lemma="band" ana="#n2" reg="bands" xml:id="A74131-1000" facs="A74131-001-a-0950">bands</w>
          <pc xml:id="A74131-1010" facs="A74131-001-a-0960">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1020" facs="A74131-001-a-0970">and</w>
          <c> </c>
          <w lemma="volunteer" ana="#n2" reg="volunteers" xml:id="A74131-1030" facs="A74131-001-a-0980">Voluntiers</w>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A74131-1040" facs="A74131-001-a-0990">both</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A74131-1050" facs="A74131-001-a-1000">horse</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1060" facs="A74131-001-a-1010">and</w>
          <c> </c>
          <w lemma="foot" ana="#n1" reg="foot" xml:id="A74131-1070" facs="A74131-001-a-1020">foot</w>
          <pc xml:id="A74131-1080" facs="A74131-001-a-1030">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1090" facs="A74131-001-a-1040">and</w>
          <c> </c>
          <w lemma="especial" ana="#av_j" reg="especially" xml:id="A74131-1100" facs="A74131-001-a-1050">especially</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1110" facs="A74131-001-a-1060">the</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74131-1120" facs="A74131-001-a-1070">great</w>
          <c> </c>
          <w lemma="care" ana="#n1" reg="care" xml:id="A74131-1130" facs="A74131-001-a-1080">care</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1140" facs="A74131-001-a-1090">and</w>
          <c> </c>
          <w lemma="pain" ana="#n2" reg="pains" xml:id="A74131-1150" facs="A74131-001-a-1100">paines</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1160" facs="A74131-001-a-1110">of</w>
          <c> </c>
          <w lemma="major" ana="#j" reg="major" xml:id="A74131-1170" facs="A74131-001-a-1120">Major</w>
          <c> </c>
          <w lemma="general" ana="#n1" reg="general" xml:id="A74131-1180" facs="A74131-001-a-1130">Generall</w>
          <c> </c>
          <hi xml:id="A74131e-150">
            <w lemma="Skippon" ana="#n1-nn" reg="Skippon" xml:id="A74131-1190" facs="A74131-001-a-1140">Skippon</w>
            <pc xml:id="A74131-1200" facs="A74131-001-a-1150">,</pc>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74131-1210" facs="A74131-001-a-1160">in</w>
          <c> </c>
          <w lemma="that" ana="#d" reg="that" xml:id="A74131-1220" facs="A74131-001-a-1170">that</w>
          <c> </c>
          <w lemma="service" ana="#n1" reg="service" xml:id="A74131-1230" facs="A74131-001-a-1180">service</w>
          <pc unit="sentence" xml:id="A74131-1240" facs="A74131-001-a-1190">.</pc>
        </p>
        <p xml:id="A74131e-160">
          <hi xml:id="A74131e-170">
            <w lemma="resolve" ana="#vvn" reg="resolved" xml:id="A74131-1270" facs="A74131-001-a-1200">Resolved</w>
            <pc xml:id="A74131-1280" facs="A74131-001-a-1210">,</pc>
          </hi>
        </p>
        <p xml:id="A74131e-180">
          <w lemma="that" ana="#cs" reg="That" xml:id="A74131-1310" facs="A74131-001-a-1220">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1320" facs="A74131-001-a-1230">the</w>
          <c> </c>
          <w lemma="thanks" ana="#n2" reg="thanks" xml:id="A74131-1330" facs="A74131-001-a-1240">thankes</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1340" facs="A74131-001-a-1250">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1350" facs="A74131-001-a-1260">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74131-1360" facs="A74131-001-a-1270">Parliament</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74131-1370" facs="A74131-001-a-1280">be</w>
          <c> </c>
          <w lemma="return" ana="#vvn" reg="returned" xml:id="A74131-1380" facs="A74131-001-a-1290">returned</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-1390" facs="A74131-001-a-1300">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1400" facs="A74131-001-a-1310">the</w>
          <c> </c>
          <w lemma="lord" ana="#n1" reg="Lord" xml:id="A74131-1410" facs="A74131-001-a-1320">Lord</w>
          <c> </c>
          <w lemma="Major" ana="#n1-nn" reg="Major" xml:id="A74131-1420" facs="A74131-001-a-1330">Major</w>
          <pc xml:id="A74131-1430" facs="A74131-001-a-1340">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1440" facs="A74131-001-a-1350">and</w>
          <c> </c>
          <w lemma="alderman" ana="#n2" reg="aldermen" xml:id="A74131-1450" facs="A74131-001-a-1360">Aldermen</w>
          <pc xml:id="A74131-1460" facs="A74131-001-a-1370">,</pc>
          <c> </c>
          <w lemma="sheriff" ana="#n2" reg="sheriffs" xml:id="A74131-1470" facs="A74131-001-a-1380">Sheriffes</w>
          <pc xml:id="A74131-1480" facs="A74131-001-a-1390">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1490" facs="A74131-001-a-1400">and</w>
          <c> </c>
          <w lemma="common-council" ana="#n1" reg="common-council" xml:id="A74131-1500" facs="A74131-001-a-1410">Common-councell</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1510" facs="A74131-001-a-1420">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1520" facs="A74131-001-a-1430">the</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A74131-1530" facs="A74131-001-a-1440">City</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1540" facs="A74131-001-a-1450">of</w>
          <c> </c>
          <hi xml:id="A74131e-190">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74131-1550" facs="A74131-001-a-1460">London</w>
            <pc xml:id="A74131-1560" facs="A74131-001-a-1470">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1570" facs="A74131-001-a-1480">and</w>
          <c> </c>
          <w lemma="likewise" ana="#av" reg="likewise" xml:id="A74131-1580" facs="A74131-001-a-1490">likewise</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-1590" facs="A74131-001-a-1500">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1600" facs="A74131-001-a-1510">the</w>
          <c> </c>
          <w lemma="several" ana="#j" reg="several" xml:id="A74131-1610" facs="A74131-001-a-1520">severall</w>
          <c> </c>
          <hi xml:id="A74131e-200">
            <w lemma="militia" ana="#n2" reg="militias" xml:id="A74131-1620" facs="A74131-001-a-1530">Militiaes</w>
          </hi>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1630" facs="A74131-001-a-1540">of</w>
          <c> </c>
          <hi xml:id="A74131e-210">
            <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74131-1640" facs="A74131-001-a-1550">London</w>
            <pc xml:id="A74131-1650" facs="A74131-001-a-1560">,</pc>
            <c> </c>
            <w lemma="Westminster" ana="#n1-nn" reg="Westminster" xml:id="A74131-1660" facs="A74131-001-a-1570">Westminster</w>
            <pc xml:id="A74131-1670" facs="A74131-001-a-1580">,</pc>
            <c> </c>
            <w lemma="Southwark" ana="#n1-nn" reg="Southwark" xml:id="A74131-1680" facs="A74131-001-a-1590">Southwarke</w>
            <pc xml:id="A74131-1690" facs="A74131-001-a-1600">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1700" facs="A74131-001-a-1610">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1710" facs="A74131-001-a-1620">the</w>
          <c> </c>
          <w lemma="hamlet" ana="#n2" reg="hamlets" xml:id="A74131-1720" facs="A74131-001-a-1630">Hamblets</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-1730" facs="A74131-001-a-1640">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1740" facs="A74131-001-a-1650">the</w>
          <c> </c>
          <hi xml:id="A74131e-220">
            <w lemma="tower" ana="#n1" reg="tower" xml:id="A74131-1750" facs="A74131-001-a-1660">Tower</w>
            <pc xml:id="A74131-1760" facs="A74131-001-a-1670">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1770" facs="A74131-001-a-1680">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-1780" facs="A74131-001-a-1690">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1790" facs="A74131-001-a-1700">the</w>
          <c> </c>
          <w lemma="colonel" ana="#n2" reg="colonels" xml:id="A74131-1800" facs="A74131-001-a-1710">Collonels</w>
          <pc xml:id="A74131-1810" facs="A74131-001-a-1720">,</pc>
          <c> </c>
          <w lemma="officer" ana="#n2" reg="officers" xml:id="A74131-1820" facs="A74131-001-a-1730">Officers</w>
          <pc xml:id="A74131-1830" facs="A74131-001-a-1740">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1840" facs="A74131-001-a-1750">and</w>
          <c> </c>
          <w lemma="soldier" ana="#n2" reg="soldiers" xml:id="A74131-1850" facs="A74131-001-a-1760">Souldiers</w>
          <pc xml:id="A74131-1860" facs="A74131-001-a-1770">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1870" facs="A74131-001-a-1780">and</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-1880" facs="A74131-001-a-1790">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-1890" facs="A74131-001-a-1800">the</w>
          <c> </c>
          <w lemma="train" ana="#vvn" reg="trained" xml:id="A74131-1900" facs="A74131-001-a-1810">Trained</w>
          <c> </c>
          <w lemma="band" ana="#n2" reg="bands" xml:id="A74131-1910" facs="A74131-001-a-1820">bands</w>
          <pc xml:id="A74131-1920" facs="A74131-001-a-1830">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1930" facs="A74131-001-a-1840">and</w>
          <c> </c>
          <w lemma="volunteer" ana="#n2" reg="volunteers" xml:id="A74131-1940" facs="A74131-001-a-1850">Voluntiers</w>
          <pc xml:id="A74131-1950" facs="A74131-001-a-1860">,</pc>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A74131-1960" facs="A74131-001-a-1870">both</w>
          <c> </c>
          <w lemma="horse" ana="#n1" reg="horse" xml:id="A74131-1970" facs="A74131-001-a-1880">horse</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-1980" facs="A74131-001-a-1890">and</w>
          <c> </c>
          <w lemma="foot" ana="#n1" reg="foot" xml:id="A74131-1990" facs="A74131-001-a-1900">foot</w>
          <pc xml:id="A74131-2000" facs="A74131-001-a-1910">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74131-2010" facs="A74131-001-a-1920">for</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74131-2020" facs="A74131-001-a-1930">their</w>
          <c> </c>
          <w lemma="great" ana="#j" reg="great" xml:id="A74131-2030" facs="A74131-001-a-1940">great</w>
          <c> </c>
          <w lemma="affection" ana="#n1" reg="affection" xml:id="A74131-2040" facs="A74131-001-a-1950">affection</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74131-2050" facs="A74131-001-a-1960">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-2060" facs="A74131-001-a-1970">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74131-2070" facs="A74131-001-a-1980">Parliament</w>
          <pc xml:id="A74131-2080" facs="A74131-001-a-1990">,</pc>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74131-2090" facs="A74131-001-a-2000">in</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74131-2100" facs="A74131-001-a-2010">their</w>
          <c> </c>
          <w lemma="cheerful" ana="#j" reg="cheerful" xml:id="A74131-2110" facs="A74131-001-a-2020">cheerfull</w>
          <c> </c>
          <w lemma="readiness" ana="#n1" reg="readiness" xml:id="A74131-2120" facs="A74131-001-a-2030">readines</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74131-2130" facs="A74131-001-a-2040">to</w>
          <c> </c>
          <w lemma="serve" ana="#vvi" reg="serve" xml:id="A74131-2140" facs="A74131-001-a-2050">serve</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-2150" facs="A74131-001-a-2060">the</w>
          <c> </c>
          <w lemma="state" ana="#n1" reg="state" xml:id="A74131-2160" facs="A74131-001-a-2070">State</w>
          <pc xml:id="A74131-2170" facs="A74131-001-a-2080">,</pc>
          <c> </c>
          <w lemma="manifest" ana="#vvn" reg="manifested" xml:id="A74131-2180" facs="A74131-001-a-2090">manifested</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A74131-2190" facs="A74131-001-a-2100">at</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74131-2200" facs="A74131-001-a-2110">their</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A74131-2210" facs="A74131-001-a-2120">last</w>
          <c> </c>
          <w lemma="appearance" ana="#n1" reg="appearance" xml:id="A74131-2220" facs="A74131-001-a-2130">appearance</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74131-2230" facs="A74131-001-a-2140">in</w>
          <c> </c>
          <hi xml:id="A74131e-230">
            <w lemma="Finsbury" ana="#n1-nn" reg="Finsbury" xml:id="A74131-2240" facs="A74131-001-a-2150">Finesbury</w>
          </hi>
          <c> </c>
          <w lemma="field" ana="#n2" reg="fields" xml:id="A74131-2250" facs="A74131-001-a-2160">feilds</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A74131-2260" facs="A74131-001-a-2170">on</w>
          <c> </c>
          <w lemma="Monday" ana="#n1-nn" reg="Monday" xml:id="A74131-2270" facs="A74131-001-a-2180">Monday</w>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A74131-2280" facs="A74131-001-a-2190">last</w>
          <pc unit="sentence" xml:id="A74131-2290" facs="A74131-001-a-2200">.</pc>
        </p>
        <p xml:id="A74131e-240">
          <hi xml:id="A74131e-250">
            <w lemma="resolve" ana="#vvn" reg="resolved" xml:id="A74131-2320" facs="A74131-001-a-2210">Resolved</w>
            <pc xml:id="A74131-2330" facs="A74131-001-a-2220">,</pc>
          </hi>
        </p>
        <p xml:id="A74131e-260">
          <w lemma="that" ana="#cs" reg="That" xml:id="A74131-2360" facs="A74131-001-a-2230">That</w>
          <c> </c>
          <w lemma="alderman" ana="#n1" reg="alderman" xml:id="A74131-2370" facs="A74131-001-a-2240">Alderman</w>
          <c> </c>
          <hi xml:id="A74131e-270">
            <w lemma="pennington" ana="#n1-nn" reg="Pennington" xml:id="A74131-2380" facs="A74131-001-a-2250">Pennington</w>
            <pc xml:id="A74131-2390" facs="A74131-001-a-2260">,</pc>
          </hi>
          <c> </c>
          <w lemma="sir" ana="#n1" reg="Sir" xml:id="A74131-2400" facs="A74131-001-a-2270">Sir</w>
          <c> </c>
          <hi xml:id="A74131e-280">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A74131-2410" facs="A74131-001-a-2280">John</w>
            <c> </c>
            <w lemma="bourchier" ana="#n1-nn" reg="Bourchier" xml:id="A74131-2420" facs="A74131-001-a-2290">Bourchier</w>
            <pc xml:id="A74131-2430" facs="A74131-001-a-2300">,</pc>
          </hi>
          <c> </c>
          <w lemma="alderman" ana="#n1" reg="alderman" xml:id="A74131-2440" facs="A74131-001-a-2310">Alderman</w>
          <c> </c>
          <hi xml:id="A74131e-290">
            <w lemma="atkin" ana="#n1-nn" reg="Atkin" xml:id="A74131-2450" facs="A74131-001-a-2320">Atkin</w>
            <pc xml:id="A74131-2460" facs="A74131-001-a-2330">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74131-2470" facs="A74131-001-a-2340">and</w>
          <c> </c>
          <w lemma="alderman" ana="#n1" reg="alderman" xml:id="A74131-2480" facs="A74131-001-a-2350">Alderman</w>
          <c> </c>
          <hi xml:id="A74131e-300">
            <w lemma="Allen" ana="#n1-nn" reg="Allen" xml:id="A74131-2490" facs="A74131-001-a-2360">Allen</w>
            <pc xml:id="A74131-2500" facs="A74131-001-a-2370">,</pc>
          </hi>
          <c> </c>
          <w lemma="do" ana="#vvb" reg="do" xml:id="A74131-2510" facs="A74131-001-a-2380">doe</w>
          <c> </c>
          <w lemma="return" ana="#vvi" reg="return" xml:id="A74131-2520" facs="A74131-001-a-2390">returne</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-2530" facs="A74131-001-a-2400">the</w>
          <c> </c>
          <w lemma="thank" ana="#n2" reg="thanks" xml:id="A74131-2540" facs="A74131-001-a-2410">thanks</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74131-2550" facs="A74131-001-a-2420">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74131-2560" facs="A74131-001-a-2430">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74131-2570" facs="A74131-001-a-2440">Parliament</w>
          <c> </c>
          <w lemma="accord" ana="#av_vg" reg="accordingly" xml:id="A74131-2580" facs="A74131-001-a-2450">accordingly</w>
          <pc unit="sentence" xml:id="A74131-2590" facs="A74131-001-a-2460">.</pc>
        </p>
        <closer xml:id="A74131e-310">
          <signed xml:id="A74131e-320">
            <w lemma="Hen." ana="#n-ab" reg="Hen." xml:id="A74131-2630" facs="A74131-001-a-2470">Hen.</w>
            <c> </c>
            <w lemma="Scobell" ana="#n1-nn" reg="Scobell" xml:id="A74131-2650" facs="A74131-001-a-2490">Scobell</w>
            <c> </c>
            <w lemma="cler." ana="#n-ab" reg="cler." xml:id="A74131-2660" facs="A74131-001-a-2500">Cler.</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74131-2670" facs="A74131-001-a-2510">Parliament</w>
            <pc unit="sentence" xml:id="A74131-2680" facs="A74131-001-a-2520">.</pc>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="A74131e-330">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>