<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A trumpeter sent from his Excellency Sir Thomas Fairfax to the Parliament and Citie.</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1647</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74095</idno>
        <idno type="STC">Thomason 669.f.11[42]</idno>
        <idno type="EEBO-CITATION">50811821</idno>
        <idno type="OCLC">ocm 50811821</idno>
        <idno type="VID">162691</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74095)</note>
        <note>Transcribed from: (Early English Books Online ; image set 162691)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f11[42])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A trumpeter sent from his Excellency Sir Thomas Fairfax to the Parliament and Citie.</title>
            <author>Fairfax, Thomas Fairfax, Baron, 1612-1671.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.</extent>
          <publicationStmt>
            <publisher>s.n.],</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1647.</date>
          </publicationStmt>
          <notesStmt>
            <note>Place of publication from Thomason catalogue.</note>
            <note>Annotation on Thomason copy: "London. 24 July 1647."</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Charles -- I, -- King of England, 1600-1649.</term>
          <term>England and Wales. -- Parliament -- Early works to 1800.</term>
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74095e-10">
    <body xml:id="A74095e-20">
      <div type="text" xml:id="A74095e-30">
        <pb facs="tcp:162691:1" rendition="simple:additions" xml:id="A74095-001-a"/>
        <head xml:id="A74095e-40">
          <w lemma="a" ana="#d" reg="A" xml:id="A74095-0030" facs="A74095-001-a-0010">A</w>
          <c> </c>
          <w lemma="trumpeter" ana="#n1" reg="trumpeter" xml:id="A74095-0040" facs="A74095-001-a-0020">TRUMPETER</w>
          <c> </c>
          <w lemma="send" ana="#vvn" reg="sent" xml:id="A74095-0050" facs="A74095-001-a-0030">SENT</w>
          <c> </c>
          <w lemma="from" ana="#acp-p" reg="from" xml:id="A74095-0060" facs="A74095-001-a-0040">FROM</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-0070" facs="A74095-001-a-0050">HIS</w>
          <c> </c>
          <w lemma="excellency" ana="#n1" reg="excellency" xml:id="A74095-0080" facs="A74095-001-a-0060">EXCELLENCY</w>
          <c> </c>
          <w lemma="sir" ana="#n1" reg="Sir" xml:id="A74095-0090" facs="A74095-001-a-0070">Sir</w>
          <c> </c>
          <hi xml:id="A74095e-50">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A74095-0100" facs="A74095-001-a-0080">Thomas</w>
            <c> </c>
            <w lemma="Fairfax" ana="#n1-nn" reg="Fairfax" xml:id="A74095-0110" facs="A74095-001-a-0090">Fairfax</w>
            <pc xml:id="A74095-0120" facs="A74095-001-a-0100">,</pc>
          </hi>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74095-0130" facs="A74095-001-a-0110">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0140" facs="A74095-001-a-0120">the</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74095-0150" facs="A74095-001-a-0130">Parliament</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-0160" facs="A74095-001-a-0140">and</w>
          <c> </c>
          <w lemma="city" ana="#n1" reg="city" xml:id="A74095-0170" facs="A74095-001-a-0150">CITIE</w>
          <pc unit="sentence" xml:id="A74095-0180" facs="A74095-001-a-0160">.</pc>
        </head>
        <p n="1" xml:id="A74095e-60">
          <w lemma="i" ana="#crd" reg="I." xml:id="A74095-0210" facs="A74095-001-a-0170">I.</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" rend="initialcharacterdecorated" xml:id="A74095-0220" facs="A74095-001-a-0180">THat</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-0230" facs="A74095-001-a-0190">His</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A74095-0240" facs="A74095-001-a-0200">Majesty</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-0250" facs="A74095-001-a-0210">and</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0260" facs="A74095-001-a-0220">the</w>
          <c> </c>
          <w lemma="army" ana="#n1" reg="army" xml:id="A74095-0270" facs="A74095-001-a-0230">Army</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74095-0280" facs="A74095-001-a-0240">are</w>
          <c> </c>
          <w lemma="full" ana="#av_j" reg="fully" xml:id="A74095-0290" facs="A74095-001-a-0250">fully</w>
          <c> </c>
          <w lemma="agree" ana="#vvn" reg="agreed" xml:id="A74095-0300" facs="A74095-001-a-0260">agreed</w>
          <pc xml:id="A74095-0310" facs="A74095-001-a-0270">,</pc>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0320" facs="A74095-001-a-0280">the</w>
          <c> </c>
          <w lemma="king" ana="#n1" reg="King" xml:id="A74095-0330" facs="A74095-001-a-0290">King</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A74095-0340" facs="A74095-001-a-0300">is</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74095-0350" facs="A74095-001-a-0310">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74095-0360" facs="A74095-001-a-0320">be</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A74095-0370" facs="A74095-001-a-0330">at</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-0380" facs="A74095-001-a-0340">his</w>
          <c> </c>
          <w lemma="own" ana="#d" reg="own" xml:id="A74095-0390" facs="A74095-001-a-0350">owne</w>
          <c> </c>
          <w lemma="disposal" ana="#n1" reg="disposal" xml:id="A74095-0400" facs="A74095-001-a-0360">disposall</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74095-0410" facs="A74095-001-a-0370">to</w>
          <c> </c>
          <w lemma="come" ana="#vvi" reg="come" xml:id="A74095-0420" facs="A74095-001-a-0380">come</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74095-0430" facs="A74095-001-a-0390">to</w>
          <c> </c>
          <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74095-0440" facs="A74095-001-a-0400">London</w>
          <c> </c>
          <w lemma="when" ana="#crq-cs" reg="when" xml:id="A74095-0450" facs="A74095-001-a-0410">when</w>
          <c> </c>
          <w lemma="he" ana="#pns" reg="he" xml:id="A74095-0460" facs="A74095-001-a-0420">he</w>
          <c> </c>
          <w lemma="please" ana="#vvz" reg="pleaseth" xml:id="A74095-0470" facs="A74095-001-a-0430">pleaseth</w>
          <pc unit="sentence" xml:id="A74095-0480" facs="A74095-001-a-0440">.</pc>
        </p>
        <p n="2" xml:id="A74095e-70">
          <w lemma="ii" ana="#crd" reg="Ii" xml:id="A74095-0510" facs="A74095-001-a-0450">II.</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74095-0520" facs="A74095-001-a-0460">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0530" facs="A74095-001-a-0470">the</w>
          <c> </c>
          <w lemma="bishops-land" ana="#n2" reg="bishops-lands" xml:id="A74095-0540" facs="A74095-001-a-0480">Bishops-Lands</w>
          <pc xml:id="A74095-0550" facs="A74095-001-a-0490">,</pc>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A74095-0560" facs="A74095-001-a-0500">with</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0570" facs="A74095-001-a-0510">the</w>
          <c> </c>
          <w lemma="arrear" ana="#n2" reg="arrears" xml:id="A74095-0580" facs="A74095-001-a-0520">Arreares</w>
          <pc xml:id="A74095-0590" facs="A74095-001-a-0530">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74095-0600" facs="A74095-001-a-0540">bee</w>
          <c> </c>
          <w lemma="accout" ana="#vvd" reg="accouted" xml:id="A74095-0610" facs="A74095-001-a-0550">accouted</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74095-0620" facs="A74095-001-a-0560">for</w>
          <pc xml:id="A74095-0630" facs="A74095-001-a-0570">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-0640" facs="A74095-001-a-0580">and</w>
          <c> </c>
          <w lemma="put" ana="#vvn" reg="put" xml:id="A74095-0650" facs="A74095-001-a-0590">put</w>
          <c> </c>
          <w lemma="into" ana="#acp-p" reg="into" xml:id="A74095-0660" facs="A74095-001-a-0600">into</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-0670" facs="A74095-001-a-0610">His</w>
          <c> </c>
          <w lemma="majesty" ana="#n1g" reg="majesty's" xml:id="A74095-0680" facs="A74095-001-a-0620">Majesties</w>
          <c> </c>
          <w lemma="hand" ana="#n2" reg="hands" xml:id="A74095-0690" facs="A74095-001-a-0630">hands</w>
          <pc unit="sentence" xml:id="A74095-0700" facs="A74095-001-a-0640">.</pc>
        </p>
        <p n="3" xml:id="A74095e-80">
          <w lemma="iii" ana="#crd" reg="Iii" xml:id="A74095-0730" facs="A74095-001-a-0650">III.</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74095-0740" facs="A74095-001-a-0660">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0750" facs="A74095-001-a-0670">the</w>
          <c> </c>
          <w lemma="custom" ana="#n2" reg="customs" xml:id="A74095-0760" facs="A74095-001-a-0680">Customes</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-0770" facs="A74095-001-a-0690">and</w>
          <c> </c>
          <w lemma="revenue" ana="#n2" reg="revenues" xml:id="A74095-0780" facs="A74095-001-a-0700">Revenews</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74095-0790" facs="A74095-001-a-0710">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0800" facs="A74095-001-a-0720">the</w>
          <c> </c>
          <w lemma="crown" ana="#n1" reg="crown" xml:id="A74095-0810" facs="A74095-001-a-0730">Crown</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="are" xml:id="A74095-0820" facs="A74095-001-a-0740">are</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74095-0830" facs="A74095-001-a-0750">to</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74095-0840" facs="A74095-001-a-0760">be</w>
          <c> </c>
          <w lemma="account" ana="#vvn" reg="accounted" xml:id="A74095-0850" facs="A74095-001-a-0770">accounted</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74095-0860" facs="A74095-001-a-0780">for</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74095-0870" facs="A74095-001-a-0790">to</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-0880" facs="A74095-001-a-0800">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A74095-0890" facs="A74095-001-a-0810">Maiesty</w>
          <pc unit="sentence" xml:id="A74095-0900" facs="A74095-001-a-0820">.</pc>
        </p>
        <p n="4" xml:id="A74095e-90">
          <w lemma="v." ana="#crd" reg="V." xml:id="A74095-0930" facs="A74095-001-a-0830">V.</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74095-0940" facs="A74095-001-a-0840">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-0950" facs="A74095-001-a-0850">the</w>
          <c> </c>
          <w lemma="public-faith" ana="#n1" reg="public-faith" xml:id="A74095-0960" facs="A74095-001-a-0860">Publick-Faith</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74095-0970" facs="A74095-001-a-0870">be</w>
          <c> </c>
          <w lemma="pay" ana="#vvn" reg="paid" xml:id="A74095-0980" facs="A74095-001-a-0880">paid</w>
          <pc unit="sentence" xml:id="A74095-0990" facs="A74095-001-a-0890">.</pc>
        </p>
        <p n="5" xml:id="A74095e-100">
          <w lemma="v." ana="#crd" reg="V." xml:id="A74095-1020" facs="A74095-001-a-0900">V.</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74095-1030" facs="A74095-001-a-0910">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-1040" facs="A74095-001-a-0920">the</w>
          <c> </c>
          <w lemma="arrear" ana="#n2" reg="arrears" xml:id="A74095-1050" facs="A74095-001-a-0930">Arreares</w>
          <c> </c>
          <w lemma="due" ana="#j" reg="due" xml:id="A74095-1060" facs="A74095-001-a-0940">due</w>
          <c> </c>
          <w lemma="unto" ana="#acp-p" reg="unto" xml:id="A74095-1070" facs="A74095-001-a-0950">unto</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-1080" facs="A74095-001-a-0960">the</w>
          <c> </c>
          <w lemma="army" ana="#n2" reg="armies" xml:id="A74095-1090" facs="A74095-001-a-0970">Armies</w>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74095-1100" facs="A74095-001-a-0980">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74095-1110" facs="A74095-001-a-0990">be</w>
          <c> </c>
          <w lemma="pay" ana="#vvn" reg="paid" xml:id="A74095-1120" facs="A74095-001-a-1000">paid</w>
          <c> </c>
          <w lemma="on" ana="#acp-p" reg="on" xml:id="A74095-1130" facs="A74095-001-a-1010">on</w>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A74095-1140" facs="A74095-001-a-1020">both</w>
          <c> </c>
          <w lemma="side" ana="#n2" reg="sides" xml:id="A74095-1150" facs="A74095-001-a-1030">sides</w>
          <pc xml:id="A74095-1160" facs="A74095-001-a-1040">,</pc>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A74095-1170" facs="A74095-001-a-1050">out</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74095-1180" facs="A74095-001-a-1060">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74095-1190" facs="A74095-001-a-1070">the</w>
          <c> </c>
          <w lemma="excise" ana="#n1" reg="excise" xml:id="A74095-1200" facs="A74095-001-a-1080">Excise</w>
          <pc unit="sentence" xml:id="A74095-1210" facs="A74095-001-a-1090">.</pc>
        </p>
        <p n="6" xml:id="A74095e-110">
          <w lemma="vi" ana="#crd" reg="Vi" xml:id="A74095-1240" facs="A74095-001-a-1100">VI</w>
          <pc unit="sentence" xml:id="A74095-1250" facs="A74095-001-a-1110">.</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="That" xml:id="A74095-1260" facs="A74095-001-a-1120">That</w>
          <c> </c>
          <w lemma="his" ana="#po" reg="his" xml:id="A74095-1270" facs="A74095-001-a-1130">his</w>
          <c> </c>
          <w lemma="majesty" ana="#n1" reg="majesty" xml:id="A74095-1280" facs="A74095-001-a-1140">Maiesty</w>
          <c> </c>
          <w lemma="will" ana="#vmb" reg="will" xml:id="A74095-1290" facs="A74095-001-a-1150">will</w>
          <c> </c>
          <w lemma="come" ana="#vvi" reg="come" xml:id="A74095-1300" facs="A74095-001-a-1160">come</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74095-1310" facs="A74095-001-a-1170">in</w>
          <c> </c>
          <w lemma="person" ana="#n1" reg="person" xml:id="A74095-1320" facs="A74095-001-a-1180">person</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-1330" facs="A74095-001-a-1190">and</w>
          <c> </c>
          <w lemma="sit" ana="#vvi" reg="sit" xml:id="A74095-1340" facs="A74095-001-a-1200">sit</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74095-1350" facs="A74095-001-a-1210">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74095-1360" facs="A74095-001-a-1220">Parliament</w>
          <c> </c>
          <w lemma="until" ana="#acp-cs" reg="until" xml:id="A74095-1370" facs="A74095-001-a-1230">untill</w>
          <c> </c>
          <w lemma="new" ana="#j" reg="new" xml:id="A74095-1380" facs="A74095-001-a-1240">new</w>
          <c> </c>
          <w lemma="writ" ana="#n2" reg="writs" xml:id="A74095-1390" facs="A74095-001-a-1250">writts</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74095-1400" facs="A74095-001-a-1260">be</w>
          <c> </c>
          <w lemma="issue" ana="#vvn" reg="issued" xml:id="A74095-1410" facs="A74095-001-a-1270">issued</w>
          <c> </c>
          <w lemma="out" ana="#av" reg="out" xml:id="A74095-1420" facs="A74095-001-a-1280">out</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74095-1430" facs="A74095-001-a-1290">to</w>
          <c> </c>
          <w lemma="summon" ana="#vvi" reg="summon" xml:id="A74095-1440" facs="A74095-001-a-1300">summon</w>
          <c> </c>
          <w lemma="a" ana="#d" reg="a" xml:id="A74095-1450" facs="A74095-001-a-1310">a</w>
          <c> </c>
          <w lemma="new" ana="#j" reg="new" xml:id="A74095-1460" facs="A74095-001-a-1320">new</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74095-1470" facs="A74095-001-a-1330">Parliament</w>
          <pc unit="sentence" xml:id="A74095-1480" facs="A74095-001-a-1340">.</pc>
        </p>
        <p n="7" xml:id="A74095e-120">
          <w lemma="vii" ana="#crd" reg="Vii" xml:id="A74095-1510" facs="A74095-001-a-1350">VII</w>
          <pc unit="sentence" xml:id="A74095-1520" facs="A74095-001-a-1360">.</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="That" xml:id="A74095-1530" facs="A74095-001-a-1370">That</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A74095-1540" facs="A74095-001-a-1380">if</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74095-1550" facs="A74095-001-a-1390">this</w>
          <c> </c>
          <w lemma="be" ana="#vvb" reg="be" xml:id="A74095-1560" facs="A74095-001-a-1400">bee</w>
          <c> </c>
          <w lemma="refuse" ana="#vvn" reg="refused" xml:id="A74095-1570" facs="A74095-001-a-1410">refused</w>
          <pc xml:id="A74095-1580" facs="A74095-001-a-1420">,</pc>
          <c> </c>
          <w lemma="they" ana="#pns" reg="they" xml:id="A74095-1590" facs="A74095-001-a-1430">they</w>
          <c> </c>
          <w lemma="must" ana="#vmb" reg="must" xml:id="A74095-1600" facs="A74095-001-a-1440">must</w>
          <c> </c>
          <w lemma="expect" ana="#vvi" reg="expect" xml:id="A74095-1610" facs="A74095-001-a-1450">expect</w>
          <c> </c>
          <w lemma="to" ana="#acp-cs" reg="to" xml:id="A74095-1620" facs="A74095-001-a-1460">to</w>
          <c> </c>
          <w lemma="answer" ana="#vvi" reg="answer" xml:id="A74095-1630" facs="A74095-001-a-1470">answer</w>
          <c> </c>
          <w lemma="it" ana="#pn" reg="it" xml:id="A74095-1640" facs="A74095-001-a-1480">it</w>
          <c> </c>
          <w lemma="with" ana="#acp-p" reg="with" xml:id="A74095-1650" facs="A74095-001-a-1490">with</w>
          <c> </c>
          <w lemma="their" ana="#po" reg="their" xml:id="A74095-1660" facs="A74095-001-a-1500">their</w>
          <c> </c>
          <w lemma="life" ana="#n2" reg="lives" xml:id="A74095-1670" facs="A74095-001-a-1510">lives</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74095-1680" facs="A74095-001-a-1520">and</w>
          <c> </c>
          <w lemma="fortune" ana="#n2" reg="fortunes" xml:id="A74095-1690" facs="A74095-001-a-1530">fortunes</w>
          <pc unit="sentence" xml:id="A74095-1700" facs="A74095-001-a-1540">.</pc>
        </p>
        <trailer xml:id="A74095e-130">
          <w lemma="finis" ana="#fw-la" reg="Finis" xml:id="A74095-1730" facs="A74095-001-a-1550">FINIS</w>
          <pc unit="sentence" xml:id="A74095-1740" facs="A74095-001-a-1560">.</pc>
        </trailer>
      </div>
    </body>
    <back xml:id="A74095e-140">
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>