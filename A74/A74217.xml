<?xml-model href="http://morphadorner.northwestern.edu/morphadorner/schemata/ma_tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>An ordinance of the Lords and Commons assembled in Parliament, die Jovis, 26. Januarii, 1642. It is this day ordeyned by the Lords and Commons in Parliament assembled, ...</title>
        <author>England and Wales. Parliament.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1643</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-03 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A74217</idno>
        <idno type="STC">Thomason 669.f.5[131]</idno>
        <idno type="STC">ESTC R211733</idno>
        <idno type="EEBO-CITATION">99870439</idno>
        <idno type="PROQUEST">99870439</idno>
        <idno type="VID">160843</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A74217)</note>
        <note>Transcribed from: (Early English Books Online ; image set 160843)</note>
        <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f5[131])</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>An ordinance of the Lords and Commons assembled in Parliament, die Jovis, 26. Januarii, 1642. It is this day ordeyned by the Lords and Commons in Parliament assembled, ...</title>
            <author>England and Wales. Parliament.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>for Iohn Wright, in the Old Baily,</publisher>
            <pubPlace>Printed at London :</pubPlace>
            <date>1642 [i.e. 1643]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title includes first words of text.</note>
            <note>Order to print dated and signed: Die Jovis, 26. Januarii, 1642 [i.e. 1643]. John Browne Cleri. Parliamen.</note>
            <note>The ordinance of 15 Nov. last, granted to Maxemelian Bard and Thomas Browne and others for the seizing of horses is hereby revoked. -- Thomason catalogue.</note>
            <note>Reproduction of the original in the British Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
          <term>Great Britain -- Politics and government -- 1642-1649 -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-08</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="A74217e-10">
    <body xml:id="A74217e-20">
      <div type="ordinance" xml:id="A74217e-30">
        <pb facs="tcp:160843:1" rendition="simple:additions" xml:id="A74217-001-a"/>
        <head xml:id="A74217e-40">
          <w lemma="a" ana="#d" reg="An" xml:id="A74217-0030" facs="A74217-001-a-0010">AN</w>
          <c> </c>
          <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A74217-0040" facs="A74217-001-a-0020">ORDINANCE</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0050" facs="A74217-001-a-0030">OF</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0060" facs="A74217-001-a-0040">THE</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A74217-0070" facs="A74217-001-a-0050">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0080" facs="A74217-001-a-0060">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74217-0090" facs="A74217-001-a-0070">Commons</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A74217-0100" facs="A74217-001-a-0080">Assembled</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74217-0110" facs="A74217-001-a-0090">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74217-0120" facs="A74217-001-a-0100">PARLIAMENT</w>
          <pc unit="sentence" xml:id="A74217-0130" facs="A74217-001-a-0110">.</pc>
        </head>
        <opener xml:id="A74217e-50">
          <dateline xml:id="A74217e-60">
            <hi xml:id="A74217e-70">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A74217-0170" facs="A74217-001-a-0120">Die</w>
              <c> </c>
              <w lemma="jovis" ana="#fw-la" reg="jovis" xml:id="A74217-0180" facs="A74217-001-a-0130">Jovis</w>
              <pc xml:id="A74217-0190" facs="A74217-001-a-0140">,</pc>
            </hi>
            <date xml:id="A74217e-80">
              <hi xml:id="A74217e-90">
                <w lemma="26." ana="#crd" reg="26." xml:id="A74217-0210" facs="A74217-001-a-0150">26.</w>
                <pc unit="sentence" xml:id="A74217-0210-eos" facs="A74217-001-a-0160"/>
                <c> </c>
                <w lemma="januarii" ana="#fw-la" reg="Januarii" xml:id="A74217-0220" facs="A74217-001-a-0170">Januarii</w>
                <pc xml:id="A74217-0230" facs="A74217-001-a-0180">,</pc>
                <c> </c>
                <w lemma="1642." ana="#crd" reg="1642." xml:id="A74217-0240" facs="A74217-001-a-0190">1642.</w>
                <pc unit="sentence" xml:id="A74217-0240-eos" facs="A74217-001-a-0200"/>
                <c> </c>
              </hi>
            </date>
          </dateline>
        </opener>
        <p xml:id="A74217e-100">
          <w lemma="it" ana="#pn" reg="It" rend="initialcharacterdecorated" xml:id="A74217-0270" facs="A74217-001-a-0210">IT</w>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A74217-0280" facs="A74217-001-a-0220">is</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74217-0290" facs="A74217-001-a-0230">this</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A74217-0300" facs="A74217-001-a-0240">day</w>
          <c> </c>
          <w lemma="ordain" ana="#vvn" reg="ordained" xml:id="A74217-0310" facs="A74217-001-a-0250">Ordeyned</w>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74217-0320" facs="A74217-001-a-0260">by</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0330" facs="A74217-001-a-0270">the</w>
          <c> </c>
          <w lemma="lord" ana="#n2" reg="Lords" xml:id="A74217-0340" facs="A74217-001-a-0280">Lords</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0350" facs="A74217-001-a-0290">and</w>
          <c> </c>
          <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74217-0360" facs="A74217-001-a-0300">Commons</w>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74217-0370" facs="A74217-001-a-0310">in</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74217-0380" facs="A74217-001-a-0320">Parliament</w>
          <c> </c>
          <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A74217-0390" facs="A74217-001-a-0330">Assembled</w>
          <pc xml:id="A74217-0400" facs="A74217-001-a-0340">,</pc>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74217-0410" facs="A74217-001-a-0350">That</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0420" facs="A74217-001-a-0360">the</w>
          <c> </c>
          <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A74217-0430" facs="A74217-001-a-0370">Ordinance</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0440" facs="A74217-001-a-0380">of</w>
          <c> </c>
          <w lemma="both" ana="#d" reg="both" xml:id="A74217-0450" facs="A74217-001-a-0390">both</w>
          <c> </c>
          <w lemma="house" ana="#n2" reg="houses" xml:id="A74217-0460" facs="A74217-001-a-0400">houses</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0470" facs="A74217-001-a-0410">of</w>
          <c> </c>
          <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74217-0480" facs="A74217-001-a-0420">Parliament</w>
          <pc xml:id="A74217-0490" facs="A74217-001-a-0430">,</pc>
          <c> </c>
          <w lemma="date" ana="#vvn" reg="dated" xml:id="A74217-0500" facs="A74217-001-a-0440">dated</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0510" facs="A74217-001-a-0450">the</w>
          <c> </c>
          <w lemma="15." ana="#crd" reg="15." xml:id="A74217-0520" facs="A74217-001-a-0460">15.</w>
          <c> </c>
          <w lemma="day" ana="#n1" reg="day" xml:id="A74217-0530" facs="A74217-001-a-0470">day</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0540" facs="A74217-001-a-0480">of</w>
          <c> </c>
          <hi xml:id="A74217e-110">
            <w lemma="November" ana="#n1-nn" reg="November" xml:id="A74217-0550" facs="A74217-001-a-0490">November</w>
          </hi>
          <c> </c>
          <w lemma="last" ana="#ord" reg="last" xml:id="A74217-0560" facs="A74217-001-a-0500">last</w>
          <c> </c>
          <w lemma="past" ana="#j" reg="past" xml:id="A74217-0570" facs="A74217-001-a-0510">past</w>
          <pc xml:id="A74217-0580" facs="A74217-001-a-0520">,</pc>
          <c> </c>
          <w lemma="grant" ana="#vvn" reg="granted" xml:id="A74217-0590" facs="A74217-001-a-0530">granted</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74217-0600" facs="A74217-001-a-0540">to</w>
          <c> </c>
          <hi xml:id="A74217e-120">
            <w lemma="maxemilian" ana="#j-nn" reg="Maxemilian" xml:id="A74217-0610" facs="A74217-001-a-0550">Maxemilian</w>
            <c> </c>
            <w lemma="bard" ana="#n1" reg="bard" xml:id="A74217-0620" facs="A74217-001-a-0560">Bard</w>
            <pc xml:id="A74217-0630" facs="A74217-001-a-0570">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0640" facs="A74217-001-a-0580">and</w>
          <c> </c>
          <hi xml:id="A74217e-130">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A74217-0650" facs="A74217-001-a-0590">Thomas</w>
            <c> </c>
            <w lemma="Browne" ana="#n1-nn" reg="Browne" xml:id="A74217-0660" facs="A74217-001-a-0600">Browne</w>
            <pc xml:id="A74217-0670" facs="A74217-001-a-0610">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0680" facs="A74217-001-a-0620">and</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A74217-0690" facs="A74217-001-a-0630">others</w>
          <pc xml:id="A74217-0700" facs="A74217-001-a-0640">,</pc>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74217-0710" facs="A74217-001-a-0650">for</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0720" facs="A74217-001-a-0660">the</w>
          <c> </c>
          <w lemma="take" ana="#vvg" reg="taking" xml:id="A74217-0730" facs="A74217-001-a-0670">taking</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0740" facs="A74217-001-a-0680">and</w>
          <c> </c>
          <w lemma="seize" ana="#vvg" reg="seizing" xml:id="A74217-0750" facs="A74217-001-a-0690">seizing</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0760" facs="A74217-001-a-0700">of</w>
          <c> </c>
          <w lemma="horse" ana="#n2" reg="horses" xml:id="A74217-0770" facs="A74217-001-a-0710">Horses</w>
          <pc xml:id="A74217-0780" facs="A74217-001-a-0720">,</pc>
          <c> </c>
          <w lemma="mare" ana="#n2" reg="mares" xml:id="A74217-0790" facs="A74217-001-a-0730">Mares</w>
          <pc xml:id="A74217-0800" facs="A74217-001-a-0740">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0810" facs="A74217-001-a-0750">and</w>
          <c> </c>
          <w lemma="gelding" ana="#n2" reg="geldings" xml:id="A74217-0820" facs="A74217-001-a-0760">Geldings</w>
          <pc xml:id="A74217-0830" facs="A74217-001-a-0770">,</pc>
          <c> </c>
          <w lemma="be" ana="#vvz" reg="is" xml:id="A74217-0840" facs="A74217-001-a-0780">is</w>
          <c> </c>
          <w lemma="hereby" ana="#av" reg="hereby" xml:id="A74217-0850" facs="A74217-001-a-0790">hereby</w>
          <c> </c>
          <w lemma="revoke" ana="#vvn" reg="revoked" xml:id="A74217-0860" facs="A74217-001-a-0800">revoked</w>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0870" facs="A74217-001-a-0810">and</w>
          <c> </c>
          <w lemma="make" ana="#vvn" reg="made" xml:id="A74217-0880" facs="A74217-001-a-0820">made</w>
          <c> </c>
          <w lemma="void" ana="#j" reg="void" xml:id="A74217-0890" facs="A74217-001-a-0830">voyd</w>
          <pc xml:id="A74217-0900" facs="A74217-001-a-0840">,</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0910" facs="A74217-001-a-0850">and</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-0920" facs="A74217-001-a-0860">of</w>
          <c> </c>
          <w lemma="none" ana="#pi-x" reg="none" xml:id="A74217-0930" facs="A74217-001-a-0870">none</w>
          <c> </c>
          <w lemma="effect" ana="#n1" reg="effect" xml:id="A74217-0940" facs="A74217-001-a-0880">effect</w>
          <pc xml:id="A74217-0950" facs="A74217-001-a-0890">;</pc>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-0960" facs="A74217-001-a-0900">And</w>
          <c> </c>
          <w lemma="that" ana="#cs" reg="that" xml:id="A74217-0970" facs="A74217-001-a-0910">that</w>
          <c> </c>
          <w lemma="if" ana="#cs" reg="if" xml:id="A74217-0980" facs="A74217-001-a-0920">if</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-0990" facs="A74217-001-a-0930">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A74217-1000" facs="A74217-001-a-0940">said</w>
          <c> </c>
          <hi xml:id="A74217e-140">
            <w lemma="maxemilian" ana="#j-nn" reg="Maxemilian" xml:id="A74217-1010" facs="A74217-001-a-0950">Maxemilian</w>
            <c> </c>
            <w lemma="bard" ana="#n1" reg="bard" xml:id="A74217-1020" facs="A74217-001-a-0960">Bard</w>
            <pc xml:id="A74217-1030" facs="A74217-001-a-0970">,</pc>
          </hi>
          <c> </c>
          <w lemma="and" ana="#cc" reg="and" xml:id="A74217-1040" facs="A74217-001-a-0980">and</w>
          <c> </c>
          <hi xml:id="A74217e-150">
            <w lemma="Thomas" ana="#n1-nn" reg="Thomas" xml:id="A74217-1050" facs="A74217-001-a-0990">Thomas</w>
            <c> </c>
            <w lemma="Browne" ana="#n1-nn" reg="Browne" xml:id="A74217-1060" facs="A74217-001-a-1000">Browne</w>
            <pc xml:id="A74217-1070" facs="A74217-001-a-1010">,</pc>
          </hi>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A74217-1080" facs="A74217-001-a-1020">or</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A74217-1090" facs="A74217-001-a-1030">any</w>
          <c> </c>
          <w lemma="other" ana="#pi2_d" reg="others" xml:id="A74217-1100" facs="A74217-001-a-1040">others</w>
          <pc xml:id="A74217-1110" facs="A74217-001-a-1050">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74217-1120" facs="A74217-001-a-1060">shall</w>
          <c> </c>
          <w lemma="seize" ana="#vvi" reg="seize" xml:id="A74217-1130" facs="A74217-001-a-1070">seize</w>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A74217-1140" facs="A74217-001-a-1080">or</w>
          <c> </c>
          <w lemma="take" ana="#vvi" reg="take" xml:id="A74217-1150" facs="A74217-001-a-1090">take</w>
          <c> </c>
          <w lemma="any" ana="#d" reg="any" xml:id="A74217-1160" facs="A74217-001-a-1100">any</w>
          <c> </c>
          <w lemma="horse" ana="#n2" reg="horses" xml:id="A74217-1170" facs="A74217-001-a-1110">Horses</w>
          <pc xml:id="A74217-1180" facs="A74217-001-a-1120">,</pc>
          <c> </c>
          <w lemma="mare" ana="#n2" reg="mares" xml:id="A74217-1190" facs="A74217-001-a-1130">Mares</w>
          <pc xml:id="A74217-1200" facs="A74217-001-a-1140">,</pc>
          <c> </c>
          <w lemma="or" ana="#cc" reg="or" xml:id="A74217-1210" facs="A74217-001-a-1150">or</w>
          <c> </c>
          <w lemma="gelding" ana="#n2" reg="geldings" xml:id="A74217-1220" facs="A74217-001-a-1160">Geldings</w>
          <c> </c>
          <w lemma="hereafter" ana="#av" reg="hereafter" xml:id="A74217-1230" facs="A74217-001-a-1170">hereafter</w>
          <pc xml:id="A74217-1240" facs="A74217-001-a-1180">,</pc>
          <c> </c>
          <w lemma="by" ana="#acp-p" reg="by" xml:id="A74217-1250" facs="A74217-001-a-1190">by</w>
          <c> </c>
          <w lemma="colour" ana="#n1" reg="colour" xml:id="A74217-1260" facs="A74217-001-a-1200">colour</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-1270" facs="A74217-001-a-1210">of</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-1280" facs="A74217-001-a-1220">the</w>
          <c> </c>
          <w lemma="say" ana="#j_vn" reg="said" xml:id="A74217-1290" facs="A74217-001-a-1230">said</w>
          <c> </c>
          <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A74217-1300" facs="A74217-001-a-1240">Ordinance</w>
          <pc xml:id="A74217-1310" facs="A74217-001-a-1250">,</pc>
          <c> </c>
          <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74217-1320" facs="A74217-001-a-1260">shall</w>
          <c> </c>
          <w lemma="be" ana="#vvi" reg="be" xml:id="A74217-1330" facs="A74217-001-a-1270">be</w>
          <c> </c>
          <w lemma="proceed" ana="#vvn" reg="proceeded" xml:id="A74217-1340" facs="A74217-001-a-1280">proceeded</w>
          <c> </c>
          <w lemma="against" ana="#acp-p" reg="against" xml:id="A74217-1350" facs="A74217-001-a-1290">against</w>
          <c> </c>
          <w lemma="as" ana="#acp-cs" reg="as" xml:id="A74217-1360" facs="A74217-001-a-1300">as</w>
          <c> </c>
          <w lemma="felon" ana="#n2" reg="felons" xml:id="A74217-1370" facs="A74217-001-a-1310">Fellons</w>
          <pc xml:id="A74217-1380" facs="A74217-001-a-1320">,</pc>
          <c> </c>
          <w lemma="according" ana="#j" reg="according" xml:id="A74217-1390" facs="A74217-001-a-1330">according</w>
          <c> </c>
          <w lemma="to" ana="#acp-p" reg="to" xml:id="A74217-1400" facs="A74217-001-a-1340">to</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-1410" facs="A74217-001-a-1350">the</w>
          <c> </c>
          <w lemma="law" ana="#n2" reg="laws" xml:id="A74217-1420" facs="A74217-001-a-1360">Lawes</w>
          <c> </c>
          <w lemma="of" ana="#acp-p" reg="of" xml:id="A74217-1430" facs="A74217-001-a-1370">of</w>
          <c> </c>
          <w lemma="this" ana="#d" reg="this" xml:id="A74217-1440" facs="A74217-001-a-1380">this</w>
          <c> </c>
          <w lemma="land" ana="#n1" reg="land" xml:id="A74217-1450" facs="A74217-001-a-1390">Land</w>
          <pc unit="sentence" xml:id="A74217-1451" facs="A74217-001-a-1400">.</pc>
        </p>
      </div>
    </body>
    <back xml:id="A74217e-160">
      <div type="imprimatur" xml:id="A74217e-170">
        <opener xml:id="A74217e-180">
          <dateline xml:id="A74217e-190">
            <hi xml:id="A74217e-200">
              <w lemma="die" ana="#fw-la" reg="die" xml:id="A74217-1510" facs="A74217-001-a-1410">Die</w>
              <c> </c>
              <w lemma="jovis" ana="#fw-la" reg="jovis" xml:id="A74217-1520" facs="A74217-001-a-1420">Jovis</w>
              <pc xml:id="A74217-1530" facs="A74217-001-a-1430">,</pc>
            </hi>
            <date xml:id="A74217e-210">
              <hi xml:id="A74217e-220">
                <w lemma="26." ana="#crd" reg="26." xml:id="A74217-1550" facs="A74217-001-a-1440">26.</w>
                <pc unit="sentence" xml:id="A74217-1550-eos" facs="A74217-001-a-1450"/>
                <c> </c>
                <w lemma="januarii" ana="#fw-la" reg="Januarii" xml:id="A74217-1560" facs="A74217-001-a-1460">Januarii</w>
                <pc xml:id="A74217-1570" facs="A74217-001-a-1470">,</pc>
                <c> </c>
                <w lemma="1642." ana="#crd" reg="1642." xml:id="A74217-1580" facs="A74217-001-a-1480">1642.</w>
                <pc unit="sentence" xml:id="A74217-1580-eos" facs="A74217-001-a-1490"/>
                <c> </c>
              </hi>
            </date>
          </dateline>
        </opener>
        <p xml:id="A74217e-230">
          <hi xml:id="A74217e-240">
            <w lemma="order" ana="#j_vn" reg="ordered" xml:id="A74217-1610" facs="A74217-001-a-1500">ORdered</w>
            <c> </c>
            <w lemma="by" ana="#acp-p" reg="by" xml:id="A74217-1620" facs="A74217-001-a-1510">by</w>
            <c> </c>
            <w lemma="the" ana="#d" reg="the" xml:id="A74217-1630" facs="A74217-001-a-1520">the</w>
            <c> </c>
            <w lemma="lord" ana="#n2" reg="Lords" xml:id="A74217-1640" facs="A74217-001-a-1530">Lords</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A74217-1650" facs="A74217-001-a-1540">and</w>
            <c> </c>
            <w lemma="commons" ana="#n2-nn" reg="Commons" xml:id="A74217-1660" facs="A74217-001-a-1550">Commons</w>
            <c> </c>
            <w lemma="assemble" ana="#vvn" reg="assembled" xml:id="A74217-1670" facs="A74217-001-a-1560">Assembled</w>
            <c> </c>
            <w lemma="in" ana="#acp-p" reg="in" xml:id="A74217-1680" facs="A74217-001-a-1570">in</w>
            <c> </c>
            <w lemma="parliament" ana="#n1" reg="parliament" xml:id="A74217-1690" facs="A74217-001-a-1580">Parliament</w>
            <pc xml:id="A74217-1700" facs="A74217-001-a-1590">,</pc>
            <c> </c>
            <w lemma="that" ana="#cs" reg="that" xml:id="A74217-1710" facs="A74217-001-a-1600">that</w>
            <c> </c>
            <w lemma="this" ana="#d" reg="this" xml:id="A74217-1720" facs="A74217-001-a-1610">this</w>
            <c> </c>
            <w lemma="ordinance" ana="#n1" reg="ordinance" xml:id="A74217-1730" facs="A74217-001-a-1620">Ordinance</w>
            <c> </c>
            <w lemma="shall" ana="#vmb" reg="shall" xml:id="A74217-1740" facs="A74217-001-a-1630">shall</w>
            <c> </c>
            <w lemma="be" ana="#vvi" reg="be" xml:id="A74217-1750" facs="A74217-001-a-1640">be</w>
            <c> </c>
            <w lemma="forthwith" ana="#av" reg="forthwith" xml:id="A74217-1760" facs="A74217-001-a-1650">forthwith</w>
            <c> </c>
            <w lemma="print" ana="#vvn" reg="printed" xml:id="A74217-1770" facs="A74217-001-a-1660">Printed</w>
            <c> </c>
            <w lemma="and" ana="#cc" reg="and" xml:id="A74217-1780" facs="A74217-001-a-1670">and</w>
            <c> </c>
            <w lemma="publish" ana="#vvn" reg="published" xml:id="A74217-1790" facs="A74217-001-a-1680">published</w>
            <pc unit="sentence" xml:id="A74217-1800" facs="A74217-001-a-1690">.</pc>
            <c> </c>
          </hi>
        </p>
        <closer xml:id="A74217e-250">
          <signed xml:id="A74217e-260">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A74217-1840" facs="A74217-001-a-1700">John</w>
            <c> </c>
            <w lemma="Browne" ana="#n1-nn" reg="Browne" xml:id="A74217-1850" facs="A74217-001-a-1710">Browne</w>
            <c> </c>
            <w lemma="cleri" ana="#fw-la" reg="cleri" xml:id="A74217-1860" facs="A74217-001-a-1720">Cleri</w>
            <pc unit="sentence" xml:id="A74217-1870" facs="A74217-001-a-1730">.</pc>
            <c> </c>
            <w lemma="parliamen" ana="#fw-la" reg="Parliamen" xml:id="A74217-1880" facs="A74217-001-a-1740">Parliamen</w>
            <pc unit="sentence" xml:id="A74217-1890" facs="A74217-001-a-1750">.</pc>
          </signed>
        </closer>
      </div>
      <div type="colophon" xml:id="A74217e-270">
        <p xml:id="A74217e-280">
          <w lemma="print" ana="#vvn" reg="Printed" xml:id="A74217-1930" facs="A74217-001-a-1760">Printed</w>
          <c> </c>
          <w lemma="at" ana="#acp-p" reg="at" xml:id="A74217-1940" facs="A74217-001-a-1770">at</w>
          <c> </c>
          <w lemma="London" ana="#n1-nn" reg="London" xml:id="A74217-1950" facs="A74217-001-a-1780">London</w>
          <c> </c>
          <w lemma="for" ana="#acp-p" reg="for" xml:id="A74217-1960" facs="A74217-001-a-1790">for</w>
          <c> </c>
          <hi xml:id="A74217e-290">
            <w lemma="John" ana="#n1-nn" reg="John" xml:id="A74217-1970" facs="A74217-001-a-1800">Iohn</w>
            <c> </c>
            <w lemma="Wright" ana="#n1-nn" reg="Wright" xml:id="A74217-1980" facs="A74217-001-a-1810">Wright</w>
            <pc xml:id="A74217-1990" facs="A74217-001-a-1820">,</pc>
          </hi>
          <c> </c>
          <w lemma="in" ana="#acp-p" reg="in" xml:id="A74217-2000" facs="A74217-001-a-1830">in</w>
          <c> </c>
          <w lemma="the" ana="#d" reg="the" xml:id="A74217-2010" facs="A74217-001-a-1840">the</w>
          <c> </c>
          <w lemma="old" ana="#j" reg="old" xml:id="A74217-2020" facs="A74217-001-a-1850">Old</w>
          <c> </c>
          <w lemma="bailie" ana="#n1-nn" reg="Bailey" xml:id="A74217-2030" facs="A74217-001-a-1860">Baily</w>
          <pc xml:id="A74217-2040" facs="A74217-001-a-1870">:</pc>
          <c> </c>
          <w lemma="1642." ana="#crd" reg="1642." xml:id="A74217-2050" facs="A74217-001-a-1880">1642.</w>
          <pc unit="sentence" xml:id="A74217-2050-eos" facs="A74217-001-a-1890"/>
        </p>
      </div>
      <div type="notes">
        <head>Notes, typically marginal, from the original text</head>
      </div>
    </back>
  </text>
</TEI>