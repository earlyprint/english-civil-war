
This site contains some 6,000  Phase 1 files from the EEBO-TCP archive. They contain texts that were published between 1640 and 1660 and have fewer than 50,000 words. 

The texts were linguistically annotated with MorphAdorner.

The source texts and the linguistically annotated texts are in the public domain.   The licensing statement of the source text reads as follows:


This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of "Creative Commons 0 1.0 Universal". (https://creativecommons.org/publicdomain/zero/1.0/">
The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.

The linguistically annotated version imposes no further restrictions. 

These texts are subject to various forms of curation and enrichment.  At the moment they lack a proper header that describes in detail the ways in which they differ from the source texts. 

For further information contact

Martin Mueller
Professor emeritus of English and Classics
Northwestern University
Evanston, Illinois 60208
martinmueller@northwestern.edu







